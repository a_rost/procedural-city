import numpy as np
import matplotlib.pyplot as plt
# import manzana
# import tools
import time


count_per_level = np.random.randint(1, 8, size = 10)


current_location = np.random.randint(1, 8, size = 10)



def addition(current_location, count_per_level, loc):
	N = len(current_location)
	current_location[loc] += 1

	for i in range(loc + 1, N):
		current_location[i] = 0

	for i in range(0, loc):
		if current_location[loc - i] >= count_per_level[loc - i]:
			current_location[loc - i] = 0
			current_location[loc - i - 1] += 1

	if current_location[0] >= count_per_level[0]:
		print("Exit")

# loc - 1 = i


count_per_level *= 0
count_per_level += 2
current_location[current_location >= count_per_level] = count_per_level[current_location >= count_per_level]-1

count_per_level += 2

current_location *= 0

for i in range(20):
	print(current_location)
	# print(count_per_level)
	addition(current_location, count_per_level, len(count_per_level) - 1)


for i in range(20):
	print(current_location)
	# print(count_per_level)
	addition(current_location, count_per_level, 6)





xcoord, ycoord, terrains, parcels = manzana.generate_manzana([110., 110.])


manzana.plot_block(xcoord, ycoord, terrains, parcels)

x_high = np.linspace(0., size_x, 100)
x_low  = np.linspace(0., size_x, 10)
y_high = np.linspace(0., size_y, 100)
y_low  = np.linspace(0., size_y, 10)

X1, Y1 = np.meshgrid(x_high, y_low)
X2, Y2 = np.meshgrid(x_low,  y_high)


plt.plot(X1.flatten(), Y1.flatten(), ".", color = "k")
plt.plot(X2.flatten(), Y2.flatten(), ".", color = "k")


X3, Y3 = transform_coords(X1.flatten(), Y1.flatten())
X4, Y4 = transform_coords(X2.flatten(), Y2.flatten())
plt.plot(X3, Y3, ".", color = "r")
plt.plot(X4, Y4, ".", color = "r")







xcoord, ycoord, terrains, parcels = manzana.generate_manzana([120., 120.])

for parcel in parcels:
	coords = np.array(parcel.parcel_shape2.exterior.coords)
	world_positions, distorted_positions, new_positions = get_real_positions(transform_coords, parcel.left_corner, parcel.width, parcel.rotation*np.pi/180, coords)

	new_poly = tools.Polygon(distorted_positions)
	tools.plot_polygon(new_poly)



street_width = 10.
blocks = []
N, M = len(layout[0]), len(layout[1])

n_blocks = N*M
time_start = time.time()
offset_x = street_width
extent = [0., np.sum(layout[0]) + street_width*len(layout[0]), 0., np.sum(layout[1]) + street_width*len(layout[1])]


offset_y = street_width
for n in range(N):
	size_x = layout[0][n]
	for m in range(M):
		#print(n, "of ", N, m, "of ", M)

		size_y = layout[1][m]

		xcoord, ycoord, terrains, parcels = manzana.generate_manzana([size_x, size_y])

		for i in range(len(parcels)):
			parcel = parcels[i]

			#parcel_position = [parcel.left_corner[0] + offset_x - size_x/2., parcel.left_corner[1] + offset_y - size_y/2.]
			parcel_position = [parcel.left_corner[0] + offset_x, parcel.left_corner[1] + offset_y]
