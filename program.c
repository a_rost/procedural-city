#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <omp.h>
#include <assert.h>

const float pi = 3.1415926535f, g = 9.81f;
float vector_up[3] = {0.f, 0.f, 1.f};

void set_seed(unsigned int seed){
	srand(seed);
}

static inline void normalize(float *A, float *B){
	float norm = sqrtf(A[0]*A[0] + A[1]*A[1] + A[2]*A[2]);
	B[0] = A[0]/norm;
	B[1] = A[1]/norm;
	B[2] = A[2]/norm;
}


static inline void substract(float *A, float *B, float *C){
	C[0] = A[0] - B[0];
	C[1] = A[1] - B[1];
	C[2] = A[2] - B[2];
}

struct height_map {
	float *height, dx, dy, x_min, x_max, y_min, y_max, max_height;
	int N, M;
};

struct height_map surface;

void load_height_map(float *array, unsigned int N, unsigned int M, float *extent){
	surface.height = (float *) malloc(M*N*sizeof(float));
	surface.max_height = -100.0f;
	for(int i = 0; i < N*M; i++){
		surface.height[i] = array[i];
		if(array[i] > surface.max_height)
			surface.max_height = array[i];
	}
	surface.dx = (extent[1] - extent[0])/N;
	surface.dy = (extent[3] - extent[2])/M;
	
	surface.x_min = extent[0];
	surface.x_max = extent[1];
	surface.y_min = extent[2];
	surface.y_max = extent[3];
	surface.N  = N;
	surface.M  = M;
}

float kernel(float x, float y, float x0, float y0, float level){
	float sqr_dist = (x - x0)*(x - x0) + (y - y0)*(y - y0);
	if(sqr_dist >= level*level)
		return 0.f;
	else
		return sqrtf(level*level - sqr_dist);
}

int max(int a, int b){
	if(a >= b)
		return a;
	else
		return b;
}
int min(int a, int b){
	if(a <= b)
		return a;
	else
		return b;
}


static inline double random(){
	return (double)rand()/(double)(RAND_MAX);
}

void determine_leveled_surface(float *height_map, unsigned int N, unsigned int M, float dL, float level, float *result){
	#pragma omp parallel num_threads(12)
	{

	float x, y, x0, y0, value, ant_value;
	int k = (int) (level/dL + 1.f);

	#pragma omp for schedule(dynamic, 5)
	for(int n = 0; n < N; n++){
		x0 = n*dL;
		for(int m = 0; m < M; m++){
			y0 = m*dL;
			for(int i = max(n - k, 0); i < min(n + k, N); i++){
				x = i*dL;
				for(int j = max(m - k, 0); j < min(m + k, M); j++){
					y = j*dL;
					value = kernel(x, y, x0, y0, level);
					if(value != 0.f)
						value += height_map[n*M + m];
					ant_value = result[i*M + j];
					result[i*M + j] = fmax(ant_value, value);
				}
			}
		}
	}
	}
}


unsigned int idx(int i, int j, int M){
	return i*M + j;
}

int mod(int a, int b){
    int r = a % b;
    return r < 0 ? r + b : r;
}

int idx_mod(int p_o, int q_o, unsigned int N, unsigned int M){
	
	unsigned int p, q;
	
	p = mod(p_o, N);
	q = mod(q_o, M);
	
	return p*M + q;
}

// int mod(

static inline float get_height(struct height_map surface, float x, float y){
	int i, j, N = surface.N, M = surface.M;
	float f1, f2, f3, f4, g1, g2, dx, dy, x_min, y_min;
	
	dx = surface.dx;
	dy = surface.dy;
	x_min = surface.x_min;
	y_min = surface.y_min;
	
	i = (int)((x - dx/2.0f - x_min)/dx);
	j = (int)((y - dy/2.0f - y_min)/dy);

	f1 = surface.height[idx_mod(i + 0, j + 0, N, M)];
	f2 = surface.height[idx_mod(i + 0, j + 1, N, M)];
	f3 = surface.height[idx_mod(i + 1, j + 0, N, M)];
	f4 = surface.height[idx_mod(i + 1, j + 1, N, M)];
	
	g1 = f1 + (f2 - f1)*(y - (y_min + j*dy))/dy;
	g2 = f3 + (f4 - f3)*(y - (y_min + j*dy))/dy;

	return g1 + (g2 - g1)*(x - (x_min + i*dx))/dx;
}


static inline float get_nearest_height(struct height_map surface, float x, float y){
	int i, j, N = surface.N, M = surface.M;
	float f1, dx, dy, x_min, y_min;

	dx = surface.dx;
	dy = surface.dy;
	x_min = surface.x_min;
	y_min = surface.y_min;

	i = (int)((x - dx/2.0f - x_min)/dx);
	j = (int)((y - dy/2.0f - y_min)/dy);

	f1 = surface.height[idx_mod(i, j, N, M)];

	return f1;
}


static inline float get_value(struct height_map surface, float *values, float x, float y){
	int i, j, N = surface.N, M = surface.M;
	float f1, f2, f3, f4, g1, g2, dx, dy, x_min, y_min;

	dx = surface.dx;
	dy = surface.dy;
	x_min = surface.x_min;
	y_min = surface.y_min;

	i = (int)((x - dx/2.0f - x_min)/dx);
	j = (int)((y - dy/2.0f - y_min)/dy);

	f1 = values[idx_mod(i + 0, j + 0, N, M)];
	f2 = values[idx_mod(i + 0, j + 1, N, M)];
	f3 = values[idx_mod(i + 1, j + 0, N, M)];
	f4 = values[idx_mod(i + 1, j + 1, N, M)];

	g1 = f1 + (f2 - f1)*(y - (y_min + j*dy))/dy;
	g2 = f3 + (f4 - f3)*(y - (y_min + j*dy))/dy;

	return g1 + (g2 - g1)*(x - (x_min + i*dx))/dx;
}

void transform2D_3D(struct height_map surface, float *point, float *result){
	result[0] = point[0];
	result[1] = point[1];
	result[2] = get_height(surface, point[0], point[1]); 
}

static inline float dot(float *A, float *B){
	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

static inline float dot2(float *A, float *B){
	return A[0]*B[0] + A[1]*B[1];
}

float norm(float *vector){
	return sqrtf(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
}

static inline void vector_multiply(float a, float *A, float *B){
	B[0] = a*A[0];
	B[1] = a*A[1];
	B[2] = a*A[2];
}

static inline void vector_add(float *A, float *B, float *C){
	C[0] = A[0] + B[0];
	C[1] = A[1] + B[1];
	C[2] = A[2] + B[2];
}

void orthogonalize(float *A, float *B, float *C){
	float component[3], cosine = dot(A, B);


	vector_multiply(-cosine, A, &component[0]);

	vector_add(B, &component[0], C);

	normalize(C, C);
}

void cross(float *A, float *B, float *result){
	result[0] = A[1]*B[2] - A[2]*B[1];
	result[1] = A[2]*B[0] - A[0]*B[2];
	result[2] = A[0]*B[1] - A[1]*B[0];
}

float vector_distance(float *A, float *B){
	float difference[3] = {B[0] - A[0], B[1] - A[1], B[2] - A[2]};
	return norm(&difference[0]);
}
void generate_point(struct height_map surface, float *point1, float *point2, float *result){
	float straight_distance, direction[3], difference[3], perpendicular[3], min_dist, new_point[3], p_surface[3], distance;
	
	difference[0] = point2[0] - point1[0];
	difference[1] = point2[1] - point1[1];
	difference[2] = point2[2] - point1[2];

	straight_distance = norm(&difference[0]);
	
	direction[0] = difference[0]/straight_distance;
	direction[1] = difference[1]/straight_distance;
	direction[2] = difference[2]/straight_distance;
	
	cross(&direction[0], &vector_up[0], &perpendicular[0]);

	float modulo = norm(&perpendicular[0]);
	perpendicular[0] *= 1.f/modulo;
	perpendicular[1] *= 1.f/modulo;
	perpendicular[2] *= 1.f/modulo;

	min_dist = 1000.f;
	for(int i = 0; i < 20; i++){
		new_point[0] = (point1[0] + point2[0])*0.5 + perpendicular[0]*straight_distance*((i - 10)/10.f);
		new_point[1] = (point1[1] + point2[1])*0.5 + perpendicular[1]*straight_distance*((i - 10)/10.f);
		new_point[2] = (point1[2] + point2[2])*0.5 + perpendicular[2]*straight_distance*((i - 10)/10.f);
		
		transform2D_3D(surface, &new_point[0], &p_surface[0]);

		if(new_point[2] < p_surface[2])
			new_point[2] = p_surface[2];

		distance = vector_distance(&new_point[0], &point1[0]) + vector_distance(&new_point[0], &point2[0]);
// 		printf("distance = %f\n", distance/straight_distance);
		if(distance < min_dist){
// 			printf("la concha la lora %f\n", distance/min_dist);
			min_dist = distance;
			result[0] = new_point[0];
			result[1] = new_point[1];
			result[2] = new_point[2];
		}
	}
}

void enderezar(struct height_map surface, float *path, unsigned int npoints, int nitera){
	float point[3];
	int n_pares, n_impares;
	n_pares   = (int)((npoints - 1)/2 - 1);
	n_impares = (int)((npoints - 1)/2);

	for(unsigned int i = 0; i < nitera; i++){
		for(unsigned int j = 0; j < n_pares; j++){
			generate_point(surface, &path[3*(2*j + 1)], &path[3*(2*j + 3)], &point[0]);
			path[3*(2*j + 2) + 0] = point[0];
			path[3*(2*j + 2) + 1] = point[1];
			path[3*(2*j + 2) + 2] = point[2];
		}
		for(unsigned int j = 0; j < n_impares; j++){
			generate_point(surface, &path[3*(2*j + 0)], &path[3*(2*j + 2)], &point[0]);
			path[3*(2*j + 1) + 0] = point[0];
			path[3*(2*j + 1) + 1] = point[1];
			path[3*(2*j + 1) + 2] = point[2];
		}
	}
}

void generate_path(float *point1_0, float *point2_0, int nsubdivision, float *result){
	float *path, *new_path, *aux, point1[3], point2[3], point[3];
	path = (float *) malloc(2*3*sizeof(float));
	int npoints1, npoints2;
	
	path[0] = point1_0[0];
	path[1] = point1_0[1];
	path[2] = point1_0[2];
	path[3] = point2_0[0];
	path[4] = point2_0[1];
	path[5] = point2_0[2];

	npoints1 = 2;
	for(unsigned int i = 0; i < nsubdivision; i++){
		npoints2 = pow(2, i + 1) + 1;

		new_path = (float *) malloc(npoints2*3*sizeof(float));
		for(unsigned int j = 0; j < npoints1 - 1; j++){
			point1[0] = path[3*(j + 0) + 0];
			point1[1] = path[3*(j + 0) + 1];
			point1[2] = path[3*(j + 0) + 2];

			point2[0] = path[3*(j + 1) + 0];
			point2[1] = path[3*(j + 1) + 1];
			point2[2] = path[3*(j + 1) + 2];

			new_path[3*(2*j + 0) + 0] = point1[0];
			new_path[3*(2*j + 0) + 1] = point1[1];
			new_path[3*(2*j + 0) + 2] = point1[2];

			new_path[3*(2*j + 1) + 0] = (point1[0] + point2[0])*0.5f;
			new_path[3*(2*j + 1) + 1] = (point1[1] + point2[1])*0.5f;
			new_path[3*(2*j + 1) + 2] = (point1[2] + point2[2])*0.5f;
		}
		new_path[3*(npoints2 - 1) + 0] = path[3*(npoints1 - 1) + 0];
		new_path[3*(npoints2 - 1) + 1] = path[3*(npoints1 - 1) + 1];
		new_path[3*(npoints2 - 1) + 2] = path[3*(npoints1 - 1) + 2];

		enderezar(surface, new_path, npoints2, 10);
		
		npoints1 = npoints2;

		aux = path;
		path = new_path;
		free(aux);
	}
	for(unsigned int i = 0; i < 3*npoints1; i++)
		result[i] = path[i];
}




float function(float x){
	float abs_x = fabsf(x);
	if( abs_x <= 0.5)
		return 1.0f - powf(abs_x, 2)*2;
	else if(abs_x <= 1)
		return powf(abs_x - 1, 2)*2;
	else
		return 0.0f;
}

float dot_2D(float position[2], float *random_gradient, int p, int q, float grad_grid, unsigned int M, float *weight){
	float grid_pos[2], grad[2], dot, u[2];
	
	grid_pos[0] = grad_grid*p - position[0];
	grid_pos[1] = grad_grid*q - position[1];
	
	u[0] = -grid_pos[0]/grad_grid;
	u[1] = -grid_pos[1]/grad_grid;

	grad[0] = random_gradient[2*(idx_mod(p, q, M, M)) + 0];
	grad[1] = random_gradient[2*(idx_mod(p, q, M, M)) + 1];

	dot = grad[0]*grid_pos[0] + grad[1]*grid_pos[1];
 	*weight = function(u[0])*function(u[1]);
	return dot*(*weight);
}

void create_perlin_2D(unsigned int gradient_resolution, float *grid, unsigned int grid_resolution){
	float position[2], grad_grid = 1.f/gradient_resolution, grid_size = 1.f/grid_resolution, suma, weight, *weights, *random_gradient, phi;

	int p, q, i, j;
	weights = (float *) malloc(4*sizeof(float));
	random_gradient = (float *) malloc(2*gradient_resolution*gradient_resolution*sizeof(float));
	
	for(i = 0; i < gradient_resolution; i++){
		for(j = 0; j < gradient_resolution; j++){
			phi = ((float)rand()/((float)RAND_MAX))*2*3.1415926535f;
			random_gradient[2*idx_mod(i, j, gradient_resolution, gradient_resolution) + 0] = cosf(phi);
			random_gradient[2*idx_mod(i, j, gradient_resolution, gradient_resolution) + 1] = sinf(phi);
		}
	}

	for(i = 0; i < grid_resolution; i++){
		for(j = 0; j < grid_resolution; j++){
			position[0] = grid_size*i;
			position[1] = grid_size*j;
				
			p = (int) (position[0]/grad_grid);
			q = (int) (position[1]/grad_grid);

			suma = 0.f;
			suma += dot_2D(position, random_gradient, p + 0, q + 0, grad_grid, gradient_resolution, &weights[0]);
			suma += dot_2D(position, random_gradient, p + 0, q + 1, grad_grid, gradient_resolution, &weights[1]);
			suma += dot_2D(position, random_gradient, p + 1, q + 0, grad_grid, gradient_resolution, &weights[2]);
			suma += dot_2D(position, random_gradient, p + 1, q + 1, grad_grid, gradient_resolution, &weights[3]);
				
			weight = weights[0] + weights[1] + weights[2] + weights[3];
			grid[idx_mod(i, j, grid_resolution, grid_resolution)] = suma/weight;
		}
		
		
	}
	free(weights);
	free(random_gradient);
}





////////// monkey-like enemy Navigation

void raycasting(float *position, float *rays, float *lengths, unsigned int nrays, float *intersections){
	int i, j, nitera;
	float dx = 0.5f, *point3D, x, h, diff1, diff2, x1, x2;
	
	point3D = (float *) malloc(3*sizeof(float));
	
	for(i = 0; i < nrays; i++){
		
		nitera = (int) (lengths[i]/dx + 1.0f);
		diff1  = position[2] - get_height(surface, position[0], position[1]);
		
		for(j = 1; j < nitera; j++){
			x = j*dx;
			point3D[0] = position[0] + x*rays[3*i + 0];
			point3D[1] = position[1] + x*rays[3*i + 1];
			point3D[2] = position[2] + x*rays[3*i + 2];

			diff2 = point3D[2] - get_height(surface, point3D[0], point3D[1]);
			
			if(diff2 <= 0.0f)
				break;
			
			diff1 = diff2;
		}
		x1 = x - dx;
		x2 = x;
		
		if(diff2 == 0.0f)
			intersections[i] = x2;
		else if(diff2 < 0.0f)
			intersections[i] = x1 - diff1*(x2 - x1)/(diff2 - diff1);
		else if(j == nitera)
			intersections[i] = -1.f;
		else
			intersections[i] = -1.f;
	}
	free(point3D);
}


void get_spherical_vectors(float r, float theta, float phi, float *versor_r, float *versor_theta, float *versor_phi){
	versor_r[0] = cosf(phi)*sinf(theta);
	versor_r[1] = sinf(phi)*sinf(theta);
	versor_r[2] = cosf(theta);

	versor_theta[0] = cosf(phi)*cosf(theta);
	versor_theta[1] = sinf(phi)*cosf(theta);
	versor_theta[2] = -sinf(theta);

	versor_phi[0] = -sinf(phi);
	versor_phi[1] = cosf(phi);
	versor_phi[2] = 0.f;
}

void transform_xyz_esf(float *xyz_coords, float *r, float *theta, float *phi){
	*r = norm(&xyz_coords[0]);
	*theta = acosf(xyz_coords[2]/(*r));
	*phi   = fmodf(atan2f(xyz_coords[1], xyz_coords[0]) + 2*pi, 2*pi);
}

void transform_esf_xyz(float *xyz_coords, float r, float theta, float phi){
	xyz_coords[0] = r*cosf(phi)*sinf(theta);
	xyz_coords[1] = r*sinf(phi)*sinf(theta);
	xyz_coords[2] = r*cosf(theta);
}

void solver(float *fixed_point, float *initial_velocity, float *initial_position, float *final_velocity, float *final_position, float *trajectory){
	float m = 1.0f, dt = 0.001f, R, phi, theta, *vector_r, *versor_r, *versor_theta, *versor_phi, *pivoted_coords, r_p, phi_p, theta_p, P_phi, phi_p_p, r_p_p, theta_p_p, theta_max = 0.0f, h, E;
	
	versor_r     = (float *) malloc(3*sizeof(float));
	versor_theta = (float *) malloc(3*sizeof(float));
	versor_phi   = (float *) malloc(3*sizeof(float));
	vector_r     = (float *) malloc(3*sizeof(float));
	
	pivoted_coords = (float *) malloc(3*sizeof(float));

	pivoted_coords[0] = initial_position[0] - fixed_point[0];
	pivoted_coords[1] = initial_position[1] - fixed_point[1];
	pivoted_coords[2] = initial_position[2] - fixed_point[2];
	
	transform_xyz_esf(&pivoted_coords[0], &R, &theta, &phi);
	get_spherical_vectors(R, theta, phi, &versor_r[0], &versor_theta[0], &versor_phi[0]);
	
	r_p     = dot(&versor_r[0],     &initial_velocity[0]);
	theta_p = dot(&versor_theta[0], &initial_velocity[0]);
	phi_p   = dot(&versor_phi[0],   &initial_velocity[0]);

	P_phi = m*powf(R*sinf(theta), 2)*phi_p;

	printf("phi momentum: %f, pos: (%f, %f, %f), vel: (%f, %f, %f)\n", P_phi, R, theta, phi, r_p, theta_p, phi_p);

	
	float vel_sign1 = theta_p, vel_sign2;
	for(int i = 0; i < 10000; i++){

		E = 0.5f*powf(theta_p*R, 2) + 0.5f*powf(P_phi, 2)*powf(R*R*sinf(theta), -2) + R*g*cosf(theta);
		printf("Energy: %f\n", E);
		
		if(theta > pi){
			theta = 2*pi - theta;
			phi   += pi;
		}
		theta_p_p = g*sinf(theta)/R + powf(P_phi, 2)*powf(m*R*R*sinf(theta), -2)*cosf(theta);

		
		theta += theta_p*dt;
		phi   += phi_p*dt;
		
		theta_p += theta_p_p*dt;
		phi_p    = P_phi/m*powf(R*sinf(theta), -2);
		

		vel_sign2 = theta_p;
		if(theta > theta_max)
			theta_max = theta;

		transform_esf_xyz(&vector_r[0], R, theta, phi);
		vector_r[0] += fixed_point[0];
		vector_r[1] += fixed_point[1];
		vector_r[2] += fixed_point[2];

		trajectory[3*i + 0] = vector_r[0];
		trajectory[3*i + 1] = vector_r[1];
		trajectory[3*i + 2] = vector_r[2];
		
		h = vector_r[2];

		if((get_height(surface, vector_r[0], vector_r[1]) > h)||((vel_sign2 > 0)&&(vel_sign1 < 0))){
			final_position[0] = vector_r[0];
			final_position[1] = vector_r[1];
			final_position[2] = vector_r[2];

			get_spherical_vectors(R, theta, phi, &versor_r[0], &versor_theta[0], &versor_phi[0]);

			final_velocity[0] = R*versor_theta[0]*theta_p + versor_phi[0]*phi_p*R*sinf(theta);
			final_velocity[1] = R*versor_theta[1]*theta_p + versor_phi[1]*phi_p*R*sinf(theta);
			final_velocity[2] = R*versor_theta[2]*theta_p + versor_phi[2]*phi_p*R*sinf(theta);

			break;
		}

		vel_sign1 = vel_sign2;
	}

	free(versor_r);
	free(versor_theta);
	free(versor_phi);
	free(vector_r);
	free(pivoted_coords);
}

void check_values(float *positions, unsigned int N, float *heights){
	for(int i = 0; i < N; i++){
		heights[i] = get_height(surface, positions[2*i + 0], positions[2*i + 1]);
	}
}

// static inline float dot(float *A, float *B){
// 	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
// }
static inline void get_tangent_vectors(float *position, float *v1, float *v2, float *v3){
	float dx = 0.00001f, p1[3], p2[3], p3[3], mod1, mod2, aux;
	
	p1[0] = position[0];
	p1[1] = position[1];
	p1[2] = get_height(surface, p1[0], p1[1]);

	p2[0] = position[0] + dx;
	p2[1] = position[1];
	p2[2] = get_height(surface, p2[0], p2[1]);

	p3[0] = position[0];
	p3[1] = position[1] + dx;
	p3[2] = get_height(surface, p3[0], p3[1]);
	
	v1[0] = p2[0] - p1[0];
	v1[1] = p2[1] - p1[1];
	v1[2] = p2[2] - p1[2];

	v2[0] = p3[0] - p1[0];
	v2[1] = p3[1] - p1[1];
	v2[2] = p3[2] - p1[2];
	
	mod1 = norm(&v1[0]);
	v1[0] *= 1.0f/mod1;
	v1[1] *= 1.0f/mod1;
	v1[2] *= 1.0f/mod1;

	mod2 = norm(&v2[0]);
	v2[0] *= 1.0f/mod2;
	v2[1] *= 1.0f/mod2;
	v2[2] *= 1.0f/mod2;
	
	aux = dot(&v1[0], &v2[0]);
	v2[0] += -aux*v1[0];
	v2[1] += -aux*v1[1];
	v2[2] += -aux*v1[2];

	mod2 = norm(&v2[0]);
	v2[0] *= 1.0f/mod2;
	v2[1] *= 1.0f/mod2;
	v2[2] *= 1.0f/mod2;
	
	cross(&v1[0], &v2[0], &v3[0]);
}

static inline float check_occlusion(struct height_map surface, float *position, float *direction, float distance, int raymarching_N){
	float dx = distance/raymarching_N, r[3], z;
	float occlusion = 1.0f, dist;

	for(unsigned int i = 0; i < raymarching_N; i++){
		dist = powf(dx*(i + 1)/distance, 2)*distance;
		r[0] = position[0] + dist*direction[0];
		r[1] = position[1] + dist*direction[1];
		r[2] = position[2] + dist*direction[2];
		
		z =  get_height(surface, r[0], r[1]);
		
		if(z > r[2]){
			occlusion = 0.0f;
			break;
		}
		// if((r[2] > surface.max_height)&&(direction[2] >= 0.0f)){
		// if(r[2] > surface.max_height){
			// occlusion = 1.0f;
			// break;
		// }
	}
	return occlusion;
}

static inline float raycasting_sphere(float *position, float distance, unsigned int Nsamples, unsigned int raymarching_N){
	float phi, theta, dphi, dtheta, dA, direction[3], occlusion, total_occlusion = 0.0f, normalization = 0.0f;
	float costheta;
	
	for(unsigned int i = 0; i < Nsamples; i++){
		costheta = ((float)rand()/((float)RAND_MAX));
		phi      = ((float)rand()/((float)RAND_MAX))*2*pi;

		theta = acosf(costheta);

		direction[0] = cosf(phi)*sinf(theta);
		direction[1] = sinf(phi)*sinf(theta);
		direction[2] = cosf(theta);
		
		occlusion = check_occlusion(surface, &position[0], &direction[0], distance, raymarching_N);
		dA        = 1.0f;
		total_occlusion += occlusion*dA;
		normalization   += dA;
	}

	return fminf(total_occlusion/normalization, 1.0f);
}

static inline float raycasting_sphere2(float *position, float *normal, float *sky_dir, float distance, unsigned int Nsamples, unsigned int raymarching_N){
	float phi, theta, dphi, dtheta, dA, direction[3], occlusion, total_occlusion = 0.0f, normalization = 0.0f, vec1_orig[3] = {1.0f, 0.0f, 0.0f}, vec1[3], vec2[3];
	float costheta;


	orthogonalize(&normal[0], &vec1_orig[0], &vec1[0]);
	cross(&vec1[0], &normal[0], &vec2[0]);
	for(unsigned int i = 0; i < Nsamples; i++){
		costheta = ((float)rand()/((float)RAND_MAX));
		phi      = ((float)rand()/((float)RAND_MAX))*2*pi;

		theta = acosf(costheta);
		for(unsigned int j = 0; j < 3; j++)
			direction[j] = cosf(phi)*sinf(theta)*vec1[j] + sinf(phi)*sinf(theta)*vec2[j] + cosf(theta)*normal[j];

		if(dot(&direction[0], &sky_dir[0]) >= 0.0f)
			occlusion = check_occlusion(surface, &position[0], &direction[0], distance, raymarching_N);
		else
			occlusion = 0.0f;
		dA        = 1.0f;
		total_occlusion += occlusion*dA;
		normalization   += dA;
	}

	return fminf(total_occlusion/normalization, 1.0f);
}

static inline float raycasting_sphere3(float *position, float *normal, float *sky_dir, float distance, unsigned int Nsamples){
	float phi, theta, dphi, dtheta, dA, direction[3], occlusion, h, total_occlusion = 0.0f, normalization = 0.0f, vec1_orig[3] = {1.0f, 0.0f, 0.0f}, vec1[3], vec2[3];
	float costheta;


	orthogonalize(&normal[0], &vec1_orig[0], &vec1[0]);
	cross(&vec1[0], &normal[0], &vec2[0]);
	for(unsigned int i = 0; i < Nsamples; i++){
		costheta = ((float)rand()/((float)RAND_MAX));
		phi      = ((float)rand()/((float)RAND_MAX))*2*pi;

		theta = acosf(costheta);
		for(unsigned int j = 0; j < 3; j++)
			direction[j] = cosf(phi)*sinf(theta)*vec1[j] + sinf(phi)*sinf(theta)*vec2[j] + cosf(theta)*normal[j];

		if(dot(&direction[0], &sky_dir[0]) >= 0.0f){
			h = get_nearest_height(surface, position[0] + direction[0]*distance, position[1] + direction[1]*distance);
			if(position[2] + direction[2]*distance <= h)
				occlusion = 0.0f;
			else
				occlusion = 1.0f;
		}

		dA        = 1.0f;
		total_occlusion += occlusion*dA;
		normalization   += dA;
	}

	return fminf(total_occlusion/normalization, 1.0f);
}
void get_normal_at_point(float *position, float dS, float *normal){
	float posX1[3], posX2[3], posY1[3], posY2[3], vec1[3], vec2[3], vec3[3], norma;

	posX2[0] = position[0] + dS;
	posX2[1] = position[1];
	posX2[2] = get_height(surface, posX2[0], posX2[1]);

	posX1[0] = position[0] - dS;
	posX1[1] = position[1];
	posX1[2] = get_height(surface, posX1[0], posX1[1]);

	posY2[0] = position[0];
	posY2[1] = position[1] + dS;
	posY2[2] = get_height(surface, posY2[0], posY2[1]);

	posY1[0] = position[0];
	posY1[1] = position[1] - dS;
	posY1[2] = get_height(surface, posY1[0], posY1[1]);

	substract(&posX2[0], &posX1[0], &vec1[0]);
	substract(&posY2[0], &posY1[0], &vec2[0]);

	cross(&vec1[0], &vec2[0], &vec3[0]);
	norma = norm(&vec3[0]);
	normal[0] = vec3[0]/norma;
	normal[1] = vec3[1]/norma;
	normal[2] = vec3[2]/norma;
}

void calculate_AO(unsigned int N, unsigned int M, float *AO_map, unsigned int Nsamples, unsigned int dist_res, float max_dist){
// 	float max_dist = fmaxf(surface.x_max - surface.x_min, surface.y_max - surface.y_min)/10.0f;
	printf("To calculate AO\n");
	#pragma omp parallel num_threads(4)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float *position;
	position = (float *) malloc(3*sizeof(float));
	#pragma omp for schedule(dynamic, 5)
	for(unsigned int i = 0; i < surface.N; i++){
		if(i%50 == 0)
			printf("i = %d of %d\n", i, surface.N);
		for(unsigned int j = 0; j < surface.M; j++){
			position[0] = surface.x_min + i*surface.dx + surface.dx/2.0f + (random() - 0.5f)*surface.dx;
			position[1] = surface.y_min + j*surface.dy + surface.dy/2.0f + (random() - 0.5f)*surface.dy;
			
			position[2] = get_height(surface, position[0], position[1]);
			
			AO_map[i*M + j] = raycasting_sphere(position, max_dist, Nsamples, dist_res);
		}
	}
	free(position);
	}
}



void calculate_advanced_AO(unsigned int N, unsigned int M, float *AO_map, float *sky_dir, unsigned int Nsamples, unsigned int dist_res, float max_dist){
// 	float max_dist = fmaxf(surface.x_max - surface.x_min, surface.y_max - surface.y_min)/10.0f;
	printf("To calculate AO\n");
	#pragma omp parallel num_threads(8)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float *position, normal[3];
	position = (float *) malloc(3*sizeof(float));
	#pragma omp for schedule(dynamic, 60)
	for(unsigned int i = 0; i < surface.N; i++){
		if(i%100 == 0)
			printf("i = %d of %d\n", i, surface.N);
		for(unsigned int j = 0; j < surface.M; j++){
			position[0] = surface.x_min + i*surface.dx + surface.dx/2.0f + (random() - 0.5f)*surface.dx;
			position[1] = surface.y_min + j*surface.dy + surface.dy/2.0f + (random() - 0.5f)*surface.dy;
			position[2] = get_height(surface, position[0], position[1]);

			// get_normal_at_point(&position[0], 0.01f, &normal[0]);

			AO_map[i*M + j] = raycasting_sphere2(position, sky_dir, sky_dir, max_dist, Nsamples, dist_res);
		}
	}
	free(position);
	}
}


void efficient_AO(unsigned int N, unsigned int M, float *AO_map, float *sky_dir, unsigned int Nsamples, float max_dist){
// 	float max_dist = fmaxf(surface.x_max - surface.x_min, surface.y_max - surface.y_min)/10.0f;
	printf("To calculate AO\n");
	#pragma omp parallel num_threads(12)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float *position, normal[3];
	position = (float *) malloc(3*sizeof(float));
	#pragma omp for schedule(dynamic, 20)
	for(unsigned int i = 0; i < surface.N; i++){
		if(i%100 == 0)
			printf("i = %d of %d\n", i, surface.N);
		for(unsigned int j = 0; j < surface.M; j++){
			position[0] = surface.x_min + i*surface.dx + surface.dx/2.0f + (random() - 0.5f)*surface.dx;
			position[1] = surface.y_min + j*surface.dy + surface.dy/2.0f + (random() - 0.5f)*surface.dy;
			position[2] = get_height(surface, position[0], position[1]);

			// get_normal_at_point(&position[0], 0.01f, &normal[0]);

			AO_map[i*M + j] = raycasting_sphere3(position, sky_dir, sky_dir, max_dist, Nsamples);
			 // = raycasting_sphere2(position, sky_dir, sky_dir, max_dist, Nsamples, dist_res);
		}
	}
	free(position);
	}
}

void fast_area_calculation(float *vertices, int *triangles, int nverts, int ntriangs, float *areas, int nthreads){
	#pragma omp parallel num_threads(12)
	{
	float vert1[3], vert2[3], vert3[3], side1[3], side2[3], cross_product[3];
	int threadID = omp_get_thread_num(), triangle[3];

	#pragma omp for schedule(dynamic, 500)
	for(unsigned int i = 0; i < ntriangs; i++){
		triangle[0] = triangles[3*i + 0];
		triangle[1] = triangles[3*i + 1];
		triangle[2] = triangles[3*i + 2];

		vert1[0] = vertices[3*triangle[0] + 0];
		vert1[1] = vertices[3*triangle[0] + 1];
		vert1[2] = vertices[3*triangle[0] + 2];

		vert2[0] = vertices[3*triangle[1] + 0];
		vert2[1] = vertices[3*triangle[1] + 1];
		vert2[2] = vertices[3*triangle[1] + 2];

		vert3[0] = vertices[3*triangle[2] + 0];
		vert3[1] = vertices[3*triangle[2] + 1];
		vert3[2] = vertices[3*triangle[2] + 2];

		side1[0] = vert2[0] - vert1[0];
		side1[1] = vert2[1] - vert1[1];
		side1[2] = vert2[2] - vert1[2];

		side2[0] = vert3[0] - vert1[0];
		side2[1] = vert3[1] - vert1[1];
		side2[2] = vert3[2] - vert1[2];

		cross_product[0] = side1[1]*side2[2] - side1[2]*side2[1];
		cross_product[1] = side1[2]*side2[0] - side1[0]*side2[2];
		cross_product[2] = side1[0]*side2[1] - side1[1]*side2[0];

		areas[i] = sqrtf(dot(&cross_product[0], &cross_product[0]))/2.0f;
	}
	}
}

static inline void assign(float *A, float *B){
	B[0] = A[0];
	B[1] = A[1];
	B[2] = A[2];
}

static inline void assign_dim(float *A, float *B, unsigned int dim){
	// B[0] = A[0];
	// B[1] = A[1];
	// if(dim >= 3)
	// 	B[2] = A[2];
	// if(dim >= 4)
	// 	B[3] = A[3];

	for(unsigned int i = 0; i < dim; i++)
		B[i] = A[i];
}

void check_triangles(float *vertices, int *triangles, float *new_vertices, int *new_triangles, int *vertex_usage, unsigned int nverts, unsigned int ntriang, float criteria, int *Ns){
	unsigned int i;
	int triangle[3], offset_triangles, *new_indices;
	float vert1[3], vert2[3], vert3[3], A, B, C, side1[3], side2[3], side3[3];
	bool validity;

	new_indices   = (int *) malloc(nverts*sizeof(int));

	for(i = 0; i < nverts; i++)
		vertex_usage[i] = 0;

	offset_triangles = 0;
	for(i = 0; i < ntriang; i++){
		triangle[0] = triangles[3*i + 0];
		triangle[1] = triangles[3*i + 1];
		triangle[2] = triangles[3*i + 2];

		assign(&vertices[3*triangle[0] + 0], &vert1[0]);
		assign(&vertices[3*triangle[1] + 0], &vert2[0]);
		assign(&vertices[3*triangle[2] + 0], &vert3[0]);

		substract(&vert2[0], &vert1[0], &side1[0]);
		substract(&vert3[0], &vert2[0], &side2[0]);
		substract(&vert1[0], &vert3[0], &side3[0]);

		A = sqrtf(dot(&side1[0], &side1[0]));
		B = sqrtf(dot(&side2[0], &side2[0]));
		C = sqrtf(dot(&side3[0], &side3[0]));

		// if((A < criteria)||(B < criteria)||(C < criteria)||(A + B - C < criteria)||(A + C - B < criteria)||(C + B - A < criteria))
		if((A < criteria)||(B < criteria)||(C < criteria))

			validity = false;
		else if(isnan(A*B*C))
			validity = false;
		else
			validity = true;

		if(validity){
			new_triangles[3*offset_triangles + 0] = triangle[0];
			new_triangles[3*offset_triangles + 1] = triangle[1];
			new_triangles[3*offset_triangles + 2] = triangle[2];

			offset_triangles += 1;
		}
	}

	for(i = 0; i < offset_triangles; i++){
		triangle[0] = new_triangles[3*i + 0];
		triangle[1] = new_triangles[3*i + 1];
		triangle[2] = new_triangles[3*i + 2];

		vertex_usage[triangle[0]] += 1;
		vertex_usage[triangle[1]] += 1;
		vertex_usage[triangle[2]] += 1;
	}

	int vertex_offset = 0;
	for(i = 0; i < nverts; i++){
		new_indices[i] = vertex_offset;
		if(vertex_usage[i]){
			assign(&vertices[3*i + 0], &new_vertices[3*vertex_offset + 0]);
// 			new_vertices[vertex_offset] = vertices[i];
			vertex_offset += 1;
		}
	}
	for(i = 0; i < offset_triangles*3; i++)
		new_triangles[i] = new_indices[new_triangles[i]];
	Ns[0] = vertex_offset;
	Ns[1] = offset_triangles;

	free(new_indices);
}

void calculate_normals(float *vertices, int *triangles, int nverts, int ntriangs, float *normals, int nthreads){
	float *shared_normals = malloc(nthreads*3*nverts*sizeof(float));

// 	printf("to calculate normals!!, nthreads = %d, nverts = %d, ntriangs = %d\n", nthreads, nverts, ntriangs);
	for(unsigned int k = 0; k < 3*nverts*nthreads; k++)
		shared_normals[k] = 0.0f;
	
	#pragma omp parallel num_threads(nthreads)
	{
	float area;
	float vert1[3], vert2[3], vert3[3], side1[3], side2[3], cross_product[3], current_normal[3];
	int threadID = omp_get_thread_num(), triangle[3];
// 	printf("thread ID = %d\n", threadID);
	#pragma omp for schedule(dynamic, 500)
	for(unsigned int i = 0; i < ntriangs; i++){
		triangle[0] = triangles[3*i + 0];
		triangle[1] = triangles[3*i + 1];
		triangle[2] = triangles[3*i + 2];
		
		assign(&vertices[3*triangle[0] + 0], &vert1[0]);
		assign(&vertices[3*triangle[1] + 0], &vert2[0]);
		assign(&vertices[3*triangle[2] + 0], &vert3[0]);

		substract(&vert2[0], &vert1[0], &side1[0]);
		substract(&vert3[0], &vert1[0], &side2[0]);

		cross_product[0] = side1[1]*side2[2] - side1[2]*side2[1];
		cross_product[1] = side1[2]*side2[0] - side1[0]*side2[2];
		cross_product[2] = side1[0]*side2[1] - side1[1]*side2[0];

		area  = sqrtf(cross_product[0]*cross_product[0] + cross_product[1]*cross_product[1] + cross_product[2]*cross_product[2])/2.f;

		current_normal[0] = cross_product[0]/(2*area);
		current_normal[1] = cross_product[1]/(2*area);
		current_normal[2] = cross_product[2]/(2*area);

		shared_normals[3*threadID*nverts + 3*triangle[0] + 0] += area*current_normal[0];
		shared_normals[3*threadID*nverts + 3*triangle[0] + 1] += area*current_normal[1];
		shared_normals[3*threadID*nverts + 3*triangle[0] + 2] += area*current_normal[2];

		shared_normals[3*threadID*nverts + 3*triangle[1] + 0] += area*current_normal[0];
		shared_normals[3*threadID*nverts + 3*triangle[1] + 1] += area*current_normal[1];
		shared_normals[3*threadID*nverts + 3*triangle[1] + 2] += area*current_normal[2];

		shared_normals[3*threadID*nverts + 3*triangle[2] + 0] += area*current_normal[0];
		shared_normals[3*threadID*nverts + 3*triangle[2] + 1] += area*current_normal[1];
		shared_normals[3*threadID*nverts + 3*triangle[2] + 2] += area*current_normal[2];
		
// 		printf("threadID = %d, number = %d, nverts = %d, ntriangs = %d \n", threadID, 3*threadID*nverts + 3*triangle[2] + 2, nverts, ntriangs);
	}
	}
	float module, sum_normals[3];
	for(unsigned int k = 0; k < nverts; k++){
		sum_normals[0] = 0.0f;
		sum_normals[1] = 0.0f;
		sum_normals[2] = 0.0f;
		for(unsigned int j = 0; j < nthreads; j++){
			sum_normals[0] += shared_normals[3*j*nverts + 3*k + 0];
			sum_normals[1] += shared_normals[3*j*nverts + 3*k + 1];
			sum_normals[2] += shared_normals[3*j*nverts + 3*k + 2];
		}
		module = sqrtf(sum_normals[0]*sum_normals[0] + sum_normals[1]*sum_normals[1] + sum_normals[2]*sum_normals[2]);
		normals[3*k + 0] = sum_normals[0]/module;
		normals[3*k + 1] = sum_normals[1]/module;
		normals[3*k + 2] = sum_normals[2]/module;
	}
	free(shared_normals);
}

unsigned int poissonRandom(double expectedValue) {
// 	const int INT_MAX = 100000;
	unsigned int n = 0; //counter of iteration
	double limit;
	double x;  //pseudo random number
	limit = -expectedValue;
	x = logf(random());
	while (x > limit) {
		n++;
		x += logf(random());
	}
	return n;
}

void test_poisson_random(float value, int *result, int N){
	for(int i = 0; i < N; i++){
		result[i] = poissonRandom(value);
	}

}


static inline float sign(float *p1, float *p2, float *p3){
	return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1]);
}

static inline bool PointInTriangle(float *pt, float *v1, float *v2, float *v3){
	float d1, d2, d3;
	bool has_neg, has_pos;
	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}

static inline void add(float *A, float *B){
	A[0] += B[0];
	A[1] += B[1];
	A[2] += B[2];
}

static inline void mat_mul(float *A, float *B, float *C){
	for(unsigned int i = 0; i < 3; i++){
		for(unsigned int j = 0; j < 3; j++)
			C[3*i + j] = A[3*i + 0]*B[3*0 + j] + A[3*i + 1]*B[3*1 + j] + A[3*i + 2]*B[3*2 + j];
	}
}

static inline void mat_mul_vec(float *M, float *U, float *V){
	for(unsigned int i = 0; i < 3; i++){
		V[i] = M[3*i + 0]*U[0] + M[3*i + 1]*U[1] + M[3*i + 2]*U[2];
	}
}

void euler_matrix(float *angles, float *matrix){
	float c1, c2, c3, s1, s2, s3;

	c1 = cosf(angles[0]);
	c2 = cosf(angles[1]);
	c3 = cosf(angles[2]);
	s1 = sinf(angles[0]);
	s2 = sinf(angles[1]);
	s3 = sinf(angles[2]);

	matrix[3*0 + 0] = c1*c3 - c2*s1*s3;  matrix[3*0 + 1] = -c1*s3 - c2*c3*s1; matrix[3*0 + 2] = s1*s2;
	matrix[3*1 + 0] = c3*s1 + c1*c2*s3;  matrix[3*1 + 1] = c1*c2*c3 - s1*s3;  matrix[3*1 + 2] = -c1*s2;
	matrix[3*2 + 0] = s2*s3;             matrix[3*2 + 1] = c3*s2;             matrix[3*2 + 2] = c2;
}

static inline float modulo(float A, float B){
	float C = floor(A/B);

	return A - C*B;
}

unsigned int uniformly_random_spread(float *X, float *Y, unsigned int Nmax, float *extent, float density, float deviation){
	int n, m, N;
	float size, new_extent[4], phaseX, phaseY = random();
	size = sqrtf(1.0f/density);

	phaseX = (random() - 0.5f)*size;
	phaseY = (random() - 0.5f)*size;

	n = (int)((extent[1] - extent[0])/size) + 1;
	m = (int)((extent[3] - extent[2])/size) + 1;

	new_extent[0] = extent[0];
	new_extent[1] = extent[0] + size*n;
	new_extent[2] = extent[2];
	new_extent[3] = extent[2] + size*m;

	N = n*m;

	for(unsigned int i = 0; i < n; i++){
		for(unsigned int j = 0; j < m; j++){
			if(i*m + j < Nmax){
				X[i*m + j] = modulo(i*size + (random() - 0.5f)*deviation*size + phaseX, n*size) + new_extent[0];
				Y[i*m + j] = modulo(j*size + (random() - 0.5f)*deviation*size + phaseY, m*size) + new_extent[2];
			}
			else
				break;
		}
	}
	return min(N, Nmax);
}

void cover_surface(float *vertices, int *triangles, unsigned int ntriang, unsigned int maximum, float surface_density, float size, float angle_min, float angle_max, float *cover_vertices, int *cover_triangles, unsigned int *n){

	float vert1[3], vert2[3], vert3[3], side1[3], side2[3], vec_i[3], vec_j[3], vec_k[3], normal_old[3], normal[3], norm, side1_2D[2], side2_2D[2],
	extent[4], rectangle_area, x, y, point[2], side0_2D[2] = {0.0f, 0.0f}, point3D[3], euler[3], vec_u[3], vec_uu[3], vec_uuu[3], vec_v[3], vec_vv[3], vec_vvv[3], vec_w[3], vec_ww[3], vec_www[3], rot_matrix[9], pos1[3] = {size/2.f, -size/2.f, 0.f}, pos2[3] = {size/2.f, size/2.f, 0.f}
	, pos3[3] = {-size/2.f, size/2.f, 0.f}, pos4[3] = {-size/2.f, -size/2.f, 0.f}, new_vert1[3], new_vert2[3], new_vert3[3], new_vert4[3], mat_tri[9], total_matrix[9], *X, *Y;
	unsigned int vert_count = 0, Nquads, quad_count = 0;
	bool intriangle;

	X = (float *) malloc(10000*sizeof(float));
	Y = (float *) malloc(10000*sizeof(float));

	for(unsigned int tri_id = 0; tri_id < ntriang; tri_id++){
		assign(&vertices[3*triangles[3*tri_id + 0]], &vert1[0]);
		assign(&vertices[3*triangles[3*tri_id + 1]], &vert2[0]);
		assign(&vertices[3*triangles[3*tri_id + 2]], &vert3[0]);

		side1[0] = vert2[0] - vert1[0];
		side1[1] = vert2[1] - vert1[1];
		side1[2] = vert2[2] - vert1[2];

		side2[0] = vert3[0] - vert1[0];
		side2[1] = vert3[1] - vert1[1];
		side2[2] = vert3[2] - vert1[2];

		cross(&side1[0], &side2[0], &normal_old[0]);

		normalize(&normal_old[0], &normal[0]);

		normalize(&side1[0], &vec_i[0]);
		cross(&normal[0], &vec_i[0], &vec_j[0]);
		assign(&normal[0], &vec_k[0]);

		mat_tri[3*0 + 0] = vec_i[0]; mat_tri[3*0 + 1] = vec_j[0]; mat_tri[3*0 + 2] = vec_k[0];
		mat_tri[3*1 + 0] = vec_i[1]; mat_tri[3*1 + 1] = vec_j[1]; mat_tri[3*1 + 2] = vec_k[1];
		mat_tri[3*2 + 0] = vec_i[2]; mat_tri[3*2 + 1] = vec_j[2]; mat_tri[3*2 + 2] = vec_k[2];


		side1_2D[0] = dot(&vec_i[0], &side1[0]);
		side1_2D[1] = 0.0f;

		side2_2D[0] = dot(&vec_i[0], &side2[0]);
		side2_2D[1] = dot(&vec_j[0], &side2[0]);

		extent[0] = fminf(0.f, side2_2D[0]);
		extent[1] = fmaxf(side1_2D[0], side2_2D[0]);
		extent[2] = fminf(0.f, side2_2D[1]);
		extent[3] = fmaxf(0.f, side2_2D[1]);

		rectangle_area = (extent[1] - extent[0])*(extent[3] - extent[2]);

// 		Nquads = poissonRandom(rectangle_area*surface_density);
		Nquads = uniformly_random_spread(X, Y, 10000, &extent[0], surface_density, 1.0f);

		if(Nquads > 0){
			for(unsigned int i = 0; i < Nquads; i++){
// 				x = random()*(extent[1] - extent[0]) + extent[0];
// 				y = random()*(extent[3] - extent[2]) + extent[2];
				x = X[i];
				y = Y[i];
				point[0] = x;
				point[1] = y;

				intriangle = PointInTriangle(&point[0], &side0_2D[0], &side1_2D[0], &side2_2D[0]);

				if(intriangle&&(quad_count < maximum - 1)){
					point3D[0] = vert1[0] + point[0]*vec_i[0] + point[1]*vec_j[0];
					point3D[1] = vert1[1] + point[0]*vec_i[1] + point[1]*vec_j[1];
					point3D[2] = vert1[2] + point[0]*vec_i[2] + point[1]*vec_j[2];

					euler[0] = random()*2*pi;
// 					euler[1] = random()*angle_spread*pi/180.0f;
					euler[1] = (random()*(angle_max - angle_min) + angle_min)*pi/180.0f;
					euler[2] = random()*2*pi;

					euler_matrix(&euler[0], &rot_matrix[0]);

					mat_mul(&mat_tri[0], &rot_matrix[0], &total_matrix[0]);

					mat_mul_vec(&total_matrix[0], &pos1[0], &new_vert1[0]);
					mat_mul_vec(&total_matrix[0], &pos2[0], &new_vert2[0]);
					mat_mul_vec(&total_matrix[0], &pos3[0], &new_vert3[0]);
					mat_mul_vec(&total_matrix[0], &pos4[0], &new_vert4[0]);

					add(&new_vert1[0], &point3D[0]);
					add(&new_vert2[0], &point3D[0]);
					add(&new_vert3[0], &point3D[0]);
					add(&new_vert4[0], &point3D[0]);

					assign(&new_vert1[0], &cover_vertices[3*(vert_count + 0) + 0]);
					assign(&new_vert2[0], &cover_vertices[3*(vert_count + 1) + 0]);
					assign(&new_vert3[0], &cover_vertices[3*(vert_count + 2) + 0]);
					assign(&new_vert4[0], &cover_vertices[3*(vert_count + 3) + 0]);

					cover_triangles[6*quad_count + 0] = vert_count + 0;
					cover_triangles[6*quad_count + 1] = vert_count + 1;
					cover_triangles[6*quad_count + 2] = vert_count + 2;
					cover_triangles[6*quad_count + 3] = vert_count + 2;
					cover_triangles[6*quad_count + 4] = vert_count + 3;
					cover_triangles[6*quad_count + 5] = vert_count + 0;

					quad_count += 1;
					vert_count += 4;
				}
			}
		}
	}
	n[0] = quad_count;
	n[1] = vert_count;
	free(X);
	free(Y);

}

void get_heightmap_from_geometry(float *vertices, int *triangles, unsigned int ntriang, float *heightmap, unsigned int N, unsigned int M, float *map_extent){
	float tri_extent[4], dx = (map_extent[1] - map_extent[0])/N, dy = (map_extent[3] - map_extent[2])/M, vert1[3], vert2[3], vert3[3], x, y, side1[3], side2[3], p[2], p0[2], A, B, C, a, b, c, h1, h2, h3, h;
	bool intriangle;

	int i_min, i_max, j_min, j_max;
	for(unsigned int tri_id = 0; tri_id < ntriang; tri_id++){
// 		if(tri_id%1000 == 0)
// 			printf("frac = %f\n", tri_id*1.0f/ntriang);
		assign(&vertices[3*triangles[3*tri_id + 0]], &vert1[0]);
		assign(&vertices[3*triangles[3*tri_id + 1]], &vert2[0]);
		assign(&vertices[3*triangles[3*tri_id + 2]], &vert3[0]);

		tri_extent[0] = fminf(vert1[0], vert2[0]);
		tri_extent[0] = fminf(vert3[0], tri_extent[0]);

		tri_extent[1] = fmaxf(vert1[0], vert2[0]);
		tri_extent[1] = fmaxf(vert3[0], tri_extent[1]);

		tri_extent[2] = fminf(vert1[1], vert2[1]);
		tri_extent[2] = fminf(vert3[1], tri_extent[2]);

		tri_extent[3] = fmaxf(vert1[1], vert2[1]);
		tri_extent[3] = fmaxf(vert3[1], tri_extent[3]);

		i_min = (int)((tri_extent[0] - map_extent[0])/dx) - 1;
		i_max = (int)((tri_extent[1] - map_extent[0])/dx) + 1;

		j_min = (int)((tri_extent[2] - map_extent[2])/dy) - 1;
		j_max = (int)((tri_extent[3] - map_extent[2])/dy) + 1;

		substract(&vert2[0], &vert1[0], &side1[0]);
		substract(&vert3[0], &vert1[0], &side2[0]);

		A = dot2(&side1[0], &side1[0]);
		B = dot2(&side1[0], &side2[0]);
		C = dot2(&side2[0], &side2[0]);

		h1 = vert1[2];
		h2 = vert2[2];
		h3 = vert3[2];

		c = h1;
		b = ((h3 - c)*A - (h2 - c)*B)/(A*C - B*B);
		a = ((h2 - c) - B*b)/A;

		for(int i = max(i_min, 0); i < min(i_max, N); i++){
			for(int j = max(j_min, 0); j < min(j_max, M); j++){
				x = map_extent[0] + i*dx + dx/2.f;
				y = map_extent[2] + j*dy + dy/2.f;

				p[0] = x;
				p[1] = y;
				intriangle = PointInTriangle(&p[0], &vert1[0], &vert2[0], &vert3[0]);

				if(intriangle){
					p0[0] = p[0] - vert1[0];
					p0[1] = p[1] - vert1[1];

					h = dot2(&p0[0], &side1[0])*a + dot2(&p0[0], &side2[0])*b + c;

					if(h > heightmap[i*M + j])
						heightmap[i*M + j] = h;
				}
			}
		}
	}
}

void efficient_maximum(float *shared_heightmap, float *shared_quantity, float *maximum_heightmap, float *maximum_quantity, unsigned int nthreads){

}

static inline unsigned int map_indexing(unsigned int i, unsigned int j, unsigned int thread_id, unsigned int N, unsigned int M, unsigned int nthreads){
	return nthreads*(i*M + j) + thread_id;
	// return thread_id*N*M + i*M + j;
}

void map_quantity_from_geometry(float *vertices, float *quantity, int *triangles, unsigned int ntriang, float *heightmap, float *quantity_map, unsigned int N, unsigned int M, float *map_extent, unsigned int nthreads, unsigned int optimize, unsigned int vertex_wise){
	float tri_extent[4], dx = (map_extent[1] - map_extent[0])/N, dy = (map_extent[3] - map_extent[2])/M, *shared_quantity, *shared_heightmap;
	unsigned int stride = N*M;

	if(nthreads > 1){
		shared_quantity  = (float *) malloc(nthreads*stride*sizeof(float));
		shared_heightmap = (float *) malloc(nthreads*stride*sizeof(float));
		for(unsigned int i = 0; i < nthreads*stride; i++){
			shared_quantity[i]  = 0.0f;
			shared_heightmap[i] = 0.0f;
		}
	}
	else{
		shared_quantity = quantity_map;
		shared_heightmap = heightmap;
	}

	#pragma omp parallel num_threads(nthreads)
{
	float vert1[3], vert2[3], vert3[3], x, y, side1[3], side2[3], p[2], p0[2], normal[3], A, B, C, a, b, c, a_q, b_q, c_q, h1, h2, h3, q1, q2, q3, q, h;
	bool intriangle, valid;
	int thread_id = omp_get_thread_num();


	int i_min, i_max, j_min, j_max;
	#pragma omp for schedule(dynamic, 2000)
	for(unsigned int tri_id = 0; tri_id < ntriang; tri_id++){
		assign(&vertices[3*triangles[3*tri_id + 0]], &vert1[0]);
		assign(&vertices[3*triangles[3*tri_id + 1]], &vert2[0]);
		assign(&vertices[3*triangles[3*tri_id + 2]], &vert3[0]);

		substract(&vert2[0], &vert1[0], &side1[0]);
		substract(&vert3[0], &vert1[0], &side2[0]);

		cross(&side1[0], &side2[0], &normal[0]);

		if((normal[2] < 0.)&(optimize == 1))
			valid = false;
		else
			valid = true;

		if(valid){
			tri_extent[0] = fminf(vert1[0], vert2[0]);
			tri_extent[0] = fminf(vert3[0], tri_extent[0]);

			tri_extent[1] = fmaxf(vert1[0], vert2[0]);
			tri_extent[1] = fmaxf(vert3[0], tri_extent[1]);

			tri_extent[2] = fminf(vert1[1], vert2[1]);
			tri_extent[2] = fminf(vert3[1], tri_extent[2]);

			tri_extent[3] = fmaxf(vert1[1], vert2[1]);
			tri_extent[3] = fmaxf(vert3[1], tri_extent[3]);

			i_min = (int)((tri_extent[0] - map_extent[0])/dx) - 1;
			i_max = (int)((tri_extent[1] - map_extent[0])/dx) + 1;

			j_min = (int)((tri_extent[2] - map_extent[2])/dy) - 1;
			j_max = (int)((tri_extent[3] - map_extent[2])/dy) + 1;

			A = dot2(&side1[0], &side1[0]);
			B = dot2(&side1[0], &side2[0]);
			C = dot2(&side2[0], &side2[0]);

			h1 = vert1[2];
			h2 = vert2[2];
			h3 = vert3[2];

			if(vertex_wise == 1){
				q1 = quantity[triangles[3*tri_id + 0]];
				q2 = quantity[triangles[3*tri_id + 1]];
				q3 = quantity[triangles[3*tri_id + 2]];
			}
			else{
				q1 = quantity[3*tri_id + 0];
				q2 = quantity[3*tri_id + 1];
				q3 = quantity[3*tri_id + 2];
			}

			c = h1;
			b = ((h3 - c)*A - (h2 - c)*B)/(A*C - B*B);
			a = ((h2 - c) - B*b)/A;

			c_q = q1;
			b_q = ((q3 - c_q)*A - (q2 - c_q)*B)/(A*C - B*B);
			a_q = ((q2 - c_q) - B*b_q)/A;

			for(int i = max(i_min, 0); i < min(i_max, N); i++){
				for(int j = max(j_min, 0); j < min(j_max, M); j++){
					x = map_extent[0] + i*dx + dx/2.f;
					y = map_extent[2] + j*dy + dy/2.f;

					p[0] = x;
					p[1] = y;
					intriangle = PointInTriangle(&p[0], &vert1[0], &vert2[0], &vert3[0]);

					if(intriangle){
						p0[0] = p[0] - vert1[0];
						p0[1] = p[1] - vert1[1];

						h = dot2(&p0[0], &side1[0])*a   + dot2(&p0[0], &side2[0])*b   + c;
						q = dot2(&p0[0], &side1[0])*a_q + dot2(&p0[0], &side2[0])*b_q + c_q;

						// if(isnan(q)){
							// printf("h = %f, q = %f, tri_idx = %d, (c, b, a) = (%f, %f, %f), (cq, bq, aq) = (%f, %f, %f), ABC = (%f, %f, %f)\n", h, q, tri_id, c, b, a, c_q, b_q, a_q, A, B, C);
						// }

						if(h > shared_heightmap[map_indexing(i, j, thread_id, N, M, nthreads)]){
							shared_heightmap[map_indexing(i, j, thread_id, N, M, nthreads)] = h;
							shared_quantity[map_indexing(i,  j, thread_id, N, M, nthreads)] = q;
						}
					}
				}
			}
		}
	}

	#pragma omp barrier

	float max_per_pixel, value_at_max;
	if(nthreads > 1){

		#pragma omp for schedule(dynamic, 50)
		for(unsigned int i = 0; i < N; i++){
			for(unsigned int j = 0; j < M; j++){
				max_per_pixel = -1.0f;
				value_at_max  = -1.0f;
				for(unsigned int k = 0; k < nthreads; k++){
					if(shared_heightmap[map_indexing(i, j, k, N, M, nthreads)] > max_per_pixel){
						max_per_pixel = shared_heightmap[map_indexing(i, j, k, N, M, nthreads)];
						value_at_max  = shared_quantity[map_indexing(i, j, k, N, M, nthreads)];
					}
				}
				heightmap[i*M + j]    = max_per_pixel;
				quantity_map[i*M + j] = value_at_max;
			}
		}
		if(thread_id == 0){
			free(shared_heightmap);
			free(shared_quantity);
		}
	}
}

}


void determine_shadows(float *light_dir, float *result){
	unsigned int N = surface.N, M = surface.M;
	float x, y, h, x_min = surface.x_min, x_max = surface.x_max, y_min = surface.y_min, y_max = surface.y_max, dx = surface.dx, dy = surface.dy;
	float theta = acosf(light_dir[2]), max_height = surface.max_height, distance;
	int raymarching_N = 500;


	printf("To calculate Shadows\n");
	#pragma omp parallel num_threads(12)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float *position;
	position = (float *) malloc(3*sizeof(float));
	#pragma omp for schedule(dynamic, 5)
	for(unsigned int i = 0; i < N; i++){
		for(unsigned int j = 0; j < M; j++){
			position[0] = x_min + i*dx + dx/2.0f;
			position[1] = y_min + j*dy + dy/2.0f;

			position[2] = get_height(surface, position[0], position[1]) + 0.1f;

			distance = (max_height - position[2] + 1.0f)*sinf(theta)/light_dir[2];
			result[i*M + j] = check_occlusion(surface, &position[0], &light_dir[0], distance, raymarching_N);

		}
	}
	free(position);
	}
}

static inline bool trace_ray(struct height_map surface, float *position, float *direction, float distance, int raymarching_N, float *hit){
	float dx = distance/raymarching_N, r[3], z, dist, z_ant, r_ant, x, ds;
	bool occlusion = false;

	z_ant = get_height(surface, position[0], position[1]);

	for(unsigned int i = 1; i < raymarching_N; i++){
		dist = powf(dx*i/distance, 2)*distance;
		// dist = powf(dx*i/distance, 1)*distance;
		r[0] = position[0] + dist*direction[0];
		r[1] = position[1] + dist*direction[1];
		r[2] = position[2] + dist*direction[2];

		z =  get_height(surface, r[0], r[1]);

		if(z > r[2]){
			occlusion = true;
			ds        = dist - powf(dx*(i - 1)/distance, 2)*distance;
			x = (z_ant - r_ant)/(direction[2] - (z - z_ant)/ds);
			hit[0] = position[0] + (dist - ds + x)*direction[0];
			hit[1] = position[1] + (dist - ds + x)*direction[1];
			hit[2] = position[2] + (dist - ds + x)*direction[2];
			break;
		}
		if((r[2] > surface.max_height)&&(direction[2] >= 0.0f)){
			break;
		}
		z_ant = z;
		r_ant = r[2];
	}
	return occlusion;
}

void determine_normals(float *result){
	unsigned int N = surface.N, M = surface.M;
	float x, y, h, x_min = surface.x_min, x_max = surface.x_max, y_min = surface.y_min, y_max = surface.y_max, dx = surface.dx, dy = surface.dy;
	float dS = 0.01f;


	printf("To calculate Shadows\n");
	#pragma omp parallel num_threads(12)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float posX1[3], posX2[3], posY1[3], posY2[3], vec1[3], vec2[3], vec3[3], normal[3], norma;
	#pragma omp for schedule(dynamic, 5)
	for(unsigned int i = 0; i < N; i++){
		for(unsigned int j = 0; j < M; j++){
			posX2[0] = x_min + i*dx + dx/2.0f + dS;
			posX2[1] = y_min + j*dy + dy/2.0f;
			posX2[2] = get_height(surface, posX2[0], posX2[1]);

			posX1[0] = x_min + i*dx + dx/2.0f - dS;
			posX1[1] = y_min + j*dy + dy/2.0f;
			posX1[2] = get_height(surface, posX1[0], posX1[1]);

			posY2[0] = x_min + i*dx + dx/2.0f;
			posY2[1] = y_min + j*dy + dy/2.0f + dS;
			posY2[2] = get_height(surface, posY2[0], posY2[1]);

			posY1[0] = x_min + i*dx + dx/2.0f;
			posY1[1] = y_min + j*dy + dy/2.0f - dS;
			posY1[2] = get_height(surface, posY1[0], posY1[1]);

			substract(&posX2[0], &posX1[0], &vec1[0]);
			substract(&posY2[0], &posY1[0], &vec2[0]);

			cross(&vec1[0], &vec2[0], &vec3[0]);
			norma = norm(&vec3[0]);
			normal[0] = vec3[0]/norma;
			normal[1] = vec3[1]/norma;
			normal[2] = vec3[2]/norma;

			assign(&normal[0], &result[3*(i*M + j)]);
		}
	}
	}
}

static inline float Phong(float *light_dir, float *normal, float *view_dir, float smoothness){
	float normal_component = dot(&light_dir[0], &normal[0]), cosine, bounce_dir[3];

	bounce_dir[0] = -light_dir[0] + 2*normal_component*normal[0];
	bounce_dir[1] = -light_dir[1] + 2*normal_component*normal[1];
	bounce_dir[2] = -light_dir[2] + 2*normal_component*normal[2];

	cosine = dot(&bounce_dir[0], &view_dir[0]);
	if(cosine < 0.0f)
		cosine = 0.0f;

	return powf(cosine, 2.0f);
}

void calculate_indirect_lighting(float *shadows, float *normals, float *light_dir, float *result, unsigned int Nsamples, float max_dist){
	unsigned int N = surface.N, M = surface.M;
	float x, y, h, x_min = surface.x_min, x_max = surface.x_max, y_min = surface.y_min, y_max = surface.y_max, dx = surface.dx, dy = surface.dy;


	printf("To calculate Indirect lighting\n");
	#pragma omp parallel num_threads(12)
	{
	printf("To use %d threads\n", omp_get_num_threads());
	float position[3], normal[3], ray_direction[3], view_dir[3], U[3], V[3], light_sum, cosine, modulo, costheta, theta, phi, hit[3], sunshine;
	bool occlusion;
	#pragma omp for schedule(dynamic, 5)
	for(unsigned int i = 0; i < N; i++){
		printf("i = %d of %d\n", i, N);
		for(unsigned int j = 0; j < M; j++){
			position[0] = x_min + i*dx + dx/2.0f;
			position[1] = y_min + j*dy + dy/2.0f;
			position[2] = get_height(surface, position[0], position[1]);

			assign(&normals[3*(i*M + j)], &normal[0]);

			U[0] = 1.0f;
			U[1] = 0.0f;
			U[2] = 0.0f;

			cosine = dot(&U[0], &normal[0]);

			U[0] = U[0] - cosine*normal[0];
			U[1] = U[1] - cosine*normal[1];
			U[2] = U[2] - cosine*normal[2];

			modulo = norm(&U[0]);

			U[0] *= 1.f/modulo;
			U[1] *= 1.f/modulo;
			U[2] *= 1.f/modulo;

			cross(&normal[0], &U[0], &V[0]);

			light_sum = 0.0f;
			for(unsigned int k = 0; k < Nsamples; k++){
				costheta = random();
				phi      = random()*2*pi;

				theta = acosf(costheta);

				ray_direction[0] = cosf(phi)*sinf(theta)*U[0] + sinf(phi)*sinf(theta)*V[0] + cosf(theta)*normal[0];
				ray_direction[1] = cosf(phi)*sinf(theta)*U[1] + sinf(phi)*sinf(theta)*V[1] + cosf(theta)*normal[1];
				ray_direction[2] = cosf(phi)*sinf(theta)*U[2] + sinf(phi)*sinf(theta)*V[2] + cosf(theta)*normal[2];

				view_dir[0] = -ray_direction[0];
				view_dir[1] = -ray_direction[1];
				view_dir[2] = -ray_direction[2];

				occlusion = trace_ray(surface, &position[0], &ray_direction[0], max_dist, 50, &hit[0]);

				if(occlusion){
					sunshine = get_value(surface, &shadows[0], hit[0], hit[1])*0.04f;

					light_sum += sunshine/Nsamples;
				}
			}
			result[i*M + j] = light_sum;

		}
	}
	}
}


float turret_spotting_probability(float min_dist, float dist){
	fminf(powf(dist/min_dist, -2), 1.0f);

}

void determine_flat_surfaces(float *delta_height, float radius){
	unsigned int N = surface.N, M = surface.M, n, m;
	float x1, y1, x2, y2, h2, x_min = surface.x_min, x_max = surface.x_max, y_min = surface.y_min, y_max = surface.y_max, dx = surface.dx, dy = surface.dy, h_min, h_max;
	n = (int)(radius/dx) + 1;
	m = (int)(radius/dy) + 1;


	for(unsigned int i = 0; i < N; i++){

		for(unsigned int j = 0; j < M; j++){
			x1 = x_min + i*dx + dx/2.0f;
			y1 = y_min + j*dy + dy/2.0f;

			h_min =  100000.f;
			h_max = -100000.f;
			for(int k = i - n; k <= i + n; k++){
				for(int l = j - m; l <= j + m; l++){
					x2 = x_min + k*dx + dx/2.0f;
					y2 = y_min + l*dy + dy/2.0f;

					if((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) <= radius*radius ){
						h2 = get_height(surface, x2, y2);
						if(h2 > h_max)
							h_max = h2;
						if(h2 < h_min)
							h_min = h2;
					}
				}
			}

			delta_height[i*M + j] = h_max - h_min;
		}
	}
}
void calculate_surface_exposure(unsigned int Nsamples, unsigned int nsteps, float min_dist, float max_dist, float exp_score, float player_height, float *result, float *result_weighted, float *turret_probability, float *turret_mean_distance){
	unsigned int N = surface.N, M = surface.M;
	float x, y, h, x_min = surface.x_min, x_max = surface.x_max, y_min = surface.y_min, y_max = surface.y_max, dx = surface.dx, dy = surface.dy;
	unsigned int counter[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, total_calculated;
	printf("To calculate exposure to hits\n");
	printf("\n");
	#pragma omp parallel num_threads(12)
	{
	float position1[3], position2[3], ray_direction[3], exposure_sum, cosine, rho, phi, hit[3], score, distance, distance2D, norma, prob, aux, detection_probability, turret_norm, turret_weighted;
	bool occlusion;
	unsigned int tid, k;
	aux = powf(min_dist/max_dist, 2);
	#pragma omp for schedule(dynamic, 5)
	for(unsigned int i = 0; i < N; i++){

		for(unsigned int j = 0; j < M; j++){
			position1[0] = x_min + i*dx + dx/2.0f;
			position1[1] = y_min + j*dy + dy/2.0f;
			position1[2] = get_height(surface, position1[0], position1[1]) + player_height;

			exposure_sum = 0.0f;
			score        = 0.0f;
			norma        = 0.0f;
			turret_norm     = 0.0f;
			turret_weighted = 0.0f;
			k = 0;
			while(true){
				phi = random()*2*pi;
				rho = sqrtf(  random()*(1 - aux) + aux)*max_dist;

				position2[0] = position1[0] + rho*cosf(phi);
				position2[1] = position1[1] + rho*sinf(phi);

				if((position2[0] >= x_min)&&(position2[0] <= x_max)&&(position2[1] >= y_min)&&(position2[1] <= y_max)){
					position2[2] = get_height(surface, position2[0], position2[1]) + player_height;

					ray_direction[0] = position2[0] - position1[0];
					ray_direction[1] = position2[1] - position1[1];
					ray_direction[2] = position2[2] - position1[2];

					distance = norm(&ray_direction[0]);
					distance2D = sqrtf(ray_direction[0]*ray_direction[0] + ray_direction[1]*ray_direction[1]);
					normalize(&ray_direction[0], &ray_direction[0]);

					// occlusion = trace_ray(surface, &position1[0], &ray_direction[0], max_dist, nsteps, &hit[0]);
					occlusion = trace_ray(surface, &position1[0], &ray_direction[0], max_dist, (int)(distance*nsteps/max_dist + 1), &hit[0]);

					prob = expf(-position2[2]/40.f)*distance2D/distance;    /// Unlikely to shoot from high buildings and also vertically
					if(position2[2] <= 0.0f)
						prob = 0.f;
					norma += prob;

					if(occlusion == false){
						exposure_sum += prob*powf(distance/max_dist, -2.0f);
						score        += prob*powf(distance/max_dist, -2.0f + exp_score);

						detection_probability = prob*turret_spotting_probability(15.f, distance);
						// score        += expf(-position2[2]/50.f)*powf(distance/max_dist, 2.5f);
						turret_norm     += detection_probability;
						// turret_weighted += detection_probability*distance;
						// if(fabsf(distance - max_dist/2.f) <= max_dist/6.f)
						if(distance >= max_dist/2.f)
							turret_weighted += detection_probability;
					}

					k += 1;
					if(k == Nsamples)
						break;
				}
			}
			result[i*M + j] = exposure_sum/norma;
			result_weighted[i*M + j] = score/norma;

			turret_probability[i*M + j] = turret_norm;
			turret_mean_distance[i*M + j] = turret_weighted;
		}
		tid = omp_get_thread_num();
		counter[tid] += 1;
		if(tid == 0){
			total_calculated = 0;
			for(unsigned int q = 0; q < 12; q++)
				total_calculated += counter[q];

			// if(total_calculated%10 == 0)
				printf("done %d out of %d (%f)     \r", total_calculated, N, total_calculated*1.0f/N);
				fflush(stdout);
		}


	}
	}
}

static inline void interpolate_intriangle(float *A, float *B, float *C, int j, int k, unsigned int N, unsigned int dim, float *result){
	float sita = j*1.0f/N, eta = k*1.0f/N;

	result[0] = A[0] + sita*(B[0] - A[0]) + eta*(C[0] - B[0]);
	result[1] = A[1] + sita*(B[1] - A[1]) + eta*(C[1] - B[1]);

	if(dim >= 3)
		result[2] = A[2] + sita*(B[2] - A[2]) + eta*(C[2] - B[2]);

	if(dim >= 4)
		result[3] = A[3] + sita*(B[3] - A[3]) + eta*(C[3] - B[3]);
}

void estimate_subdivisions(float *vertices, int *triangles, unsigned int ntris, float cutoff, int *n_sub){
	float vert_A[3], vert_B[3], vert_C[3], side1[3], side2[3], side3[3], a, b, c, mean_D;
	int triangle[3];

	mean_D = 0.0f;
	for(unsigned int i = 0; i < ntris; i++){
		triangle[0] = triangles[3*i + 0];
		triangle[1] = triangles[3*i + 1];
		triangle[2] = triangles[3*i + 2];

		assign_dim(&vertices[3*triangle[0] + 0], &vert_A[0], 3);
		assign_dim(&vertices[3*triangle[1] + 0], &vert_B[0], 3);
		assign_dim(&vertices[3*triangle[2] + 0], &vert_C[0], 3);

		substract(&vert_B[0], &vert_A[0], &side1[0]);
		substract(&vert_C[0], &vert_B[0], &side2[0]);
		substract(&vert_A[0], &vert_C[0], &side3[0]);

		a = norm(&side1[0]);
		b = norm(&side2[0]);
		c = norm(&side3[0]);

		mean_D += (a + b + c)/3.0f/ntris;
		n_sub[i] = max((int)(fmaxf(fmaxf(a, b), c)/cutoff), 1);
	}
	n_sub[0] = mean_D;
}

void triangle_subdivision(float *vertices, float *uvs, float *colors, int *triangles, unsigned int ntris, int *new_N, float *new_vertices, float *new_uvs, float *new_colors, int *new_tris){
	unsigned int tri_offset = 0, vertex_offset = 0, tri_count;
	float vert_A[3], vert_B[3], vert_C[3], uv_A[2], uv_B[2], uv_C[2], color_A[4], color_B[4], color_C[4], a[3], b[3], c[3], d[3], uv_a[2], uv_b[2], uv_c[2], uv_d[2], color_a[4], color_b[4], color_c[4], color_d[4];

	int triangle[3], N;
	for(unsigned int i = 0; i < ntris; i++){
		triangle[0] = triangles[3*i + 0];
		triangle[1] = triangles[3*i + 1];
		triangle[2] = triangles[3*i + 2];

		assign_dim(&vertices[3*triangle[0] + 0], &vert_A[0], 3);
		assign_dim(&vertices[3*triangle[1] + 0], &vert_B[0], 3);
		assign_dim(&vertices[3*triangle[2] + 0], &vert_C[0], 3);

		assign_dim(&colors[4*triangle[0] + 0], &color_A[0], 4);
		assign_dim(&colors[4*triangle[1] + 0], &color_B[0], 4);
		assign_dim(&colors[4*triangle[2] + 0], &color_C[0], 4);

		assign_dim(&uvs[2*triangle[0] + 0], &uv_A[0], 2);
		assign_dim(&uvs[2*triangle[1] + 0], &uv_B[0], 2);
		assign_dim(&uvs[2*triangle[2] + 0], &uv_C[0], 2);

		N = new_N[i];
		tri_count = 0;
		for(unsigned int j = 1; j < N + 1; j++){
			for(unsigned int k = 0; k < j; k++){
				interpolate_intriangle(&vert_A[0], &vert_B[0], &vert_C[0], j - 1, k + 0, N, 3, &a[0]);
				interpolate_intriangle(&vert_A[0], &vert_B[0], &vert_C[0], j - 0, k + 0, N, 3, &b[0]);
				interpolate_intriangle(&vert_A[0], &vert_B[0], &vert_C[0], j - 0, k + 1, N, 3, &c[0]);
				interpolate_intriangle(&vert_A[0], &vert_B[0], &vert_C[0], j - 1, k + 1, N, 3, &d[0]);

				interpolate_intriangle(&color_A[0], &color_B[0], &color_C[0], j - 1, k + 0, N, 4, &color_a[0]);
				interpolate_intriangle(&color_A[0], &color_B[0], &color_C[0], j - 0, k + 0, N, 4, &color_b[0]);
				interpolate_intriangle(&color_A[0], &color_B[0], &color_C[0], j - 0, k + 1, N, 4, &color_c[0]);
				interpolate_intriangle(&color_A[0], &color_B[0], &color_C[0], j - 1, k + 1, N, 4, &color_d[0]);

				interpolate_intriangle(&uv_A[0], &uv_B[0], &uv_C[0], j - 1, k + 0, N, 4, &uv_a[0]);
				interpolate_intriangle(&uv_A[0], &uv_B[0], &uv_C[0], j - 0, k + 0, N, 4, &uv_b[0]);
				interpolate_intriangle(&uv_A[0], &uv_B[0], &uv_C[0], j - 0, k + 1, N, 4, &uv_c[0]);
				interpolate_intriangle(&uv_A[0], &uv_B[0], &uv_C[0], j - 1, k + 1, N, 4, &uv_d[0]);

				assign_dim(&a[0], &new_vertices[3*(vertex_offset + 0) + 0], 3);
				assign_dim(&b[0], &new_vertices[3*(vertex_offset + 1) + 0], 3);
				assign_dim(&c[0], &new_vertices[3*(vertex_offset + 2) + 0], 3);

				assign_dim(&color_a[0], &new_colors[4*(vertex_offset + 0) + 0], 4);
				assign_dim(&color_b[0], &new_colors[4*(vertex_offset + 1) + 0], 4);
				assign_dim(&color_c[0], &new_colors[4*(vertex_offset + 2) + 0], 4);

				assign_dim(&uv_a[0], &new_uvs[2*(vertex_offset + 0) + 0], 2);
				assign_dim(&uv_b[0], &new_uvs[2*(vertex_offset + 1) + 0], 2);
				assign_dim(&uv_c[0], &new_uvs[2*(vertex_offset + 2) + 0], 2);

				new_tris[3*tri_offset + 0] = vertex_offset + 0;
				new_tris[3*tri_offset + 1] = vertex_offset + 1;
				new_tris[3*tri_offset + 2] = vertex_offset + 2;

				tri_offset    += 1;
				vertex_offset += 3;
				tri_count     += 1;

				if(k != j - 1){
					assign_dim(&a[0], &new_vertices[3*(vertex_offset + 0) + 0], 3);
					assign_dim(&c[0], &new_vertices[3*(vertex_offset + 1) + 0], 3);
					assign_dim(&d[0], &new_vertices[3*(vertex_offset + 2) + 0], 3);

					assign_dim(&color_a[0], &new_colors[4*(vertex_offset + 0) + 0], 4);
					assign_dim(&color_c[0], &new_colors[4*(vertex_offset + 1) + 0], 4);
					assign_dim(&color_d[0], &new_colors[4*(vertex_offset + 2) + 0], 4);

					assign_dim(&uv_a[0], &new_uvs[2*(vertex_offset + 0) + 0], 2);
					assign_dim(&uv_c[0], &new_uvs[2*(vertex_offset + 1) + 0], 2);
					assign_dim(&uv_d[0], &new_uvs[2*(vertex_offset + 2) + 0], 2);

					new_tris[3*tri_offset + 0] = vertex_offset + 0;
					new_tris[3*tri_offset + 1] = vertex_offset + 1;
					new_tris[3*tri_offset + 2] = vertex_offset + 2;

					tri_offset    += 1;
					vertex_offset += 3;
					tri_count     += 1;

				}
			}
		}
		// printf("actual tris vs expected: (%d, %d)\n", tri_count, N*N);
	}
}


void get_quad_normal(float *vertices, int *quad_idxs, float *dir3){
	float v1[3], v2[3], v3[3], dir1[3], dir2[3];

	assign_dim(&vertices[3*quad_idxs[0] + 0], &v1[0], 3);
	assign_dim(&vertices[3*quad_idxs[1] + 0], &v2[0], 3);
	assign_dim(&vertices[3*quad_idxs[2] + 0], &v3[0], 3);

	substract(&v3[0], &v2[0], &dir1[0]);
	substract(&v1[0], &v2[0], &dir2[0]);

	cross(&dir1[0], &dir2[0], &dir3[0]);
	normalize(&dir3[0], &dir3[0]);
}


unsigned int DEBUG = 0;

void toggle_debug(){
	if(DEBUG == 0)
		DEBUG = 1;
	else
		DEBUG = 0;
}

static inline void check_if_quad_triangles(float *vertices, unsigned int *triangles, unsigned int tri_A, unsigned int tri_B, bool *result, int *quad_idxs){

	// This fails when triangles have repeated vertices or are highly acute, check check_triangles function
	float criterium = 0.001f, vert1[3], vert2[3], diff[3], discrepancy[3];
	int idx1, idx2, link[3] = {-1, -1, -1}, check_count = 0, idxB_sum = 0;
	*result = false;
	quad_idxs[0] = -1;
	quad_idxs[1] = -1;
	quad_idxs[2] = -1;
	quad_idxs[3] = -1;
	for(unsigned int i = 0; i < 3; i++){
		idx1 = triangles[3*tri_A + i];

		assign_dim(&vertices[3*idx1 + 0], &vert1[0], 3);
		for(unsigned int j = 0; j < 3; j++){
			idx2 = triangles[3*tri_B + j];

			assign_dim(&vertices[3*idx2 + 0], &vert2[0], 3);

			substract(&vert2[0], &vert1[0], &diff[0]);

			if(norm(&diff[0]) <= criterium){
				link[i] = j;
				check_count += 1;
				idxB_sum += idx2;
				break;
			}
		}
	}
	if(check_count == 2){
		float pointA[3], pointB[3], extreme1[3], extreme2[3], seg_A_to_1[3], mirror_extreme1[3];
		int idx3, idx4 = triangles[3*tri_B + 0] + triangles[3*tri_B + 1] + triangles[3*tri_B + 2] - idxB_sum, order;

		if(link[0] != -1){
			idx1 = triangles[3*tri_A + 0];
			if(link[1] != -1){
				idx2 = triangles[3*tri_A + 1];
				idx3 = triangles[3*tri_A + 2];

				order = -1;
			}
			else{
				idx2 = triangles[3*tri_A + 2];
				idx3 = triangles[3*tri_A + 1];

				order = 1;
			}
		}
		else{
			idx1 = triangles[3*tri_A + 1];
			idx2 = triangles[3*tri_A + 2];
			idx3 = triangles[3*tri_A + 0];

			order = -1;
		}

		assign_dim(&vertices[3*idx1 + 0], &pointA[0], 3);
		assign_dim(&vertices[3*idx2 + 0], &pointB[0], 3);

		assign_dim(&vertices[3*idx3 + 0], &extreme1[0], 3);
		assign_dim(&vertices[3*idx4 + 0], &extreme2[0], 3);

		substract(&extreme1[0], &pointA[0], &seg_A_to_1[0]);
		substract(&pointB[0], &seg_A_to_1[0], &mirror_extreme1[0]);

		substract(&extreme2[0], &mirror_extreme1[0], &discrepancy[0]);


		if(order == 1){
			quad_idxs[0] = idx1;
			quad_idxs[1] = idx3;
			quad_idxs[2] = idx2;
			quad_idxs[3] = idx4;
		}
		else{
			quad_idxs[0] = idx4;
			quad_idxs[1] = idx2;
			quad_idxs[2] = idx3;
			quad_idxs[3] = idx1;
		}
		// To rearrange indexes so that the first is the lower-right vertex.
		float v1[3], v2[3], v3[3], v4[3], normal[3], vec_up_orig[3] = {1.0f, 0.f, 1.0f}, vec_up[3], vec_right[3], baricenter[3], vec[3], s, t;
		unsigned int idx, new_indices[4];

		assign_dim(&vertices[3*quad_idxs[0] + 0], &v1[0], 3);
		assign_dim(&vertices[3*quad_idxs[1] + 0], &v2[0], 3);
		assign_dim(&vertices[3*quad_idxs[2] + 0], &v3[0], 3);
		assign_dim(&vertices[3*quad_idxs[3] + 0], &v4[0], 3);

		baricenter[0] = (v1[0] + v2[0] + v3[0] + v4[0])/4.f;
		baricenter[1] = (v1[1] + v2[1] + v3[1] + v4[1])/4.f;
		baricenter[2] = (v1[2] + v2[2] + v3[2] + v4[2])/4.f;

		get_quad_normal(vertices, quad_idxs, &normal[0]);
		orthogonalize(&normal[0], &vec_up_orig[0], &vec_up[0]);
		if(DEBUG)
			printf("check_if_quad_triangles-> dot = %f, norm = %f\n", dot(&normal[0], &vec_up[0]), norm(&vec_up[0]));
		cross(&normal[0], &vec_up[0], &vec_right[0]);

		for(idx = 0; idx < 4; idx++){
			substract(&vertices[3*quad_idxs[idx] + 0], &baricenter[0], &vec[0]);
			s = dot(&vec[0], &vec_right[0]);
			t = dot(&vec[0], &vec_up[0]);
			if((t <= 0.f)&(s >= 0.f)){
				// printf(" Found lower right, %f, %f, %d\n", s, t, idx);
				break;
			}
		}

		new_indices[0] = quad_idxs[(idx + 0)%4];
		new_indices[1] = quad_idxs[(idx + 1)%4];
		new_indices[2] = quad_idxs[(idx + 2)%4];
		new_indices[3] = quad_idxs[(idx + 3)%4];

		quad_idxs[0] = new_indices[0];
		quad_idxs[1] = new_indices[1];
		quad_idxs[2] = new_indices[2];
		quad_idxs[3] = new_indices[3];

		if(norm(&discrepancy[0]) <= criterium)
			*result = true;
	}
}


struct quad_array{
	unsigned int N_quads, *indexes;
	bool *active_quads;
	float *normals;
};

void defragment_quad_array(struct quad_array quads, unsigned int *N){
	unsigned int count = 0;
	float current_normal[3];
	unsigned int current_idxs[4];
	bool current_status;
	for(unsigned int i = 0; i < quads.N_quads; i++){
		current_status = quads.active_quads[i];
		assign_dim(&quads.normals[3*i + 0], &current_normal[0], 3);

		for(unsigned int j = 0; j < 4; j++) current_idxs[j] = quads.indexes[4*i + j];

		if(current_status){
			quads.active_quads[i] = false;

			assign_dim(&current_normal[0], &quads.normals[3*count + 0], 3);
			for(unsigned int j = 0; j < 4; j++) quads.indexes[4*count + j] = current_idxs[j];
			quads.active_quads[count] = true;

			count += 1;
		}
	}
	*N = count;
}

void add_quad(float *vertices, struct quad_array quads, int *quad_idxs, unsigned int idx){
	// Assuming vertical quads
	// float v1[3], v2[3], v3[3], v4[3], seg1[3], seg2[3];

	get_quad_normal(vertices, quad_idxs, &quads.normals[3*idx + 0]);
	quads.active_quads[idx] = true;
	for(unsigned int i = 0; i < 4; i++){
		quads.indexes[4*idx + i] = quad_idxs[i];
	}
}

void compare_quads(float *vertices, struct quad_array quads, unsigned int quad_A, unsigned int quad_B, bool *are_quads, int *quad_idxs){
	float vert1[3], vert2[3], normal1[3], normal2[3], cosine, angle, criterium = 0.001f, angle_criterium = 0.1f*pi/360.f, diff[3];
	int link[4] = {-1, -1, -1, -1}, idx1, idx2, check_count = 0;

	quad_idxs[0] = -1;
	quad_idxs[1] = -1;
	quad_idxs[2] = -1;
	quad_idxs[3] = -1;

	for(unsigned int i = 0; i < 4; i++){
		idx1 = quads.indexes[4*quad_A + i];

		assign_dim(&vertices[3*idx1 + 0], &vert1[0], 3);
		assign_dim(&quads.normals[3*quad_A + 0], &normal1[0], 3);

		for(unsigned int j = 0; j < 4; j++){
			idx2 = quads.indexes[4*quad_B + j];

			assign_dim(&vertices[3*idx2 + 0], &vert2[0], 3);
			assign_dim(&quads.normals[3*quad_B + 0], &normal2[0], 3);

			substract(&vert2[0], &vert1[0], &diff[0]);
			cosine  = dot(&normal1[0], &normal2[0]);
			angle   = acosf(fminf(fmaxf(cosine, -1.0f), 1.0f));
			if(DEBUG == 1)
				printf("vert1 = (%f, %f, %f), vert2 = (%f, %f, %f), dist = %f\n", vert1[0], vert1[1], vert1[2], vert2[0], vert2[1], vert2[2], norm(&diff[0]));
			if((norm(&diff[0]) <= criterium)&(angle < angle_criterium)){
				link[i] = j;
				check_count += 1;
				break;
			}
		}
	}
	if(DEBUG == 1){
		if(check_count != 0)
			printf("compare_quads -> Check count = %d, link = (%d, %d, %d, %d), ids1 = (%d, %d, %d, %d), ids2 = (%d, %d, %d, %d)\n", check_count, link[0], link[1], link[2], link[3], quads.indexes[4*quad_A + 0], quads.indexes[4*quad_A + 1], quads.indexes[4*quad_A + 2], quads.indexes[4*quad_A + 3], quads.indexes[4*quad_B + 0], quads.indexes[4*quad_B + 1], quads.indexes[4*quad_B + 2], quads.indexes[4*quad_B + 3]);
	}
	if(check_count == 2){

		*are_quads = true;
		if((link[0] != -1)&(link[1] != -1)){
			quad_idxs[0] = quads.indexes[4*quad_B + 0];
			quad_idxs[1] = quads.indexes[4*quad_B + 1];
			quad_idxs[2] = quads.indexes[4*quad_A + 2];
			quad_idxs[3] = quads.indexes[4*quad_A + 3];
		}
		else if((link[1] != -1)&(link[2] != -1)){
			quad_idxs[0] = quads.indexes[4*quad_A + 0];
			quad_idxs[1] = quads.indexes[4*quad_B + 1];
			quad_idxs[2] = quads.indexes[4*quad_B + 2];
			quad_idxs[3] = quads.indexes[4*quad_A + 3];
		}
		else if((link[2] != -1)&(link[3] != -1)){
			quad_idxs[0] = quads.indexes[4*quad_A + 0];
			quad_idxs[1] = quads.indexes[4*quad_A + 1];
			quad_idxs[2] = quads.indexes[4*quad_B + 2];
			quad_idxs[3] = quads.indexes[4*quad_B + 3];
		}
		else{
			quad_idxs[0] = quads.indexes[4*quad_B + 0];
			quad_idxs[1] = quads.indexes[4*quad_A + 1];
			quad_idxs[2] = quads.indexes[4*quad_A + 2];
			quad_idxs[3] = quads.indexes[4*quad_B + 3];
		}
	}
	else
		*are_quads = false;
}

void optimize_submesh(float *vertices, float *normals, float *colors, unsigned int *triangles, unsigned int ntriang, unsigned int *new_triangles, unsigned int *final_ntriang){
	bool are_quads, *triangle_mask;
	unsigned int tri_A, tri_B, count = 0, total_checked = 0;
	int quad_idxs[4];
	unsigned long int total_pairs = ntriang*(ntriang - 1)/2, pair_count = 0, i;
	float normal_quad[3], normal1[3], cosine;
	struct quad_array quads_identified;
	quads_identified.N_quads = ntriang/2;
	quads_identified.normals = (float *) malloc(3*quads_identified.N_quads*sizeof(float));
	quads_identified.indexes = (unsigned int *) malloc(4*quads_identified.N_quads*sizeof(unsigned int));
	quads_identified.active_quads = (bool *) malloc(quads_identified.N_quads*sizeof(bool));

	for(i = 0; i < quads_identified.N_quads; i++)
		quads_identified.active_quads[i] = false;


	triangle_mask = (bool *) malloc(ntriang*sizeof(bool));
	for(i = 0; i < ntriang; i++)
		triangle_mask[i] = true;

	for(tri_A = 0; tri_A < ntriang; tri_A++){
		for(tri_B = tri_A + 1; tri_B < ntriang; tri_B++){
			pair_count += 1;
			if(triangle_mask[tri_A]&triangle_mask[tri_B]){
				check_if_quad_triangles(vertices, triangles, tri_A, tri_B, &are_quads, &quad_idxs[0]);

				total_checked += 1;
				if(are_quads){
					triangle_mask[tri_A] = false;
					triangle_mask[tri_B] = false;

					get_quad_normal(vertices, &quad_idxs[0], &normal_quad[0]);

					add_quad(vertices, quads_identified, &quad_idxs[0], count);
					normal1[0] = normals[3*triangles[3*tri_A + 0] + 0];
					normal1[1] = normals[3*triangles[3*tri_A + 0] + 1];
					normal1[2] = normals[3*triangles[3*tri_A + 0] + 2];
					cosine = dot(&normal1[0], &normal_quad[0]);
					// if(fabsf(cosine - 1.0) >= 0.001f)
						// printf("Progress = %f, Normals dot = %f, count = %d, max = %d\n", (pair_count*1.0f)/(total_pairs), cosine, count, quads_identified.N_quads);
					count += 1;
				}
			}
		}
	}
	// printf("Percentage of pairs that are quads = %f, %f, Quad number = %d\n", count*100.0f/total_checked, count*200.0f/ntriang, count);
	unsigned int original_quad_count = count;
	quads_identified.N_quads = count;

	//Quads simplification

	// unsigned int quad_pairs = count*(count - 1)/2;
	//
	// pair_count = 0;
	// total_checked = 0;

	for(unsigned int itera = 0; itera < 10; itera++){
		count = 0;
		for(unsigned int quad_A = 0; quad_A < quads_identified.N_quads; quad_A++){
			for(unsigned int quad_B = quad_A + 1; quad_B < quads_identified.N_quads; quad_B++){
				// pair_count += 1;
				if(quads_identified.active_quads[quad_A]&quads_identified.active_quads[quad_B]){
					compare_quads(vertices, quads_identified, quad_A, quad_B, &are_quads, &quad_idxs[0]);

					// total_checked += 1;
					if(are_quads){
						quads_identified.active_quads[quad_B] = false;

						add_quad(vertices, quads_identified, &quad_idxs[0], quad_A);
						count += 1;
					}
				}
			}
		}
		// quads_identified.N_quads = count;
		defragment_quad_array(quads_identified, &quads_identified.N_quads);
		if(count == 0)
			break;
	}

	unsigned int tri_count = 0, triangle[3], quad_count = 0;
	for(i = 0; i < ntriang; i++){
		if(triangle_mask[i]){
			new_triangles[3*tri_count + 0] = triangles[3*i + 0];
			new_triangles[3*tri_count + 1] = triangles[3*i + 1];
			new_triangles[3*tri_count + 2] = triangles[3*i + 2];
			tri_count += 1;
		}
	}

	for(i = 0; i < quads_identified.N_quads; i++){
		new_triangles[3*tri_count + 6*i + 0] = quads_identified.indexes[4*i + 0];
		new_triangles[3*tri_count + 6*i + 1] = quads_identified.indexes[4*i + 1];
		new_triangles[3*tri_count + 6*i + 2] = quads_identified.indexes[4*i + 2];
		new_triangles[3*tri_count + 6*i + 3] = quads_identified.indexes[4*i + 0];
		new_triangles[3*tri_count + 6*i + 4] = quads_identified.indexes[4*i + 2];
		new_triangles[3*tri_count + 6*i + 5] = quads_identified.indexes[4*i + 3];
	}
	final_ntriang[0] = tri_count + 2*i;

	free(triangle_mask);
	free(quads_identified.normals);
	free(quads_identified.indexes);
	free(quads_identified.active_quads);
}

struct oct_tree{
	unsigned int depth;
	unsigned int ntriang;
	unsigned int **cells, **cell_count, **cell_total_count, **starting_idx;
	float *extent;
};

int power(int a, int b){
	int value = 1;
	if(b >= 0){
		for(unsigned int i = 0; i < b; i++)
			value *= a;
	}
	else{
		printf("Problem a = %d, b = %d!!\n", a, b);
		value = 1;
	}
	return value;
}
unsigned int error_count = 0;

void get_block_ids2(float *vec, float *extent, int depth, int *array){
	//Always  0 <= array < N
	float dx[3];
	unsigned int j, N = power(2, depth);

	for(j = 0; j < 3; j++){
		dx[j] = (extent[2*j + 1] - extent[2*j])/N;
		array[j] = (int)((vec[j] - extent[2*j])/dx[j]);
		array[j] = min(array[j], N - 1);
		array[j] = max(array[j], 0);
	}
}

void get_block_ids(float *vec, float *extent, int depth, int *array){
	//Always  0 <= array < N
	float dx[3];
	unsigned int ids[3], i, j, N = power(2, depth);

	for(i = 0; i < depth + 1; i++){
		for(j = 0; j < 3; j++){
			dx[j] = (extent[2*j + 1] - extent[2*j])/power(2, i);
			ids[j] = (int)((vec[j] - extent[2*j])/dx[j]);

			ids[j] = min(ids[j], N - 1);
			ids[j] = max(ids[j], 0);
		}

		array[i] = ids[2]*power(2, 2*i) + ids[1]*power(2, i) + ids[0];
	}
}

struct oct_tree tree;


void generate_oct_tree(float *vertices, unsigned int *triangles, unsigned int ntriang, unsigned int depth, float *extent){
	float vert1[3], vert2[3], vert3[3];
	unsigned int i, j, idx_locations1[depth + 1], idx_locations2[depth + 1], idx_locations3[depth + 1], level, offset, cell_count, cell_id;

	printf("Initializing tree, depth = %d, ntriang = %d\n", depth, ntriang);
	tree.ntriang          = ntriang;
	tree.depth            = depth;
	tree.cells            = (unsigned int **) malloc((depth + 1)*sizeof(unsigned int *));
	tree.cell_total_count = (unsigned int **) malloc((depth + 1)*sizeof(unsigned int *));
	tree.cell_count       = (unsigned int **) malloc((depth + 1)*sizeof(unsigned int *));
	tree.starting_idx     = (unsigned int **) malloc((depth + 1)*sizeof(unsigned int *));
	tree.extent           = (float *)         malloc(6*sizeof(float));

	for(i = 0; i < 6; i++)
		tree.extent[i] = extent[i];

	for(i = 0; i < depth + 1; i++){
		tree.cell_total_count[i] = (unsigned int *) malloc(power(2, 3*i)*sizeof(unsigned int));
		tree.cell_count[i]       = (unsigned int *) malloc(power(2, 3*i)*sizeof(unsigned int));
		tree.starting_idx[i]     = (unsigned int *) malloc(power(2, 3*i)*sizeof(unsigned int));

		for(j = 0; j < power(2, 3*i); j++){
			tree.cell_count[i][j]       = 0;
			tree.cell_total_count[i][j] = 0;
		}
	}

	printf("Counting triangles\n");

	for(i = 0; i < ntriang; i++){
		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		get_block_ids(&vert1[0], extent, depth, &idx_locations1[0]);
		get_block_ids(&vert2[0], extent, depth, &idx_locations2[0]);
		get_block_ids(&vert3[0], extent, depth, &idx_locations3[0]);

		if(error_count >= 1){
			printf("tri = %d\n", i);
			break;
		}

		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j]))
				break;
		}
		level = j - 1;

		// tree.cell_count[level][idx_locations1[level]] += 1;
		for(j = 0; j < level + 1; j++){
			tree.cell_total_count[j][idx_locations1[j]] += 1;
		}
	}


	printf("Determining starting index, cumulative sum, tot count = %d\n", tree.cell_total_count[0][0]);

	for(i = 0; i < depth + 1; i++){
		offset = 0;
		for(unsigned int idx = 0; idx < power(2, 3*i); idx++){
			cell_count = tree.cell_total_count[i][idx];

			tree.starting_idx[i][idx] = offset;
			offset += cell_count;
		}
		printf("offset = %d\n", offset);
		tree.cells[i] = (unsigned int *) malloc(offset*sizeof(unsigned int));
	}

	printf("To set cell listing of triangles\n");
	for(i = 0; i < ntriang; i++){
		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		get_block_ids(&vert1[0], extent, depth, &idx_locations1[0]);
		get_block_ids(&vert2[0], extent, depth, &idx_locations2[0]);
		get_block_ids(&vert3[0], extent, depth, &idx_locations3[0]);


		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j]))
				break;
		}
		level = j - 1;

		cell_id = idx_locations1[level];
		if(cell_id >= power(2, 3*level))
			printf("Error, idx too high, idx = %d, max = %d\n", cell_id, power(2, 3*level));

		offset = tree.cell_count[level][cell_id];

		tree.cells[level][tree.starting_idx[level][cell_id] + offset] = i;
		tree.cell_count[level][cell_id] += 1;

	}
}

void check_tree(unsigned int depth, unsigned int cell_id){
	printf("Starting point: %d, Total count = %d, cell count = %d\n", tree.starting_idx[depth][cell_id], tree.cell_total_count[depth][cell_id], tree.cell_count[depth][cell_id]);
}

void obtain_triangle_ids_fromcell(unsigned int depth, unsigned int cell_id, unsigned int *triangles, unsigned int max_triang, unsigned int *ntriang){
	*ntriang = min(max_triang, tree.cell_count[depth][cell_id]);
	for(unsigned int i = 0; i < (*ntriang); i++){
		triangles[i] = tree.cells[depth][tree.starting_idx[depth][cell_id] + i];
	}
}



void triangle_query(unsigned int base_idx, unsigned int depth, unsigned int ntriang_limit, unsigned int *triangle_idxs, unsigned int *depth_and_cell, unsigned int *ntriang){
	unsigned int count = 0, idx, id_x, id_y, id_z, id_x0, id_y0, id_z0, id_x00, id_y00, id_z00, N, N_r, estimate, total_count = 0, stop = 0, base;

	N_r = power(2, depth);
	id_z00 = base_idx/(N_r*N_r);
	id_y00 = (base_idx - id_z00*N_r*N_r)/N_r;
	id_x00 =  base_idx - id_z00*N_r*N_r - id_y00*N_r;

	for(unsigned int i = depth; i < tree.depth + 1; i++){
		N = power(2, i - depth);
		N_r = power(2, i);

		base = power(N, 3)*base_idx;

		id_z0 = id_z00*N;
		id_x0 = id_x00*N;
		id_y0 = id_y00*N;

		for(unsigned int k = 0; k < power(N, 3); k++){

			id_z = k/(N*N);
			id_y = (k - id_z*N*N)/N;
			id_x = k - id_z*N*N - id_y*N;

			idx = (id_z + id_z0)*N_r*N_r + (id_y + id_y0)*N_r + id_x + id_x0;

			// printf("i = %d, k = %d, idx = %d, id = (%d, %d, %d)\n", i, k, idx, id_x, id_y, id_z);
			if(idx >= power(2, 3*i))
				printf("ERROR, idx too high %d, vs %d\n", idx, power(2, 3*i));
			if(tree.cell_total_count[i][idx] != 0){
				total_count += tree.cell_count[i][idx];
				// printf("id = (%d, %d, %d), idx = %d out of %d, k = %d out of %d, count at cell = %d, offset = %d\n", id_x, id_y, id_z, idx, power(2, 3*i), k, power(N, 3), tree.cell_count[i][idx], tree.starting_idx[i][idx]);

				for(unsigned int j = 0; j < tree.cell_count[i][idx]; j++){
					triangle_idxs[count] = tree.cells[i][tree.starting_idx[i][idx] + j];
					depth_and_cell[2*count + 0] = i;
					depth_and_cell[2*count + 1] = idx;
					count += 1;

					if(count >= ntriang_limit)
						break;
				}

			}
			if(count >= ntriang_limit)
				break;

		}
		if(count >= ntriang_limit)
			break;

	}
	*ntriang = count;
}




static inline unsigned int check_isin_box(float *vector, float *extent){
	if((vector[0] >= extent[0])&(vector[0] <= extent[1])&(vector[1] >= extent[2])&(vector[1] <= extent[3])&(vector[2] >= extent[4])&(vector[2] <= extent[5]))
		return 1;
	else
		return 0;
}

static inline unsigned int check_isin_square(float *vec, unsigned int idx, float *extent){
	if       ((idx == 0)&(vec[1] >= extent[2])&(vec[1] < extent[3])&(vec[2] >= extent[4])&(vec[2] < extent[5]))
		return 1;
	else if  ((idx == 1)&(vec[0] >= extent[0])&(vec[0] < extent[1])&(vec[2] >= extent[4])&(vec[2] < extent[5]))
		return 1;
	else if  ((idx == 2)&(vec[0] >= extent[0])&(vec[0] < extent[1])&(vec[1] >= extent[2])&(vec[1] < extent[3]))
		return 1;
	else
		return 0;
}


unsigned int intersect_line_cube(float *start, float *end, float *extent, float *t_min_o, float *t_max_o){
	unsigned int intersects, start_in, end_in, isin;
	float vector[3], t_min, t_max, t, touch_point[3], t_intersect;

	substract(&end[0], &start[0], &vector[0]);

	intersects = 0;
	t_min = 1.0f;
	t_max = 0.0f;

	start_in = check_isin_box(start, extent);
	end_in   = check_isin_box(end,   extent);

	// printf("Start in = %d, end in = %d, start = (%f, %f, %f), end = (%f, %f, %f), extent = [%f, %f, %f, %f, %f, %f]\n", start_in, end_in, start[0], start[1], start[2], end[0], end[1], end[2], extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);
	if((start_in == 1)&(end_in == 1)){
		intersects = 1;
		t_min  = 0.f;
		t_max  = 1.0f;
	}
	else{
		for(unsigned int idx = 0; idx < 3; idx++){
			for(unsigned int side = 0; side < 2; side++){
				t = (extent[2*idx + side] - start[idx])/vector[idx];


				if((t >= 0.)&(t <= 1.)){
					touch_point[0] = start[0] + t*vector[0];
					touch_point[1] = start[1] + t*vector[1];
					touch_point[2] = start[2] + t*vector[2];

					// printf("t = %f, touch_point = (%f, %f, %f)\n", t, touch_point[0], touch_point[1], touch_point[2]);
					isin = check_isin_square(&touch_point[0], idx, extent);

					// printf("Comparison done, touch_point = (%f, %f, %f), isin = %d\n", touch_point[0], touch_point[1], touch_point[2], isin);

					if(isin)
						intersects = 1;

					if((isin == 1)&(t < t_min))
						t_min = t;
					if((isin == 1)&(t > t_max))
						t_max = t;
				}
			}
		}
		if((start_in == 1)|(end_in == 1)){
			t_intersect = t_min;
			// if(t_intersect != t_max)
				// print("WARNING", start_in, end_in, t_intersect, t_min, t_max)

			if(start_in == 1){
				intersects = 1;
				t_min  = 0.f;
				t_max  = t_intersect;
			}
			else{
				intersects = 1;
				t_min  = t_intersect;
				t_max  = 1.0f;
			}
		}
	}
	*t_min_o = t_min;
	*t_max_o = t_max;
	return intersects;
}

static inline void min_argsort(float *array, unsigned int N, unsigned int *output){
	unsigned int i, j, idx_minimum, aux_int, j_min = 0;
	float minimum = INFINITY, aux_float;
	// for(i = 0; i < N; i++)
		// output[i] = i;

	for(i = 0; i < N; i++){
		minimum = INFINITY;
		for(j = i; j < N; j++){
			if(array[j] < minimum){
				j_min       = j;
				minimum     = array[j];
				idx_minimum = output[j];
			}
		}
		aux_float = array[i];
		aux_int   = output[i];

		array[i] = minimum;
		output[i] = idx_minimum;

		array[j_min] = aux_float;
		output[j_min] = aux_int;
	}
}

void check_subcell_intersection(float *start, float *end, float *extent, unsigned int *arg_sorted, unsigned int *nsubcells, float *t_mins, float *t_maxs){
	unsigned int intersects[8], k, count = 0, idx[8], i, offset, local_pos[3*8], idx0, idy0, idz0;
	float u_min[8], u_max[8], t_max, DX[3], subcell_extent[6];

	DX[0] = (extent[1] - extent[0])/2.0f;
	DX[1] = (extent[3] - extent[2])/2.0f;
	DX[2] = (extent[5] - extent[4])/2.0f;

	for(i = 0; i < 8; i++){
		idz0 = i/4;
		idy0 = (i - 4*idz0)/2;
		idx0 = i - 4*idz0 - 2*idy0;
		local_pos[3*i + 0] = idx0;
		local_pos[3*i + 1] = idy0;
		local_pos[3*i + 2] = idz0;
	}

	for(i = 0; i < 8; i++){
		for(k = 0; k < 3; k++){
			subcell_extent[2*k + 0] = extent[2*k + 0] + DX[k]*(local_pos[3*i + k]);
			subcell_extent[2*k + 1] = extent[2*k + 0] + DX[k]*(local_pos[3*i + k] + 1);
		}
		intersects[i] = intersect_line_cube(start, end, &subcell_extent[0], &u_min[i], &u_max[i]);

		if(DEBUG == 1){
			printf("i = %d, intersects: %d, locpos = (%d, %d, %d), subextent = (", i, intersects[i], local_pos[3*i + 0], local_pos[3*i + 1], local_pos[3*i + 2]);

			for(k = 0; k < 6; k++)
				printf("%f ", subcell_extent[k]);
			printf("\n");


		}

		count += intersects[i];
	}
	float u_min_sort[count], u_max_sort[count];
	offset = 0;
	for(i = 0; i < 8; i++){
		if(intersects[i] == 1){
			u_min_sort[offset]  = u_min[i];
			u_max_sort[offset]  = u_max[i];
			arg_sorted[offset]  = i;
			offset += 1;
		}
	}


	min_argsort(&u_min_sort[0], count, &arg_sorted[0]);


	for(i = 0; i < count; i++){
		t_mins[i] = u_min_sort[i];    //Already sorted by min_argsort
		t_maxs[i] = u_max_sort[arg_sorted[i]];
	}
	*nsubcells = count;
}


static inline void trace_line_triangle(float *vertices, float *base, unsigned int *triangles, unsigned int tri_id, float *start_segment, float *end_segment, int *tri_hit, float *u){
	float vert1[3], normal[3], u0, vector[3], base1[3], base2[3], aux[3], vert2_base2[3], vert3_base2[3], start_base2[3], end_base2[3], touch_point[3], zeros[3] = {0.f, 0.f, 0.f};
	unsigned int nattributes = 6;

	// Optimize = true ignores rays that satisfy dot(tri_normal, ray) > 0.
	bool optimize = true;

	assign_dim(&base[3*(nattributes*tri_id + 0)],  &base1[0], 3);
	assign_dim(&base[3*(nattributes*tri_id + 1)],  &base2[0], 3);
	assign_dim(&base[3*(nattributes*tri_id + 2)], &normal[0], 3);
	assign_dim(&base[3*(nattributes*tri_id + 3)],  &vert1[0], 3);

	substract(&start_segment[0], &vert1[0], &aux[0]);
	start_base2[2] = dot(&aux[0], &normal[0]);

	if((start_base2[2] >= 0.f)|(!optimize)){
		start_base2[0] = dot(&aux[0], &base1[0]);
		start_base2[1] = dot(&aux[0], &base2[0]);

		substract(&end_segment[0], &vert1[0], &aux[0]);
		end_base2[0] = dot(&aux[0], &base1[0]);
		end_base2[1] = dot(&aux[0], &base2[0]);
		end_base2[2] = dot(&aux[0], &normal[0]);

		if(start_base2[2]*end_base2[2] >= 0.){
			*tri_hit = 0;
			*u       = -2.0f;
		}
		else{
			substract(&end_segment[0], &start_segment[0], &vector[0]);

			assign_dim(&base[3*(nattributes*tri_id + 4)], &vert2_base2[0], 3);
			assign_dim(&base[3*(nattributes*tri_id + 5)], &vert3_base2[0], 3);

			u0 = (0.f - start_base2[2])/(end_base2[2] - start_base2[2]);
			touch_point[0] = start_base2[0] + u0*(end_base2[0] - start_base2[0]);
			touch_point[1] = start_base2[1] + u0*(end_base2[1] - start_base2[1]);
			touch_point[2] = 0.f;

			*tri_hit = PointInTriangle(&touch_point[0], &zeros[0], &vert2_base2[0], &vert3_base2[0]);
			*u       = u0*norm(&vector[0]);
		}
	}
	else{
		*tri_hit = 0;
		*u       = -2.0f;
	}
}

unsigned int check_local_pos(int *local_pos){
	unsigned int good = 1;
	for(unsigned int i = 0; i < 3; i++){
		if((local_pos[i] < 0)|(local_pos[i] >= 2)){
			good = 0;
			break;
		}
	}
	return good;
}

void raytrace_within_cell(float *, float *, unsigned int *, unsigned int, unsigned int, float *, float *, float *, int *, float *);

void raytrace_within_cell(float *vertices, float *base, unsigned int *triangles, unsigned int depth, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length){
	float u, u_min = INFINITY, subcell_extent[6], DX[3], delta_t[3];    // In physical units

	unsigned int i, tri_id, tri_hit, subcell_id, local_pos[3], N_depth, N, id_x0, id_y0, id_z0;
	int current_hit = -1;
	*hit = -1;
	*length = -1.0f;

	if(tree.cell_total_count[depth][cell_id] != 0){
		DX[0] = (extent[1] - extent[0])/2.0f;
		DX[1] = (extent[3] - extent[2])/2.0f;
		DX[2] = (extent[5] - extent[4])/2.0f;

		N_depth = power(2, depth);
		id_z0 = cell_id/(N_depth*N_depth);
		id_y0 = (cell_id - id_z0*N_depth*N_depth)/N_depth;
		id_x0 =  cell_id - id_z0*N_depth*N_depth - id_y0*N_depth;

		id_z0 *= 2; id_y0 *= 2; id_x0 *= 2;

		N = N_depth*2;

		// Triangles within cell
		for(i = 0; i < tree.cell_count[depth][cell_id]; i++){
			tri_id = tree.cells[depth][tree.starting_idx[depth][cell_id] + i];

			trace_line_triangle(vertices, base, triangles, tri_id, start_segment, end_segment, &tri_hit, &u);
			if((tri_hit == 1)&(u < u_min)){
				u_min       = u;
				current_hit = tri_id;
				*length = u_min;
				*hit    = current_hit;
			}
		}
		if(depth < tree.depth){
			float t_mins[8], t_maxs[8];
			unsigned int count = 0, arg_sorted[8], idx;

			check_subcell_intersection(&start_segment[0], &end_segment[0], extent, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);

			if(DEBUG == 1){
				for(unsigned int j = 0; j < count; j++)
					printf("%d ", arg_sorted[j]);
				printf("\n");
			}
			for(unsigned int k = 0; k < count; k++){
				idx = arg_sorted[k];
				local_pos[2] = idx/4;
				local_pos[1] = (idx - 4*local_pos[2])/2;
				local_pos[0] = idx - 4*local_pos[2] - 2*local_pos[1];

				subcell_id = (id_z0 + local_pos[2])*N*N + (id_y0 + local_pos[1])*N + id_x0 + local_pos[0];

				for(unsigned int j = 0; j < 3; j++){
					subcell_extent[2*j + 0] = extent[2*j + 0] + DX[j]*(local_pos[j]);
					subcell_extent[2*j + 1] = extent[2*j + 0] + DX[j]*(local_pos[j] + 1);
				}

				raytrace_within_cell(vertices, base, triangles, depth + 1, subcell_id, &start_segment[0], &end_segment[0], &subcell_extent[0], &tri_hit, &u);

				if((tri_hit != -1)&(u < u_min)){
					u_min       = u;
					current_hit = tri_hit;
					*hit    = current_hit;
					*length = u_min;


					break;
				}
			}

		}
	}
}




void subdivide_cell_along_axis(float *extent, float *new_extent1, float *new_extent2){
	float dx, dy, dz, dmax;
	unsigned int i, done = 0;

	dx = extent[1] - extent[0];
	dy = extent[3] - extent[2];
	dz = extent[5] - extent[4];

	dmax = fmaxf(dx, fmaxf(dy, dz));

	for(i = 0; i < 6; i++){
		new_extent1[i] = extent[i];
		new_extent2[i] = extent[i];
	}

	if(dx == dmax){
		new_extent1[1] = extent[0] + (extent[1] - extent[0])*0.5f;
		new_extent2[0] = extent[0] + (extent[1] - extent[0])*0.5f;
		done = 1;
	}
	else if((dy == dmax)&(done == 0)){
		new_extent1[3] = extent[2] + (extent[3] - extent[2])*0.5f;
		new_extent2[2] = extent[2] + (extent[3] - extent[2])*0.5f;
		done = 1;
	}
	else if((dz == dmax)&(done == 0)){
		new_extent1[5] = extent[4] + (extent[5] - extent[4])*0.5f;
		new_extent2[4] = extent[4] + (extent[5] - extent[4])*0.5f;
		done = 1;
	}
}


const unsigned int npartition = 1;

void subdivide_cell(float *extent, float *new_extent){
	unsigned int nlist, division, offset, N_subcells = power(2, npartition);
	float new_extent1[6], new_extent2[6], list_extent1[6*N_subcells], list_extent2[6*N_subcells];

	nlist  = 1;
	assign_dim(&extent[0], &list_extent1[0], 6*nlist);
	for(unsigned int i = 0; i < npartition; i++){
		offset = 0;
		for(unsigned int j = 0; j < nlist; j++){

			subdivide_cell_along_axis(&list_extent1[6*j + 0], &new_extent1[0], &new_extent2[0]);

			assign_dim(&new_extent1[0], &list_extent2[6*offset + 0], 6);
			offset += 1;

			assign_dim(&new_extent2[0], &list_extent2[6*offset + 0], 6);
			offset += 1;
		}
		assign_dim(&list_extent2[0], &list_extent1[0], 6*offset);
		nlist = offset;
	}

	assign_dim(&list_extent1[0], &new_extent[0], 6*nlist);
}

int compare(const void *a, const void *b) {
    // Convert the void pointers to float pointers
    float fa = *(float *)a;
    float fb = *(float *)b;

    // Return comparison result
    if (fa < fb) return -1; // fa is smaller, so put it first
    if (fa > fb) return 1;  // fb is smaller, so put it first
    return 0; // fa == fb, so no change
}

void get_median(float *vertices, unsigned int *cell_triangles, unsigned int ncell_triangles, float *original_extent, float *extent1, float *extent2, unsigned int *cell_triangles1, unsigned int *cell_triangles2, unsigned int *ntris1, unsigned int *ntris2, unsigned int axis){
	unsigned int half_count = ncell_triangles/2, i, tri_idx, offset1 = 0, offset2 = 0;
	float *array, vert1[3], vert2[3], vert3[3], median, mean = 0.f, proposed_extent1[6], proposed_extent2[6], half = (original_extent[2*axis + 0] + original_extent[2*axis + 1])/2.f;

	for(i = 0; i < 6; i++){
		extent1[i] =  original_extent[i];
		extent2[i] =  original_extent[i];
	}
	float value = (original_extent[2*axis + 0] + original_extent[2*axis + 1])/2.f;
	extent1[2*axis + 1] = half;
	extent2[2*axis + 0] = half;

	if(ncell_triangles > 0){
		array = (float *) malloc(ncell_triangles*sizeof(float));

		for(i = 0; i < ncell_triangles; i++){
			assign_dim(&vertices[3*cell_triangles[3*i + 0] + 0], &vert1[0], 3);
			assign_dim(&vertices[3*cell_triangles[3*i + 1] + 0], &vert2[0], 3);
			assign_dim(&vertices[3*cell_triangles[3*i + 2] + 0], &vert3[0], 3);

			array[i] = fmaxf(fmaxf(vert1[axis], vert2[axis]), vert3[axis]);
			mean += array[i]/ncell_triangles;
		}

		qsort(array, ncell_triangles, sizeof(float), compare);
		median = array[half_count];

		// extent1[2*axis + 1] = median;
		// extent2[2*axis + 0] = median;

		for(i = 0; i < 3; i++){
			proposed_extent1[2*i + 0] =  100000;
			proposed_extent1[2*i + 1] =  -100000;
			proposed_extent2[2*i + 0] =  100000;
			proposed_extent2[2*i + 1] =  -100000;
		}



		for(i = 0; i < ncell_triangles; i++){
			assign_dim(&vertices[3*cell_triangles[3*i + 0] + 0], &vert1[0], 3);
			assign_dim(&vertices[3*cell_triangles[3*i + 1] + 0], &vert2[0], 3);
			assign_dim(&vertices[3*cell_triangles[3*i + 2] + 0], &vert3[0], 3);

			if((check_isin_box(&vert1[0], extent1) == 1)&(check_isin_box(&vert2[0], extent1) == 1)&(check_isin_box(&vert3[0], extent1) == 1)){
				proposed_extent1[0] = fminf(proposed_extent1[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
				proposed_extent1[1] = fmaxf(proposed_extent1[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
				proposed_extent1[2] = fminf(proposed_extent1[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
				proposed_extent1[3] = fmaxf(proposed_extent1[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
				proposed_extent1[4] = fminf(proposed_extent1[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
				proposed_extent1[5] = fmaxf(proposed_extent1[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));

				assert((check_isin_box(&vert1[0], &proposed_extent1[0]) == 1)&(check_isin_box(&vert2[0], &proposed_extent1[0]) == 1)&(check_isin_box(&vert3[0], &proposed_extent1[0]) == 1));
				cell_triangles1[3*offset1 + 0] = cell_triangles[3*i + 0];
				cell_triangles1[3*offset1 + 1] = cell_triangles[3*i + 1];
				cell_triangles1[3*offset1 + 2] = cell_triangles[3*i + 2];
				offset1 += 1;
			}
			else if((check_isin_box(&vert1[0], extent2) == 1)&(check_isin_box(&vert2[0], extent2) == 1)&(check_isin_box(&vert3[0], extent2) == 1)){
				proposed_extent2[0] = fminf(proposed_extent2[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
				proposed_extent2[1] = fmaxf(proposed_extent2[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
				proposed_extent2[2] = fminf(proposed_extent2[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
				proposed_extent2[3] = fmaxf(proposed_extent2[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
				proposed_extent2[4] = fminf(proposed_extent2[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
				proposed_extent2[5] = fmaxf(proposed_extent2[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));

				assert((check_isin_box(&vert1[0], &proposed_extent2[0]) == 1)&(check_isin_box(&vert2[0], &proposed_extent2[0]) == 1)&(check_isin_box(&vert3[0], &proposed_extent2[0]) == 1));
				cell_triangles2[3*offset2 + 0] = cell_triangles[3*i + 0];
				cell_triangles2[3*offset2 + 1] = cell_triangles[3*i + 1];
				cell_triangles2[3*offset2 + 2] = cell_triangles[3*i + 2];
				offset2 += 1;
			}

		}

		for(i = 0; i < 3; i++){
			if(offset1 != 0){
				// extent1[2*i + 0] = fmaxf(proposed_extent1[2*i + 0], extent1[2*i + 0]);
				// extent1[2*i + 1] = fminf(proposed_extent1[2*i + 1], extent1[2*i + 1]);
				extent1[2*i + 0] = proposed_extent1[2*i + 0];
				extent1[2*i + 1] = proposed_extent1[2*i + 1];


			}

			if(offset2 != 0){
				// extent2[2*i + 0] = fmaxf(proposed_extent2[2*i + 0], extent2[2*i + 0]);
				// extent2[2*i + 1] = fminf(proposed_extent2[2*i + 1], extent2[2*i + 1]);
				extent2[2*i + 0] = proposed_extent2[2*i + 0];
				extent2[2*i + 1] = proposed_extent2[2*i + 1];
			}

			// extent2[i] = proposed_extent2[i];
		}


		free(array);
	}
	else{
		*ntris1 = 0;
		*ntris2 = 0;

		// printf("No triangles, value = %f\n", value);
	}

	*ntris1 = offset1;
	*ntris2 = offset2;

}

void subdivide_box(float *vertices, unsigned int *triangles, unsigned int *cell_triangle_ids, unsigned int ncell_triangles, float *extent, float *extent1, float *extent2, float *extent3, unsigned int *cell_triangles1, unsigned int *cell_triangles2, unsigned int *cell_triangles3, unsigned int *ntris1, unsigned int *ntris2, unsigned int *ntris3, unsigned int axis){
	unsigned int i, tri_id, offset1 = 0, offset2 = 0, offset3 = 0;
	float vert1[3], vert2[3], vert3[3], proposed_extent1[6], proposed_extent2[6], proposed_extent3[6], half = (extent[2*axis + 0] + extent[2*axis + 1])/2.f;

	for(i = 0; i < 6; i++){
		extent1[i] =  extent[i];
		extent2[i] =  extent[i];
		extent3[i] =  extent[i];
	}
	extent1[2*axis + 1] = half;
	extent2[2*axis + 0] = half;

	if(ncell_triangles > 0){
		for(i = 0; i < 3; i++){
			proposed_extent1[2*i + 0] =  100000;
			proposed_extent1[2*i + 1] =  -100000;
			proposed_extent2[2*i + 0] =  100000;
			proposed_extent2[2*i + 1] =  -100000;
			proposed_extent3[2*i + 0] =  100000;
			proposed_extent3[2*i + 1] =  -100000;
		}

		for(i = 0; i < ncell_triangles; i++){
			tri_id = cell_triangle_ids[i];
			assign_dim(&vertices[3*triangles[3*tri_id + 0] + 0], &vert1[0], 3);
			assign_dim(&vertices[3*triangles[3*tri_id + 1] + 0], &vert2[0], 3);
			assign_dim(&vertices[3*triangles[3*tri_id + 2] + 0], &vert3[0], 3);

			if((check_isin_box(&vert1[0], extent1) == 1)&(check_isin_box(&vert2[0], extent1) == 1)&(check_isin_box(&vert3[0], extent1) == 1)){
				proposed_extent1[0] = fminf(proposed_extent1[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
				proposed_extent1[1] = fmaxf(proposed_extent1[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
				proposed_extent1[2] = fminf(proposed_extent1[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
				proposed_extent1[3] = fmaxf(proposed_extent1[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
				proposed_extent1[4] = fminf(proposed_extent1[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
				proposed_extent1[5] = fmaxf(proposed_extent1[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));

				assert((check_isin_box(&vert1[0], &proposed_extent1[0]) == 1)&(check_isin_box(&vert2[0], &proposed_extent1[0]) == 1)&(check_isin_box(&vert3[0], &proposed_extent1[0]) == 1));
				cell_triangles1[offset1] = tri_id;
				offset1 += 1;
			}
			else if((check_isin_box(&vert1[0], extent2) == 1)&(check_isin_box(&vert2[0], extent2) == 1)&(check_isin_box(&vert3[0], extent2) == 1)){
				proposed_extent2[0] = fminf(proposed_extent2[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
				proposed_extent2[1] = fmaxf(proposed_extent2[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
				proposed_extent2[2] = fminf(proposed_extent2[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
				proposed_extent2[3] = fmaxf(proposed_extent2[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
				proposed_extent2[4] = fminf(proposed_extent2[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
				proposed_extent2[5] = fmaxf(proposed_extent2[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));

				assert((check_isin_box(&vert1[0], &proposed_extent2[0]) == 1)&(check_isin_box(&vert2[0], &proposed_extent2[0]) == 1)&(check_isin_box(&vert3[0], &proposed_extent2[0]) == 1));
				cell_triangles2[offset2] = tri_id;
				offset2 += 1;
			}
			else{
				proposed_extent3[0] = fminf(proposed_extent3[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
				proposed_extent3[1] = fmaxf(proposed_extent3[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
				proposed_extent3[2] = fminf(proposed_extent3[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
				proposed_extent3[3] = fmaxf(proposed_extent3[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
				proposed_extent3[4] = fminf(proposed_extent3[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
				proposed_extent3[5] = fmaxf(proposed_extent3[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));

				assert((check_isin_box(&vert1[0], extent) == 1)&(check_isin_box(&vert2[0], extent) == 1)&(check_isin_box(&vert3[0], extent) == 1));
				cell_triangles3[offset3] = tri_id;
				offset3 += 1;
			}

		}

		for(i = 0; i < 3; i++){
			if(offset1 != 0){
				extent1[2*i + 0] = proposed_extent1[2*i + 0];
				extent1[2*i + 1] = proposed_extent1[2*i + 1];
			}
			if(offset2 != 0){
				extent2[2*i + 0] = proposed_extent2[2*i + 0];
				extent2[2*i + 1] = proposed_extent2[2*i + 1];
			}
			if(offset3 != 0){
				extent3[2*i + 0] = proposed_extent3[2*i + 0];
				extent3[2*i + 1] = proposed_extent3[2*i + 1];
			}
		}
	}

	*ntris1 = offset1;
	*ntris2 = offset2;
	*ntris3 = offset3;
}

struct general_tree{
	unsigned int max_depth, N_subcells;
	unsigned int ntriang;
	unsigned int *cells, *cell_count, *cell_total_count, *starting_idx, *depth_offset;
	float *extent, *cell_extent;
};
struct general_tree gen_tree;

struct BVH{
	unsigned int max_depth, max_per_cell, max_nodes, ntriang;
	unsigned int *cell_triangles, *cell_tri_count, *triangle_offset, *children_offset, *children, *children_count;
	float *extent, *cell_extent;
};
struct BVH bvh;

void free_bvh(){
	free(bvh.cell_triangles);
	free(bvh.cell_tri_count);
	free(bvh.triangle_offset);
	free(bvh.children_offset);
	free(bvh.children);
	free(bvh.children_count);
	free(bvh.extent);
	free(bvh.cell_extent);
}

void check_bvh(unsigned int cell_id, unsigned int *result, float *extent){
	result[0] = bvh.cell_tri_count[cell_id];
	result[1] = bvh.triangle_offset[cell_id];
	result[2] = bvh.children_count[cell_id];
	result[3] = bvh.children_offset[cell_id];
	result[4] = bvh.children[cell_id];

	assign_dim(&bvh.cell_extent[6*cell_id + 0], extent, 6);
}


static inline unsigned int geometric_sum(unsigned int nsubcells, unsigned int i){
	return (power(nsubcells, i + 1) - 1)/(nsubcells - 1);
}

static inline unsigned int get_idx_cell(unsigned int depth, unsigned int cell_id){
	return gen_tree.depth_offset[depth] + cell_id;
}

/*
static inline unsigned int bvh_get_idx(unsigned int depth, unsigned int cell_id){
	return bvh.depth_offset[depth] + cell_id;
}*/



void subdivide_cell_along_axis_median(float *vertices, unsigned int *cell_triangles, unsigned int ncell_triangles, float *extent, float *new_extent1, float *new_extent2, unsigned int *cell_triangles1, unsigned int *cell_triangles2, unsigned int *size1, unsigned int *size2){
	float dx, dy, dz, dmax;
	unsigned int i, done = 0;

	dx = extent[1] - extent[0];
	dy = extent[3] - extent[2];
	dz = extent[5] - extent[4];

	dmax = fmaxf(dx, fmaxf(dy, dz));

	if(dx == dmax){
		get_median(vertices, cell_triangles, ncell_triangles, extent, new_extent1, new_extent2, cell_triangles1, cell_triangles2, size1, size2, 0);
		done = 1;
	}
	else if((dy == dmax)&(done == 0)){
		get_median(vertices, cell_triangles, ncell_triangles, extent, new_extent1, new_extent2, cell_triangles1, cell_triangles2, size1, size2, 1);
		done = 1;
	}
	else if((dz == dmax)&(done == 0)){
		get_median(vertices, cell_triangles, ncell_triangles, extent, new_extent1, new_extent2, cell_triangles1, cell_triangles2, size1, size2, 2);
		done = 1;
	}
}

void subdivide_recursively(float *vertices, unsigned int *cell_triangles, unsigned int ncell, unsigned int depth, unsigned int cell_id, float *extent){
	float extent1[6], extent2[6];
	unsigned int *cell_triangles1, size1, size2, *cell_triangles2;

	// printf("Function called, depth: %d, cell_id  %d, ntris = %d, current extent = (%f, %f, %f, %f, %f %f)\n", depth, cell_id, ncell, extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);

	assign_dim(extent, &gen_tree.cell_extent[6*get_idx_cell(depth, cell_id) + 0], 6);

	if(depth < gen_tree.max_depth){
		// printf("To go deeper...\n");
		cell_triangles1 = (unsigned int *) malloc(3*ncell*sizeof(unsigned int));
		cell_triangles2 = (unsigned int *) malloc(3*ncell*sizeof(unsigned int));

		subdivide_cell_along_axis_median(vertices, cell_triangles, ncell, extent, &extent1[0], &extent2[0], cell_triangles1, cell_triangles2, &size1, &size2);

		subdivide_recursively(vertices, cell_triangles1, size1, depth + 1, 2*cell_id + 0, &extent1[0]);
		subdivide_recursively(vertices, cell_triangles2, size2, depth + 1, 2*cell_id + 1, &extent2[0]);

		free(cell_triangles1);
		free(cell_triangles2);
	}
}

unsigned int ERROR_COUNT = 0;
void locate_vertex(float *vertex, unsigned int depth, int *coords){
	unsigned int N_subcells = power(2, npartition), isin, j_ant = 0;
	coords[0] = 0;
	if(!(check_isin_box(vertex, &gen_tree.cell_extent[6*get_idx_cell(0, 0) + 0]) == 1)){
		printf("vertex = (%f, %f, %f), extent = [%f, %f, %f, %f, %f, %f]\n", vertex[0], vertex[1], vertex[2], gen_tree.cell_extent[0], gen_tree.cell_extent[1], gen_tree.cell_extent[2], gen_tree.cell_extent[3], gen_tree.cell_extent[4], gen_tree.cell_extent[5]);
	}
	assert(check_isin_box(vertex, &gen_tree.cell_extent[6*get_idx_cell(0, 0) + 0]) == 1);
	for(unsigned int i = 1; i < depth + 1; i++){
		// N = power(2, 3*i);
		for(unsigned int j = N_subcells*j_ant; j < N_subcells*(j_ant + 1); j++){
			isin = check_isin_box(vertex, &gen_tree.cell_extent[6*get_idx_cell(i, j) + 0]);

			if(isin == 1){
				coords[i] = j;
				j_ant     = j;
				break;
			}
			else{
				coords[i] = -1;
			}
		}
		if(false){
			printf("isin = %d, i = %d, j_ant = %d, coords[i] = %d\n", isin, i, j_ant, coords[i]);
			ERROR_COUNT += 1;

			if(ERROR_COUNT > 10)
				exit(1);
		}
	}
}


void free_gen_tree(){
	free(gen_tree.depth_offset);
	free(gen_tree.cells);
	free(gen_tree.cell_count);
	free(gen_tree.cell_total_count);
	free(gen_tree.starting_idx);
	free(gen_tree.extent);
	free(gen_tree.cell_extent);
}

void fill_in_subcell(unsigned int depth, unsigned int subcell_id, unsigned int *offset_out);

void fill_in_subcell(unsigned int depth, unsigned int cell_id, unsigned int *offset_out){
	unsigned int offset = *offset_out, ncurrent_cell, nsubcells = power(2, npartition), subcell_id;

	ncurrent_cell = gen_tree.cell_count[get_idx_cell(depth, cell_id)];

	gen_tree.starting_idx[get_idx_cell(depth, cell_id)] = offset;
	offset += ncurrent_cell;

	if(depth < gen_tree.max_depth){
		for(unsigned int i = 0; i < nsubcells; i++){
			subcell_id = cell_id*nsubcells + i;

			fill_in_subcell(depth + 1, subcell_id, &offset);
		}
	}
	*offset_out = offset;
}
float calculate_extents_volume(unsigned int depth){
	unsigned int nsubcells = power(2, npartition);
	float volume = 0.f, extent[6];
	for(unsigned int j = 0; j < power(nsubcells, depth); j++){
		assign_dim(&gen_tree.cell_extent[6*get_idx_cell(depth, j) + 0], &extent[0], 6);

		volume += (extent[1] - extent[0])*(extent[3] - extent[2])*(extent[5] - extent[4]);
	}
	// printf("Depth: %d, Equivalent size: %f\n", depth, powf(volume, 1/3.0f));
	return volume;
}
void recursive_search(unsigned int depth, unsigned int cell_id, unsigned int *result, unsigned int *offset_out);

void reduce_extent(float *vertices, unsigned int *triangles){
	unsigned int nsubcells = power(2, npartition), *triangles_ids, ntris, tri_idx;
	float proposed_extent[6], extent[6], vert1[3], vert2[3], vert3[3];

	printf("To malloc %d triangles\n", gen_tree.ntriang);
	triangles_ids = (unsigned int *) malloc(gen_tree.ntriang*sizeof(unsigned int));

	for(unsigned int depth = 0; depth <= gen_tree.max_depth; depth++){
		for(unsigned int cell_id = 0; cell_id < power(nsubcells, depth); cell_id++){
			ntris = 0;
			recursive_search(depth, cell_id, triangles_ids, &ntris);
			assign_dim(&gen_tree.cell_extent[6*get_idx_cell(depth, cell_id) + 0], &extent[0], 6);

			if(ntris != 0){

				proposed_extent[0] =  1e8;
				proposed_extent[1] = -1e8;
				proposed_extent[2] =  1e8;
				proposed_extent[3] = -1e8;
				proposed_extent[4] =  1e8;
				proposed_extent[5] = -1e8;
				for(unsigned int i = 0; i < ntris; i++){
					tri_idx = triangles_ids[i];
					assign_dim(&vertices[3*triangles[3*tri_idx + 0] + 0], &vert1[0], 3);
					assign_dim(&vertices[3*triangles[3*tri_idx + 1] + 0], &vert2[0], 3);
					assign_dim(&vertices[3*triangles[3*tri_idx + 2] + 0], &vert3[0], 3);

					if((check_isin_box(&vert1[0], extent) == 1)&(check_isin_box(&vert2[0], extent) == 1)&(check_isin_box(&vert3[0], extent) == 1)){
						proposed_extent[0] = fminf(proposed_extent[0], fminf(vert1[0], fminf(vert2[0], vert3[0])));
						proposed_extent[1] = fmaxf(proposed_extent[1], fmaxf(vert1[0], fmaxf(vert2[0], vert3[0])));
						proposed_extent[2] = fminf(proposed_extent[2], fminf(vert1[1], fminf(vert2[1], vert3[1])));
						proposed_extent[3] = fmaxf(proposed_extent[3], fmaxf(vert1[1], fmaxf(vert2[1], vert3[1])));
						proposed_extent[4] = fminf(proposed_extent[4], fminf(vert1[2], fminf(vert2[2], vert3[2])));
						proposed_extent[5] = fmaxf(proposed_extent[5], fmaxf(vert1[2], fmaxf(vert2[2], vert3[2])));
					}
				}
			}
			else{
				proposed_extent[0] = (extent[1] + extent[0])*0.5f;
				proposed_extent[1] = (extent[1] + extent[0])*0.5f;
				proposed_extent[2] = (extent[3] + extent[2])*0.5f;
				proposed_extent[3] = (extent[3] + extent[2])*0.5f;
				proposed_extent[4] = (extent[5] + extent[4])*0.5f;
				proposed_extent[5] = (extent[5] + extent[4])*0.5f;
			}
			assign_dim(&proposed_extent[0], &gen_tree.cell_extent[6*get_idx_cell(depth, cell_id) + 0], 6);
		}
	}
	free(triangles_ids);
}

void generate_general_tree(float *vertices, unsigned int *triangles, unsigned int ntriang, unsigned int depth, float *extent, unsigned int optimized){
	float vert1[3], vert2[3], vert3[3];
	unsigned int i, j, level, offset, cell_count, cell_id, nsubcells = power(2, npartition), level_contiguous = 0;
	int idx_locations1[depth + 1], idx_locations2[depth + 1], idx_locations3[depth + 1];

	printf("Initializing tree, depth = %d, ntriang = %d\n", depth, ntriang);
	gen_tree.ntriang          = ntriang;
	gen_tree.max_depth        = depth;

	gen_tree.cells            = (unsigned int *) malloc(ntriang*sizeof(unsigned int));
	gen_tree.cell_total_count = (unsigned int *) malloc(geometric_sum(nsubcells, depth)*sizeof(unsigned int));
	gen_tree.cell_count       = (unsigned int *) malloc(geometric_sum(nsubcells, depth)*sizeof(unsigned int));
	gen_tree.starting_idx     = (unsigned int *) malloc(geometric_sum(nsubcells, depth)*sizeof(unsigned int));
	gen_tree.depth_offset     = (unsigned int *) malloc((depth + 1)*sizeof(unsigned int));

	gen_tree.cell_extent      = (float *)        malloc(geometric_sum(nsubcells, depth)*6*sizeof(float));
	gen_tree.extent           = (float *)        malloc(6*sizeof(float));

	for(i = 0; i < 6; i++)
		gen_tree.extent[i] = extent[i];

	for(i = 0; i < depth + 1; i++){
		offset = geometric_sum(nsubcells, i - 1);
		gen_tree.depth_offset[i] = offset;

		for(j = 0; j < power(nsubcells, i); j++){
			gen_tree.cell_count[offset + j]       = 0;
			gen_tree.cell_total_count[offset + j] = 0;
		}
	}



	if(optimized == 0){
		assign_dim(&extent[0], &gen_tree.cell_extent[0], 6);
		for(i = 0; i < depth; i++){
			offset = gen_tree.depth_offset[i];
			for(j = 0; j < power(nsubcells, i); j++){

				subdivide_cell(&gen_tree.cell_extent[6*(offset + j)  + 0], &gen_tree.cell_extent[6*(offset + power(nsubcells, i) + nsubcells*j) + 0]);
			}
		}
	}
	else
		subdivide_recursively(vertices, triangles, ntriang, 0, 0, &extent[0]);

	offset = gen_tree.depth_offset[depth];
	float cell_size_x = gen_tree.cell_extent[6*(offset + 0)  + 1] - gen_tree.cell_extent[6*(offset + 0)  + 0],
	cell_size_y = gen_tree.cell_extent[6*(offset + 0)  + 3] - gen_tree.cell_extent[6*(offset + 0)  + 2],
	cell_size_z = gen_tree.cell_extent[6*(offset + 0)  + 5] - gen_tree.cell_extent[6*(offset + 0)  + 4];
	printf("Minimum cell size = (%f, %f, %f)\n", cell_size_x, cell_size_y, cell_size_z);


	printf("Counting triangles\n");

	for(i = 0; i < ntriang; i++){
		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		locate_vertex(&vert1[0], depth, &idx_locations1[0]);
		locate_vertex(&vert2[0], depth, &idx_locations2[0]);
		locate_vertex(&vert3[0], depth, &idx_locations3[0]);

		if(error_count >= 1){
			printf("tri = %d\n", i);
			break;
		}

		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j])|(idx_locations1[j] == -1)|(idx_locations2[j] == -1)|(idx_locations3[j] == -1))
				break;
		}
		level = j - 1;

		// Count triangle at level
		gen_tree.cell_count[get_idx_cell(level, idx_locations1[level])] += 1;

		// Count triangle at all higher levels
		for(j = 0; j < level + 1; j++){
			gen_tree.cell_total_count[get_idx_cell(j, idx_locations1[j])] += 1;
		}


	}


	gen_tree.cells = (unsigned int *) malloc(ntriang*sizeof(unsigned int));

	printf("Determining starting index, cumulative sum, tot count = %d\n", gen_tree.cell_total_count[0]);

	offset = 0;
	if(level_contiguous == 1){
		// Level-contiguous
		for(i = 0; i < depth + 1; i++){
			for(unsigned int idx = 0; idx < power(nsubcells, i); idx++){

				cell_count = gen_tree.cell_count[get_idx_cell(i, idx)];

				gen_tree.starting_idx[get_idx_cell(i, idx)] = offset;
				offset += cell_count;
			}
		}
	}
	else{
		// Cell-contiguous
		fill_in_subcell(0, 0, &offset);
	}
	printf("offset = %d\n", offset);


	// Reset cell count
	for(i = 0; i < geometric_sum(nsubcells, depth); i++)
		gen_tree.cell_count[i] = 0;

	printf("To set cell listing of triangles\n");
	for(i = 0; i < ntriang; i++){
		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);


		locate_vertex(&vert1[0], depth, &idx_locations1[0]);
		locate_vertex(&vert2[0], depth, &idx_locations2[0]);
		locate_vertex(&vert3[0], depth, &idx_locations3[0]);


		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j])|(idx_locations1[j] == -1)|(idx_locations2[j] == -1)|(idx_locations3[j] == -1))
				break;
		}
		level = j - 1;

		cell_id = idx_locations1[level];
		if(cell_id >= power(nsubcells, level))
			printf("Error, idx too high, idx = %d, max = %d\n", cell_id, power(nsubcells, level));

		offset = gen_tree.cell_count[get_idx_cell(level, cell_id)];

		gen_tree.cells[gen_tree.starting_idx[get_idx_cell(level, cell_id)] + offset] = i;
		gen_tree.cell_count[get_idx_cell(level, cell_id)] += 1;

	}

	reduce_extent(vertices, triangles);
	for(i = 0; i <= depth; i++){
		printf("Volume fraction = %f\n", calculate_extents_volume(i)/calculate_extents_volume(0));
	}

}

void subdivide_box_along_axis(float *vertices, unsigned int *triangles, unsigned int *cell_triangle_ids, unsigned int ncell_triangles, float *extent, float *new_extent1, float *new_extent2, float *new_extent3, unsigned int *cell_triangles1, unsigned int *cell_triangles2, unsigned int *cell_triangles3, unsigned int *size1, unsigned int *size2, unsigned int *size3){
	float dx, dy, dz, dmax;
	unsigned int i, done = 0;

	dx = extent[1] - extent[0];
	dy = extent[3] - extent[2];
	dz = extent[5] - extent[4];

	dmax = fmaxf(dx, fmaxf(dy, dz));

	if(dx == dmax){
		subdivide_box(vertices, triangles, cell_triangle_ids, ncell_triangles, extent, new_extent1, new_extent2, new_extent3, cell_triangles1, cell_triangles2, cell_triangles3, size1, size2, size3, 0);
		done = 1;
	}
	else if((dy == dmax)&(done == 0)){
		subdivide_box(vertices, triangles, cell_triangle_ids, ncell_triangles, extent, new_extent1, new_extent2, new_extent3, cell_triangles1, cell_triangles2, cell_triangles3, size1, size2, size3, 1);
		done = 1;
	}
	else if((dz == dmax)&(done == 0)){
		subdivide_box(vertices, triangles, cell_triangle_ids, ncell_triangles, extent, new_extent1, new_extent2, new_extent3, cell_triangles1, cell_triangles2, cell_triangles3, size1, size2, size3, 2);
		done = 1;
	}
}

bool compare_extents(float *extent1, float *extent2){
	bool equal = true;

	for(unsigned int i = 0; i < 6; i++){
		if(extent1[i] != extent2[i]){
			equal = false;
			break;
		}
	}

	return equal;
}

void recursive_bvh(float *vertices, unsigned int *triangles, unsigned int *cell_triangles, unsigned int ntris_cell, float *extent, unsigned int depth, unsigned int parent_id, unsigned int *cell_offset, unsigned int *child_offset, unsigned int *tri_offset, unsigned int is_leaf);

void recursive_bvh(float *vertices, unsigned int *triangles, unsigned int *cell_triangles, unsigned int ntris_cell, float *extent, unsigned int depth, unsigned int parent_id, unsigned int *cell_offset, unsigned int *child_offset, unsigned int *tri_offset, unsigned int is_leaf){
	float extent1[6], extent2[6], extent3[6];
	unsigned int *cell_triangles1, *cell_triangles2, *cell_triangles3, size1, size2, size3, current_id = *cell_offset, current_child_offset = *child_offset, parent_children_offset, n_children = 0, child_id, force_leaf[3];

	// printf("recursive_bvh call, ntris = %d\n", ntris_cell);
	if(ntris_cell != 0){
		// printf("Current id = %d, depth = %d, ntris = %d, parent id = %d\n", current_id, depth, ntris_cell, parent_id);
		assign_dim(extent, &bvh.cell_extent[6*current_id + 0], 6);
		bvh.cell_tri_count[current_id]  = ntris_cell;
		bvh.triangle_offset[current_id] = *tri_offset;

		bvh.children_offset[current_id] = current_child_offset;
		*cell_offset = current_id + 1;

		if(current_id != 0){
			parent_children_offset = bvh.children_offset[parent_id];
			child_id = bvh.children_count[parent_id];

			bvh.children[parent_children_offset + child_id] = current_id;
			bvh.children_count[parent_id] += 1;
		}

		if((ntris_cell >= bvh.max_per_cell)&(depth < bvh.max_depth)&(current_id + 3 < bvh.max_nodes)&(is_leaf == 0)){
			// printf("to divide\n");
			cell_triangles1 = (unsigned int *) malloc(ntris_cell*sizeof(unsigned int));
			cell_triangles2 = (unsigned int *) malloc(ntris_cell*sizeof(unsigned int));
			cell_triangles3 = (unsigned int *) malloc(ntris_cell*sizeof(unsigned int));

			force_leaf[0] = 0;
			force_leaf[1] = 0;
			force_leaf[2] = 0;

			subdivide_box_along_axis(vertices, triangles, cell_triangles, ntris_cell, extent, &extent1[0], &extent2[0], &extent3[0], cell_triangles1, cell_triangles2, cell_triangles3, &size1, &size2, &size3);
			// printf("subdivided in three potential cells, sizes = (%d, %d, %d) ", size1, size2, size3);

			if(size1 > 0){
				force_leaf[0] = compare_extents(extent, &extent1[0]);
				n_children += 1;
			}
			if(size2 > 0){
				force_leaf[1] = compare_extents(extent, &extent2[0]);
				n_children += 1;
			}
			if(size3 > 0){
				force_leaf[2] = compare_extents(extent, &extent3[0]);
				n_children += 1;
			}
			// }
			// printf(", children = %d\n", n_children);
			(*child_offset) += n_children;
			recursive_bvh(vertices, triangles, cell_triangles1, size1, &extent1[0], depth + 1, current_id, cell_offset, child_offset, tri_offset, force_leaf[0]);
			recursive_bvh(vertices, triangles, cell_triangles2, size2, &extent2[0], depth + 1, current_id, cell_offset, child_offset, tri_offset, force_leaf[1]);
			recursive_bvh(vertices, triangles, cell_triangles3, size3, &extent3[0], depth + 1, current_id, cell_offset, child_offset, tri_offset, force_leaf[2]);

			free(cell_triangles1);
			free(cell_triangles2);
			free(cell_triangles3);
		}
		else{
			// printf(" leaf\n");
			// Leaf cell
			unsigned int tri_off = *tri_offset;

			for(unsigned int i = 0; i < ntris_cell; i++){
				bvh.cell_triangles[tri_off] = cell_triangles[i];
				tri_off += 1;
			}
			*tri_offset = tri_off;
		}
	}
}



void generate_BVH(float *vertices, unsigned int *triangles, unsigned int ntriang, unsigned int depth, float *extent, unsigned int threshold_divide){
	float vert1[3], vert2[3], vert3[3];
	unsigned int i, j, level, offset, cell_count, cell_id, nsubcells = 3, level_contiguous = 0, MAX_NODES, *triangles_ids;

	MAX_NODES            = 3*(ntriang/threshold_divide);

	printf("Initializing tree, depth = %d, ntriang = %d, MAX_NODES = %d\n", depth, ntriang, MAX_NODES);
	bvh.ntriang          = ntriang;
	bvh.max_depth        = depth;
	bvh.max_per_cell     = threshold_divide;
	bvh.max_nodes        = MAX_NODES;


//cell_tri_count, triangle_offset, children_offset, cell_extent, children, cell_triangles

	triangles_ids       = (unsigned int *) malloc(ntriang*sizeof(unsigned int));
	bvh.cell_tri_count  = (unsigned int *) malloc(MAX_NODES*sizeof(unsigned int));
	bvh.triangle_offset = (unsigned int *) malloc(MAX_NODES*sizeof(unsigned int));
	bvh.children_count  = (unsigned int *) malloc(MAX_NODES*sizeof(unsigned int));
	bvh.children_offset = (unsigned int *) malloc(MAX_NODES*sizeof(unsigned int));
	bvh.children        = (unsigned int *) malloc(MAX_NODES*sizeof(unsigned int));
	bvh.cell_triangles  = (unsigned int *) malloc(ntriang*sizeof(unsigned int));

	bvh.cell_extent      = (float *)        malloc(MAX_NODES*6*sizeof(float));
	bvh.extent           = (float *)        malloc(6*sizeof(float));

	for(i = 0; i < 6; i++)
		bvh.extent[i] = extent[i];

	for(i = 0; i < ntriang; i++){
		triangles_ids[i] = i;
	}
	for(i = 0; i < MAX_NODES; i++){
		bvh.cell_tri_count[i]  = 0;
		bvh.triangle_offset[i] = 0;
		bvh.children_offset[i] = 0;
		bvh.children[i]        = 0;
		bvh.children_count[i]  = 0;
	}

	printf("Memory allocated\n");
	unsigned int cell_offset = 0, child_offset = 0, tri_offset = 0;
	recursive_bvh(vertices, triangles, triangles_ids, ntriang, extent, 0, 0, &cell_offset, &child_offset, &tri_offset, 0);

	printf("BVH generated, ntriang = %d, MAX_NODES = %d, cell_offset = %d, child_offset = %d, tri_offset = %d\n", ntriang, MAX_NODES, cell_offset, child_offset, tri_offset);
	free(triangles_ids);
}


unsigned int error_counter = 0;


void recursive_search(unsigned int depth, unsigned int cell_id, unsigned int *result, unsigned int *offset_out){
	if(gen_tree.cell_total_count[get_idx_cell(depth, cell_id)] != 0){
		unsigned int offset = *offset_out, tri_id, N_subcells = power(2, npartition), subcell_id;
		// Current cell
		for(unsigned int i = 0; i < gen_tree.cell_count[get_idx_cell(depth, cell_id)]; i++){
			tri_id = gen_tree.cells[gen_tree.starting_idx[get_idx_cell(depth, cell_id)] + i];

			result[offset] = tri_id;
			offset += 1;
		}

		// Subcells
		if(depth < gen_tree.max_depth){
			for(unsigned int k = 0; k < N_subcells; k++){
				subcell_id = cell_id*N_subcells + k;

				recursive_search(depth + 1, subcell_id, result, &offset);
			}
		}

		*offset_out = offset;

	}
}



void get_triangle_within_cell_count(unsigned int depth, unsigned int cell_id, unsigned int *result){
	*result = gen_tree.cell_count[get_idx_cell(depth, cell_id)];
}

void get_triangle_within_cell_total_count(unsigned int depth, unsigned int cell_id, unsigned int *result){
	*result = gen_tree.cell_total_count[get_idx_cell(depth, cell_id)];
}

void get_gentree_extent(unsigned int depth, unsigned int cell_id, float *result){
	assign_dim(&gen_tree.cell_extent[6*get_idx_cell(depth, cell_id) + 0], result, 6);
}

void check_subcell_intersection_general(float *start, float *end, unsigned int cell_id, unsigned int depth, unsigned int *arg_sorted, unsigned int *nsubcells, float *t_mins, float *t_maxs){
	unsigned int N_subcells = power(2, npartition);
	unsigned int intersects[N_subcells], count = 0, i, idx, offset, subcell_id;
	float u_min[N_subcells], u_max[N_subcells];


	for(i = 0; i < N_subcells; i++){
		idx = i + cell_id*N_subcells;
		if(idx >= power(N_subcells, depth + 1)){
			printf("Error!! idx = %d, max = %d, i = %d, depth = %d\n", idx, power(N_subcells, depth + 1), i, depth);
			error_counter += 1;
			if(error_counter >= 10)
				exit(0);
		}
		subcell_id = get_idx_cell(depth + 1, idx);

		intersects[i] = intersect_line_cube(start, end, &gen_tree.cell_extent[6*get_idx_cell(depth + 1, idx) + 0], &u_min[i], &u_max[i]);

		count += intersects[i];
		// printf("i = %d, count = %d, intersects = %d, cell_id = %d, depth = %d, umin = %f, umax = %f\n", i, count, intersects[i], cell_id, depth, u_min[0], u_max[0]);
	}
	if((count == 0)&false){
		printf("Error!!!, start = (%f, %f, %f), end = (%f, %f, %f)\n", start[0], start[1], start[2], end[0], end[1], end[2]);
		for(i = 0; i < N_subcells; i++){
			idx = i + cell_id*N_subcells;


			printf("i %d, inter = %d, inter0 = %d, cell_id = %d, idx = %d, extent = (%f, %f, %f, %f, %f, %f)\n\n", i, intersects[i],
			intersect_line_cube(start, end, &gen_tree.cell_extent[6*get_idx_cell(depth - 1, cell_id) + 0], &u_min[i], &u_max[i]), cell_id, idx,
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 0],
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 1],
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 2],
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 3],
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 4],
			gen_tree.cell_extent[6*get_idx_cell(depth, idx) + 5]);
		}

	}
	if(count != 0){
		float u_min_sort[count], u_max_sort[count];
		offset = 0;
		for(i = 0; i < N_subcells; i++){
			if(intersects[i] == 1){
				u_min_sort[offset]  = u_min[i];
				u_max_sort[offset]  = u_max[i];
				arg_sorted[offset]  = i;
				offset += 1;
			}
		}


		min_argsort(&u_min_sort[0], count, &arg_sorted[0]);


		for(i = 0; i < count; i++){
			t_mins[i] = u_min_sort[i];    //Already sorted by min_argsort
			t_maxs[i] = u_max_sort[arg_sorted[i]];
		}
	}
	*nsubcells = count;
}


void check_subcell_intersection_bvh(float *start, float *end, unsigned int cell_id, unsigned int *arg_sorted, unsigned int *nsubcells, float *t_mins, float *t_maxs){
	unsigned int N_subcells;
	unsigned int intersects[3], count = 0, i, offset, subcell_id;
	float u_min[3], u_max[3];

	N_subcells = bvh.children_count[cell_id];
	for(i = 0; i < N_subcells; i++){
		offset = bvh.children_offset[cell_id];
		subcell_id = bvh.children[offset + i];

		intersects[i] = intersect_line_cube(start, end, &bvh.cell_extent[6*subcell_id + 0], &u_min[i], &u_max[i]);

		count += intersects[i];
	}

	if(count != 0){
		float u_min_sort[count], u_max_sort[count];
		offset = 0;
		for(i = 0; i < N_subcells; i++){
			if(intersects[i] == 1){
				u_min_sort[offset]  = u_min[i];
				u_max_sort[offset]  = u_max[i];
				arg_sorted[offset]  = i;
				offset += 1;
			}
		}


		min_argsort(&u_min_sort[0], count, &arg_sorted[0]);


		for(i = 0; i < count; i++){
			t_mins[i] = u_min_sort[i];    //Already sorted by min_argsort
			t_maxs[i] = u_max_sort[arg_sorted[i]];
			arg_sorted[i] = bvh.children[bvh.children_offset[cell_id] + arg_sorted[i]];
		}
	}
	*nsubcells = count;
}
void check_location(unsigned int *current_location, unsigned int *count_per_level, unsigned int depth, unsigned int max_depth){
	current_location[depth] += 1;
	for(unsigned int i = depth + 1; i < max_depth + 1; i++)
		current_location[i] = 0;

	for(int i = depth; i >= 0; i--){
		if(current_location[i] >= count_per_level[i]);
	}

}


void addition(unsigned int *current_location, unsigned int *count_per_level, unsigned int N, unsigned int loc, unsigned int *first_changed_idx, unsigned int *output){
	*output = 0;
	if(loc == 0)
		*output = 1;
	else{
		current_location[loc] += 1;

		for(unsigned int i = loc + 1; i < N; i++)
			current_location[i] = 0;

		for(unsigned int i = 0; i < loc; i++){
			if(current_location[loc - i] >= count_per_level[loc - i]){
				current_location[loc - i] = 0;
				current_location[loc - i - 1] += 1;

				*first_changed_idx = loc - i - 1;
			}
		}
		if(current_location[0] >= count_per_level[0])
			*output = 1;
	}
}

static inline bool range_overlap(float x_min1, float x_max1, float x_min2, float x_max2){
	return !((x_min2 > x_max1)|(x_max2 < x_min1));
}

bool overlap_extents(float *extent1, float *extent2){
	bool overlap_x, overlap_y, overlap_z;

	overlap_x = range_overlap(extent1[0], extent1[1], extent2[0], extent2[1]);
	overlap_y = range_overlap(extent1[2], extent1[3], extent2[2], extent2[3]);
	overlap_z = range_overlap(extent1[4], extent1[5], extent2[4], extent2[5]);

	return overlap_x&overlap_y&overlap_z;
}

void get_triangle_extent(float *vertices, unsigned int *triangle, float *triangle_extent){
	float vert1[3], vert2[3], vert3[3];

	assign_dim(&vertices[3*triangle[0] + 0], &vert1[0], 3);
	assign_dim(&vertices[3*triangle[1] + 0], &vert2[0], 3);
	assign_dim(&vertices[3*triangle[2] + 0], &vert3[0], 3);

	triangle_extent[0] = fminf(fminf(vert1[0], vert2[0]), vert3[0]);
	triangle_extent[1] = fmaxf(fmaxf(vert1[0], vert2[0]), vert3[0]);
	triangle_extent[2] = fminf(fminf(vert1[1], vert2[1]), vert3[1]);
	triangle_extent[3] = fmaxf(fmaxf(vert1[1], vert2[1]), vert3[1]);
	triangle_extent[4] = fminf(fminf(vert1[2], vert2[2]), vert3[2]);
	triangle_extent[5] = fmaxf(fmaxf(vert1[2], vert2[2]), vert3[2]);
}

float triangle_to_triangle_distance(float *triangle_verts1, float *triangle_verts2){
	return 0.0f;
}

void find_close_triangles(float *vertices, unsigned int *triangles, unsigned int depth, unsigned int cell_id, unsigned int current_tri_id, float *verts_current_triangle, float *triangle_extent, float link_range, unsigned int *result, unsigned int N_max, unsigned int *offset_out);

void find_close_triangles(float *vertices, unsigned int *triangles, unsigned int depth, unsigned int cell_id, unsigned int current_tri_id, float *verts_current_triangle, float *triangle_extent, float link_range, unsigned int *result, unsigned int N_max, unsigned int *offset_out){
	float cell_extent[6], distance, verts_triangle2[9];
	unsigned int offset = *offset_out, N_subcells = power(2, npartition), subcell_id, tri_id;

	if(gen_tree.cell_total_count[get_idx_cell(depth, cell_id)] != 0){
		assign_dim(&gen_tree.cell_extent[6*get_idx_cell(depth, cell_id) + 0], &cell_extent[0], 6);

		if(overlap_extents(&triangle_extent[0], &cell_extent[0])){
			for(unsigned int i = 0; i < gen_tree.cell_count[get_idx_cell(depth, cell_id)]; i++){
				tri_id = gen_tree.cells[gen_tree.starting_idx[get_idx_cell(depth, cell_id)] + i];

				assign_dim(&vertices[triangles[3*tri_id + 0] + 0], &verts_triangle2[0], 3);
				assign_dim(&vertices[triangles[3*tri_id + 1] + 0], &verts_triangle2[3], 3);
				assign_dim(&vertices[triangles[3*tri_id + 2] + 0], &verts_triangle2[6], 3);

				distance = triangle_to_triangle_distance(verts_current_triangle, &verts_triangle2[0]);
				if((tri_id != current_tri_id)&(distance <= link_range)&(offset < N_max)){
					result[offset] = tri_id;
					offset += 1;
				}
			}

			// Subcells
			if(depth < gen_tree.max_depth){
				for(unsigned int k = 0; k < N_subcells; k++){
					subcell_id = cell_id*N_subcells + k;

					find_close_triangles(vertices, triangles, depth + 1, subcell_id, current_tri_id, verts_current_triangle, triangle_extent, link_range, result, N_max, &offset);
				}
			}

			*offset_out = offset;
		}
	}
}


void link_triangles(float *vertices, unsigned int *triangles, unsigned int ntriang, float link_range, unsigned int *result, unsigned int *count, unsigned int N_max, unsigned int *array_size){
	unsigned int i, triangle[3], offset = 0, previous_offset = 0;
	float triangle_extent[6], tri_vert[9];
	double start, end;
	printf("linking triangles\n\n");

	start = omp_get_wtime();
	for(unsigned int tri_id = 0; tri_id < ntriang; tri_id++){
		if(tri_id%10000 == 0){
			end = omp_get_wtime();
			printf("\rtri_id: %d, avg time: %lf[us]\n", tri_id, 1000000*(end - start)/(tri_id));
		}
		triangle[0] = triangles[3*tri_id + 0];
		triangle[1] = triangles[3*tri_id + 1];
		triangle[2] = triangles[3*tri_id + 2];

		assign_dim(&vertices[3*triangle[0] + 0], &tri_vert[0], 3);
		assign_dim(&vertices[3*triangle[1] + 0], &tri_vert[3], 3);
		assign_dim(&vertices[3*triangle[2] + 0], &tri_vert[6], 3);

		get_triangle_extent(vertices, &triangle[0], &triangle_extent[0]);

		triangle_extent[0] -= link_range;
		triangle_extent[1] += link_range;
		triangle_extent[2] -= link_range;
		triangle_extent[3] += link_range;
		triangle_extent[4] -= link_range;
		triangle_extent[5] += link_range;

		find_close_triangles(vertices, triangles, 0, 0, tri_id, &tri_vert[0], &triangle_extent[0], link_range, result, N_max, &offset);

		count[tri_id] = offset - previous_offset;
		previous_offset = offset;
	}
	*array_size = offset;
}


static inline void raytrace_recursive_bvh(float *vertices, float *base, unsigned int *triangles, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length);

static inline void raytrace_recursive_bvh(float *vertices, float *base, unsigned int *triangles, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length){
	float u, u_min = INFINITY;    // In physical units

	unsigned int i, tri_id, subcell_id, offset;
	int current_hit = -1, tri_hit;
	*hit = -1;
	*length = -1.0f;

	if(bvh.cell_tri_count[cell_id] != 0){
		if(bvh.children_count[cell_id] == 0){
			offset = bvh.triangle_offset[cell_id];
			for(i = 0; i < bvh.cell_tri_count[cell_id]; i++){
				tri_id = bvh.cell_triangles[offset + i];

				trace_line_triangle(vertices, base, triangles, tri_id, start_segment, end_segment, &tri_hit, &u);
				if((tri_hit == 1)&(u < u_min)){
					u_min       = u;
					current_hit = tri_id;
					*length = u_min;
					*hit    = current_hit;
				}
			}
		}
		else{
			float t_mins[3], t_maxs[3];
			unsigned int count = 0, arg_sorted[3], idx;

			check_subcell_intersection_bvh(&start_segment[0], &end_segment[0], cell_id, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);

			for(unsigned int k = 0; k < count; k++){
				subcell_id = arg_sorted[k];

				raytrace_recursive_bvh(vertices, base, triangles, subcell_id, &start_segment[0], &end_segment[0], &extent[0], &tri_hit, &u);

				if((tri_hit != -1)&(u < u_min)){
					u_min       = u;
					current_hit = tri_hit;
					*hit    = current_hit;
					*length = u_min;

					if(k == count - 1)
						break;
					else if(u_min <= t_mins[k + 1])
						break;
				}
			}

		}
	}
}



static inline void raytrace_recursive2(float *, float *, unsigned int *, unsigned int, unsigned int, float *, float *, float *, int *, float *);

static inline void raytrace_recursive2(float *vertices, float *base, unsigned int *triangles, unsigned int depth, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length){
	float u, u_min = INFINITY;    // In physical units

	unsigned int i, tri_id, subcell_id, N_depth, N, id_x0, id_y0, id_z0, nsubcells = power(2, npartition);
	int current_hit = -1, tri_hit;
	*hit = -1;
	*length = -1.0f;

	if(gen_tree.cell_total_count[get_idx_cell(depth, cell_id)] != 0){
		// Triangles within cell
		for(i = 0; i < gen_tree.cell_count[get_idx_cell(depth, cell_id)]; i++){
			tri_id = gen_tree.cells[gen_tree.starting_idx[get_idx_cell(depth, cell_id)] + i];

			trace_line_triangle(vertices, base, triangles, tri_id, start_segment, end_segment, &tri_hit, &u);
			if((tri_hit == 1)&(u < u_min)){
				u_min       = u;
				current_hit = tri_id;
				*length = u_min;
				*hit    = current_hit;
			}
		}
		if(depth < gen_tree.max_depth){
			float t_mins[8], t_maxs[8];
			unsigned int count = 0, arg_sorted[8], idx;

			// check_subcell_intersection(&start_segment[0], &end_segment[0], extent, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);

			check_subcell_intersection_general(&start_segment[0], &end_segment[0], cell_id, depth, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);
			if(DEBUG == 1){
				for(unsigned int j = 0; j < count; j++)
					printf("%d ", arg_sorted[j]);
				printf("\n");
			}
			for(unsigned int k = 0; k < count; k++){
				idx = arg_sorted[k];

				subcell_id = cell_id*nsubcells + idx;


				raytrace_recursive2(vertices, base, triangles, depth + 1, subcell_id, &start_segment[0], &end_segment[0], &extent[0], &tri_hit, &u);

				if((tri_hit != -1)&(u < u_min)){
					u_min       = u;
					current_hit = tri_hit;
					*hit    = current_hit;
					*length = u_min;


					break;
				}
			}

		}
	}
}

void raytracing(float *vertices, unsigned int *triangles, unsigned int ntriang, float *starting_points, float *end_points, unsigned int nvectors, int *hits, float *lengths, unsigned int mode, unsigned int nthreads, unsigned int chunk_size){
	float  vert1[3], vert2[3], vert3[3], seg_AB[3], seg_AC[3], *base, normal[3], base1[3], vert2_base2[3], vert3_base2[3], base2[3];
	double time_start, time_end;
	unsigned int nattributes = 6;

	base = (float *) malloc(3*nattributes*ntriang*sizeof(float));

	printf("Calculating %d triangle normals\n", ntriang);
	for(unsigned int i = 0; i < ntriang; i++){
		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		substract(&vert2[0], &vert1[0], &seg_AB[0]);
		substract(&vert3[0], &vert1[0], &seg_AC[0]);

		cross(&seg_AB[0], &seg_AC[0], &normal[0]);
		normalize(&normal[0], &normal[0]);


		normalize(&seg_AB[0], &base1[0]);
		cross(&normal[0], &base1[0], &base2[0]);


		vert2_base2[0] = dot(&seg_AB[0], &base1[0]);
		vert2_base2[1] = dot(&seg_AB[0], &base2[0]);
		vert2_base2[2] = dot(&seg_AB[0], &normal[0]);

		vert3_base2[0] = dot(&seg_AC[0], &base1[0]);
		vert3_base2[1] = dot(&seg_AC[0], &base2[0]);
		vert3_base2[2] = dot(&seg_AC[0], &normal[0]);


		assign_dim(&base1[0],       &base[3*(nattributes*i + 0)], 3);
		assign_dim(&base2[0],       &base[3*(nattributes*i + 1)], 3);
		assign_dim(&normal[0],      &base[3*(nattributes*i + 2)], 3);
		assign_dim(&vert1[0],       &base[3*(nattributes*i + 3)], 3);
		assign_dim(&vert2_base2[0], &base[3*(nattributes*i + 4)], 3);
		assign_dim(&vert3_base2[0], &base[3*(nattributes*i + 5)], 3);

	}

	printf("To actually raytrace %d rays\n", nvectors);

	time_start = omp_get_wtime();

	#pragma omp parallel num_threads(nthreads)
{
	float start[3], end[3], t_min, t_max;
	unsigned int contains;
	#pragma omp for schedule(dynamic, chunk_size)
	for(unsigned int i = 0; i < nvectors; i++){
		assign_dim(&starting_points[3*i + 0], &start[0], 3);
		assign_dim(&end_points[3*i + 0],        &end[0], 3);

		if(mode == 0)
			contains = intersect_line_cube(&start[0], &end[0], &gen_tree.extent[0], &t_min, &t_max);
		else
			contains = intersect_line_cube(&start[0], &end[0], &bvh.extent[0], &t_min, &t_max);


		if(contains == 1){
			// Perform raytracing
			//
			if(mode == 0)
				raytrace_recursive2(vertices, base, triangles, 0, 0, &start[0], &end[0], &gen_tree.extent[0], &hits[i], &lengths[i]);
			else
				raytrace_recursive_bvh(vertices, base, triangles, 0, &start[0], &end[0], &bvh.extent[0], &hits[i], &lengths[i]);


		}
		else{
			hits[i]    = -1;
			lengths[i] = -1.f;
		}
	}
}
	time_end = omp_get_wtime();
	printf("perfomance: %lf Mrays per second, total time = %lf\n", (nvectors/1000000.)/(time_end - time_start), time_end - time_start);

	free(base);
}


void stack_quantities_pipeline(unsigned int *resolution, unsigned int *mat_ids, unsigned int nmaterials, unsigned int nthreads, unsigned int block_id, float *u_array, float *v_array, float *mat_map, float *block_map, float *quantity, unsigned int N, float *conc_arrays){
	unsigned int block_size = N/nthreads + 1;
	printf("block size = %d\n", block_size);
	#pragma omp parallel num_threads(nthreads)
{
	float u, v;
	unsigned int thread_id = omp_get_thread_num(), mat_id, I, J, offset;
	int idx, res;
	for(unsigned int i = block_size*thread_id; i < min(block_size*(thread_id + 1), N); i++){
		u = u_array[i];
		v = v_array[i];

		offset = 0;
		for(unsigned int j = 0; j < nmaterials; j++){
			res = resolution[j];
			mat_id = mat_ids[j];

			if(block_map[i] == ((float) block_id)){

				if(mat_map[i] == mat_id){

					I = (unsigned int)(u*res);
					J = (unsigned int)(v*res);

					idx = I*res + J;
					if(idx >= res*res)
						idx += -res*res;

					conc_arrays[nthreads*offset + res*res*thread_id + idx] += quantity[i];

				}

			}

			offset += res*res;
		}
	}
}
}

void determine_hit_location(float *vertices, unsigned int *triangles, float *starts, float *ends, int *hits, float *lengths, unsigned int N, float *coords){

	#pragma omp parallel num_threads(18)
{
	int tri_id;
	unsigned int x_axis, y_axis;
	float direction[3], hit_location[3], A[3], B[3], C[3], vert1[3], vert2[3], vert3[3], u, v, det;
	#pragma omp for schedule(dynamic, 500)
	for(unsigned int i = 0; i < N; i++){
		tri_id = hits[i];
		if(tri_id >= 0){
			substract(&ends[3*i + 0], &starts[3*i + 0], &direction[0]);
			normalize(&direction[0], &direction[0]);
			assign_dim(&starts[3*i + 0], &hit_location[0], 3);

			hit_location[0] += lengths[i]*direction[0];
			hit_location[1] += lengths[i]*direction[1];
			hit_location[2] += lengths[i]*direction[2];

			assign_dim(&vertices[3*triangles[3*tri_id + 0] + 0], &vert1[0], 3);
			assign_dim(&vertices[3*triangles[3*tri_id + 1] + 0], &vert2[0], 3);
			assign_dim(&vertices[3*triangles[3*tri_id + 2] + 0], &vert3[0], 3);

			substract(&vert2[0], &vert1[0], &A[0]);
			substract(&vert3[0], &vert1[0], &B[0]);
			substract(&hit_location[0], &vert1[0], &C[0]);

			x_axis = 0;
			y_axis = 1;

			det = B[y_axis]*A[x_axis] - B[x_axis]*A[y_axis];
			if(fabsf(det) <= 0.00001f){
				x_axis = 0;
				y_axis = 2;
				det = B[y_axis]*A[x_axis] - B[x_axis]*A[y_axis];
				if(fabsf(det) <= 0.00001f){
					x_axis = 1;
					y_axis = 2;
					det = B[y_axis]*A[x_axis] - B[x_axis]*A[y_axis];
				}
			}
			u = (B[y_axis]*C[x_axis] - B[x_axis]*C[y_axis])/det;
			v = C[y_axis]/B[y_axis] - A[y_axis]/B[y_axis]*u;

			coords[2*i + 0] = u;
			coords[2*i + 1] = v;
		}
	}



}
}
void generate_rays_from_heightmap(float *heightmap, float *extent, unsigned int N, unsigned int M, unsigned int block_size, float *result){
	unsigned int n = N/block_size + 1, m = M/block_size + 1, offset = 0, I, J;
	float x, y, z, dx = (extent[1] - extent[0])/(N - 1), dy = (extent[3] - extent[2])/(M - 1);

	for(unsigned int i = 0; i < n; i++){
		for(unsigned int j = 0; j < m; j++){
			for(unsigned int p = 0; p < block_size; p++){
				for(unsigned int q = 0; q < block_size; q++){
					I = i*block_size + p;
					J = j*block_size + q;
					if((I < N) & (J < M)&(offset < N*M)){
						x = extent[0] + I*dx;
						y = extent[2] + J*dy;
						z = heightmap[I*M + J];
						result[3*offset + 0] = x;
						result[3*offset + 1] = y;
						result[3*offset + 2] = z;
						offset += 1;
					}
				}

			}

		}

	}

}

void get_list(int idx, unsigned int subdivision, unsigned int *S_count, unsigned int *offset_array, unsigned int *tri_ids, unsigned int *triangle_mask, unsigned int *result, unsigned int *n_neighbors){
	unsigned int count = 0, ncell, tri_id;
	for(unsigned int i = max(idx - 1, 0); i < min(idx + 2, subdivision); i++){
		ncell = S_count[i];
		for(unsigned int j = 0; j < ncell; j++){
			tri_id = tri_ids[offset_array[i] + j];
			if(triangle_mask[tri_id] == 0){
				result[count] = tri_id;
				count += 1;
			}
		}
	}
	*n_neighbors = count;
}

void determine_triangle_overlap(float *vertices, unsigned int *triangles, unsigned int ntriang, unsigned int *surface_count, unsigned int *nsurfaces, unsigned int *organized_triangles){
	float *triangle_normals, *S_array, *vert0, vert1[3], vert2[3], vert3[3], seg_AB[3], seg_AC[3], normal[3], S, s_min = INFINITY, s_max = -INFINITY, dS, criterium_S = 1e-2, criterium_N = 1 - cosf(1.0f*pi/180.f);
	unsigned int *S_count, *offset_array, *tri_ids, i, offset, subdivision, *triangle_mask, ncell;
	int idx;

	triangle_mask    = (unsigned int *) malloc(ntriang*sizeof(unsigned int));
	tri_ids          = (unsigned int *) malloc(ntriang*sizeof(unsigned int));
	triangle_normals = (float *)        malloc(ntriang*3*sizeof(float));
	S_array          = (float *)        malloc(ntriang*sizeof(float));
	vert0            = (float *)        malloc(ntriang*3*sizeof(float));


	for(i = 0; i < ntriang; i++){
		triangle_mask[i] = 0;

		assign_dim(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		substract(&vert2[0], &vert1[0], &seg_AB[0]);
		substract(&vert3[0], &vert1[0], &seg_AC[0]);

		cross(&seg_AB[0], &seg_AC[0], &normal[0]);
		normalize(&normal[0], &normal[0]);

		assign_dim(&normal[0], &triangle_normals[3*i], 3);

		assign_dim(&vert1[0], &vert0[3*i], 3);

		S = fabsf(dot(&normal[0], &vert1[0]));
		S_array[i] = S;

		s_min = fminf(S, s_min);
		s_max = fmaxf(S, s_max);
	}
	subdivision = max((int)((s_max - s_min)/criterium_S) - 1, 1);
	dS = (s_max - s_min)/subdivision;
	if(s_max == s_min){
		subdivision = 1;
		s_max = s_min + criterium_S;
	}

	printf("s_min = %f, s_max = %f, dS = %f, criterium = %f, subdivision = %d\n", s_min, s_max, dS, criterium_S, subdivision);
	assert(dS > criterium_S);

	S_count      = (unsigned int *) malloc(subdivision*sizeof(unsigned int));
	offset_array = (unsigned int *) malloc(subdivision*sizeof(unsigned int));

	for(i = 0; i < subdivision; i++){
		S_count[i]      = 0;
		offset_array[i] = 0;
	}

	for(i = 0; i < ntriang; i++){
		S = S_array[i];
		idx = (int)((S - s_min)/dS);
		idx = max(0, min(subdivision - 1, idx));

		S_count[idx] += 1;
	}

	offset = 0;
	for(i = 0; i < subdivision; i++){
		offset_array[i] = offset;
		offset += S_count[i];
		S_count[i] = 0;
	}

	for(i = 0; i < ntriang; i++){
		S = S_array[i];
		idx = (int)((S - s_min)/dS);
		idx = max(0, min(subdivision - 1, idx));

		offset = offset_array[idx] + S_count[idx];
		tri_ids[offset] = i;
		S_count[idx] += 1;
	}

	unsigned int tri_id1, tri_id2, tri_offset = 0, surf_offset = 0, n_neighbors, *aux_list;
	float normal1[3], normal2[3], S1, S2, cos, diff, origin1[3], origin2[3], vector[3];

	aux_list = (unsigned int*) malloc(ntriang*sizeof(unsigned int));

	for(idx = 0; idx < subdivision; idx++){
		ncell = S_count[idx];

		for(i = 0; i < ncell; i++){
			tri_id1 = tri_ids[offset_array[idx] + i];

			if(triangle_mask[tri_id1] == 0){
				triangle_mask[tri_id1] = 1;

				organized_triangles[tri_offset] = tri_id1;
				tri_offset                 += 1;
				surface_count[surf_offset] += 1;

				get_list(idx, subdivision, S_count, offset_array, tri_ids, triangle_mask, aux_list, &n_neighbors);

				for(unsigned int j = 0; j < n_neighbors; j++){
					tri_id2 = aux_list[j];
					// S1      = S_array[tri_id1];
					// S2      = S_array[tri_id2];
					assign_dim(&triangle_normals[3*tri_id1], &normal1[0], 3);
					assign_dim(&triangle_normals[3*tri_id2], &normal2[0], 3);

					assign_dim(&vert0[3*tri_id1], &origin1[0], 3);
					assign_dim(&vert0[3*tri_id2], &origin2[0], 3);

					substract(&origin2[0], &origin1[0], &vector[0]);
					S1 = fabsf(dot(&normal1[0], &vector[0]));
					S2 = fabsf(dot(&normal2[0], &vector[0]));

					cos = fabsf(dot(&normal1[0], &normal2[0]));
					// diff = fabsf(S2 - S1);
					if((S1 < criterium_S)&(S2 < criterium_S)&(fabsf(1.0f - cos) < criterium_N)){
						triangle_mask[tri_id2] = 1;
						organized_triangles[tri_offset] = tri_id2;
						tri_offset                 += 1;
						surface_count[surf_offset] += 1;
					}
				}
				surf_offset += 1;
			}

		}

	}
	*nsurfaces = surf_offset;
	free(triangle_mask);
	free(aux_list);
	free(S_array);
	free(triangle_normals);
	free(tri_ids);
	free(S_count);
	free(offset_array);
	free(vert0);
}
/*
bool triangle_intersect(float *verts_tri1, float *verts_tri2, unsigned int *intersection_tris, float *intersection_verts, unsigned int *N_inters, unsigned int *remaining_tris, float *remaining_verts, unsigned int *N_remain){
	// 2D triangles
	unsigned int intriangle1[3], intriangle2[3], total_inside = 0, count1 = 0, count2 = 0;
	bool they_intersect = false;

	for(i = 0; i < 3; i++){
		intriangle1[i] = (unsigned int) PointInTriangle(&verts_tri1[2*i + 0], &verts_tri2[2*0 + 0], &verts_tri2[2*1 + 0], &verts_tri2[2*2 + 0]);
		intriangle2[i] = (unsigned int) PointInTriangle(&verts_tri2[2*i + 0], &verts_tri1[2*0 + 0], &verts_tri1[2*1 + 0], &verts_tri1[2*2 + 0]);
		count1 += intriangle1[i];
		count2 += intriangle2[i];

		total_inside += intriangle1[i] + intriangle2[i];
	}

	if(total_inside != 0)
		they_intersect = true;

	if(they_intersect){
		unsigned int N_a, N_b;
		float *verts_A, *verts_B;

		if(count2 >= count1){
			verts_A = verts_tri1;
			verts_B = verts_tri2;
			N_a = count1;
			N_b = count2;
		}
		else{
			verts_A = verts_tri2;
			verts_B = verts_tri1;
			N_a = count2;
			N_b = count1;
		}

		if((N_a == 0)&(N_b == 1)){
			// Case 1


		}


	}
	return they_intersect;
}*/




//gcc -c -O3 -fPIC program.c -o program.o -lm -fopenmp -std=c99
//gcc -shared program.o -o module.so -lm -fopenmp
