import tools
import numpy as np
import time
import matplotlib.pyplot as plt
import modulo
import os
from importlib import reload
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light

plt.ion()

if not(os.path.isdir("map_textures")): os.mkdir("map_textures")


itera, optimize = 150, True
nthreads = 18

ambient_occlusion, raytracing = True, True

if ambient_occlusion:
	np.random.seed(1)
	cosines = np.random.random(itera)
	thetas = np.arccos(cosines)*180/np.pi
	phis   = np.linspace(0., 360., itera, endpoint = False)

else:
	latitude, rotation = 33*np.pi/180., 15.*np.pi/180
	thetas = np.zeros(itera)
	phis   = np.zeros(itera)
	rot_matrix1 = np.array([[np.cos(rotation), np.sin(rotation), 0.], [-np.sin(rotation), np.cos(rotation), 0.], [0., 0., 1]]).T
	rot_matrix2 = np.array([[1., 0., 0.], [0., np.cos(latitude), -np.sin(latitude)], [0., np.sin(latitude), np.cos(latitude)]]).T

	rot_matrix = np.matmul(rot_matrix1, rot_matrix2)
	for i in range(itera):
		angle = np.pi*i/(itera - 1)
		vec_primed = np.array([np.cos(angle), 0., np.sin(angle)])
		vec = np.matmul(rot_matrix, vec_primed)
		thetas[i] = np.arccos(vec[2])*180/np.pi
		phis[i]   = np.arctan2(vec[1], vec[0])*180/np.pi

if raytracing:
	latitude, rotation = 33*np.pi/180., 15.*np.pi/180
	rot_matrix1 = np.array([[np.cos(rotation), np.sin(rotation), 0.], [-np.sin(rotation), np.cos(rotation), 0.], [0., 0., 1]]).T
	rot_matrix2 = np.array([[1., 0., 0.], [0., np.cos(latitude), -np.sin(latitude)], [0., np.sin(latitude), np.cos(latitude)]]).T

	rot_matrix = np.matmul(rot_matrix1, rot_matrix2)
	angle = np.pi/2

	vec_primed = np.array([np.cos(angle), 0., np.sin(angle)])
	vec = np.matmul(rot_matrix, vec_primed)

	theta_light, phi_light = np.arccos(vec[2])*180/np.pi, np.arctan2(vec[1], vec[0])*180/np.pi


def calculate_area(mesh_block, mask_vertices):
	fake_vertices       = mesh_block.vertices[:]*0.
	fake_vertices[:, 0] = mesh_block.uv2[:, 0]
	fake_vertices[:, 1] = mesh_block.uv2[:, 1]

	triangles = mesh_block.triangles + 0
	mask_triangles = mask_vertices[triangles[:, 0]]*mask_vertices[triangles[:, 1]]*mask_vertices[triangles[:, 2]]

	uv_area = modulo.calculate_area(fake_vertices, mesh_block.triangles[mask_triangles])

	real_area = modulo.calculate_area(mesh_block.vertices, mesh_block.triangles[mask_triangles])
	return real_area, uv_area

def get_mask_uv(submesh, block_id, mat_id, target_resolution):
	nvert    = len(submesh.uv2)
	triangles= submesh.triangles
	uv2      = submesh.uv2

	mask_mat = (submesh.mat_id == mat_id)*(submesh.block_id == block_id)


	dx = 1/target_resolution

	fake_vertices = np.zeros(shape = [nvert, 3], dtype = np.float32)
	fake_vertices[:, 0], fake_vertices[:, 1], fake_vertices[:, 2] = uv2[:, 0], uv2[:, 1], 0.

	fake_vertices[mask_mat, 2] = 1.
	heightmap_aux, uv2_mapping, _ = modulo.map_quantity(fake_vertices, np.ones(len(fake_vertices))*mask_mat, triangles, [0., 1., 0., 1.], dx = dx, optimize = False, force_resolution = target_resolution)
	mask_map = uv2_mapping != 0.

	weights = gaussian_filter(mask_map*1., 1)*mask_map
	return weights


def get_simplified_mesh(lod, block_id, nblocks):
	rest = []
	all_materials, offset = {}, 0
	for i in range(nblocks):
		if i != block_id:
			mesh = light.load_mesh(lod = 2, block_id = i)

			for j in range(len(mesh.materials)):
				material = mesh.materials[j]
				if material in all_materials.keys():
					mat_id = all_materials[material]
				else:
					mat_id = offset
					all_materials[material] = offset
					offset += 1
				mesh.submeshes[material].add_quantity("block_id",     i, dtype = "uint8", dim = 1, per_vert = True)
				mesh.submeshes[material].add_quantity("mat_id",  mat_id, dtype = "uint8", dim = 1, per_vert = True)

			mesh = tools.combine_submeshes(list(mesh.submeshes.values()), additional_attributes=["uv2", "block_id", "mat_id"], per_vert=True)

			rest.append(mesh)

		else:

			mesh_lod = light.load_mesh(lod = lod, block_id = i)
			# mesh_lod.calculate_whole_mesh()

			for j in range(len(mesh_lod.materials)):
				material = mesh_lod.materials[j]
				if material in all_materials.keys():
					mat_id = all_materials[material]
				else:
					mat_id = offset
					all_materials[material] = offset
					offset += 1

				mesh_lod.submeshes[mesh_lod.materials[j]].add_quantity("block_id", i, dtype = "uint8", dim = 1, per_vert = True)
				mesh_lod.submeshes[mesh_lod.materials[j]].add_quantity("mat_id", mat_id, dtype = "uint8", dim = 1, per_vert = True)

			whole_mesh = tools.combine_submeshes(list(mesh_lod.submeshes.values()), additional_attributes=["uv2", "block_id", "mat_id"], per_vert=True)

	return rest, whole_mesh, all_materials

def calculate_ambient_occlusion(thetas, phis, mesh, mesh_lod, block_id):
	result = np.zeros(np.sum(resolutions**2)*nthreads, dtype = np.float32)

	timer1, timer2, timer3, dx = 0., 0., 0., 0.1
	for k in range(itera):
		print("Itera: " + str(k))

		time_point1 = time.time()
		theta, phi = thetas[k], phis[k]

		vertices_block, normals_block, triangles_block = light.rotate_mesh(mesh_lod, theta, phi)
		vertices_all,   normals_all, triangles_all = light.rotate_mesh(mesh, theta, phi)

		block_extent = modulo.get_extent(vertices_block)

		time_point2 = time.time()
		quantity = mesh.uv2[:, 0]
		heightmap_aux, map_u, _ = modulo.map_quantity(vertices_all, quantity, triangles_all, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)

		quantity = mesh.uv2[:, 1]
		heightmap_aux, map_v, _ = modulo.map_quantity(vertices_all, quantity, triangles_all, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)

		map_u[map_u > 1.] = 1.
		map_u[map_u < 0.] = 0.
		map_v[map_v > 1.] = 1.
		map_v[map_v < 0.] = 0.


		mask_block = mesh.block_id == block_id

		quantity = mesh.mat_id
		quantity[~mask_block] = -1
		heightmap_aux, map_mat, _ = modulo.map_quantity(vertices_all, quantity, triangles_all, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)


		time_point3 = time.time()
		value = np.ones(map_mat.shape, dtype = np.float32)
		map_block = np.ones(map_mat.shape, dtype = np.float32)*block_id

		factor = 2*dx**2/(dl**2)/itera
		modulo.stack_quantities_pipeline(resolutions, relevant_materials_ids, nmat, block_id, map_u, map_v, map_mat, map_block, value*factor, result, nthreads = nthreads)

		time_point4 = time.time()
		timer1 += time_point2 - time_point1
		timer2 += time_point3 - time_point2
		timer3 += time_point4 - time_point3

	print("Time per iteration (rotation):   ", timer1/itera, " [s]")
	print("Time per iteration (render):     ", timer2/itera, " [s]")
	print("Time per iteration (projection): ", timer3/itera, " [s]")

	weights_sum, offset = [], 0
	for k in range(nmat):
		size = resolutions[k]**2
		matrix = np.zeros(size, dtype = np.float32)
		for j in range(nthreads):
			matrix += result[offset + size*j: offset + size*(j + 1)]
		offset += size*nthreads
		weights_sum.append(np.reshape(matrix, [resolutions[k], resolutions[k]]))

	return weights_sum

def undo_rotations(vector, theta_light, phi_light):
	aux = tools.rotate(vector, tools.vec_right, -theta_light*tools.deg2rad)
	return tools.rotate(aux, tools.vec_up, -phi_light*tools.deg2rad)

def get_positions_from_heightmap(heightmap, extent):
	x_space, y_space = np.linspace(extent[0], extent[1], len(heightmap)), np.linspace(extent[2], extent[3], len(heightmap[0]))

	shape = heightmap.shape
	X, Y = np.meshgrid(x_space, y_space, indexing = "ij")

	I, J = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing = "ij")

	vertices = np.zeros([shape[0]*shape[1], 3], dtype = np.float32)

	vertices[:, 0] = X.flatten()
	vertices[:, 1] = Y.flatten()
	vertices[:, 2] = heightmap.flatten()

	return vertices


def get_hits_uv_coords(hits, vert0, seg1, seg2, triang_normals, start_points, end_points, lengths):
	directions = end_points - start_points
	hit_points = start_points*0.
	for i in range(3): directions[:, i] *= 1/np.linalg.norm(end_points - start_points, axis = 1)

	for i in range(3): hit_points[:, i] = start_points[:, i] + directions[:, i]*lengths

	valid = hits != -1
	tri_ids = hits[valid]
	hit2 = hit_points[valid] - vert0[tri_ids]

	vector   = np.zeros([len(seg1), 3], dtype = np.float32)
	matrices = np.zeros([len(seg1), 3, 3], dtype = np.float32)

	matrices[:, :, 0] = seg1
	matrices[:, :, 1] = seg2
	matrices[:, :, 2] = triang_normals

	inv_matrices = np.linalg.inv(matrices)

	new_vec = np.zeros([len(tri_ids), 3], dtype = np.float32)

	hit3 = (seg1[tri_ids] + seg2[tri_ids])*0.5
	for i in range(len(tri_ids)):
		tri_id = tri_ids[i]
		new_vec[i] = np.matmul(inv_matrices[tri_id, :, :], hit2[i, :])

	return new_vec, tri_ids

def calculate_raytracing(theta_light, phi_light, mesh_lod, mesh, block_id, dx = 0.1):
	nitera = 50
	cam_direction = np.array([0., 0., 1.])

	time_point1 = time.time()
	vertices_block_rot, normals_block_rot, triangles_block = light.rotate_mesh(mesh_lod, theta_light, phi_light)
	vertices_all_rot,   normals_all_rot,   triangles_all   = light.rotate_mesh(mesh,     theta_light, phi_light)

	vertices_all, normals_all = mesh.vertices, mesh.normals

	heightmap_rot, block_extent_rot = modulo.get_heightmap(vertices_block_rot, triangles_block, dx = dx)

	mask = heightmap_rot != 0.


	quantity = np.matmul(normals_all_rot, cam_direction)
	quantity[quantity < 0.] = 0.
	heightmap, cos, _ = modulo.map_quantity(vertices_all_rot, quantity, triangles_all, block_extent_rot, dx = dx, optimize = True)

	heightmap += vertices_all_rot[:, 2].min()

	### Getting normals (rotated) map
	vert0_rot = vertices_all_rot[triangles_all[:, 0]]
	seg1_rot  = vertices_all_rot[triangles_all[:, 1]] - vert0_rot
	seg2_rot  = vertices_all_rot[triangles_all[:, 2]] - vert0_rot

	triang_normals_rot = np.cross(seg1_rot, seg2_rot, axis = 1)
	norma = np.linalg.norm(triang_normals_rot, axis = 1)
	for i in range(3): triang_normals_rot[:, i] *= 1/norma

	map_normals = np.zeros(cos.shape + (3,) )
	for i in range(3):
		heightmap_aux, map_normals[:, :, i], _ = modulo.map_quantity(vertices_all_rot, np.repeat(triang_normals_rot[:, i], 3), triangles_all, block_extent_rot, dx = dx, optimize = True, vertex_wise = False)

	### Getting tangent (rotated) maps
	tang_map1 = map_normals*0.
	tang_map1[:, :, 0] = 1.

	dot = tang_map1[:, :, 0]*map_normals[:, :, 0] + tang_map1[:, :, 1]*map_normals[:, :, 1] + tang_map1[:, :, 2]*map_normals[:, :, 2]

	for i in range(3): tang_map1[:, :, i] += -dot*map_normals[:, :, i]

	norm = np.linalg.norm(tang_map1, axis=2)
	for i in range(3): tang_map1[:, :, i] *= 1/norm

	tang_map2 = np.cross(map_normals, tang_map1, axis = 2)

	### Getting ray colors
	heightmap_aux, texture_R, _ = modulo.map_quantity(vertices_all_rot, mesh.colors[:, 0], triangles_all, block_extent_rot, dx = dx)
	heightmap_aux, texture_G, _ = modulo.map_quantity(vertices_all_rot, mesh.colors[:, 1], triangles_all, block_extent_rot, dx = dx)
	heightmap_aux, texture_B, _ = modulo.map_quantity(vertices_all_rot, mesh.colors[:, 2], triangles_all, block_extent_rot, dx = dx)


	ray_colors = np.zeros([len(heightmap)*len(heightmap[0]), 3], dtype = np.float32)
	ray_colors[:, 0] = texture_R.flatten()
	ray_colors[:, 1] = texture_G.flatten()
	ray_colors[:, 2] = texture_B.flatten()

	ray_colors = ray_colors[mask.flatten()]



	vert0          = undo_rotations(vert0_rot, theta_light, phi_light)
	seg1           = undo_rotations(seg1_rot,  theta_light, phi_light)
	seg2           = undo_rotations(seg2_rot,  theta_light, phi_light)
	triang_normals = undo_rotations(triang_normals_rot, theta_light, phi_light)

	extent = [vertices_all[:, 0].min(), vertices_all[:, 0].max(), vertices_all[:, 1].min(), vertices_all[:, 1].max(), vertices_all[:, 2].min(), vertices_all[:, 2].max()]


	depth = 19
	modulo.generate_BVH(vertices_all, triangles_all, extent, depth = depth)


	results = []
	offset = 0
	for i in range(nmat):
		results.append(np.zeros([resolutions[i], resolutions[i], 3], dtype = np.float32))
		offset += resolutions[i]**2

	result_R = np.zeros(offset*nthreads, dtype = np.float32)
	result_G = np.zeros(offset*nthreads, dtype = np.float32)
	result_B = np.zeros(offset*nthreads, dtype = np.float32)

	timer1, timer2, timer3, timer4 = 0., 0., 0., 0.0
	for i in range(nitera):
		block_size = 200
		time_point1 = time.time()
		# start_points_rot = get_positions_from_heightmap(heightmap_rot + 0.1, block_extent_rot)

		start_points_rot = modulo.generate_rays_from_heightmap(heightmap_rot + 0.1, block_extent_rot, block_size = block_size)

		u        = np.random.random(size = map_normals.shape[0:2])
		theta    = np.arcsin(u**0.5)
		phi      = np.random.random(size = map_normals.shape[0:2])*2*np.pi

		directions_rot = map_normals*0
		for i in range(3):
			directions_rot[:, :, i] = np.sin(theta)*np.cos(phi)*tang_map1[:, :, i] + np.sin(theta)*np.sin(phi)*tang_map2[:, :, i] + np.cos(theta)*map_normals[:, :, i]

		directions_rot = np.reshape(directions_rot, [directions_rot.shape[0]*directions_rot.shape[1], 3])

		end_points_rot = start_points_rot + directions_rot*200


		start_points_rot = start_points_rot[mask.flatten()]
		end_points_rot   = end_points_rot[mask.flatten()]


		start_points = undo_rotations(start_points_rot, theta_light, phi_light)
		end_points   = undo_rotations(end_points_rot,   theta_light, phi_light)


		time_point2 = time.time()
		hits, lengths = modulo.raytracing(vertices_all, triangles_all, start_points, end_points, mode = 1, nthreads = nthreads)
		time_point3 = time.time()

		coords = modulo.determine_hit_locations(vertices_all, triangles_all, start_points, end_points, hits, lengths)

		new_vec = coords[hits != -1]
		tri_ids = hits[hits != -1]
		# new_vec, tri_ids = get_hits_uv_coords(hits, vert0, seg1, seg2, triang_normals, start_points, end_points, lengths)

		uv1 = mesh.uv2[triangles_all[:, 0]]
		uv2 = mesh.uv2[triangles_all[:, 1]]
		uv3 = mesh.uv2[triangles_all[:, 2]]

		seguv1, seguv2 = uv2 - uv1, uv3 - uv1

		interpolated_uvs = np.zeros([len(tri_ids), 2], dtype = np.float32)

		for i in range(2): interpolated_uvs[:, i] = uv1[tri_ids, i] + seguv1[tri_ids, i]*new_vec[:, 0] + seguv2[tri_ids, i]*new_vec[:, 1]

		mat_triang = mesh.mat_id[triangles_all[:, 0]]
		block_triang = mesh.block_id[triangles_all[:, 0]]

		rays_mat   = mat_triang[tri_ids]
		rays_block = block_triang[tri_ids]

		rays_mat[rays_block != block_id] = -1

		valid = hits != -1

		time_point4 = time.time()

		map_block = np.ones(rays_mat.shape, dtype = np.float32)*block_id
		modulo.stack_quantities_pipeline(resolutions, relevant_materials_ids, nmat, block_id, interpolated_uvs[:, 0], interpolated_uvs[:, 1], rays_mat, map_block, (ray_colors[valid])[:, 0], result_R, nthreads = nthreads)
		modulo.stack_quantities_pipeline(resolutions, relevant_materials_ids, nmat, block_id, interpolated_uvs[:, 0], interpolated_uvs[:, 1], rays_mat, map_block, (ray_colors[valid])[:, 1], result_G, nthreads = nthreads)
		modulo.stack_quantities_pipeline(resolutions, relevant_materials_ids, nmat, block_id, interpolated_uvs[:, 0], interpolated_uvs[:, 1], rays_mat, map_block, (ray_colors[valid])[:, 2], result_B, nthreads = nthreads)
		time_point5 = time.time()

		timer1 += time_point2 - time_point1
		timer2 += time_point3 - time_point2
		timer3 += time_point4 - time_point3
		timer4 += time_point5 - time_point4

	print("Time per iteration (ray generation):", timer1/nitera, " [s]")
	print("Time per iteration (raytracing):    ", timer2/nitera, " [s]")
	print("Time per iteration (interpreting):  ", timer3/nitera, " [s]")
	print("Time per iteration (stacking):      ", timer4/nitera, " [s]")

	weights_sum, offset = [], 0
	for k in range(nmat):
		mat_id = relevant_materials_ids[k]
		size = resolutions[k]**2
		matrix   = np.zeros([resolutions[k], resolutions[k], 3])
		matrix_R = np.zeros(size, dtype = np.float32)
		matrix_G = np.zeros(size, dtype = np.float32)
		matrix_B = np.zeros(size, dtype = np.float32)

		for j in range(nthreads):
			matrix_R += result_R[offset + size*j: offset + size*(j + 1)]
			matrix_G += result_G[offset + size*j: offset + size*(j + 1)]
			matrix_B += result_B[offset + size*j: offset + size*(j + 1)]

		offset += size*nthreads

		matrix[:, :, 0] = np.reshape(matrix_R, [resolutions[k], resolutions[k]])
		matrix[:, :, 1] = np.reshape(matrix_G, [resolutions[k], resolutions[k]])
		matrix[:, :, 2] = np.reshape(matrix_B, [resolutions[k], resolutions[k]])

		norm     = get_mask_uv(mesh, block_id, mat_id, resolutions[k])

		for j in range(3): matrix[:, :, j] = gaussian_filter(matrix[:, :, j], 1)/norm

		matrix[~np.isfinite(matrix)] = 0.
		# print(abs(results[k] - matrix).sum())
		weights_sum.append(matrix)


	print("Time average raytracing:", time_accum/nitera)
	modulo.free_bvh()



nblocks = 4
lods = np.repeat([0, 1, 2], nblocks)
block_ids = np.tile(np.arange(nblocks), 3)

for i in range(nblocks*3*0+1):
# for i in range(1):
	block_id, lod = block_ids[i], lods[i]


	rest, mesh_lod, all_materials = get_simplified_mesh(lod = lod, block_id = block_id, nblocks = nblocks)


	whole_mesh = tools.combine_submeshes(rest + [mesh_lod], additional_attributes=["uv2", "block_id", "mat_id"], per_vert=True)


	mesh = whole_mesh
	mesh.calculate_normals()

	mask_block = mesh.block_id == block_id

	relevant_materials_ids = np.unique(mesh.mat_id[mask_block])
	nmat = len(relevant_materials_ids)


	max_f = -np.inf
	for k in range(nmat):
		mask_vert = (mesh.mat_id == relevant_materials_ids[k])*(mesh.block_id == block_id)
		A, f = calculate_area(mesh, mask_vert)
		if A/f >= max_f:
			max_f = A/f


	dl = np.sqrt(max_f)/2048


	resolutions = np.zeros(nmat, dtype = np.int32)
	weights_sum = []
	for k in range(nmat):
		mask_vert = (mesh.mat_id == relevant_materials_ids[k])*(mesh.block_id == block_id)
		A, f = calculate_area(mesh, mask_vert)

		N = int(np.sqrt(A/f)/dl)

		resolutions[k] = N
		weights_sum.append(np.zeros([N, N], dtype = np.float32))

	if ambient_occlusion:
		weights_sum = calculate_ambient_occlusion(thetas, phis, mesh, mesh_lod, block_id)

	for k in range(nmat):
		mat_id   = relevant_materials_ids[k]
		material = list(all_materials.keys())[mat_id]
		norm     = get_mask_uv(mesh, block_id, mat_id, resolutions[k])

		array    = gaussian_filter(weights_sum[k], 1)/norm

		array[~np.isfinite(array)] = 0.

		image = np.ones(array.shape + (4,), dtype = np.float32)
		image[:,:,0] = array
		image[:,:,1] = array
		image[:,:,2] = array

		image[image > 1] = 1.

		plt.imsave("map_textures/lod" + str(lod) + "_block_" + str(block_id) + "_" + material + ".png", image)

