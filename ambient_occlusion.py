import tools
import numpy as np
import matplotlib.pyplot as plt
import modulo
import xatlas
import os
from importlib import reload
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light

plt.ion()


if os.path.isdir("map_textures"):
	pass
else:
	os.mkdir("map_textures")

meshes_lod0 = light.load_meshes(lod = 0, usefile = False, nblocks = 5)
meshes_lod2 = light.load_meshes(lod = 2, usefile = False, nblocks = 5)

mesh_lod0 = tools.Mesh()
for mesh in meshes_lod0:
	mesh_lod0 += mesh
mesh_lod0 = mesh_lod0.whole_mesh


block_id = 0

mesh = meshes_lod2[block_id].whole_mesh

mesh.calculate_normals()

vmapping, indices, uvs = xatlas.parametrize(mesh.vertices, mesh.triangles)

mesh.vertices = mesh.vertices[vmapping]
mesh.triangles = indices
mesh.normals   = mesh.normals[vmapping]
mesh.uvs       = mesh.uvs[vmapping]
mesh.colors    = mesh.colors[vmapping]
mesh.uv2       = uvs


# os.system("rm AO/*.npy")

itera =  8

cosines = np.cos(np.pi/4) + (1 - np.cos(np.pi/4))*np.random.random(itera)
thetas  = np.arccos(cosines)*180/np.pi

thetas[0:4] = 5

# thetas = np.ones(itera)*45
phis   = np.linspace(0, 2*360 + 0, itera, endpoint = False)


resolution = [2048, 2048]

averaged_AO  = np.zeros(resolution, dtype = np.float32)

weights_sum = -np.ones(resolution, dtype = np.float32)



# dx = 0.1
dx = 0.1

for i in range(itera):
	print("Itera: " + str(i))
	# theta = i*45/itera
	theta, phi = thetas[i], phis[i]

	vertices, normals, triangles = light.rotate_mesh(mesh_lod0, theta, phi)
	cam_direction = np.array([0., 0., 1.])
	# cam_direction = tools.rotate(cam_direction, tools.vec_right, 30*tools.deg2rad)
	# cam_direction = tools.rotate(cam_direction, tools.vec_up, -30*tools.deg2rad)
	sky_dir = tools.rotate(tools.rotate(np.array([0., 0., 1.]), tools.vec_up, phi*tools.deg2rad), tools.vec_right, theta*tools.deg2rad)

	heightmap, extent = modulo.get_heightmap(vertices, triangles, dx = dx)

	# AO = modulo.advanced_ambient_occlusion(heightmap, extent, max_dist = 40., Nsamples = 100, dist_res = 40, sky_dir = sky_dir)
	AO = modulo.advanced_ambient_occlusion(heightmap, extent, max_dist = 0.7, Nsamples = 32, dist_res = 1, sky_dir = sky_dir)
	np.save("AO/itera_" + str(i), AO)

	AO = gaussian_filter(AO, 1)


	vertices, normals, triangles = light.rotate_mesh(mesh, theta, phi)
	heightmap_aux, block_extent = modulo.get_heightmap(vertices, triangles, dx = dx/2)


	quantity = mesh.uv2[:, 0]
	heightmap_aux, map_u, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx/2, optimize = True)

	quantity = mesh.uv2[:, 1]
	heightmap_aux, map_v, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx/2, optimize = True)

	quantity = np.matmul(normals, cam_direction)
	quantity[quantity < 0.] = 0.
	heightmap_aux, cos, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx/2, optimize = True)


	map_color_A = light.interpolate(AO,                 extent, block_extent, map_u.shape)

	texture_A = light.wrap_texture(map_color_A, map_u, map_v, res = resolution)

	weight = light.wrap_texture(cos, map_u, map_v, res = resolution)

	mask_higher = weights_sum < weight
	averaged_AO[mask_higher] = texture_A[mask_higher]

	weights_sum[mask_higher] = weight[mask_higher]


averaged_AO[~np.isfinite(averaged_AO)] = 0.




fake_vertices = np.zeros(shape = vertices.shape, dtype = np.float32)
fake_vertices[:, 0], fake_vertices[:, 1], fake_vertices[:, 2] = mesh.uv2[:, 0], mesh.uv2[:, 1], np.random.random(len(fake_vertices))

heightmap_aux, uv2_mapping, _ = modulo.map_quantity(fake_vertices, np.ones(len(fake_vertices)), triangles, [0., 1., 0., 1.], dx = 0.001)

array = light.interpolate(uv2_mapping, [0., 1., 0., 1.], [0., 1., 0., 1.], averaged_AO.shape)
mask_map = array != 0.

averaged_AO = light.fill_in_holes(averaged_AO, mask_map, 4)


baked_lighting = np.zeros(shape = averaged_AO.shape + (3,), dtype = np.float32)
for i in range(3): baked_lighting[:, :, i] = averaged_AO
plt.imsave("map_textures/block_" + str(block_id) + "_AO.png", baked_lighting)

mesh2 = light.wrap_texture2(mesh, baked_lighting)





