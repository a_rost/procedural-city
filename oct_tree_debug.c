#include <stdio.h>
#include <stdlib.h>
#include <math.h>

extern void generate_oct_tree(float *, unsigned int *, unsigned int , unsigned int , float *);
extern void generate_general_tree(float *, unsigned int *, unsigned int , unsigned int , float *);

extern void triangle_query(unsigned int base_idx, unsigned int depth, unsigned int ntriang_limit, unsigned int *triangle_idxs, unsigned int *ntriang);

extern void raytracing(float *vertices, unsigned int *triangles, unsigned int ntriang, float *starting_points, float *vectors, unsigned int nvectors, int *hits, float *lengths, unsigned int mode);


extern void free_gen_tree();
extern void free_oct_tree();

static inline double Random(){
	return (double)rand()/(double)(RAND_MAX);
}


int main(){
	float extent[6], *vertices, *starting_points, *end_points, *lengths, *lengths_ref;
	int *triangles, *triangle_idxs, nvert, ntriang, ntriang2, *hits, *hits_ref;

	FILE *pfin = fopen("oct_tree_debug.txt", "r");
	fscanf(pfin, "%f %f %f %f %f %f\n", &extent[0], &extent[1], &extent[2], &extent[3], &extent[4], &extent[5]);

	fscanf(pfin, "%d\n", &nvert);
	vertices = (float *)malloc(3*nvert*sizeof(float));
	for(unsigned int i = 0; i < nvert; i++){
		fscanf(pfin, "%f %f %f\n", &vertices[3*i + 0], &vertices[3*i + 1], &vertices[3*i + 2]);
	}
	fscanf(pfin, "%d\n", &ntriang);


	triangles = (unsigned int *)malloc(3*ntriang*sizeof(unsigned int));
	for(unsigned int i = 0; i < ntriang; i++){
		fscanf(pfin, "%d %d %d\n", &triangles[3*i + 0], &triangles[3*i + 1], &triangles[3*i + 2]);
	}
	fclose(pfin);

	generate_general_tree(vertices, triangles, ntriang, 6*3, &extent[0]);


	generate_oct_tree(vertices, triangles, ntriang, 5, &extent[0]);

	// triangle_idxs = (unsigned int *) malloc(10000*sizeof(unsigned int));
	// triangle_query(0, 0, 10000, triangle_idxs, &ntriang2);



	unsigned int nrays = 100000;
	starting_points = (float *) malloc(3*nrays*sizeof(float));
	end_points      = (float *) malloc(3*nrays*sizeof(float));

	lengths_ref  = (float *) malloc(1*nrays*sizeof(float));
	lengths      = (float *) malloc(1*nrays*sizeof(float));

	hits         = (int *) malloc(nrays*sizeof(int));
	hits_ref     = (int *) malloc(nrays*sizeof(int));

	for(unsigned int i = 0; i < nrays; i++){
		for(unsigned int j = 0; j < 3; j++){
			starting_points[3*i + j] = extent[2*j] + (extent[2*j + 1] - extent[2*j])*Random();
			end_points[3*i + j]      = extent[2*j] + (extent[2*j + 1] - extent[2*j])*Random();
		}
	}

	printf("To raytrace 1\n");
	raytracing(vertices, triangles, ntriang, starting_points, end_points, nrays, hits_ref, lengths_ref, 0);
	printf("To raytrace 2\n");
	raytracing(vertices, triangles, ntriang, starting_points, end_points, nrays, hits,         lengths, 2);

	printf("Finished raytracing\n");

	unsigned int errors = 0;
	for(unsigned int i = 0; i < nrays; i++){
		if(hits_ref[i] != hits[i]){
			errors += 1;
			printf("hits_ref = %d, hits = %d, %f, %f\n", hits_ref[i], hits[i], lengths_ref[i], lengths[i]);
		}

	}
	printf("percentage of errors: %f, %d out of %d\n", errors*100.f/nrays, errors, nrays);

	free(triangles);
	free(vertices);
	// free(triangle_idxs);
	free(starting_points);
	free(end_points);
	free(hits);
	free(lengths);
	free_gen_tree():
	free_oct_tree();
}

//gcc -c -g -O0 -fPIC program.c -o program.o -lm -fopenmp -std=c99
//gcc oct_tree_debug.c program.o -o debug.x -lm -fopenmp -g
