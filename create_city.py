#### export LD_PRELOAD=/usr/lib64/libstdc++.so.6
#### ipython

import numpy as np
import matplotlib.pyplot as plt
import modulo
import manzana
import tools
import time
import structure_generation as gen
from importlib import reload
import os
from render_city import assign_color

plt.ion()
laptop = False


if laptop:
	path = "/run/media/agustin/Projects/unity-game/"
else:
	path = "/home/agustin/unity-game/"
	path = "/home/agustin/unity-game_HDRP/"
	path = "/media/agustin/Windows-SSD/backup_legion/unity-game_HDRP/"
	path = "/media/agustin/Windows-SSD/backup_legion/My_project/"
	path = "output/"

seed = np.random.randint(500)*0

def list2str(list_set):
	array = np.array(list_set)
	
	string = ""
	for i in array:
		string += str(i) + " "
	#string += "\n"
	return string

def write_cubes2(block, mesh, idx, lod):
	if os.path.isdir("data/blocks") == False:
		os.mkdir("data/blocks")
	filename = "data/blocks/block_" + str(idx) + "_lod_" + str(lod) + ".txt"
	archivo = open(filename, "w")

	# archivo.write("block " + str(0) +  " " + str(block["center_x"]) + " " + str(block["center_y"]) + "\n")

	### Mesh
	archivo.write("Mesh\n")
	n_materials = len(mesh.materials)
	archivo.write("materials: " + str(n_materials) + "\n")

	for material in mesh.materials:
		submesh     = mesh.submeshes[material]
		n_vert      = len(submesh.vertices)
		n_triangles = len(submesh.triangles)

		archivo.write(material + " vertices: " + str(n_vert) + " triangles: " + str(n_triangles) + "\n")
		for i in range(n_vert):
			vertex, uv, uv2, color, normal = submesh.vertices[i], submesh.uvs[i], submesh.uv2[i], submesh.colors[i], submesh.normals[i]

			string = list2str([vertex[0], vertex[1], vertex[2], normal[0], normal[1], normal[2], uv[0], uv[1], uv2[0], uv2[1], color[0], color[1], color[2], color[3]])
			archivo.write(string + "\n")
		for i in range(n_triangles):
			triangle = submesh.triangles[i]
			string = list2str([triangle[0], triangle[1], triangle[2]])
			archivo.write(string + "\n")

	archivo.close()
	os.system("sed -i 's/\./,/g' " + filename)

def check_if_exists(idx, lod):
	filename = "data/blocks/block_" + str(idx) + "_lod_" + str(lod) + ".txt"
	return os.path.isfile(filename)

def change_characters():
	os.system("sed -i 's/\./,/g' " + path + "Assets/Scripts/cubes.txt")
	os.system("sed -i 's/\./,/g' " + path + "Assets/Resources/map.txt")

buildings = []




def create_recipes(layout):
	tools.initialize_maps(layout)
	manzana.initialize_maps(layout, distortion = 0.14)

	distortion_function = manzana.transform_coords

	street_width = 10.
	blocks  = []
	N, M = len(layout[0]), len(layout[1])

	n_blocks = N*M
	time_start = time.time()
	offset_x = street_width
	extent = [0., np.sum(layout[0]) + street_width*len(layout[0]), 0., np.sum(layout[1]) + street_width*len(layout[1])]

	for n in range(N):
		size_x = layout[0][n]
		offset_y = street_width
		for m in range(M):
			print(n, "of ", N, m, "of ", M)

			block_mesh, block_objects, block_trees, block_facades, building_info = tools.Mesh(), gen.Objects(), gen.Trees(), gen.Facades(), []

			size_y = layout[1][m]

			block_origin = [offset_x, offset_y, 0.]
			xcoord, ycoord, terrains, parcels = manzana.generate_manzana([size_x, size_y], block_origin)
			recipes = []
			manzana_shape, sidewalk_shape, street_shape = manzana.get_manzana_shapes(xcoord, ycoord, block_origin)

			sidewalk_mesh, sidewalk_objects, sidewalk_trees = gen.generate_block_structures(manzana_shape, sidewalk_shape, street_shape)
			sidewalk_mesh.pivot(np.zeros(3), 0., np.array([0., 0., 0.]))
			block_mesh += sidewalk_mesh
			#block_trees.merge(sidewalk_trees)
			block_objects.merge(sidewalk_objects)

			for i in range(len(parcels)):
				parcel = parcels[i]

				#parcel_position = [parcel.left_corner[0] + offset_x - size_x/2., parcel.left_corner[1] + offset_y - size_y/2.]
				parcel_position = [parcel.left_corner[0] + offset_x, parcel.left_corner[1] + offset_y]

				props = tools.determine_map_properties(extent, parcel_position)
				parcel.properties = props
				time0 = time.time()
				recipe   = gen.manage_buildings(parcel)
				recipes.append(recipe)

			print("Parcels: " + str(len(parcels)))
			print("Recipes: " + str(len(recipes)))

			block = {"center_x": 0., "center_y": 0., "mesh": block_mesh, "objects": block_objects, "trees": block_trees, "facades": block_facades, "recipes": recipes, "parcels": parcels}
			blocks.append(block)

			offset_y += size_y + street_width
		offset_x += size_x + street_width

	return blocks

def create_city2(blocks, lod = 0, mask = None, save = False):
	# mesh = tools.Mesh()
	generate = False
	for i in range(len(blocks)):
		block_mesh = tools.Mesh()
		block = blocks[i]
		if type(mask) == type(None):
			generate = True
		else:
			generate = mask[i]

		exists = check_if_exists(i, lod)

		if generate and exists == False:
			nterrains = len(block["parcels"])
			block_mesh += block["mesh"]
			for j in range(nterrains):
				parcel = block["parcels"][j]

				np.random.seed(0)
				modulo.set_seed()
				try:
					print("i: ", i, " j: ", j)

					building_mesh = gen.generate_structure(block["recipes"][j], block["shape_info"][j], lod = lod)

					building_mesh.pivot   (np.zeros(3), parcel.rotation*np.pi/180., parcel.left_corner + parcel.block_origin)
				except:
					print("j: ", j, " failed")
					building_mesh = tools.Mesh()
					pass

				block_mesh += building_mesh
			print("i: ", i, " j: ", j, "N triangles: ", block_mesh.whole_mesh.triangles.shape)
			# mesh += block_mesh

			block_mesh.generate_uv2()
			write_cubes2(block, block_mesh, i, lod)
			np.save("data/blocks/mesh_block_" + str(i) + "_lod" + str(lod), block_mesh)
	# return mesh

def create_file_list(blocks):
	nblocks = len(blocks)
	archivo = open("data/blocks/file_list.txt", "w")
	archivo.write("nblocks " + str(nblocks) + "\n")
	archivo.write("block lod0 lod1 lod2\n")
	for idx in range(nblocks):
		lod0 = check_if_exists(idx, 0)
		lod1 = check_if_exists(idx, 0)
		lod2 = check_if_exists(idx, 0)

		center = blocks[idx]["mesh"].whole_mesh.vertices.mean(axis = 0)
		archivo.write(str(idx) + " " + str(int(lod0)) + " " + str(int(lod1)) + " " + str(int(lod2)) + " " + str(center[0]) + " " + str(center[1]) + "\n")

	archivo.close()


layout = [[120, 80, 100, 120, 110, 120, 112, 110, 110, 120][0:1], [120, 112, 130, 140, 60, 90, 100, 90, 150, 120][0:1]]

if True:
	blocks = create_recipes(layout = layout)
	for block in blocks:
		block["shape_info"] = []
		for idx in range(len(block["recipes"])):
			shape_info = gen.generate_shape_information(block["recipes"][idx], block["parcels"][idx])
			block["shape_info"].append(shape_info)

	np.save("data/blocks", blocks)

else:
	blocks = np.load("data/blocks.npy", allow_pickle = True)

modulo.set_seed()


I, J = np.meshgrid(np.arange(len(layout[0])), np.arange(len(layout[1])))
dist = ((I - np.arange(len(layout[0])).mean())**2 + (J - np.arange(len(layout[1])).mean())**2)**0.5

# blocks = create_recipes(layout = layout)
for block in blocks:
	block["shape_info"] = []
	for idx in range(len(block["recipes"])):
		shape_info = gen.generate_shape_information(block["recipes"][idx], block["parcels"][idx])
		block["shape_info"].append(shape_info)

mask0 = dist.flatten() <= 2.
mask1 = dist.flatten() <= 4.
mask2 = dist.flatten() <= 6

create_city2(blocks, lod = 2, mask = None)
create_city2(blocks, lod = 1, mask = None)
create_city2(blocks, lod = 0, mask = None)


create_file_list(blocks)



#
#
# idx = 1
# mesh = gen.generate_structure(block["recipes"][idx], block["shape_info"][idx], lod = 0)

