import os
import numpy as np
import time

import numpy.ctypeslib as npct
from ctypes import c_int, c_float

array_1d_float = npct.ndpointer(dtype=np.float32, ndim=1, flags='C_CONTIGUOUS')
array_1d_int   = npct.ndpointer(dtype=np.int32,   ndim=1, flags='C_CONTIGUOUS')


os.system("nvcc -arch=sm_89 --compiler-options '-fPIC' -o raytracing.so --shared raytracing.cu")

libcd = npct.load_library("raytracing.so", ".")

deg2rad = np.pi/180

libcd.generate_general_tree.restype = None
libcd.generate_general_tree.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float]

libcd.free_gen_tree.restype = None
libcd.free_gen_tree.argtypes = []

libcd.raytracing.restype = None
libcd.raytracing.argtypes = [array_1d_float, array_1d_int, c_int, array_1d_float, array_1d_float, c_int, array_1d_int, array_1d_float, c_int]


def raytracing(vertices, triangles, starting_points, end_points, mode = 2):
	array_vert = np.ascontiguousarray(vertices.flatten(),         dtype = np.float32)
	array_start = np.ascontiguousarray(starting_points.flatten(), dtype = np.float32)
	array_end   = np.ascontiguousarray(end_points.flatten(),      dtype = np.float32)
	array_tris  = np.ascontiguousarray(triangles.flatten(),       dtype = np.int32)
	ntriang     = np.int32(len(triangles))
	nvectors    = np.int32(len(starting_points))

	array_hits    = np.zeros(nvectors, dtype = np.int32)
	array_lengths = np.zeros(nvectors, dtype = np.float32)
	libcd.raytracing(array_vert, array_tris, ntriang, array_start, array_end, nvectors, array_hits, array_lengths, np.int32(mode))
	return array_hits, array_lengths


def create_gen_tree(vertices, triangles, extent, depth = 6):
	vertices_array  = np.ascontiguousarray(vertices.flatten(), dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)
	ntriang = np.int32(len(triangles))

	libcd.generate_general_tree(vertices_array, triangles_array, ntriang, np.int32(depth), np.float32(extent))


def free_gen_tree():
	libcd.free_gen_tree()
