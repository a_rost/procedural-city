#### export LD_PRELOAD=/usr/lib64/libstdc++.so.6
#### ipython

import numpy as np
import matplotlib.pyplot as plt
import modulo
import manzana
import tools
import time
import structure_generation as gen
from importlib import reload
import os

plt.ion()
laptop = False


def plot_city(blocks):
	import copy
	city_mesh = tools.Mesh()
	for block in blocks:
		block_mesh = copy.deepcopy(block["mesh"])

		block_mesh.pivot(np.zeros(3), 0., [block["center_x"], block["center_y"], 0.])
		city_mesh += block_mesh

	return city_mesh


layout = [[120], [120]]


seed = np.random.randint(500)



buildings = []

tools.initialize_maps(layout)
manzana.initialize_maps(layout, distortion = 0.1)

distortion_function = manzana.transform_coords


np.random.seed(seed)

street_width = 10.
blocks = []
N, M = len(layout[0]), len(layout[1])

n_blocks = N*M
time_start = time.time()

extent = [0., np.sum(layout[0]) + street_width, 0., np.sum(layout[1]) + street_width]

size_x, size_y = layout[0][0], layout[1][0]
offset_y = offset_x = street_width

block_mesh, block_objects, block_trees, block_facades, building_info = tools.Mesh(), gen.Objects(), gen.Trees(), gen.Facades(), []
			
			
block_origin = [offset_x, offset_y, 0.]
# manzana.maximum_width, manzana.minimum_width = 22., 10.
xcoord, ycoord, terrains, parcels = manzana.generate_manzana([size_x, size_y], block_origin)
manzana_shape, sidewalk_shape, street_shape = manzana.get_manzana_shapes(xcoord, ycoord, block_origin)

sidewalk_mesh, sidewalk_objects, sidewalk_trees = gen.generate_block_structures(manzana_shape, sidewalk_shape, street_shape)
sidewalk_mesh.pivot(np.zeros(3), 0., np.array([0., 0., 0.]))
block_mesh += sidewalk_mesh
block_objects.merge(sidewalk_objects)


if False:
	reload(gen)
	for i in range(len(parcels)):
		parcel = parcels[i]

		parcel_position = [parcel.left_corner[0] + offset_x, parcel.left_corner[1] + offset_y]

		props = tools.determine_map_properties(extent, parcel_position)
		parcel.properties = props

		recipe   = gen.manage_buildings(parcel)
		building = gen.generate_appartment(recipe, parcel, only_meshes = False)
		buildings.append(building)

		building.mesh.pivot   (np.zeros(3), parcel.rotation*np.pi/180., parcel.left_corner + parcel.block_origin)
		building.objects.pivot(np.zeros(3), parcel.rotation*np.pi/180., parcel.left_corner + parcel.block_origin)
		building.facades.pivot(np.zeros(3), parcel.rotation*np.pi/180., parcel.left_corner + parcel.block_origin)
		building.trees.pivot  (np.zeros(3), parcel.rotation*np.pi/180., parcel.left_corner + parcel.block_origin)

		block_mesh += building.mesh
		block_objects.merge(building.objects)
		block_trees.merge(building.trees)
		block_facades.merge(building.facades)


		building_info.append(building)

	new_mesh = tools.Mesh()
	if True:
		for item in block_mesh.submeshes.values():
			new_mat = gen.replace_material(item.material, False)

			item.material = new_mat
			new_mesh += item.get_mesh()
	new_mesh.calculate_normals()

	new_mesh.remove_empty()
	block = {"center_x": 0., "center_y": 0., "mesh": new_mesh, "objects": block_objects, "trees": block_trees, "facades": block_facades, "buildings": building_info, "parcels": parcels}
	blocks.append(block)

	city_mesh = plot_city(blocks)
	city_mesh.whole_mesh.plot("asd")




def show_recipe(recipe):
	print("Back length: ", recipe.back_length)
	print("Back offset: ", recipe.back_offset)
	print("Back struct: ", recipe.back_structures)
	print("Balconizable", recipe.balconyzable)
	print("Build type: ", recipe.building_type)
	print("floors: ", recipe.floors)
	print("Kind: ", recipe.kind)
	print("Main block size: ", recipe.main_block_size)
	print("Main block width: ", recipe.main_block_width)
	print("terraceable: ", recipe.terraceable)
	print("yards: ", recipe.yards)
# show_recipe(recipe)

if False:
	reload(gen)
	parcel_idx = 12
	parcel = parcels[parcel_idx]

	parcel_position = [parcel.left_corner[0] + offset_x, parcel.left_corner[1] + offset_y]
	props = tools.determine_map_properties(extent, parcel_position)
	parcel.properties = props

	recipe   = gen.manage_buildings(parcel)

	building = gen.building_collection()

	whole_shapes, built_shapes, levels, validity = gen.build_appartment_shapes(recipe, parcel)

	max_reached = max(levels)
	shape_info = gen.building_shape(built_shapes, whole_shapes, levels, parcel.is_corner, parcel.corner_direction, parcel.medianera)
	if levels.max() == max_reached and levels.max() >= recipe.floors*recipe.floor_height and validity:
		shapes_success = True

	shape_info.determine_facets()
	gen.process_facets(shape_info)
	gen.classify_appartment_facets(shape_info, recipe)
	gen.plot_hfacets(shape_info)
	show_recipe(recipe)



	medianera_mesh = gen.generate_medianera(shape_info)
	building_mesh  = gen.create_mesh(shape_info, recipe)
	terraces_mesh  = gen.generate_terraces(shape_info, recipe)
	roof_mesh      = gen.generate_roof(shape_info, recipe)
	object_meshes, things, trees = gen.populate_facets(shape_info, recipe)
	building.mesh += building_mesh + medianera_mesh + terraces_mesh + roof_mesh + object_meshes

	facades = gen.generate_appartment_facades(shape_info, recipe)
	doors   = gen.generate_appartment_doors(shape_info, recipe)

	if False:
		facades_mesh   = gen.generate_facade_mesh(facades)
		facades_mesh  += gen.generate_facade_mesh(doors)
	else:
		facades_mesh   = gen.generate_facade_mesh2(shape_info)

	objects_mesh   = gen.generate_objects_mesh(things)

	building.mesh += objects_mesh + facades_mesh

	building.mesh.plot()


#
# hs = []
# for facet in shape_info.facets:
# 	if facet.kind == "vertical":
# 		if facet.min_height >= 40.2:
# 			hs.append(facet)
#
#
