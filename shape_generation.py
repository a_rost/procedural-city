import tools
import numpy as np

def place_yards(linestring, yard_width, yard_length, yard_sep, n_rep, offset, flip = False):
	perimeter_length = tools.LineString(linestring).length
	total_length   = yard_sep

	patios = []
	for i in range(n_rep):
		if flip == False:
			position, direction = tools.position_along_polygon(linestring, offset + total_length*i)
			patio = tools.create_box(position, [yard_length, yard_width*2.], rotation = np.arctan2(direction[1], direction[0]))
		else:
			position, direction = tools.position_along_polygon(linestring, perimeter_length - offset - total_length*i)
			patio = tools.create_box(position, [yard_length, yard_width*2.], rotation = np.arctan2(-direction[1], -direction[0]))

		if i*total_length <= perimeter_length:
			patios.append(patio)
	return tools.unary_union(patios)


def place_yards2(shape, point):
	ring = tools.get_rings(tools.orient(shape))[0]

	idx = 0
	for j in range(len(ring)):
		segment = ring[j]
		if np.sqrt((segment[0] - point[0])**2 + (segment[1] - point[1])**2) <= 0.001:
			idx = j
	ring = np.roll(ring, -idx, axis = 0)

	indexes = np.unique(ring, axis = 0, return_index=True)[1]
	indexes.sort()
	ring = list(ring[indexes][1:])
	ring.append(np.array([0., 0.]))

	linestring = tools.LineString(ring).coords
	return linestring

def position_point_polygon(shape):
	success, pos = False, np.zeros(2)
	if shape.type in ["Polygon", "MultiPolygon"]:
		extent, center, width, length = tools.determine_extent(shape)

		ntrials = 50
		for i in range(ntrials):
			x = np.random.random()*(extent[1] - extent[0]) + extent[0]
			y = np.random.random()*(extent[3] - extent[2]) + extent[2]
			if shape.contains(tools.Point([x, y])):
				success = True
				pos = np.array([x, y])
				break
	if success == False:
		print("Failed to find inner point")
	return success, pos


debug_swell, debug_orig, debug_spawn = 0, 0, 0

def attach_shape(original, size, orientation = 0.):
	global debug_swell, debug_orig, debug_spawn
	shrunk_size = original.buffer(-size/4., join_style = 2)
	swell_size  = original.buffer(size/4., join_style = 2)
	#swell_size  = original

	spawnable_area = swell_size.difference(shrunk_size)

	debug_swell, debug_orig, debug_spawn = swell_size, original, spawnable_area

	success, position = position_point_polygon(spawnable_area)
	if success:
		new_shape = tools.create_box(position, [size, size], orientation)
	else:
		new_shape = tools.Polygon()
	return tools.unary_union([new_shape, original])


def generate_shape(terrain, params, N = 3, passages_check = True, orientation = 0.0):
	extent, center, width, length = tools.determine_extent(terrain)
	success = False

	min_sizeX, max_sizeX, mean_sizeX, std_sizeX, min_sizeY, max_sizeY, mean_sizeY, std_sizeY, std_locX, std_locY = params(width, length)
	centroid = terrain.centroid.coords[0]
	for j in range(10):
		main_sizeX = tools.suggest_value(min_sizeX, max_sizeX, mean_sizeX, std_sizeX, 0.5)
		main_sizeY = tools.suggest_value(min_sizeY, max_sizeY, mean_sizeY, std_sizeY, 0.5)

		#pos_x = extent[0] + tools.suggest_value(main_sizeX/2.,  width - main_sizeX/2.,  width/2.,  width/4., 0.1)
		#pos_y = extent[2] + tools.suggest_value(main_sizeY/2., length - main_sizeY/2., length/2., length/6., 0.1)
		pos_x = tools.suggest_value(centroid[0] - main_sizeX/2., centroid[0] + main_sizeX/2., centroid[0], std_locX, 0.5)
		pos_y = tools.suggest_value(centroid[1] - main_sizeY/2., centroid[1] + main_sizeY/2., centroid[1], std_locY, 0.5)

		# orientation = np.pi/4. if tools.pick_bool(rotation_probability) else 0.

		caja = tools.create_box([pos_x, pos_y], [main_sizeX, main_sizeY], orientation)
		caja = caja.intersection(terrain)

		if caja.area != 0.:
			min_size = min(main_sizeX, main_sizeY)

			for i in range(N):
				caja = attach_shape(caja, tools.suggest_value(min_size/1.5, min_size, 5., 1.5, 1), orientation)
				caja = caja.intersection(terrain)

			caja_inner = caja.buffer(-1.0, join_style = 2)
			shape      = (caja_inner.buffer(1.0, join_style = 2)).intersection(terrain)

			if passages_check:
				for i in range(2):
					patios = terrain.difference(shape)
					patios_inner = patios.buffer(-0.5, join_style = 2)
					passages = ((terrain.exterior).difference(shape)).buffer(1.5, join_style = 2, cap_style = 2)


					patios = tools.unary_union([patios_inner.buffer(0.5, join_style = 2), passages])

					if patios.area == 0.:
						success = False
						break

					shape  = terrain.difference(patios)

					shape_inner = shape.buffer(-1., join_style = 2)
					if shape_inner.area == 0.:
						success = False
						break

					shape       = (shape_inner.buffer(1., join_style = 2)).intersection(terrain)

			shape = tools.choose_biggest(shape)

			shape = tools.adjust_shape(shape, 1.0)

			if shape.area >= 0.5**2 and shape.area <= terrain.area*0.95:
				success = True
				break

	#if j == 10:
		#print("Failed")
	#else:
		#print("Success " + str(j) + " attempts")
	if success == False: shape = tools.Polygon()

	return success, shape



debug_objects = 0
def generate_shape2(terrain, params, N = 3, orientation = 0.):
	global debug_objects
	if terrain.type in ["Polygon", "MultiPolygon"]:
		extent, center, width, length = tools.determine_extent(terrain)
		success = False

		min_sizeX, max_sizeX, mean_sizeX, std_sizeX, min_sizeY, max_sizeY, mean_sizeY, std_sizeY, std_locX, std_locY = params(width, length)
		centroid = terrain.centroid.coords[0]
		for j in range(10):
			main_sizeX = tools.suggest_value(min_sizeX, max_sizeX, mean_sizeX, std_sizeX, 1.0)
			main_sizeY = tools.suggest_value(min_sizeY, max_sizeY, mean_sizeY, std_sizeY, 1.0)

			pos_x = tools.suggest_value(centroid[0] - main_sizeX/2., centroid[0] + main_sizeX/2., centroid[0], std_locX, 1.0)
			pos_y = tools.suggest_value(centroid[1] - main_sizeY/2., centroid[1] + main_sizeY/2., centroid[1], std_locY, 1.0)

			caja = tools.create_box([pos_x, pos_y], [main_sizeX, main_sizeY], orientation)

			debug_objects = caja, terrain
			caja = caja.intersection(terrain)

			if caja.area != 0.:
				min_size = min(main_sizeX, main_sizeY)

				for i in range(N):
					caja = attach_shape(caja, tools.suggest_value(min_size/1.5, min_size, 5., 1.5, 1), orientation)
					caja = caja.intersection(terrain)

				caja_inner = caja.buffer(-1.0, join_style = 2)
				shape      = (caja_inner.buffer(1.0, join_style = 2)).intersection(terrain)

				for i in range(2):
					terrain_rings = tools.get_rings(terrain)
					terrain_perimeter = tools.MultiLineString(terrain_rings)

					patios = terrain.difference(shape)
					patios_inner = patios.buffer(-0.5, join_style = 2)
					passages = (terrain_perimeter.difference(shape)).buffer(1.5, join_style = 2, cap_style = 2)

					patios = tools.unary_union([patios_inner.buffer(0.5, join_style = 2), passages])

					if patios.area == 0.:
						success = False
						break

					shape  = terrain.difference(patios)

					shape_inner = shape.buffer(-1., join_style = 2)
					if shape_inner.area == 0. or shape_inner.is_valid == False:
						success = False
						break

					shape       = (shape_inner.buffer(1., join_style = 2)).intersection(terrain)

				shape = tools.choose_biggest(shape)

				shape = tools.adjust_shape(shape, 1.0)
				if shape.area >= 0.5*terrain.area and shape.area <= terrain.area*0.95 and shape.is_valid:
					success = True
					break


		if success == False: shape = tools.Polygon()

		return success, shape
	else:
		#print("Trying to generate shape in empty shape")
		return False, tools.Polygon()



def better_combine(terrain, shapes_unite, levels_unite, shapes_substract, levels_substract):
	to_unite     = list.copy(shapes_unite)
	to_substract = list.copy(shapes_substract)

	all_levels = levels_unite + levels_substract
	all_shapes = shapes_unite + shapes_substract
	unite_substract = np.array([1]*len(shapes_unite) + [-1]*len(shapes_substract))

	mask_unite     = unite_substract ==  1
	mask_substract = unite_substract == -1
	unique_levels  = np.unique(all_levels)

	idxs = np.arange(len(all_levels))
	unique_unite, unique_substract = [], []
	for level in unique_levels:
		mask = abs(all_levels - level) <= 0.001

		repeated_unite     = idxs[mask*mask_unite]
		repeated_substract = idxs[mask*mask_substract]

		current_unite, current_substract = [], []
		for i in repeated_unite:
			current_unite.append(all_shapes[i])

		for i in repeated_substract:
			current_substract.append(all_shapes[i])

		unique_unite.append(tools.unary_union(current_unite))
		unique_substract.append(tools.unary_union(current_substract))

	nlevels    = len(unique_levels)
	arg_sorted = np.argsort(unique_levels)

	unite_shapes = [""]*nlevels
	yards_shapes = [""]*nlevels
	levels       = np.zeros(nlevels)

	shape_above = tools.Polygon()
	for i in range(nlevels - 1, -1, -1):
		index                  = arg_sorted[i]

		unite_shapes[i]  = tools.orient(tools.unary_union([unique_unite[index], shape_above]))
		levels[i]        = unique_levels[index]

		shape_above = unite_shapes[i]

	below_shape = tools.Polygon()

	for i in range(nlevels):
		index = arg_sorted[i]
		yards_shapes[i] = tools.orient(tools.unary_union([unique_substract[index], below_shape]))
		below_shape     = yards_shapes[i]


	whole_shapes, built_shapes = [""]*nlevels, [""]*nlevels

	for i in range(nlevels):
		if i > 0:
			whole_shape = unite_shapes[i].difference(yards_shapes[i - 1])
		else:
			whole_shape = unite_shapes[i]
		whole_shapes[i] = whole_shape.intersection(terrain)

	for i in range(nlevels):
		if i != nlevels - 1:
			built_shapes[i] = whole_shapes[i + 1]
		else:
			built_shapes[i] = tools.Polygon()

	return whole_shapes, built_shapes, list(unique_levels)

def get_dimensions(shape):
	if shape.area != 0.:
		perimeter = np.array(tools.get_rings(shape.minimum_rotated_rectangle)[0])
		A = perimeter[1] - perimeter[0]
		B = perimeter[2] - perimeter[1]
		a, b = np.linalg.norm(A), np.linalg.norm(B)
		length, width = max(a, b), min(a, b)

	else:
		length = width = 0.
	return length, width

def check_shapes(shape):
	if shape.type == "Polygon":
		length, width = get_dimensions(shape)
		if shape.area >= 2**2 and width >= 2.5:
			return shape
		else:
			return tools.Polygon()

	elif shape.type == "MultiPolygon":
		shapes = []
		for minishape in shape:
			shapes.append(check_shapes(minishape))
		return tools.MultiPolygon(shapes)

	else:
		return tools.Polygon()


def correct_shapes(whole_shapes, levels):
	rounded = tools.pick_bool(0.0)
	join_style = 3 if rounded else 2

	n_levels = len(whole_shapes)

	new_whole, new_built, new_levels = [], [], []

	to_intesect = whole_shapes[0]

	new_whole.append(to_intesect)
	new_levels.append(levels[0])
	for i in range(1, n_levels):
		difference = to_intesect.difference(whole_shapes[i])
		patios = difference.buffer(-0.5, join_style = join_style)
		patios = patios.buffer( 0.5, join_style = join_style)

		shape = to_intesect.difference(patios.buffer(1e-7))

		# shape = tools.adjust_shape(shape, -0.5)
		shape = tools.adjust_shape(shape, 1.4995)
		shape = shape.intersection(to_intesect)

		shape = tools.orient(shape)
		shape = check_shapes(shape)

		shape = tools.remove_junk_segments(shape)
		if shape.area == 0.:
			break
		new_whole.append(shape)
		new_levels.append(levels[i])
		new_built.append(shape)

		to_intesect = shape

	new_built.append(tools.Polygon())
	return new_whole, new_built, np.array(new_levels)
