import tools
import modulo
from importlib import reload
import classes
import numpy as np

def assign_color(mesh):
	count = 0
	for mat in mesh.materials:
		submesh = mesh.submeshes[mat]
		nverts  = len(submesh.colors)
		color   = submesh.colors[0]
		new_color = np.ones(3)
		update_color = False
		if mat == "Wall":
			print("option 1")
			# new_color = (np.random.random(3) + np.ones(3))/2.
			update_color = True
			# new_color = np.ones(3)*0.8
		elif mat == "Asphalt":
			new_color = np.ones(3)*0.1
		elif mat in ["Normal_Interior", "Curtains_Interior", "Blind_Interior", "Glass"]:
			new_color = np.ones(3)*0.2
		elif mat in classes.mat_wall_rich or mat in classes.mat_wall_poor:
			print("option 2")
			new_color = np.array([252, 73, 3])/256
		elif mat in classes.mat_floor_rich or mat in classes.mat_floor_poor:
			print("option 3")
			update_color = True
			# new_color = np.array([1., 0., 0.])
		elif mat in classes.mat_metal_poor or mat in classes.mat_metal_rich:
			print("option 4")
			new_color = np.array([0.3, 0.3, 0.3])
		elif mat in classes.mat_terr_poor or mat in classes.mat_terr_rich:
			print("option 5")
			new_color = np.array([191, 110, 29])/256
		else:
			print("option 0 " + mat)


		if update_color:
			mesh.whole_mesh.colors[count:count + nverts, 0] = mesh.submeshes[mat].colors[:, 0]
			mesh.whole_mesh.colors[count:count + nverts, 1] = mesh.submeshes[mat].colors[:, 1]
			mesh.whole_mesh.colors[count:count + nverts, 2] = mesh.submeshes[mat].colors[:, 2]
			mesh.whole_mesh.colors[count:count + nverts, 3] = 1.
		else:
			mesh.whole_mesh.colors[count:count + nverts, 0] = new_color[0]
			mesh.whole_mesh.colors[count:count + nverts, 1] = new_color[1]
			mesh.whole_mesh.colors[count:count + nverts, 2] = new_color[2]
			mesh.whole_mesh.colors[count:count + nverts, 3] = 1.
		count += nverts
	print(count, len(mesh.whole_mesh.colors))
# assign_color(city_mesh)
# city_mesh.whole_mesh.plot("coso")

#
# # mask = city_mesh.mat_ids == 5
# tri_offset = 0
# for mat in city_mesh.materials:
# 	ntriang = len(city_mesh.submeshes[mat].triangles)
# 	if mat == "Wall":
# 		break
#
# 	tri_offset += ntriang
#
# idx1 = city_mesh.whole_mesh.triangles[:, 0]
# idx2 = city_mesh.whole_mesh.triangles[:, 1]
# idx3 = city_mesh.whole_mesh.triangles[:, 2]
#
# np.mean(city_mesh.whole_mesh.colors[idx1[tri_offset: tri_offset + ntriang]], axis = 0)
#
# std_colors = np.zeros(len(city_mesh.whole_mesh.triangles))
#
# for i in range(len(city_mesh.whole_mesh.triangles)):
# 	tri = city_mesh.whole_mesh.triangles[i]
# 	std_colors[i] = np.sum(np.std(city_mesh.whole_mesh.colors[tri], axis = 0))
# 	city_mesh.whole_mesh.colors[tri, 0] = (1 + (-1)**i)/2.
# 	city_mesh.whole_mesh.colors[tri, 1] = (1 + (-1)**i)/2.
# 	city_mesh.whole_mesh.colors[tri, 2] = (1 + (-1)**i)/2.

"""

def get_texture(my_uvs, img):
	material = texture.SimpleMaterial(image=img)
	tex = TextureVisuals(uv=my_uvs, image=img, material=material)
	return tex

def load_texture(my_uvs, name):
	if name in ["Sidewalk", "Stripped_Red", "Stripped_Blue", "Stripped_White"]:
		texture_name = "textures/stripped_tiles_albedo.png"
	elif name in ["Dirty_Poor1", "Bricks", "Smooth_Bricks", "Brick_Rough", "Rough_Shiny", "Brick_Shinny", "Rough_Rricks", "Dirty_Poor2"]:
		texture_name = "textures/rough_bricks_albedo.png"
	elif name in ["Blind_Interior", "Curtains_Interior", "Normal_Interior"]:
		texture_name = "textures/frame_window_albedo.png"
	elif name in ["Roof_Grey", "Rusty_Metal", "Dirty_Roof2", "Rusty_Roof3", "Roof_Black", "Roof_Green", "Rusty_Roof"]:
		texture_name = "textures/rusty_roof_height.png"
	elif name in ["Plastic_Roof"]:
		texture_name = "textures/rusty_roof_metallic.png"
	elif name in ["Roof_Tiles"]:
		texture_name = "textures/rough_tiles_albedo.png"
	elif name in ["Vertex_Shinny", "Vertex_Rough"]:
		texture_name = "textures/tiles_normal.png"
	else:
		texture_name = "textures/rough_bricks_height.png"
	texture = plt.imread(texture_name)
	img = np.ones([texture.shape[0], texture.shape[1], 3], dtype = np.uint8)
	img[:, :, 0] = texture[:, :, 0]/texture[:, :, 0].max()*255
	img[:, :, 1] = texture[:, :, 1]/texture[:, :, 1].max()*255
	img[:, :, 2] = texture[:, :, 2]/texture[:, :, 2].max()*255

	img = Image.fromarray(img)
	sqrWidth = np.ceil(np.sqrt(img.size[0]*img.size[1])).astype(int)
	im_resize = img.resize((sqrWidth, sqrWidth))

	return get_texture(my_uvs, im_resize)


def wrap_texture(mesh):
	new_mesh = Trimesh()
	for mat in mesh.submeshes:
		submesh = mesh.submeshes[mat]
		texture_visual = load_texture(submesh.uvs, submesh.material)
		new_mesh += Trimesh(vertices=submesh.vertices, faces=submesh.triangles, vertex_normals = submesh.normals, visual = texture_visual)

	return new_mesh
"""


