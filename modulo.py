import os
import numpy as np
import time

import numpy.ctypeslib as npct
from ctypes import c_int, c_float

array_1d_float = npct.ndpointer(dtype=np.float32, ndim=1, flags='C_CONTIGUOUS')
array_1d_int   = npct.ndpointer(dtype=np.int32,   ndim=1, flags='C_CONTIGUOUS')


os.system("gcc -c -Ofast -march=native -fPIC program.c -o program.o -lm -fopenmp -std=c99")
os.system("gcc -shared program.o -o module.so -lm -fopenmp -std=c99")


libcd = npct.load_library("module.so", ".")

deg2rad = np.pi/180

libcd.toggle_debug.restype = None
libcd.toggle_debug.argtypes = []

# libcd.free_oct_tree.restype = None
# libcd.free_oct_tree.argtypes = []

libcd.free_gen_tree.restype = None
libcd.free_gen_tree.argtypes = []

libcd.determine_leveled_surface.restype = None
libcd.determine_leveled_surface.argtypes = [array_1d_float, c_int, c_int, c_float, c_float,  array_1d_float]

libcd.set_seed.restype  = None
libcd.set_seed.argtypes = [c_int]

libcd.generate_path.restype = None
libcd.generate_path.argtypes = [array_1d_float, array_1d_float, c_int, array_1d_float]

libcd.load_height_map.restype = None
libcd.load_height_map.argtypes = [array_1d_float, c_int, c_int, array_1d_float]

libcd.create_perlin_2D.restype = None
libcd.create_perlin_2D.argtypes = [c_int, array_1d_float, c_int]

libcd.raycasting.restype = None
libcd.raycasting.argtypes = [array_1d_float, array_1d_float, array_1d_float, c_int, array_1d_float]

libcd.check_values.restype = None
libcd.check_values.argtypes = [array_1d_float, c_int, array_1d_float]

libcd.solver.restype = None
libcd.solver.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float]
#void solver(float *fixed_point, float *initial_velocity, float *initial_position, float *trajectory)

libcd.calculate_normals.restype = None
libcd.calculate_normals.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float, c_int]

#calculate_normals(float *vertices, int *triangles, int nverts, int ntriangs, float *normals, int nthreads)
libcd.calculate_AO.restype = None
libcd.calculate_AO.argtypes = [c_int, c_int, array_1d_float, c_int, c_int, c_float]

libcd.calculate_advanced_AO.restype = None
libcd.calculate_advanced_AO.argtypes = [c_int, c_int, array_1d_float, array_1d_float, c_int, c_int, c_float]

libcd.efficient_AO.restype = None
libcd.efficient_AO.argtypes = [c_int, c_int, array_1d_float, array_1d_float, c_int, c_float]

libcd.determine_triangle_overlap.restype = None
libcd.determine_triangle_overlap.argtypes = [array_1d_float, array_1d_int, c_int, array_1d_int, array_1d_int, array_1d_int]

#
#
#determine_flat_surfaces(float *delta_height, float radius)
def determine_coplanar_surface(vertices, triangles):
	ntriang = np.int32(len(triangles))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)

	surface_count = np.zeros(ntriang, dtype = np.int32)
	nsurfaces     = np.zeros(1, dtype = np.int32)
	organized_triangles = np.zeros(ntriang, dtype = np.int32)

	libcd.determine_triangle_overlap(vertices_array, triangles_array, ntriang, surface_count, nsurfaces, organized_triangles)
	nsurfaces = nsurfaces[0]
	return surface_count[0:nsurfaces], organized_triangles

libcd.determine_flat_surfaces.restype = None
libcd.determine_flat_surfaces.argtypes = [array_1d_float, c_float]

libcd.calculate_surface_exposure.restype = None
libcd.calculate_surface_exposure.argtypes = [c_int, c_int, c_float, c_float, c_float, c_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float]

libcd.raytracing.restype = None
libcd.raytracing.argtypes = [array_1d_float, array_1d_int, c_int, array_1d_float, array_1d_float, c_int, array_1d_int, array_1d_float, c_int, c_int, c_int]

libcd.recursive_search.restype = None
libcd.recursive_search.argtypes = [c_int, c_int, array_1d_int, array_1d_int]

libcd.get_triangle_within_cell_total_count.restype = None
libcd.get_triangle_within_cell_total_count.argtypes = [c_int, c_int, array_1d_int]


libcd.get_triangle_within_cell_count.restype = None
libcd.get_triangle_within_cell_count.argtypes = [c_int, c_int, array_1d_int]

libcd.get_gentree_extent.restype = None
libcd.get_gentree_extent.argtypes = [c_int, c_int, array_1d_float]


libcd.link_triangles.restype = None
libcd.link_triangles.argtypes = [array_1d_float, array_1d_int, c_int, c_float, array_1d_int, array_1d_int, c_int, array_1d_int]

libcd.generate_BVH.restype = None
libcd.generate_BVH.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float, c_int]

libcd.check_bvh.restype = None
libcd.check_bvh.argtypes = [c_int, array_1d_int, array_1d_float]

libcd.stack_quantities_pipeline.restype = None
libcd.stack_quantities_pipeline.argtypes = [array_1d_int, array_1d_int, c_int, c_int, c_int, array_1d_float, array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int, array_1d_float]

libcd.generate_rays_from_heightmap.restype = None
libcd.generate_rays_from_heightmap.argtypes = [array_1d_float, array_1d_float, c_int, c_int, c_int, array_1d_float]

libcd.determine_hit_location.restype = None
libcd.determine_hit_location.argtypes = [array_1d_float, array_1d_int, array_1d_float, array_1d_float, array_1d_int, array_1d_float, c_int, array_1d_float]

def determine_hit_locations(vertices, triangles, starts, ends, hits, lengths):
	N = np.int32(len(starts))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)
	starts_array    = np.ascontiguousarray(starts.flatten(),    dtype = np.float32)
	ends_array      = np.ascontiguousarray(ends.flatten(),      dtype = np.float32)
	hits_array      = np.ascontiguousarray(hits.flatten(),      dtype = np.int32)
	lengths_array   = np.ascontiguousarray(lengths.flatten(),   dtype = np.float32)
	coords_array    = np.zeros(2*N, dtype = np.float32)

	libcd.determine_hit_location(vertices_array, triangles_array, starts_array, ends_array, hits_array, lengths_array, N, coords_array)
	return np.reshape(coords_array, [N, 2])


def generate_rays_from_heightmap(heightmap, extent, block_size = 10):
	heightmap_array = np.ascontiguousarray(heightmap.flatten(),     dtype = np.float32)
	extent_array    = np.ascontiguousarray(extent,     dtype = np.float32)
	N, M = np.int32(len(heightmap)), np.int32(len(heightmap[0]))
	result = np.zeros(N*M*3, dtype = np.float32)
	libcd.generate_rays_from_heightmap(heightmap_array, extent_array, N, M, np.int32(block_size), result)
	return np.reshape(result, [N*M, 3])

def stack_quantities_pipeline(resolutions, mat_ids, nmaterials, block_id, map_u, map_v, map_mat, map_block, value, result, nthreads = 18):
	resolutions_array  = np.ascontiguousarray(resolutions,  dtype = np.int32)
	mat_ids_array      = np.ascontiguousarray(mat_ids,      dtype = np.int32)


	u_array     = np.ascontiguousarray(map_u.flatten(),     dtype = np.float32)
	v_array     = np.ascontiguousarray(map_v.flatten(),     dtype = np.float32)
	mat_array   = np.ascontiguousarray(map_mat.flatten(),   dtype = np.float32)
	block_array = np.ascontiguousarray(map_block.flatten(), dtype = np.float32)
	value_array = np.ascontiguousarray(value.flatten(),     dtype = np.float32)

	libcd.stack_quantities_pipeline(resolutions_array, mat_ids_array, np.int32(nmaterials), np.int32(nthreads), np.int32(block_id), u_array, v_array, mat_array, block_array, value_array, np.int32(len(u_array)), result)


libcd.free_bvh.restype = None
libcd.free_bvh.argtypes = []

def free_bvh():
	libcd.free_bvh()
#check_bvh(unsigned int cell_id, unsigned int *result, float *extent)

def check_bvh(cell_id):
	result = np.zeros(5, dtype = np.int32)
	extent = np.zeros(6, dtype = np.float32)
	libcd.check_bvh(np.int32(cell_id), result, extent)

	print("cell_tri_count", result[0], "triangle_offset", result[1], "children_count", result[2], "children_offset", result[3], "children", result[4])
	return result, extent

def get_extent_gentree(depth, cell_id):
	result = np.zeros(6, dtype = np.float32)
	libcd.get_gentree_extent(np.int32(depth), np.int32(cell_id), result)
	return result

def link_triangles(vertices, triangles, link_dist = 1., multiplier = 30):
	ntriang = np.int32(len(triangles))
	array_vert  = np.ascontiguousarray(vertices.flatten(),      dtype = np.float32)
	array_tris  = np.ascontiguousarray(triangles.flatten(),     dtype = np.int32)

	N_max = np.int32(multiplier*ntriang)
	result  = np.zeros(N_max, dtype = np.int32)
	count   = np.zeros(ntriang, dtype = np.int32)
	offset  = np.zeros(1, dtype = np.int32)

	libcd.link_triangles(array_vert, array_tris, ntriang, np.float32(link_dist), result, count, N_max, offset)

	return result[0:offset[0]], count


def get_triangle_count(depth, cell_id):
	total_count  = np.zeros(1, dtype = np.int32)
	count        = np.zeros(1, dtype = np.int32)

	libcd.get_triangle_within_cell_total_count(np.int32(depth), np.int32(cell_id), total_count)
	libcd.get_triangle_within_cell_count(np.int32(depth), np.int32(cell_id), count)

	print("Total triangle count within cell:", total_count, " Count at that level:", count)
	return total_count, count

def retrieve_tree_triangles(depth, cell_id):
	n = np.zeros(1, dtype = np.int32)
	libcd.get_triangle_within_cell_total_count(depth, cell_id, n)
	print("to get", n[0], "triangles")
	tri_ids = np.zeros(n[0], dtype = np.int32)
	n[0] = 0
	libcd.recursive_search(depth, cell_id, tri_ids, n)
	return tri_ids



def raytracing(vertices, triangles, starting_points, end_points, mode = 2, nthreads = 18, chunk_size = 500):
	array_vert = np.ascontiguousarray(vertices.flatten(),         dtype = np.float32)
	array_start = np.ascontiguousarray(starting_points.flatten(), dtype = np.float32)
	array_end   = np.ascontiguousarray(end_points.flatten(),      dtype = np.float32)
	array_tris  = np.ascontiguousarray(triangles.flatten(),       dtype = np.int32)
	ntriang     = np.int32(len(triangles))
	nvectors    = np.int32(len(starting_points))

	array_hits    = np.zeros(nvectors, dtype = np.int32)
	array_lengths = np.zeros(nvectors, dtype = np.float32)
	libcd.raytracing(array_vert, array_tris, ntriang, array_start, array_end, nvectors, array_hits, array_lengths, np.int32(mode), np.int32(nthreads), np.int32(chunk_size))
	return array_hits, array_lengths


#void
#void
libcd.cover_surface.restype = None
libcd.cover_surface.argtypes = [array_1d_float, array_1d_int, c_int, c_int, c_float, c_float, c_float, c_float, array_1d_float, array_1d_int, array_1d_int]
#

libcd.fast_area_calculation.restype = None
libcd.fast_area_calculation.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float, c_int]
#

libcd.test_poisson_random.restype = None
libcd.test_poisson_random.argtypes = [c_float, array_1d_int, c_int]
#

libcd.check_triangles.restype = None
libcd.check_triangles.argtypes = [array_1d_float, array_1d_int, array_1d_float, array_1d_int, array_1d_int, c_int, c_int, c_float, array_1d_int]
#

libcd.get_heightmap_from_geometry.restype = None
libcd.get_heightmap_from_geometry.argtypes = [array_1d_float, array_1d_int, c_int, array_1d_float, c_int, c_int, array_1d_float]

libcd.map_quantity_from_geometry.restype = None
libcd.map_quantity_from_geometry.argtypes = [array_1d_float, array_1d_float, array_1d_int, c_int, array_1d_float, array_1d_float, c_int, c_int, array_1d_float, c_int, c_int, c_int]
#

libcd.determine_shadows.restype = None
libcd.determine_shadows.argtypes = [array_1d_float, array_1d_float]

libcd.determine_normals.restype = None
libcd.determine_normals.argtypes = [array_1d_float]

libcd.calculate_indirect_lighting.restype = None
libcd.calculate_indirect_lighting.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_float, c_int, c_float]

libcd.triangle_subdivision.restype = None
libcd.triangle_subdivision.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_int, c_int, array_1d_int, array_1d_float, array_1d_float, array_1d_float, array_1d_int]

libcd.estimate_subdivisions.restype = None
libcd.estimate_subdivisions.argtypes = [array_1d_float, array_1d_int, c_int, c_float, array_1d_int]

libcd.optimize_submesh.restype = None
libcd.optimize_submesh.argtypes = [array_1d_float, array_1d_float, array_1d_float, array_1d_int, c_int, array_1d_int, array_1d_int]

libcd.generate_oct_tree.restype = None
libcd.generate_oct_tree.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float]


libcd.generate_general_tree.restype = None
libcd.generate_general_tree.argtypes = [array_1d_float, array_1d_int, c_int, c_int, array_1d_float, c_int]


libcd.triangle_query.restype = None
libcd.triangle_query.argtypes = [c_int, c_int, c_int, array_1d_int, array_1d_int, array_1d_int]

libcd.check_tree.restype = None
libcd.check_tree.argtypes = [c_int, c_int]

libcd.obtain_triangle_ids_fromcell.restype = None
libcd.obtain_triangle_ids_fromcell.argtypes = [c_int, c_int, array_1d_int, c_int, array_1d_int]

#
#check_tree(unsigned int depth, unsigned int cell_id)
#triangle_query(unsigned int base_idx, unsigned int depth, unsigned int ntriang_limit, unsigned int *triangle_idxs, unsigned int *ntriang)

def check_tree(depth, cell_id):
	libcd.check_tree(np.int32(depth), np.int32(cell_id))

def retrieve_tree_cell(depth, cell_id, max_triang = 50000):
	triangles = np.zeros(max_triang, dtype = np.int32)
	amount    = np.zeros(1, dtype = np.int32)
	libcd.obtain_triangle_ids_fromcell(np.int32(depth), np.int32(cell_id), triangles, np.int32(max_triang), amount)
	return triangles[0:amount[0]]
#
# def free_oct_tree():
# 	libcd.free_oct_tree()


def free_gen_tree():
	libcd.free_gen_tree()

def create_oct_tree(vertices, triangles, extent, depth = 6):
	vertices_array  = np.ascontiguousarray(vertices.flatten(), dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)
	ntriang = np.int32(len(triangles))

	libcd.generate_oct_tree(vertices_array, triangles_array, ntriang, np.int32(depth), np.float32(extent))


def create_gen_tree(vertices, triangles, extent, depth = 6, optimized = True):
	vertices_array  = np.ascontiguousarray(vertices.flatten(), dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)
	ntriang = np.int32(len(triangles))

	opt = 1 if optimized else 0
	libcd.generate_general_tree(vertices_array, triangles_array, ntriang, np.int32(depth), np.float32(extent), np.int32(opt))

def generate_BVH(vertices, triangles, extent, depth = 15, threshold_divide = 10):
	vertices_array  = np.ascontiguousarray(vertices.flatten(), dtype = np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), dtype = np.int32)
	ntriang = np.int32(len(triangles))
	array_extent    = np.ascontiguousarray(extent, dtype = np.float32)

	libcd.generate_BVH(vertices_array, triangles_array, ntriang, np.int32(depth), array_extent, np.int32(threshold_divide))
#




def triangle_query(idx, depth, ntriang_limit = 10000):
	triangle_idxs = np.zeros(ntriang_limit, dtype = np.int32)
	tri_number   = np.zeros(1, dtype = np.int32)

	depth_and_cellid = np.zeros(2*ntriang_limit, dtype = np.int32)
	libcd.triangle_query(np.int32(idx), np.int32(depth), np.int32(ntriang_limit), triangle_idxs, depth_and_cellid, tri_number)
	return triangle_idxs[0:tri_number[0]], (depth_and_cellid[0::2])[0:tri_number[0]], (depth_and_cellid[1::2])[0:tri_number[0]]


def toggle_debug():
	libcd.toggle_debug()

def optimize_submesh(submesh):
	vertices_array = np.ascontiguousarray(submesh.vertices.flatten(), dtype = np.float32)
	colors_array = np.ascontiguousarray(submesh.colors.flatten(), dtype = np.float32)
	normals_array = np.ascontiguousarray(submesh.normals.flatten(), dtype = np.float32)
	tri_array     = np.ascontiguousarray(submesh.triangles.flatten(), dtype = np.int32)

	new_triangles = np.zeros(tri_array.size, dtype = np.int32)
	N_tris        = np.zeros(1, dtype = np.int32)
	libcd.optimize_submesh(vertices_array, normals_array, colors_array, tri_array, np.int32(len(submesh.triangles)), new_triangles, N_tris)
	new_triangles = np.reshape(new_triangles[0:3*N_tris[0]], [N_tris[0], 3])
	submesh.triangles = new_triangles

def optimize_mesh(mesh):
	for item in mesh.materials:
		optimize_submesh(mesh.submeshes[item])
	mesh.calculate_whole_mesh()



def set_seed(seed = 0):
	libcd.set_seed(seed)



set_seed()
#
#
def estimate_triangle_amp(vertices, triangles, cutoff = 1.0):
	count = np.zeros(len(triangles), dtype = np.int32)
	for i in range(len(triangles)):
		triangle = triangles[i]
		vert1, vert2, vert3 = vertices[triangle[0]], vertices[triangle[1]], vertices[triangle[2]]
		A, B, C = np.linalg.norm(vert2 - vert1), np.linalg.norm(vert3 - vert2), np.linalg.norm(vert1 - vert3)
		L = np.max([A, B, C])
		n = max(int(L/cutoff), 1)
		count[i] = n

	return count

def subdivide_triangles(submesh, cutoff = 1.0):
	ntris           = np.int32(len(submesh.triangles))
	if ntris != 0:
		vertices_array  = np.ascontiguousarray(submesh.vertices.flatten(),  np.float32)
		colors_array    = np.ascontiguousarray(submesh.colors.flatten(),    np.float32)
		uvs_array       = np.ascontiguousarray(submesh.uvs.flatten(),       np.float32)
		triangles_array = np.ascontiguousarray(submesh.triangles.flatten(), np.int32)


		new_count      = np.zeros(ntris, dtype = np.int32)
		libcd.estimate_subdivisions(vertices_array, triangles_array, ntris, np.float32(cutoff), new_count)

		new_count[:]  = np.int32(np.max([new_count[0]/cutoff, 1]))
		N_new         = np.sum(new_count**2)
		new_vertices  = np.zeros(N_new*3*3, dtype = np.float32)
		new_colors    = np.zeros(N_new*3*4, dtype = np.float32)
		new_uvs       = np.zeros(N_new*3*2, dtype = np.float32)
		new_triangles = np.zeros(N_new*3*1, dtype = np.int32)

		libcd.triangle_subdivision(vertices_array, uvs_array, colors_array, triangles_array, ntris, new_count, new_vertices, new_uvs, new_colors, new_triangles)

		nverts = N_new*3
		submesh.vertices  = np.reshape(new_vertices,  [nverts, 3])
		submesh.uvs       = np.reshape(new_uvs,       [nverts, 2])
		submesh.colors    = np.reshape(new_colors,    [nverts, 4])
		submesh.triangles = np.reshape(new_triangles, [N_new, 3])
		submesh.calculate_normals(check = False)


def estimate_lighting(height_map, extent, phi = 45., theta = 45., Nsamples = 200, max_dist = 40.):
	light_dir = np.array([np.sin(theta*deg2rad)*np.cos(phi*deg2rad), np.sin(theta*deg2rad)*np.sin(phi*deg2rad), np.cos(theta*deg2rad)], dtype = np.float32)

	shadows = estimate_shadows(height_map, extent, phi, theta)
	normals = estimate_normals(height_map, extent)

	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))
	shadows_array = np.ascontiguousarray(shadows.flatten(), np.float32)
	normals_array = np.ascontiguousarray(normals.flatten(), np.float32)

	result = np.zeros(N*M, dtype = np.float32)
	libcd.calculate_indirect_lighting(shadows_array, normals_array, light_dir, result, np.int32(Nsamples), np.float32(max_dist))
	return np.reshape(result, [N, M])


def determine_flat_spots(height_map, radius = 1.0):
	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))
	result = np.zeros(N*M, dtype = np.float32)
	libcd.determine_flat_surfaces(result, np.float32(radius))
	return np.reshape(result, [N, M])


def calculate_exposure(height_map, Nsamples = 100, nsteps = 100, min_dist = 20., max_dist = 200., height = 1.5, exp_score = 1.0):
	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))
	result = np.zeros(N*M, dtype = np.float32)
	result2 = np.zeros(N*M, dtype = np.float32)
	result3 = np.zeros(N*M, dtype = np.float32)
	result4 = np.zeros(N*M, dtype = np.float32)

	libcd.calculate_surface_exposure(np.int32(Nsamples), np.int32(nsteps), np.float32(min_dist), np.float32(max_dist), np.float32(exp_score), np.float32(height), result, result2, result3, result4)
	return np.reshape(result, [N, M]), np.reshape(result2, [N, M]), np.reshape(result3, [N, M]), np.reshape(result4, [N, M])

def estimate_shadows(height_map, extent, phi = 45., theta = 45.):
	light_dir = np.array([np.sin(theta*deg2rad)*np.cos(phi*deg2rad), np.sin(theta*deg2rad)*np.sin(phi*deg2rad), np.cos(theta*deg2rad)], dtype = np.float32)
	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))

	map_array = np.ascontiguousarray(height_map.flatten(), np.float32)
	libcd.load_height_map(map_array, np.int32(N), np.int32(M), np.float32(extent))

	result = np.zeros(N*M, dtype = np.float32)

	libcd.determine_shadows(light_dir, result)
	return np.reshape(result, [N, M])

def estimate_normals(height_map, extent):
	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))

	map_array = np.ascontiguousarray(height_map.flatten(), np.float32)
	libcd.load_height_map(map_array, np.int32(N), np.int32(M), np.float32(extent))

	result = np.zeros(3*N*M, dtype = np.float32)

	libcd.determine_normals(result)
	return np.reshape(result, [N, M, 3])


def non_nan(array):
	if np.isnan(array).sum() != 0:
		print("Error vertices are not numbers!!")
	return array[np.isfinite(array)]

def get_extent(vertices):
	x_min, x_max = non_nan(vertices[:, 0]).min(), non_nan(vertices[:, 0]).max()
	y_min, y_max = non_nan(vertices[:, 1]).min(), non_nan(vertices[:, 1]).max()
	return np.array([x_min, x_max, y_min, y_max])

def get_heightmap(vertices_orig, triangles, dx = 1.0):
	vertices = vertices_orig*1.
	x_min, x_max = non_nan(vertices[:, 0]).min(), non_nan(vertices[:, 0]).max()
	y_min, y_max = non_nan(vertices[:, 1]).min(), non_nan(vertices[:, 1]).max()

	z_min = non_nan(vertices[:, 2]).min()
	vertices[:, 2] += -z_min
	N, M = np.int32((x_max - x_min)/dx), np.int32((y_max - y_min)/dx)

	ntriang = np.int32(len(triangles))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), np.int32)
	extent_array    = np.ascontiguousarray(np.array([x_min, x_max, y_min, y_max]),  np.float32)
	heightmap       = np.zeros(N*M, dtype = np.float32)

	libcd.get_heightmap_from_geometry(vertices_array, triangles_array, ntriang, heightmap, N, M, extent_array)
	return np.reshape(heightmap, [N, M]), extent_array



def map_quantity(vertices_orig, quantity, triangles, extent, dx = 1.0, nthreads = 8, optimize = False, vertex_wise = True, force_resolution = -1):
	vertices = vertices_orig*1.
	x_min, x_max = extent[0], extent[1]
	y_min, y_max = extent[2], extent[3]

	quantity_per_vertex = np.int32(1) if vertex_wise else np.int32(0)

	optimize_value = np.int32(1) if optimize else np.int32(0)

	z_min = non_nan(vertices[:, 2]).min()
	vertices[:, 2] += -z_min
	if force_resolution == -1:
		N, M = np.int32((x_max - x_min)/dx), np.int32((y_max - y_min)/dx)
	else:
		N, M = np.int32(force_resolution), np.int32(force_resolution)

	ntriang = np.int32(len(triangles))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  np.float32)
	quantity_array  = np.ascontiguousarray(quantity.flatten(),  np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), np.int32)
	extent_array    = np.ascontiguousarray(np.array(extent),  np.float32)
	heightmap       = np.zeros(N*M, dtype = np.float32)
	quantity_map    = np.zeros(N*M, dtype = np.float32)

	time_start = time.time()

	libcd.map_quantity_from_geometry(vertices_array, quantity_array, triangles_array, ntriang, heightmap, quantity_map, N, M, extent_array, np.int32(nthreads), optimize_value, quantity_per_vertex)
	time_end   = time.time()
	print("Time elapsed: ", time_end - time_start, " [s]")
	return np.reshape(heightmap, [N, M]), np.reshape(quantity_map, [N, M]), extent_array



def check_triangles(vertices, triangles, criteria = 0.0001):
	nverts = np.int32(len(vertices))
	ntriang = np.int32(len(triangles))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), np.int32)

	new_vertices  = np.zeros(nverts*3, dtype = np.float32)
	new_triangles = np.zeros(ntriang*3, dtype = np.int32)
	mask_vertices = np.zeros(nverts, dtype = np.int32)

	Ns            = np.zeros(2, dtype = np.int32)
	libcd.check_triangles(vertices_array, triangles_array, new_vertices, new_triangles, mask_vertices, nverts, ntriang, np.float32(criteria), Ns)
	return np.reshape(new_vertices, [nverts, 3])[0:Ns[0], :], np.reshape(new_triangles, [ntriang, 3])[0:Ns[1], :], mask_vertices

def test_randoms(N, value):
	result = np.zeros(N, dtype = np.int32)
	libcd.test_poisson_random(np.float32(value), result, np.int32(N))
	return result

def calculate_area(vertices, triangles, nthreads = 8):
	nverts, ntriangs = np.int32(len(vertices)), np.int32(len(triangles))
	vertices_array  = np.ascontiguousarray(vertices.flatten(),  np.float32)
	triangles_array = np.ascontiguousarray(triangles.flatten(), np.int32)
	areas = np.zeros(ntriangs, dtype = np.float32)

	libcd.fast_area_calculation(vertices_array, triangles_array, nverts, ntriangs, areas, np.int32(nthreads))
	return np.sum(areas)

def cover_surface(mesh, surface_density, material, size = 0.4, angle_min = 0., angle_max = 10.):
	total_area = mesh.whole_mesh.calculate_area()

	std = (total_area*surface_density)**0.5

	maximum = np.int32(total_area*surface_density + 3*std)
	ntriang = np.int32(len(mesh.whole_mesh.triangles))

	vertices_array  = np.ascontiguousarray(mesh.whole_mesh.vertices.flatten(),  np.float32)
	triangles_array = np.ascontiguousarray(mesh.whole_mesh.triangles.flatten(), np.int32)

	cover_vertices  = np.zeros(12*maximum, dtype = np.float32)
	cover_triangles = np.zeros(6*maximum, dtype = np.int32)
	n               = np.zeros(2, dtype = np.int32)

	libcd.cover_surface(vertices_array, triangles_array, ntriang, maximum, np.float32(surface_density), np.float32(size), np.float32(angle_min), np.float32(angle_max), cover_vertices, cover_triangles, n)

	verts = np.reshape(cover_vertices, [4*maximum, 3])
	tris  = np.reshape(cover_triangles, [2*maximum, 3])

	total_verts, total_tris = n[0]*4, n[0]*2

	return verts[0:total_verts], tris[0:total_tris], n[0]


def estimate_ambient_occlusion(height_map, extent, Nsamples = 200, dist_res = 50, max_dist = "fraction"):
	if type(max_dist) == str:
		max_dist = np.float32(max(extent[1] - extent[0], extent[3] - extent[2])*0.1)
	else:
		max_dist = np.float32(max_dist)

	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))

	map_array = np.ascontiguousarray(height_map.flatten(), np.float32)
	libcd.load_height_map(map_array, np.int32(N), np.int32(M), np.float32(extent))

	result = np.zeros(N*M, dtype = np.float32)

	time0 = time.time()
	libcd.calculate_AO(N, M, result, np.int32(Nsamples), np.int32(dist_res), max_dist)
	time1 = time.time()
	print("Time elapsed:" + str(time1 - time0))
	return np.reshape(result, [N, M])

def advanced_ambient_occlusion(height_map, extent, sky_dir = [0., 0., 1.], Nsamples = 200, dist_res = 50, max_dist = "fraction", optimize = True):
	if type(max_dist) == str:
		max_dist = np.float32(max(extent[1] - extent[0], extent[3] - extent[2])*0.1)
	else:
		max_dist = np.float32(max_dist)

	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))

	map_array = np.ascontiguousarray(height_map.flatten(), np.float32)
	libcd.load_height_map(map_array, np.int32(N), np.int32(M), np.float32(extent))

	result = np.zeros(N*M, dtype = np.float32)

	time0 = time.time()
	if optimize == False:
		libcd.calculate_advanced_AO(N, M, result, np.float32(sky_dir), np.int32(Nsamples), np.int32(dist_res), max_dist)
	else:
		libcd.efficient_AO(N, M, result, np.float32(sky_dir), np.int32(Nsamples), max_dist)
	time1 = time.time()
	print("Time elapsed:" + str(time1 - time0))
	return np.reshape(result, [N, M])

#void load_height_map(float *array, unsigned int N, unsigned int M, float *extent)
def determine_surface(height_map, scale, level):
	N, M = np.int32(len(height_map)), np.int32(len(height_map[0]))

	map_array = np.ascontiguousarray(np.reshape(height_map, height_map.size), np.float32)

	result = np.zeros(N*M, dtype = np.float32)

	libcd.determine_leveled_surface(map_array, N, M, np.float32(scale), np.float32(level), result)
	return np.reshape(result, [N, M])

def load_map(height_map, extent):
	array = np.ascontiguousarray(height_map.flatten(), np.float32)
	#array = np.ascontiguousarray(np.reshape(height_map, height_map.size), np.float32)

	N, M = len(height_map), len(height_map[0])
	libcd.load_height_map(array, np.int32(N), np.int32(M), np.float32(extent))
	

def geodesic(point1, point2, height_map, extent, nitera = 5):
	array = np.ascontiguousarray(np.reshape(height_map, height_map.size), np.float32)
	N, M = len(height_map), len(height_map[0])
	# extent = np.array([0., 90., 0., 110.])
	# load height map
	libcd.load_height_map(array, np.int32(N), np.int32(M), np.float32(extent))
	result = np.zeros((2**nitera + 1)*3, dtype = np.float32)
	tiempo1 = time.time()
	libcd.generate_path(np.float32(point1), np.float32(point2), np.int32(nitera), result)
	tiempo2 = time.time()
	print("Time elapsed = ", (tiempo2 - tiempo1)/0.001, " ms")
	return np.reshape(result, [2**nitera + 1, 3])
	
def perlin(size, scale, resolution, seed = 0):
	lin_x = np.linspace(0, size[0]/scale, resolution, endpoint = False)
	lin_y = np.linspace(0, size[1]/scale, resolution, endpoint = False)
	x, y  = np.meshgrid(lin_x, lin_y) # FIX3: I thought I had to invert x and y here but it was a mistake
	
	# permutation table
	#np.random.seed(seed)
	p = np.arange(256, dtype = int)
	np.random.shuffle(p)
	p = np.stack([p,p]).flatten()
	# coordinates of the top-left
	xi = x.astype(int)
	yi = y.astype(int)
	# internal coordinates
	xf = x - xi
	yf = y - yi
	# fade factors
	u = fade(xf)
	v = fade(yf)
	# noise components
	n00 = gradient(p[p[xi]+yi],xf,yf)
	n01 = gradient(p[p[xi]+yi+1],xf,yf-1)
	n11 = gradient(p[p[xi+1]+yi+1],xf-1,yf-1)
	n10 = gradient(p[p[xi+1]+yi],xf-1,yf)
	# combine noises
	x1 = lerp(n00,n10,u)
	x2 = lerp(n01,n11,u) # FIX1: I was using n10 instead of n01
	return lerp(x1,x2,v) # FIX2: I also had to reverse x1 and x2 here

def lerp(a,b,x):
    "linear interpolation"
    return a + x * (b-a)

def fade(t):
    "6t^5 - 15t^4 + 10t^3"
    return 6 * t**5 - 15 * t**4 + 10 * t**3

def gradient(h,x,y):
    "grad converts h to the right gradient vector and return the dot product with (x,y)"
    vectors = np.array([[0, 1], [0, -1], [1, 0], [-1, 0]])
    g = vectors[h%4]
    return g[:,:, 0]*x + g[:,:, 1]*y


def perlin2D(size, scale, M):
	L = size[0]
	grid_resolution = int(L/scale) + 1

	result      = np.zeros(M**2, dtype = np.float32)
	
	libcd.create_perlin_2D(np.int32(grid_resolution), result, np.int32(M))
	result = (result - result.min())/(result.max() - result.min())
	return np.reshape(result, [M, M])


def raycast(position, rays, lengths):
	timer = time.time()
	position = np.ascontiguousarray(position, np.float32)
	lengths  = np.ascontiguousarray(lengths, np.float32)
	rays_arr = np.ascontiguousarray(rays.flatten(), np.float32)
	nrays    = len(rays)

	result = np.zeros(nrays, dtype = np.float32)

	libcd.raycasting(position, rays_arr, lengths, np.int32(nrays), result)
	
	delta_time = time.time() - timer
	print("time elapsed: " + str(delta_time))
	print("time per raycast: " + str(delta_time/nrays))
	
	return result
	
def evaluate_heights(positions):
	positions = np.array(positions)
	result    = np.zeros(len(positions), dtype = np.float32)
	positions = np.ascontiguousarray(positions.flatten(), np.float32)
	libcd.check_values(positions, np.int32(len(result)), result)
	return result

def solve_pendulum(position, velocity, fixed_point):
	trajectory  = np.zeros(10000*3, dtype = np.float32)
	position    = np.ascontiguousarray(position, np.float32)
	velocity    = np.ascontiguousarray(velocity,  np.float32)
	fixed_point = np.ascontiguousarray(fixed_point, np.float32)

	final_vel = np.zeros(3, dtype = np.float32)
	final_pos = np.zeros(3, dtype = np.float32)
	libcd.solver(fixed_point, velocity, position, final_vel, final_pos, trajectory)
	return np.reshape(trajectory, [10000, 3]), final_vel, final_pos

def calculate_normals(vertices, triangles, nthreads = 12):
	nverts     = np.int32(len(vertices))
	ntriangs   = np.int32(len(triangles))

	vertices_array = np.ascontiguousarray(vertices.flatten(),  np.float32)
	triangles_array= np.ascontiguousarray(triangles.flatten(), np.int32)
	
	normals = np.zeros(nverts*3, dtype = np.float32)
	libcd.calculate_normals(vertices_array, triangles_array, nverts, ntriangs, normals, np.int32(nthreads))
	return np.reshape(normals, [nverts, 3])




def create_heightmap_mesh(heightmap, extent):
	x_space, y_space = np.linspace(extent[0], extent[1], len(heightmap)), np.linspace(extent[2], extent[3], len(heightmap[0]))

	shape = heightmap.shape
	X, Y = np.meshgrid(x_space, y_space, indexing = "ij")

	I, J = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing = "ij")

	vertices = np.zeros([shape[0]*shape[1], 3], dtype = np.float32)

	vertices[:, 0] = X.flatten()
	vertices[:, 1] = Y.flatten()
	vertices[:, 2] = heightmap.flatten()

	uvs = np.zeros([shape[0]*shape[1], 2], dtype = np.float32)
	uvs[:, 0], uvs[:, 1] = (X.flatten() - extent[0])/(extent[1] - extent[0]), (Y.flatten() - extent[2])/(extent[3] - extent[2])


	triangles = np.zeros([(shape[0] - 1)*(shape[1] - 1)*2, 3], dtype = np.int32)

	first = (I*shape[1] + J)[0:shape[0] - 1, 0:shape[1] - 1].flatten()
	second = first + 1

	third = (first + shape[1])
	fourth  = third + 1

	triangles[::2, 0] = second
	triangles[::2, 1] = first
	triangles[::2, 2] = third

	triangles[1::2, 0] = fourth
	triangles[1::2, 1] = second
	triangles[1::2, 2] = third

	mesh = tools.SubMesh(vertices = vertices, triangles = triangles, uvs = uvs)
	mesh.colors = np.ones([len(vertices), 3], dtype = np.float32)

	return mesh
