import numpy as np
import tools
import modulo


def generate_rim(shape_orig, available, width, level, height, material):
	shape = shape_orig.buffer(width, join_style = 2).difference(shape_orig)
	upper_lid  = meshify_shape(shape, level + height, material, [1]*4)
	bottom_lid = meshify_shape(shape, level, material, [1]*4, flip = True)
	wall       = extrude_rings(shape, level, level + height, material, [1]*4)
	return bottom_lid + wall + upper_lid

def subdivide_shape(facet_shape, res = 0.5):
	minishapes, perimeter = [], []
	length, width, orientation, coords, centroid = tools.get_orientation(facet_shape)
	u = np.array([ np.cos(orientation), np.sin(orientation)])
	v = np.array([-np.sin(orientation), np.cos(orientation)])
	s_min, s_max = (coords[:, 0]*u[0] + coords[:, 1]*u[1]).min(), (coords[:, 0]*u[0] + coords[:, 1]*u[1]).max()

	nrep = max(int(width/res), 1)
	dx = width/nrep

	shapes = []
	for ring in tools.get_rings(tools.orient(facet_shape)):
		perimeter.append(tools.get_intersections(ring, u, v, s_min, dx))

	for i in range(nrep):
		center = [centroid[0]  + (i*dx - width/2 + dx/2.)*u[0], centroid[1]  + (i*dx - width/2 + dx/2.)*u[1]]
		minishape = tools.create_box(center, [dx, length], orientation)
		minishapes.append(minishape)
	return minishapes, perimeter, u, v, s_min, s_max, dx

def stripped_mesh(facet_shape, minishapes, u, s_min, s_max, h, kind, material, color = np.ones(4)):
	mesh = tools.Mesh()
	for minishape in minishapes:
		surf = meshify_shape(minishape.intersection(facet_shape), h, material, color)
		mesh += surf

	if material in mesh.materials:
		nverts = len(mesh.submeshes[material].vertices)

		new_vertices = np.zeros([nverts, 3])
		for i in range(nverts):
			vert = mesh.submeshes[material].vertices[i]
			s = vert[0]*u[0] + vert[1]*u[1]

			if kind == "curved":
				vert[2] = curve(h, s_min, s_max, s)
			elif kind == "wave":
				vert[2] = sine(h, s_min, s_max, s)
			elif kind == "twosided":
				vert[2] = twosided(h, s_min, s_max, s)

			new_vertices[i] = vert

		mesh.submeshes[material].vertices = new_vertices

	return mesh

def generate_curved_rim(shape, minishapes, u, v, dx, s_min, s_max, level, kind, thickness = 0.1, material = "Metal"):
	perimeter = []
	for ring in tools.get_rings(tools.orient(shape)):
		perimeter.append(tools.get_intersections(ring, u, v, s_min, dx))

	top_lid    = stripped_mesh(shape, minishapes, u, s_min, s_max, level + thickness/2., kind, material)
	bottom_lid = stripped_mesh(shape, minishapes, u, s_min, s_max, level - thickness/2., kind, material)
	bottom_lid.flip()

	walls = extrude_rings_list(perimeter, -1., 1.0, material, color = np.ones(4))

	nverts = len(walls.submeshes[material].vertices)
	new_vertices = np.zeros([nverts, 3])
	for i in range(nverts):
		vert = walls.submeshes[material].vertices[i]
		s = vert[0]*u[0] + vert[1]*u[1]

		offset = level + thickness/2. if vert[2] > 0 else level - thickness/2.

		if kind == "curved":
			vert[2] = curve(offset, s_min, s_max, s)
		elif kind == "wave":
			vert[2] = sine(offset, s_min, s_max, s)
		elif kind == "twosided":
			vert[2] = twosided(offset, s_min, s_max, s)

		new_vertices[i] = vert
	walls.submeshes[material].vertices = new_vertices
	final_mesh = walls + top_lid + bottom_lid

	final_mesh.submeshes[material].uvs = tools.wrap_uvs_method1(final_mesh.submeshes[material])
	return final_mesh



def generate_ventiluz(ventiluz_area, level):
	kind = tools.weigh_output([0, 1, 2, 3], [30, 20, 5, 10])

	available = ventiluz_area.buffer(0.2, join_style = 2)
	L = ventiluz_area.area**0.5
	h = L*0.5
	elevation = 0.3
	if kind == 0:
		mesh, success = generate_general_roof(ventiluz_area, available, elevation + level, h, "Plastic_Roof", "Wall", kind = "curved", orientation = -1, extension = 0.2)

	elif kind == 1:
		mesh, success = generate_general_roof(ventiluz_area, available, elevation + level, h, "Plastic_Roof", "Wall", kind = "twosided", orientation = -1, extension = 0.2)

	elif kind == 2:
		mesh, success = generate_general_roof(ventiluz_area, available, elevation + level, h, "Plastic_Roof", "Wall", kind = "wave", orientation = -1, extension = 0.2)

	else:
		mesh, success = generate_complex_roof(ventiluz_area, available, elevation + level, elevation + level + h, "Glassy", "Smooth_Metal", dx = 0.1, kind = "linear", nlevels = 5, mini_wall = 0.05)

		if success:
			wall_shape = ventiluz_area.buffer(0.15, join_style = 2).difference(ventiluz_area)
			walls      = extrude_shape(wall_shape, elevation + level, elevation + level + 0.1, "Smooth_Wall", color1 = [1.]*4, color2 = [1.]*4)
			walls      = extrude_shape(wall_shape, elevation + level, elevation + level + 0.1, "Smooth_Wall", color1 = [1.]*4, color2 = [1.]*4)


			mesh     += walls

	if success:
		walls      = extrude_rings(ventiluz_area, level, elevation + level, "Smooth_Wall", color = [1.]*4)
		mesh += walls

	return mesh, success

params = 0
def generate_general_roof(facet_shape_orig, available, level, height, material1, material2, kind = "curved", orientation = -1, extension = 0.5):
	thickness = 0.1
	global params
	# facet_shape = facet_shape_o.buffer(0.5, join_style = 2).intersection(available)
	big_shape      = facet_shape_orig.buffer(extension, join_style = 2).intersection(available)
	facet_shape    = facet_shape_orig.buffer(extension - thickness, join_style = 2).intersection(available)
	underlid_shape = facet_shape.difference(facet_shape_orig)
	rim_shape      = (facet_shape_orig.buffer(extension, join_style = 2).intersection(available)).difference(facet_shape)

	if facet_shape.area != 0.0:
		minishapes, perimeter, u, v, s_min, s_max, dx = subdivide_shape(big_shape)
		roof     = stripped_mesh(facet_shape, minishapes, u, s_min, s_max, level, kind, material1)
		underlid = stripped_mesh(underlid_shape, minishapes, u, s_min, s_max, level - 0.05, kind, material1)
		underlid.flip()

		params = rim_shape, minishapes, u, v, dx, s_min, s_max, level, kind, thickness
		rim = generate_curved_rim(rim_shape, minishapes, u, v, dx, s_min, s_max, level, kind, thickness)


		perimeter = []
		for ring in tools.get_rings(tools.orient(facet_shape_orig)):
			perimeter.append(tools.get_intersections(ring, u, v, s_min, dx))

		walls = extrude_rings_list(perimeter, level, level + 1.0, material2, color = np.ones(4))

		nverts = len(walls.submeshes[material2].vertices)
		new_vertices = np.zeros([nverts, 3])
		for i in range(nverts):
			vert = walls.submeshes[material2].vertices[i]
			s = vert[0]*u[0] + vert[1]*u[1]

			if vert[2] >= level + 0.5:
				if kind == "curved":
					vert[2] = curve(level, s_min, s_max, s)
				elif kind == "wave":
					vert[2] = sine(level, s_min, s_max, s)
				elif kind == "twosided":
					vert[2] = twosided(level, s_min, s_max, s)

			new_vertices[i] = vert
		walls.submeshes[material2].vertices = new_vertices
		walls.submeshes[material2].uvs = tools.wrap_uvs_method1(walls.submeshes[material2])

		return roof + walls + underlid + rim, True
	else:
		return tools.Mesh(), False

def generate_railing(whole_shape, built_shape, facet, low_wall_area, height, material1, material2):
	bar_size, bar_separation, w = 0.1, 0.2, 0.1

	rings      = tools.get_exterior(whole_shape.buffer(-w/2, join_style = 2))
	#wall_line  = better_linemerge(rings.intersection(low_wall_area))
	wall_line  = rings.intersection(low_wall_area)

	shapes = []
	if wall_line.type == "MultiLineString":
		for line in wall_line:
			shapes.append(railing_along_line(line, bar_size, bar_separation))
		shapes = tools.unary_union(shapes)
	else:
		shapes = railing_along_line(wall_line, bar_size, bar_separation)

	shapes = shapes.intersection(facet)
	terrace_perimeter = whole_shape.difference(whole_shape.buffer(-w, join_style = 2))
	wall_shape        = terrace_perimeter.intersection(facet)
	bottom_lid = wall_shape.difference(shapes)

	mesh_bars    = extrude_rings(shapes, height, height + 1.0, material1, color = np.ones(4))
	mesh_railing = extrude_shape(wall_shape, height + 1.0, height + 1.1, material2, color1 = [1.]*4, color2 = [1.]*4)
	mesh_bottom  = meshify_shape(bottom_lid, height + 1.0, material2, [1.]*4, flip = True)

	# mesh_bottom.flip()

	return mesh_bars + mesh_railing + mesh_bottom

def extrude_shape(polygon_object_original, level, height, material, color1 = [1., 1., 1., 1.], color2 = [1., 1., 1., 1.]):
	walls = extrude_rings(polygon_object_original, level, height, material, color1)
	lid   = meshify_shape(polygon_object_original, height, material, color2)

	return walls + lid

def meshify_shape(polygon_object_original, level, material, color, organicity = 0., flip = False, return_submesh = False):
	vector_up = np.array([0., 0., 1.])
	if polygon_object_original.type == "Polygon":
		if polygon_object_original.area != 0.:
			polygon_object = tools.orient(polygon_object_original)
			rings = [polygon_object.exterior.coords] + [ interior_ring.coords for interior_ring in polygon_object.interiors]

			triangles, vertices, normals = [], [], []

			nrings = len(rings)
			nverts = [len(ring) - 1 for ring in rings]

			for i in range(nrings):
				ring = rings[i]
				ring_vertices = [np.array([vert[0], vert[1], level]) for vert in ring[0:len(ring) - 1]]
				vertices     += ring_vertices

			lid1 = tools.triangulate_within(polygon_object)
			lid2 = [[item[0], item[1], item[2]] for item in lid1 ]

			triangles += lid2

			colors = np.zeros([len(vertices), 4], dtype = np.float32)
			colors[:, 0], colors[:, 1], colors[:, 2], colors[:, 3] = color[0], color[1], color[2], color[3]

			uvs = np.zeros([len(vertices), 2])
			uvs[:, 0:2] = np.array(vertices)[:, 0:2]

			submesh = tools.SubMesh(vertices, triangles, uvs, material = material, colors = colors)
			if organicity != 0.:
				extent, center, width, length = tools.determine_extent(polygon_object_original)
				deviations, bla, blo = create_distribution(extent, 2.)

				reference_size = np.max([width, length])/2.

				distortion = interp2d(x = np.linspace(extent[0], extent[1], 100), y = np.linspace(extent[2], extent[3], 100), z = deviations)

				modulo.subdivide_triangles(submesh, cutoff = 1.0)
				for vert in submesh.vertices:
					distance = tools.Point(vert[0:2]).distance(MultiLineString(rings))
					factor = distance/reference_size*organicity*0.5
					vert[2] += -factor*distortion(vert[0], vert[1])
			mesh = submesh.get_mesh()
			if flip:
				mesh.flip()
			if return_submesh:
				return [submesh]
			else:
				return mesh
		else:
			submesh = tools.SubMesh(vertices = np.zeros([0, 3]), triangles = np.zeros([0, 3], dtype = np.int32), uvs = np.zeros([0, 2]), material = material, colors = np.ones([0, 4]))
			if return_submesh:
				return [submesh]
			else:
				return submesh.get_mesh()
	if polygon_object_original.__class__.__name__ in ["MultiPolygon", "GeometryCollection"]:
		if return_submesh:
			combination = []
		else:
			combination = tools.Mesh()

		if polygon_object_original.is_empty == False:
			for item in polygon_object_original:
				if item.area != 0.:
					combination += meshify_shape(item, level, material, color, flip = flip, return_submesh = return_submesh)
		return combination
	else:
		submesh = tools.SubMesh(vertices = np.zeros([0, 3]), triangles = np.zeros([0, 3], dtype = np.int32), uvs = np.zeros([0, 2]), material = material, colors = np.ones([0, 4]))
		if return_submesh:
			return [submesh]
		else:
			return submesh.get_mesh()

def extrude_rings(polygon_object_original, level, height, material, color):
	if polygon_object_original.type == "MultiPolygon":
		combination = tools.Mesh()
		for item in polygon_object_original:
			if item.area != 0.:
				combination += extrude_rings(item, level, height, material, color)
		return combination

	elif polygon_object_original.type == "Polygon":
		polygon_object = tools.orient(polygon_object_original)
		rings = [polygon_object.exterior.coords] + [ interior_ring.coords for interior_ring in polygon_object.interiors]
		return extrude_rings_list(rings, level, height, material, color)
	else:
		return tools.Mesh()

def extrude_rings_list(rings, level, height, material, color):
	vector_up = np.array([0., 0., 1.])
	triangles, vertices, normals, uvs1, uvs2 = [], [], [], [], []

	nrings = len(rings)
	nverts = [len(ring) - 1 for ring in rings]

	for i in range(nrings):
		ring = rings[i]
		ring_vertices = [np.array([vert[0], vert[1], level]) for vert in ring[0:len(ring) - 1] for _ in (0, 1)]
		vertices     = vertices + ring_vertices

		nvert_half = int(len(ring_vertices)/2.)
		for j in range(nvert_half):
			if j < nvert_half - 1:
				vert1, vert2 = ring_vertices[2*j + 1], ring_vertices[2*j + 2]
			else:
				vert1, vert2 = ring_vertices[2*j + 1], ring_vertices[0]

			distance     = np.linalg.norm((vert2 - vert1)[0:2])
			direction    = (vert2 - vert1)/distance
			coord1       = direction[0]*vert1[0] + direction[1]*vert1[1]
			coord2       = direction[0]*vert2[0] + direction[1]*vert2[1]

			uvs_pair     = [[coord1, vert1[2]], [coord2, vert1[2]]]
			#uvs_pair     = [[coord1, vert1[2]], [coord1 + distance, vert1[2]]]
			uvs1          = uvs1 + uvs_pair
	uvs1 = list(np.roll(uvs1, 1, axis = 0))

	for i in range(nrings):
		ring = rings[i]
		ring_vertices = [np.array([vert[0], vert[1], height]) for vert in ring[0:len(ring) - 1] for _ in (0, 1)]
		vertices      = vertices + ring_vertices

		nvert_half = int(len(ring_vertices)/2.)
		for j in range(nvert_half):
			if j < nvert_half - 1:
				vert1, vert2 = ring_vertices[2*j + 1], ring_vertices[2*j + 2]
			else:
				vert1, vert2 = ring_vertices[2*j + 1], ring_vertices[0]
			distance     = np.linalg.norm((vert2 - vert1)[0:2])
			direction    = (vert2 - vert1)/distance
			coord1       = direction[0]*vert1[0] + direction[1]*vert1[1]
			coord2       = direction[0]*vert2[0] + direction[1]*vert2[1]

			uvs_pair     = [[coord1, vert1[2]], [coord2, vert1[2]]]
			#uvs_pair     = [[coord1, vert1[2]], [coord1 + distance, vert1[2]]]
			uvs2          = uvs2 + uvs_pair

	uvs2 = list(np.roll(uvs2, 1, axis = 0))
	uvs = uvs1 + uvs2

	stride = int(len(vertices)/2)
	offset = 0

	for i in range(len(rings)):
		ring   = rings[i]
		nsides  = len(ring) - 1
		nvert   = 2*nsides

		for side_idx in range(nsides):
			i = 2*side_idx + 1
			if side_idx == nsides - 1:
				triangle1 = [   offset + i,       offset,          offset + stride]
				triangle2 = [ triangle1[0], triangle1[2], triangle1[0] + stride]
			else:
				triangle1 = [    offset + i,  offset + i + 1,  stride + offset + i + 1]
				triangle2 = [  triangle1[0],    triangle1[2],         triangle1[2] - 1]

			triangles.append(triangle1)
			triangles.append(triangle2)

		offset += nvert


	colors = np.zeros([len(vertices), 4], dtype = np.float32)
	colors[:, 0], colors[:, 1], colors[:, 2], colors[:, 3] = color[0], color[1], color[2], color[3]
	submesh = tools.SubMesh(vertices, triangles, uvs = np.array(uvs), material = material, colors = colors)
	return submesh.get_mesh()

def generate_techito(facet_shape, built_shape, level, material, width = 1.5, slope = 0.3, join_style = 3):
	roof = tools.Mesh()

	outer_shape = (built_shape.buffer(width, join_style = join_style)).intersection(facet_shape)

	diff = outer_shape.difference(built_shape)

	if diff.area != 0.:
		surf = extrude_shape(diff, level - 0.1, level, material, color1 = [1.]*4, color2 = [1.]*4)
		bottom = meshify_shape(diff, level - 0.1, material, np.ones(4), flip = True)
		# bottom.flip()

		surf = surf + bottom

		verts = surf.submeshes[material].vertices

		nvert = len(verts)

		for j in range(nvert):
			point = verts[j, 0: 2]

			dist2inn = tools.Point(point).distance(built_shape)

			verts[j, 2] += -slope*dist2inn/width

		surf = tools.concatenate_triangle_mesh(surf.submeshes[material]).get_mesh()
		surf.submeshes[material].uvs = tools.wrap_uvs_method1(surf.submeshes[material])

		return surf, True
	else:
		return tools.Mesh(), False

def generate_bellow_roof(facet_shape, level, height, material1, material2):
	roof = tools.Mesh()
	h = 0.3

	extent, center, width, length = tools.determine_extent(facet_shape)
	nrep = max(int(length/h), 1)
	dx = length/nrep

	#max_level = level(0)
	shapes = []

	for i in range(nrep):
		minishape = tools.create_box([extent[0] + width/2., extent[2] + dx*i + dx/2], [width, dx], 0.).intersection(facet_shape)
		#if minishape.area != 0.:
		surf = meshify_shape(minishape, level, material1, np.ones(4))
		roof += surf

	nverts = len(roof.submeshes[material1].vertices)
	new_vertices = np.zeros([nverts, 3])
	for i in range(nverts):
		vert = roof.submeshes[material1].vertices[i]

		vert[2] = saw_wave(vert[1] - extent[2], 2*dx)*height + level

		new_vertices[i] = vert
	roof.submeshes[material1].vertices = new_vertices


	wall_shape = facet_shape.difference(facet_shape.buffer(-0.15, join_style = 2))
	walls = extrude_shape(wall_shape, level, level + height, material2, color1 = [1.]*4, color2 = [1.]*4)

	return roof + walls, True

def generate_complex_roof(facet_shape, whole_shape, level1, level2, material1, material2, dx = 0.5, kind = "linear", nlevels = 10, mini_wall = 0.1):
	roof = tools.Mesh()

	outer_shape = facet_shape.convex_hull.intersection(whole_shape)
	h = tools.det_inner_distance(outer_shape)/2.

	if kind == "linear": level = lambda x: level1 + x*(level2 - level1)/h

	elif kind == "quadratic":
		h = min(3.0, h)
		c = (level1 - level2)/h**2
		b = -2*c*h
		a = level1
		level = lambda x: a + b*x + c*x**2 if x <= h else level2

	#if h < 1:
		#print("h very small:", h*2)
	nlevels = int(h*0.999/dx)

	max_level = level(0)
	for i in range(nlevels):
		exterior = tools.LineString(tools.get_rings(outer_shape)[0])

		inner_shape = outer_shape.buffer(-dx, cap_style = 2, join_style = 2)

		if inner_shape.area <= 0.001:
			#inner_shape = outer_shape
			break


		diff = (outer_shape.difference(inner_shape)).intersection(facet_shape)
		surf = meshify_shape(diff, level(i), material1, np.ones(4))

		verts = surf.submeshes[material1].vertices

		nvert = len(verts)

		for j in range(nvert):
			point = verts[j, 0: 2]

			dist2out = tools.Point(point).distance(exterior)
			dist2inn = tools.Point(point).distance(inner_shape)
			#if dist <= 0.001:
				#verts[j, 2] = level(i*dx)
			if dist2out <= dx:
				verts[j, 2] = level(i*dx + dist2out)
			else:
				verts[j, 2] = level((i + 1)*dx - dist2inn)

		surf = tools.concatenate_triangle_mesh(surf.submeshes[material1]).get_mesh()

		surf.submeshes[material1].uvs = tools.wrap_uvs_method1(surf.submeshes[material1])

		roof = roof + surf
		max_level = level((i + 1)*dx)

		outer_shape = inner_shape
	lid   = meshify_shape(outer_shape, max_level, material1, np.ones(4))

	wall_shape = outer_shape.difference(outer_shape.buffer(-0.15, join_style = 2))
	walls = extrude_shape(wall_shape, max_level, max_level + mini_wall, material2, color1 = [1.]*4, color2 = [1.]*4)

	if h < 1:
		return tools.Mesh(), False
	else:
		return roof + lid + walls, True


def saw_wave(x, P):
	s = x/P - int(x/P)
	if s <= 0.5:
		return s
	else:
		return 1 - s



def generate_bushes(shape, level, height = 1.0, density = 50., size = 0.2, angle_max = 30.):
	mesh1  = extrude_shape(shape.buffer(-0.2), level, level + height - 0.1, "Plant", [1]*4)
	mesh2  = extrude_shape(shape.buffer(-0.1), level, level + height, "Plant", [1]*4)
	cover = cover_surface2(mesh2, density, "Plant", size = size, angle_max = angle_max)

	#return mesh + cover
	return cover + mesh1

def generate_grass(shape, level, density = 10.):
	mesh  = meshify_shape(shape, level + 0.2, "standard", [1.0]*4)
	cover = cover_surface2(mesh, density, "Grass2", size = 0.2, angle_min = 60., angle_max = 90.)

	return cover

def generate_glass(shape, level, density = 400.):
	smaller = shape.buffer(-0.01)
	concrete = extrude_shape(smaller, level, level + 0.05, "Concrete", [1.0]*4)
	mesh  = meshify_shape(smaller, level + 0.05, "standard", [1.0]*4)
	cover = cover_surface2(mesh, density, "Glass", size = 0.1, angle_min = 60., angle_max = 90.)

	return cover + concrete



def cover_surface2(mesh, surface_density, material, size = 0.4, angle_min = 0., angle_max = 10.):

	cover_vertices, cover_triangles, Nquads = modulo.cover_surface(mesh, surface_density, material, size, angle_min, angle_max)

	cover_submesh = tools.SubMesh(vertices = cover_vertices, triangles = cover_triangles, uvs = [[0, 0]]*4*Nquads, colors = [[1]*4]*4*Nquads, material = material)

	cover_submesh.uvs = tools.wrap_uvs_method1(cover_submesh)

	return cover_submesh.get_mesh()



def twosided(level, s_start, s_end, s):
	D = s_end - s_start
	h = 0.3*D
	if s <= s_start + D/2:
		return (s - s_start)*h/D*2 + level
	else:
		return (s_end - s)*h/D*2 + level

def curve(level, s_start, s_end, s, R = 10.):
	D, sm  = s_end - s_start, (s_end - s_start)/2. + s_start
	l0 = np.sqrt(R**2 - (D/2)**2)
	h  = np.sqrt(R**2 - (sm - s)**2) - l0
	return h + level

def sine(level, s_start, s_end, s, P = 1.0):
	N = max(int((s_end - s_start)/P), 1)
	P = (s_end - s_start)/N
	h = P/(2*np.pi)
	return h*np.sin((s - s_start)/P*2*np.pi) + h + level
