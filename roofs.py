import numpy as np
import tools
import shape_generation as shape_gen
import geometry_generation as geo_gen

def determine_roofs(whole_shapes):
	nlevels = len(whole_shapes)
	roofs, levels = [], []
	for i in range(nlevels):
		current_shape = whole_shapes[i]

		if current_shape.type == "Polygon":
			shapes = [current_shape]
			nshapes = 1
		elif current_shape.type == "MultiPolygon":
			shapes = current_shape
			nshapes = len(current_shape)
		else:
			nshapes = 0

		for j in range(nshapes):
			shape = shapes[j]

			if i == nlevels - 1:
				roofs.append(shape)
				levels.append(i)

			else:
				shape_above = whole_shapes[i + 1]
				if (shape.intersection(shape_above)).area < 0.001:
					roofs.append(shape)
					levels.append(i)
	return roofs, levels

def merge_levels(whole_shapes_o, built_shapes_o, levels_height_o, new_shapes, new_levels):
	whole_shapes = list.copy(whole_shapes_o)
	built_shapes = list.copy(built_shapes_o)
	levels_height= list.copy(levels_height_o)

	whole_shapes  += new_shapes
	levels_height += new_levels

	unique_levels = np.unique(levels_height)
	unique_whole  = []

	idxs = np.arange(len(levels_height))
	for level in unique_levels:
		mask = abs(levels_height - level) <= 0.001
		repeated = idxs[mask]

		current_shapes = []
		for i in repeated:
			current_shapes.append(whole_shapes[i])
		unique_whole.append(tools.unary_union(current_shapes))

	nlevels    = len(unique_levels)
	arg_sorted = np.argsort(unique_levels)


	merged_built_shapes = [""]*nlevels
	merged_whole_shapes = [""]*nlevels
	merged_levels       = np.zeros(nlevels)

	above_shape         = tools.Polygon()
	for i in range(nlevels - 1, -1, -1):
		index                  = arg_sorted[i]
		merged_whole_shapes[i] = tools.orient(tools.unary_union([unique_whole[index], above_shape]))
		merged_levels[i]       = unique_levels[index]

		above_shape = merged_whole_shapes[i]

	merged_built_shapes[-1] = tools.Polygon()
	for i in range(nlevels - 1):
		merged_built_shapes[i] = merged_whole_shapes[i + 1]
	return merged_whole_shapes, merged_built_shapes, merged_levels

def roof_shape_info(width, length):
	min_sizeX = max(width, 3.0)
	max_sizeX = max(width, min_sizeX)

	min_sizeY = max(length/2., 4.0)
	max_sizeY = max(min(length*0.4, length - 2), min_sizeY)

	mean_sizeX, mean_sizeY = width, length/2.
	mean_sizeX, mean_sizeY = width, 8.
	std_sizeX, std_sizeY   = width/4., length/4.

	std_locX, std_locY = width/2.*0, length/8*0

	return min_sizeX, max_sizeX, mean_sizeX, std_sizeX, min_sizeY, max_sizeY, mean_sizeY, std_sizeY, std_locX, std_locY

miniroof_shapeinfo = lambda width, length: (2., 5., 3., 3., 2., 5., 3., 3., 4., 4.)

def generate_roofs(whole_shapes, built_shapes, levels_height):
	roofs, levels = determine_roofs(whole_shapes)

	minilevel = False

	new_shapes, new_levels = [], []

	nroofs = len(roofs)
	for i in range(nroofs):
		roof  = roofs[i]
		level = levels[i]
		height = levels_height[level]

		generate_shape = True if height >= 3*3.0 else False

		if generate_shape:
			N = tools.weigh_output([1, 2, 3], [1, 2, 1])

			success, main_shape = shape_gen.generate_shape(roof, params = roof_shape_info, N = N, orientation = 0.0, passages_check = False)
			if success:
				new_shapes.append(main_shape)
				new_levels.append(height + 3.)

			if minilevel and success:
				success, mini_shape = shape_gen.generate_shape(main_shape, params = miniroof_shapeinfo, N = 1, passages_check = False)
				if success:
					new_shapes.append(mini_shape)
					new_levels.append(height + 4.)

	new_whole_shapes, new_built_shapes, new_levels = merge_levels(whole_shapes, built_shapes, levels_height, new_shapes, new_levels)
	return new_whole_shapes, new_built_shapes, new_levels

def create_imperfections(facet_shape, free_space):
	room_size = 5.0
	wall_width = tools.suggest_value(0.2, 0.3, 0.2, 0.2, 0.01)

	if facet_shape.area != 0.:
		extent, center, width, length = tools.determine_extent(facet_shape)

		n = int(width/room_size) + 1
		m = int(width/room_size) + 1

		x_space = np.linspace(extent[0] + 0.15 + wall_width/2., extent[1] - wall_width/2. - 0.15, n)
		y_space = np.linspace(extent[2] + 0.15 + wall_width/2., extent[3] - wall_width/2. - 0.15, m)

		shapes = []
		for i in range(n):
			line = tools.LineString([[x_space[i], extent[2]], [x_space[i], extent[3]]])
			shapes.append(line.buffer(wall_width/2., join_style = 2))

		for j in range(m):
			line = tools.LineString([[extent[0], y_space[j]], [extent[1], y_space[j]]])
			shapes.append(line.buffer(wall_width/2., join_style = 2))

		return tools.unary_union(shapes).intersection(free_space)

	else:
		return tools.Polygon()

def get_furthest(center, points):
	dist2center = np.sqrt((points[:, 0] - center[0])**2 + (points[:, 1] - center[1])**2)
	i = np.where(dist2center == dist2center.max())[0][0]
	return i

def determine_rest_points(shape):
	success = False
	if shape.type == "Polygon":
		rest_points = np.zeros([4, 2])
		list_positions = np.array(tools.get_rings(shape)[0])
		center = np.array(shape.centroid)

		dist2center = np.sqrt((list_positions[:, 0] - center[0])**2 + (list_positions[:, 1] - center[1])**2)

		A = list_positions[get_furthest(center, list_positions)]
		# print(A)
		C = list_positions[get_furthest(A, list_positions)]

		u = (C - A)/np.linalg.norm(C - A)
		v = np.array([-u[1], u[0]])

		component_v = list_positions[:, 0]*v[0] + list_positions[:, 1]*v[1]

		B = list_positions[np.where(component_v == component_v.max())[0]][0]
		D = list_positions[np.where(component_v == component_v.min())[0]][0]

		rest_points[0] = A
		rest_points[1] = B
		rest_points[2] = C
		rest_points[3] = D

		success = True
		return success, rest_points
	else:
		return success, np.zeros([0, 2])

def generate_watertank(shape, volume, level, height = 2.0, rest_height = 1.2, material = "Wall", color = np.ones(4)):
	# print("To generate water tank")
	length, width, orientation, coords, centroid = tools.get_orientation(shape)
	factor_form = 1.5

	area = volume/height

	L = (area*factor_form)**0.5

	tank_shape = tools.create_box(np.array(shape.centroid), [L/factor_form, L], orientation).intersection(shape)
	tank_shape = tools.choose_biggest(tank_shape)

	if tank_shape.area != 0.:
		height = min(volume/tank_shape.area,  3.)
		mode = tools.weigh_output(["patas", "distinto"], [1., 2.])
		success, rest = determine_rest_points(tank_shape)

		if success and mode == "patas":
			rest_shapes = []
			for point in rest:
				rest_shapes.append(tools.create_box(point, [2, 2], orientation).intersection(tank_shape))

			rest_shapes = tools.unary_union(rest_shapes)
		else:
			eroded_shape = tank_shape.buffer(-0.5)
			if eroded_shape.area != 0.:
				rest_shapes = eroded_shape
			else:
				rest_shapes = tank_shape

		feet = geo_gen.extrude_rings(rest_shapes, level, level + rest_height, material, np.ones(4)*0.2)
		lid1 = geo_gen.meshify_shape(tank_shape.difference(rest_shapes), level + rest_height, "Wall", color*0.8)
		mesh = geo_gen.extrude_shape(tank_shape, level + rest_height, level + rest_height + height, material, color*0.8, color*0.8)

		lid1.flip()
		return True, mesh + feet + lid1
	else:
		print("La concha")
		return False, tools.Mesh()
