import numpy as np
import tools
from tools import deg2rad, rad2deg, vec_right, vec_left, vec_up, vec_down, vec_front, vec_back
import geometry_generation as geo_gen

window_presets = ["glass", "normal", "curtains", "blind_wood", "blind_metal", "blind_plastic"]

DEBUG = False


def extract_shape(submesh, i = 1, j = 2):
	shape = tools.Polygon()
	for triangle in submesh.triangles:
		vert1, vert2, vert3 = submesh.vertices[triangle[0]], submesh.vertices[triangle[1]], submesh.vertices[triangle[2]]

		tri_shape = tools.Polygon([ [vert1[i], vert1[j]],  [vert2[i], vert2[j]], [vert3[i], vert3[j]], [vert1[i], vert1[j]] ])
		shape = tools.unary_union([shape, tri_shape])

	return tools.remove_junk_segments(shape)

def from_facade_shapes(mesh):
	shapes = {}
	keys = ["color1", "color2", "color3", "window_reference", "balcony"]

	for key in keys:
		if key in mesh.submeshes.keys():
			submesh = mesh.submeshes[key]
			shapes[key] = extract_shape(submesh)
		else:
			shapes[key] = tools.Polygon()
	return shapes

def check_quads(vertices, triangles, uvs):
	criterium = 0.001
	ntris, nvert = len(triangles), len(vertices)

	windows = -np.ones(nvert, dtype = np.int32)
	doors   = -np.ones(nvert, dtype = np.int32)

	window_ids, door_ids, win_scales, win_pos, door_scales, door_pos = [], [], [], [], [], []
	new_verts, new_tris, new_uvs = [], [], []
	win_count, door_count, vert_offset = 0, 0, 0
	for i in range(ntris):
		triangle1 = triangles[i]
		for j in range(i + 1, ntris):
			neighbors = False
			triangle2 = triangles[j]

			### Check if neighbors
			A1, B1, C1 = vertices[triangle1]
			A2, B2, C2 = vertices[triangle2]

			normal1 = np.cross(B1 - A1, C1 - A1)
			normal2 = np.cross(B2 - A2, C2 - A2)

			normal1 *= 1./np.linalg.norm(normal1)
			normal2 *= 1./np.linalg.norm(normal2)

			triang1_verts = np.zeros(9, dtype = np.int32)
			triang2_verts = np.zeros(9, dtype = np.int32)

			for k in range(3):
				triang1_verts[3*0 + k] = triangle1[0]
				triang1_verts[3*1 + k] = triangle1[1]
				triang1_verts[3*2 + k] = triangle1[2]

				triang2_verts[3*k + 0] = triangle2[0]
				triang2_verts[3*k + 1] = triangle2[1]
				triang2_verts[3*k + 2] = triangle2[2]


			combinations = vertices[triang1_verts] - vertices[triang2_verts]
			distances = np.linalg.norm(combinations, axis = 1)

			if np.dot(normal1, normal2) >= 1 - criterium and abs(normal1[2]) <= criterium:
				mask = distances <= criterium

				if mask.sum() == 2:
					shared_idxs1 = triang1_verts[mask]
					shared_idxs2 = triang2_verts[mask]
					P1, Q1 = vertices[shared_idxs1[0]], vertices[shared_idxs1[1]]
					P2, Q2 = vertices[shared_idxs2[0]], vertices[shared_idxs2[1]]

					for item in triangle1:
						if not(item in shared_idxs1):
							nonshared1 = item
					for item in triangle2:
						if not(item in shared_idxs2):
							nonshared2 = item

					R1, R2 = vertices[nonshared1], vertices[nonshared2]

					segment1 = np.linalg.norm(P1 - Q1)
					segment2 = np.linalg.norm(R1 - R2)
					center   = (P1 + P2 + Q1 + Q2)/4.

					is_quad = np.abs(segment1/segment2 - 1.) <= criterium

					x_scale = np.linalg.norm((P1 - Q1)[0:2])
					z_scale = abs(P1[2] - Q1[2])

					if is_quad and x_scale >= 0.2 and z_scale >= 0.2:
						min_height = min([A1[2], B1[2], C1[2], A2[2], B2[2], C2[2]])

						windows[triangle1] = win_count
						windows[triangle2] = win_count
						win_count += 1

						window_ids.append([vert_offset + 0, vert_offset + 1, vert_offset + 2] + [vert_offset + 3, vert_offset + 4, vert_offset + 5])
						win_scales.append([x_scale, z_scale])
						win_pos.append([center[1], center[2]])

						new_verts += [vertices[triangle1]]
						new_verts += [vertices[triangle2]]

						new_uvs   += [uvs[triangle1]]
						new_uvs   += [uvs[triangle2]]

						total_count = win_count + door_count

						new_tris += [[vert_offset + 0, vert_offset + 1, vert_offset + 2]]
						new_tris += [[vert_offset + 3, vert_offset + 4, vert_offset + 5]]

						vert_offset += 6
	if vert_offset != 0:
		mesh = tools.SubMesh(np.concatenate(new_verts), np.array(new_tris), np.concatenate(new_uvs), colors = [np.ones(4)]*vert_offset, material = "window_reference")
	else:
		mesh = tools.SubMesh(material = "window_reference")
	return window_ids, door_ids, win_scales, win_pos, door_scales, door_pos, mesh

class facade_model:
	def __init__(self, width = 10., height = 3.):
		self.mesh    = None
		self.windows = None
		self.doors   = None
		self.width   = width
		self.height  = height
		self.shapes  = {}

	def get_shapes(self):
		self.shapes = from_facade_shapes(self.mesh)

	def load(self):
		pass

	def identify_windows(self, win_mat):
		if "window_reference" in self.mesh.submeshes.keys():
			tris, verts, uvs = self.mesh.submeshes[win_mat].triangles, self.mesh.submeshes[win_mat].vertices, self.mesh.submeshes[win_mat].uvs
			self.windows, self.doors, self.win_scales, self.win_pos, self.door_scales, self.door_pos, self.mesh.submeshes["window_reference"] = check_quads(verts, tris, uvs)
			self.max_width = np.max(abs(self.mesh.whole_mesh.vertices[:, 1]))*2
		else:
			self.windows, self.doors, self.win_scales, self.door_scales = [], [], [], []


	def get_facade(self, nfloors, ncols, total_width, floor_height, position, orientation):
		width = total_width/ncols
		materials, new_tris, new_verts, new_colors, new_uvs = self.mesh.materials, [], [], [], []
		for material in materials:
			tris   = self.mesh.submeshes[material].triangles
			verts  = self.mesh.submeshes[material].vertices
			uvs    = self.mesh.submeshes[material].uvs
			colors = self.mesh.submeshes[material].colors

			#triangle_correction = np.reshape(np.tile(np.repeat(np.arange(nfloors*ncols)*len(verts), len(tris)), 3), [nfloors*ncols*len(tris), 3])
			new_tris.append(np.tile(tris,     [ncols*nfloors, 1]))
			new_verts.append(np.tile(verts,   [ncols*nfloors, 1]))
			new_uvs.append(np.tile(uvs,       [ncols*nfloors, 1]))
			new_colors.append(np.tile(colors, [ncols*nfloors, 1]))
		facade_mesh = tools.Mesh(vertices_list = new_verts, triangles_list = new_tris, uvs_list = new_uvs, colors_list = new_colors, materials_list = materials)

		vec_i = np.array([ np.cos(orientation), np.sin(orientation), 0.])
		vec_j = np.array([-np.sin(orientation), np.cos(orientation), 0.])

		v_offset, t_offset = 0, 0
		for material in materials:
			verts  = np.copy(self.mesh.submeshes[material].vertices)
			nverts = len(verts)
			ntris  = len(self.mesh.submeshes[material].triangles)
			rotated_verts = np.zeros(verts.shape)
			for i in range(ncols):
				for j in range(nfloors):
					current_pos = position + vec_i*(-total_width + width*(0.5 + i)) + vec_up*floor_height*j

					rotated_verts[:, 0] = current_pos[0] + verts[:, 0]*vec_i[0] + verts[:, 1]*vec_j[0]*width/self.width + verts[:, 2]*vec_up[0]*floor_height/self.height
					rotated_verts[:, 1] = current_pos[1] + verts[:, 0]*vec_i[1] + verts[:, 1]*vec_j[1]*width/self.width + verts[:, 2]*vec_up[1]*floor_height/self.height
					rotated_verts[:, 2] = current_pos[2] + verts[:, 0]*vec_i[2] + verts[:, 1]*vec_j[2]*width/self.width + verts[:, 2]*vec_up[2]*floor_height/self.height

					facade_mesh.submeshes[material].vertices[v_offset:v_offset + nverts] = rotated_verts
					facade_mesh.submeshes[material].triangles[t_offset: t_offset + ntris] += np.ones([ntris, 3], dtype = np.int32)*v_offset
					v_offset += nverts
					t_offset += ntris

		return facade_mesh

def generate_facade_preset(tag, main_color):
	color1 = generate_color_palette2(main_color[0:3]/main_color[0:3].max(), strength = 0.9, light = 0.0)*np.random.random()
	color2 = generate_color_palette2(main_color[0:3]/main_color[0:3].max(), strength = 0.9, light = 0.0)*np.random.random()
	color3 = main_color

	window1 = tools.rand_pick(window_presets)
	window2 = tools.rand_pick(window_presets)
	window3 = tools.rand_pick(window_presets)

	return {"tag": tag, "asset_id": np.random.randint(100), "color1": color1, "color2": color2, "color3": color3, "window1": window1, "window2": window2, "window3": window3}

real_materials = False

mat_wall_rich = ["Wall", "Smooth_Bricks", "Rough_Bricks", "Rough_Shiny", "Dirty_Poor1", "Dirty_Poor2"]
mat_floor_rich= ["Vertex_Rough", "Brick_Shinny", "Hexagonal", "Vertex_Shinny", "Smooth", "Brick_Rough", "Smooth2", "Stripped_Red", "Stripped_Blue", "Stripped_White", "Chess_Tiles", "Wooden_Tiles"]
mat_metal_rich= ["Roof_Black", "Clean_Zinc", "Plastic_Roof", "Rusty_Roof"]
mat_terr_rich = ["Vertex_Rough", "Brick_Shinny", "Hexagonal", "Vertex_Shinny", "Smooth", "Brick_Rough", "Smooth2", "Stripped_Red", "Stripped_Blue", "Stripped_White", "Chess_Tiles", "Wooden_Tiles"]

mat_wall_poor = ["Wall", "Smooth_Bricks", "Dirty_Rough", "Rough_Rricks", "Dirty_Poor1", "Dirty_Poor2"]
mat_floor_poor= ["Vertex_Rough", "Brick_Shinny", "Hexagonal", "Vertex_Shinny", "Smooth", "Brick_Rough", "Smooth2", "Stripped_Red", "Stripped_Blue", "Stripped_White", "Chess_Tiles"]
mat_metal_poor= ["Clean_Zinc", "Plastic_Roof", "Rusty_Roof"]
mat_terr_poor = ["Vertex_Rough", "Brick_Shinny", "Hexagonal", "Vertex_Shinny", "Smooth", "Brick_Rough", "Smooth2", "Stripped_Red", "Stripped_Blue", "Stripped_White", "Chess_Tiles"]

mat_wall_office = ["Wall"]
mat_roof_office = ["Dirty_Roof", "Dirty_Roof2", "Dirty_Roof3", "Dirty_Roof4", "Concrete"]
mat_roof2_office= ["Roof_Green", "Roof_Blue", "Roof_Tiles", "Roof_Grey"]
mat_floor_office= ["Vertex_Rough", "Brick_Shinny", "Hexagonal", "Vertex_Shinny", "Smooth", "Brick_Rough", "Smooth2", "Stripped_Red", "Stripped_Blue", "Stripped_White", "Chess_Tiles"]
mat_metal_office= ["Clean_Zinc", "Plastic_Roof", "Rusty_Roof"]
mat_ground      = ["Grass"]

class building_presets:
	def __init__(self, kind = "offices", wealth = "rich", dirtyness = 0.):
		if kind == "appartments":
			if real_materials:
				self.mat_roof    = tools.rand_pick(["Dirty_Roof", "Dirty_Roof2", "Dirty_Roof3", "Dirty_Roof4", "Concrete"])
			else:
				self.mat_roof    = tools.rand_pick(["Dirty_Roof", "Dirty_Roof2", "Dirty_Roof3", "Dirty_Roof4", "Real_Concrete", "Real_Concrete2"])

			if wealth == "rich":

				if real_materials:
					self.mat_wall    = tools.weigh_output(["Real_Wall", "Real_Bricks", "Real_Rough_Bricks", "Real_Rough_Bricks2", "Real_Poor_Bricks"], [12, 3, 0.5, 2, 0.])
					self.mat_metal   = tools.weigh_output(["Roof_Black", "Clean_Zinc", "Plastic_Roof", "Rusty_Roof"], [3., 10., 8., 3.])
					self.mat_floor   = tools.weigh_output(["Real_Brick_Tiles", "Real_Brick_Tiles2", "Real_Brick_Tiles3", "Real_Chess_Tiles", "Real_Tiles1", "Real_Tiles2", "Real_Tiles3", "Real_Tiles4", "Real_Tiles5", "Real_Vertex_Tiles", "Real_Wooden_Tiles"], [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 6])
					self.mat_terrace = tools.weigh_output(["Real_Brick_Tiles", "Real_Brick_Tiles2", "Real_Brick_Tiles3", "Real_Chess_Tiles", "Real_Tiles1", "Real_Tiles2", "Real_Tiles3", "Real_Tiles4", "Real_Tiles5", "Real_Vertex_Tiles", "Real_Wooden_Tiles"], [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 6])
				else:
					self.mat_wall    = tools.weigh_output(mat_wall_rich, [12, 3, 0.5, 2, 0., 0.])
					self.mat_metal   = tools.weigh_output(mat_metal_rich, [3., 10., 8., 3.])
					self.mat_floor   = tools.weigh_output(mat_floor_rich, [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 6, 2])
					self.mat_terrace = tools.weigh_output(mat_terr_rich, [10, 4, 4, 3, 5, 5, 2, 5, 1, 1, 2, 5])

			else:
				if real_materials:
					self.mat_wall    = tools.weigh_output(["Real_Wall", "Real_Wall2", "Real_Bricks", "Real_Rough_Bricks", "Real_Rough_Bricks2", "Real_Poor_Bricks"], [7, 2, 0.5, 0.5, 0.5, 2])
					self.mat_metal   = tools.weigh_output(["Roof_Black", "Clean_Zinc", "Plastic_Roof", "Rusty_Roof"], [3., 10., 8., 3.])
					self.mat_floor   = tools.weigh_output(["Real_Brick_Tiles", "Real_Brick_Tiles2", "Real_Brick_Tiles3", "Real_Chess_Tiles", "Real_Tiles1", "Real_Tiles2", "Real_Tiles3", "Real_Tiles4", "Real_Tiles5", "Real_Vertex_Tiles", "Real_Wooden_Tiles"], [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 0])
					self.mat_terrace = tools.weigh_output(["Real_Brick_Tiles", "Real_Brick_Tiles2", "Real_Brick_Tiles3", "Real_Chess_Tiles", "Real_Tiles1", "Real_Tiles2", "Real_Tiles3", "Real_Tiles4", "Real_Tiles5", "Real_Vertex_Tiles", "Real_Wooden_Tiles"], [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 0])
				else:
					self.mat_wall    = tools.weigh_output(mat_wall_poor, [10, 1, 0.5, 1, 5., 2.])
					self.mat_metal   = tools.weigh_output(mat_metal_poor, [5., 8., 7.])
					self.mat_floor   = tools.weigh_output(mat_floor_poor, [0, 4, 4, 1, 1, 3, 3, 1, 1, 4, 1])
					self.mat_terrace = tools.weigh_output(mat_terr_poor,  [0, 4, 4, 1, 1, 3, 3, 1, 1, 4, 0.5])

			if real_materials:
				self.mat_roof2   = tools.rand_pick(["Real_Roof_Tiles", "Real_Roof_Tiles2", "Real_Roof_Tiles3"])
			else:
				self.mat_roof2   = tools.rand_pick(["Roof_Black", "Roof_Green", "Roof_Blue", "Roof_Tiles", "Roof_Grey"])
			self.mat_ground  = mat_ground[0]

			red_terrace   = generate_color_palette2([0.70, 0.35, 0.20], strength = 0.9, light = 0)
			brick_terrace = generate_color_palette2([0.70, 0.35, 0.20], strength = 0.9, light = 0.3)
			green_terrace = generate_color_palette2([0.30, 0.55, 0.30], strength = 0.9, light = 0)
			white_terrace = generate_color_palette2([0.50, 0.50, 0.50], strength = 0.8, light = 0.8)
			grey_color    = np.random.random()*0.3 + 0.7
			self.terrace_color =  tools.weigh_output([red_terrace, brick_terrace, green_terrace, white_terrace], [5, 5, 6, 4])
			self.terrace_color[3] = dirtyness

			self.main_color    = np.ones(4, dtype = np.float32)*grey_color if np.random.random() >= 0.5 else generate_color_palette2(np.array([0.8, 0.15, 0.15])*0.5, strength = .6, light = 0.4)
			self.main_color[3] = dirtyness


			self.facade_main_normal1 = generate_facade_preset("front",   self.main_color)
			self.facade_main_normal2 = generate_facade_preset("back",    self.main_color)
			self.facade_main_balcony = generate_facade_preset("balcony", self.main_color)
			self.facade_small_yard   = generate_facade_preset("yard",    self.main_color)
			self.facade_PB           = generate_facade_preset("PB",      self.main_color)
			self.facade_terrace      = generate_facade_preset("terrace", self.main_color)
			self.facade_parking      = generate_facade_preset("parking", self.main_color)
			self.service_door        = generate_facade_preset("door",    self.main_color)
			self.service_window      = generate_facade_preset("window",  self.main_color)

		elif kind == "offices":
			if real_materials:
				self.mat_roof2   = tools.rand_pick(["Real_Roof_Tiles", "Real_Roof_Tiles2", "Real_Roof_Tiles3"])
				self.mat_roof    = tools.rand_pick(["Dirty_Roof", "Dirty_Roof2", "Dirty_Roof3", "Dirty_Roof4", "Real_Concrete", "Real_Concrete2"])
				self.mat_terrace = "Real_Vertex_Tiles"
				self.mat_wall    = "Real_Wall"
				self.mat_floor   = tools.weigh_output(["Real_Brick_Tiles", "Real_Brick_Tiles2", "Real_Brick_Tiles3", "Real_Chess_Tiles", "Real_Tiles1", "Real_Tiles2", "Real_Tiles3", "Real_Tiles4", "Real_Tiles5", "Real_Vertex_Tiles", "Real_Wooden_Tiles"], [5, 3, 3, 2, 5, 5, 2, 5, 3, 1, 6])
				self.mat_ground  = "Grass"
				self.mat_metal   = tools.weigh_output(["Clean_Zinc", "Plastic_Roof", "Rusty_Roof"], [10, 2])
			else:
				self.mat_roof2   = tools.rand_pick(mat_roof2_office)
				self.mat_roof    = tools.rand_pick(mat_roof_office)
				self.mat_terrace = "Vertex_Shinny"

				self.mat_wall    = mat_wall_office[0]
				self.mat_floor   = tools.rand_pick(mat_floor_office)
				self.mat_ground  = mat_ground[0]
				self.mat_metal   = tools.weigh_output(mat_metal_office, [10, 2])
			red_terrace   = generate_color_palette2([0.70, 0.35, 0.20], strength = 0.9, light = 0)
			brick_terrace = generate_color_palette2([0.70, 0.35, 0.20], strength = 0.9, light = 0.3)
			green_terrace = generate_color_palette2([0.30, 0.55, 0.30], strength = 0.9, light = 0)
			white_terrace = generate_color_palette2([0.50, 0.50, 0.50], strength = 0.8, light = 0.8)
			self.terrace_color =  [red_terrace, brick_terrace, green_terrace, white_terrace][tools.weigh_decision([5, 5, 6, 4])]
			self.terrace_color[3] = dirtyness
			grey_color = np.random.random()*0.1 + 0.9
			self.main_color = np.ones(4, dtype = np.float32)*grey_color if tools.pick_bool(0.6) else generate_color_palette2([0.0, 0.5, 0.8], strength = .7, light = 0.8)
			self.main_color[3] = dirtyness

			self.facade_main_normal1 = generate_facade_preset("offices", self.main_color)
			self.facade_main_normal2 = generate_facade_preset("offices", self.main_color)
			self.facade_main_balcony = generate_facade_preset("balcony", self.main_color)
			self.facade_small_yard   = generate_facade_preset("yard",    self.main_color)
			self.facade_PB           = generate_facade_preset("PB",      self.main_color)
			self.facade_terrace      = generate_facade_preset("terrace", self.main_color)
			self.facade_parking      = generate_facade_preset("parking", self.main_color)
			self.service_door        = generate_facade_preset("door",    self.main_color)
			self.service_window      = generate_facade_preset("window",  self.main_color)

		print("Color = " + str(self.main_color))
		#self.mat_wall, self.mat_terrace, self.mat_roof, self.mat_roof2, self.mat_metal, self.mat_ground, self.mat_floor = "Generic_Wall", "Generic_Tiles", "Generic_Concrete", "Generic_Concrete", "Generic_Metal", "Generic_Ground", "Generic_Tiles"

def replace_material(mat_name, replace = False):
	"""
	if replace:
		if mat_name in ["Wall", "Smooth_Wall"]:
			return "Generic_Wall"
		elif mat_name in ["Smooth_Bricks"]:
			return "Generic_Smooth"
		elif mat_name in ["Rough_Rricks", "Rough_Shiny"]:
			return "Generic_Rough"
		elif mat_name in ["Dirty_Poor1", "Dirty_Poor2"]:
			return "Generic_Poor"
		elif mat_name in ["Vertex_Rough", "Vertex_Shinny"]:
			return "Generic_Tiles"
		elif mat_name in ["Chess_Tiles"]:
			return "Generic_Chess"
		elif mat_name in ["Wooden_Tiles"]:
			return "Generic_Wooden"
		elif mat_name in ["Stripped_Red", "Stripped_Blue", "Stripped_White"]:
			return "Generic_Stripped"
		elif mat_name in ["Brick_Shinny", "Brick_Rough"]:
			return "Generic_Brick"
		elif mat_name in ["Hexagonal", "Smooth", "Smooth2"]:
			return "Generic_Hex"
		elif mat_name in ["Roof_Black", "Clean_Zinc", "Plastic_Roof", "Rusty_Roof"]:
			return "Generic_Zinc"
		elif mat_name in ["Dirty_Roof", "Dirty_Roof2", "Dirty_Roof3", "Dirty_Roof4", "Concrete"]:
			return "Generic_Roof"
		else:
			return mat_name
	"""
	if replace:
		return "Wall"
	else:
		return mat_name

class Trees:
	def __init__(self):
		self.kind         = "Trees"
		self.positions    = []
		self.heights      = []
		self.radii        = []
		self.tags         = []
		self.orientations = []

	def add(self, position, height, radius, tag = "default", orientation = 0.):
		self.positions.append(position)
		self.heights.append(height)
		self.tags.append(tag)
		self.radii.append(radius)
		self.orientations.append(orientation)

	def merge(self, other):
		self.positions    = self.positions    + other.positions
		self.heights      = self.heights      + other.heights
		self.tags         = self.tags         + other.tags
		self.radii        = self.radii        + other.radii
		self.orientations = self.orientations + other.orientations

	def pivot(self, pivot_point, rotation, translation):
		self.positions, self.orientations = pivot(self.positions, self.orientations, pivot_point, rotation, translation)

def create_windows_doors(asset, app_pos_2D, scale_factor_x, scale_factor_z, preset, win_depth):
	### preset comes from generate_facade_preset
	N_windows = len(asset.win_scales)
	# parte_win = np.copy(asset.mesh.submeshes["window_reference"])
	# mesh_copy = np.copy(asset.mesh)
	# mesh_copy.rotate(tools.vec_up, -90*tools.deg2rad)
	# mesh_copy.rotate(tools.vec_up,    facet.orientation)

	facade_windows_mesh = tools.Mesh()
	for k in range(N_windows):
		x_scale = asset.win_scales[k][0]
		z_scale = asset.win_scales[k][1]

		window_shape = tools.create_box(asset.win_pos[k], [x_scale, z_scale], 0.)
		real_pos     = np.array(app_pos_2D) + np.array(asset.win_pos[k])*np.array([scale_factor_x, scale_factor_z])
		real_scale   = np.array([x_scale*scale_factor_x, z_scale*scale_factor_z])
		new_verts    = np.array([
		[real_pos[0] - real_scale[0]/2., real_pos[1] - real_scale[1]/2., -win_depth],
		[real_pos[0] + real_scale[0]/2., real_pos[1] - real_scale[1]/2., -win_depth],
		[real_pos[0] + real_scale[0]/2., real_pos[1] + real_scale[1]/2., -win_depth],
		[real_pos[0] - real_scale[0]/2., real_pos[1] + real_scale[1]/2., -win_depth]])

		new_uvs      = np.array([
		[0., 0.],
		[1., 0.],
		[1., 1.],
		[0., 1.]])

		new_tris   = np.array([[0, 1, 2], [2, 3, 0]])

		new_colors = np.zeros([4, 4], dtype = np.float32)

		#verices = parte_win.mesh
		if   np.mod(k, 3) == 0: win_mat = preset["window1"]
		elif np.mod(k, 3) == 1: win_mat = preset["window2"]
		elif np.mod(k, 3) == 2: win_mat = preset["window3"]

		if win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
			material = "Blind_Interior"

		elif win_mat == "curtains":
			material = "Curtains_Interior"

		elif win_mat == "glass":
			material = "Normal_Interior"

		elif win_mat == "normal":
			material = "Normal_Interior"

		aperture = 0. if np.random.random() <= 0.4 else np.random.random()
		R, G, B  = np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5

		# actual_size_x = width/asset_width*x_scale
		# actual_size_x = scale_factor_x*x_scale
		# actual_size_z = height/asset_height*z_scale

		new_colors[:, 2] = real_scale[0]/5.0
		new_colors[:, 3] = real_scale[1]/5.0

		if win_mat in ["glass", "curtains"]:
			new_colors[:, 0] = R
			new_colors[:, 1] = aperture*0.5

		elif win_mat == "normal":
			new_colors[:, 0] = np.random.random()
			new_colors[:, 1] = np.random.random()

		elif win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
			if   win_mat == "blind_wood":    parameter = 0.0
			elif win_mat == "blind_metal":   parameter = 0.5
			else:                            parameter = 1.0

			new_colors[:, 0] = parameter
			new_colors[:, 1] = aperture

		window_mesh = tools.SubMesh(vertices = new_verts, triangles = new_tris, uvs = new_uvs, colors = new_colors, material = material).get_mesh()
		facade_windows_mesh += window_mesh
	return facade_windows_mesh
"""
def create_balcony(shape, scale_factor_x, scale_factor_z, depth):
	if current_shape.area != 0.:
		rings = tools.get_rings(current_shape)

		# exterior = np.array(rings[0])
		for ring in rings:
			ring = np.array(ring)
			if len(ring) >= 3:
				ring[:, 0] *= scale_factor_x
				ring[:, 1] *= scale_factor_z

				ring[:, 0] += offset[0]
				ring[:, 1] += offset[1]

		front_shape
		wall_shape

"""

def get_mesh_from_shapes(presets_list, facet, facade_assets, x_pos, z_pos, app_widths, facade_height):
	# facade_shape = tools.create_box_from_extent([-facet.width/2, facet.width/2, 0., facet.max_height - facet.min_height])
	facade_shape = tools.create_box_from_extent([0., facet.width, 0., facet.max_height - facet.min_height])
	nfacades = len(x_pos)
	combined_exteriors = {"color1": [], "color2": [], "color3": [], "window_reference": []}
	keys = ["color1", "color2", "color3", "window_reference"]

	mesh = tools.Mesh()
	windows_mesh = tools.Mesh()

	nfacades = len(x_pos)
	win_depth = tools.suggest_value(0.05, 0.3, 0.2, 0.1, 0.01)

	if len(x_pos) >= 2 or x_pos[0] != 0:
		balcony_mesh = tools.Mesh()
		for i in range(nfacades):
			preset     = presets_list[i]
			asset_name = preset["tag"]
			asset      = facade_assets[asset_name][np.mod(preset["asset_id"], len(facade_assets[asset_name]))]
			shapes     = asset.shapes

			# scale_factor_x = max(facade_width/10., facet.width/asset.max_width)
			scale_factor_x = min(app_widths[i]/10., facet.width/asset.max_width)
			scale_factor_x = app_widths[i]/asset.max_width
			scale_factor_z = facade_height/3.

			offset = np.array([x_pos[i], z_pos[i]])
			for key in keys:
				current_shape = shapes[key]
				if current_shape.area != 0.:
					rings = tools.get_rings(current_shape)

					# exterior = np.array(rings[0])
					for ring in rings:
						ring = np.array(ring)
						if len(ring) >= 3:
							ring[:, 0] *= scale_factor_x
							ring[:, 1] *= scale_factor_z

							ring[:, 0] += offset[0]
							ring[:, 1] += offset[1]

							# combined_exteriors[key] = combined_exteriors[key] + [exterior]
							combined_exteriors[key].append(tools.Polygon(ring))
			windows_mesh += create_windows_doors(asset, offset, scale_factor_x, scale_factor_z, preset, win_depth)
		color1_shape = tools.unary_union(combined_exteriors["color1"]).buffer(1e-4, join_style = 2)
		color2_shape = tools.unary_union(combined_exteriors["color2"]).buffer(1e-4, join_style = 2)
		color3_shape = tools.unary_union(combined_exteriors["color3"]).buffer(1e-4, join_style = 2)
		window_shape = tools.unary_union(combined_exteriors["window_reference"]).buffer(1e-4, join_style = 2)

		# color1_shape = tools.MultiPolygon(combined_exteriors["color1"]).buffer(1e-6, join_style = 2)
		# color2_shape = tools.MultiPolygon(combined_exteriors["color2"]).buffer(1e-6, join_style = 2)
		# color3_shape = tools.MultiPolygon(combined_exteriors["color3"]).buffer(1e-6, join_style = 2)
		# window_shape = tools.MultiPolygon(combined_exteriors["window_reference"]).buffer(1e-6, join_style = 2)

		feature_shape = tools.unary_union([color1_shape, color2_shape, color3_shape, window_shape])
		facade_shape = facade_shape.difference(feature_shape)


		# mesh += geo_gen.meshify_shape(window_shape, -win_depth, "window_reference", [1.]*4)
		mesh += geo_gen.meshify_shape(facade_shape, 0., "Wall", preset["color3"])
		mesh += geo_gen.extrude_rings(window_shape, 0., -win_depth, "Smooth_Wall", preset["color3"])
		mesh += windows_mesh

		mesh += geo_gen.extrude_shape(color1_shape, 0., np.random.random()*0.3, "Smooth_Wall", preset["color1"], preset["color1"])
		mesh += geo_gen.extrude_shape(color2_shape, 0., np.random.random()*0.3, "Smooth_Wall", preset["color2"], preset["color2"])
		mesh += geo_gen.extrude_shape(color3_shape, 0., np.random.random()*0.3, "Smooth_Wall", preset["color3"], preset["color3"])

	else:
		preset = presets_list[0]
		mesh += geo_gen.meshify_shape(facade_shape, 0., "Wall", preset["color3"])

	mesh.rotate(tools.vec_right, 90*tools.deg2rad)
	mesh.rotate(tools.vec_up,    facet.orientation)
	mesh.translate(facet.bottom_left)
	if "Wall" in mesh.submeshes.keys():
		mesh.submeshes["Wall"].uvs = tools.wrap_uvs_method1(mesh.submeshes["Wall"])
	if "Smooth_Wall" in mesh.submeshes.keys():
		mesh.submeshes["Smooth_Wall"].uvs = tools.wrap_uvs_method1(mesh.submeshes["Smooth_Wall"])

	return mesh


class Complex_Facades:
	def __init__(self, appartment_widths, floor_height, xpos, zpos, preset_list):
		self.presets      = preset_list
		# self.facet        = facet
		self.app_widths   = np.array(appartment_widths)
		self.floor_height = floor_height
		self.xpos         = np.array(xpos)
		self.zpos         = np.array(zpos)
		# self.facade_asset = facade_assets[preset["tag"]][np.mod(preset["asset_id"], len(facade_assets[preset["tag"]]))]

	def merge(self, other):
		self.floor_height = other.floor_height
		self.presets     = self.presets     + other.presets
		self.app_widths  = np.concatenate([self.app_widths, other.app_widths])
		self.xpos        = np.concatenate([self.xpos, other.xpos])
		self.zpos        = np.concatenate([self.zpos, other.zpos])

	def generate_mesh(self, facade_assets, facet):
		return get_mesh_from_shapes(self.presets, facet, facade_assets, self.xpos, self.zpos, self.app_widths, self.floor_height)

class Facades:
	def __init__(self):
		self.kind         = "Facades"
		self.tags         = []
		self.asset_ids    = []
		self.x_scales     = []
		self.widths       = []
		self.z_scales     = []
		self.positions    = []
		self.orientations = []
		self.colors1      = []
		self.colors2      = []
		self.colors3      = []
		self.windows1     = []
		self.windows2     = []
		self.windows3     = []

	def add(self, preset, position, x_scale, width, height, orientation):
		self.tags.append(preset["tag"])
		self.asset_ids.append(preset["asset_id"])
		self.positions.append(position)
		self.x_scales.append(x_scale)
		self.widths.append(width)
		self.z_scales.append(height)
		self.orientations.append(orientation)
		self.colors1.append(preset["color1"])
		self.colors2.append(preset["color2"])
		self.colors3.append(preset["color3"])
		self.windows1.append(preset["window1"])
		self.windows2.append(preset["window2"])
		self.windows3.append(preset["window3"])

	def merge(self, other):
		self.tags         = self.tags         + other.tags
		self.asset_ids    = self.asset_ids    + other.asset_ids
		self.x_scales     = self.x_scales     + other.x_scales
		self.widths       = self.widths       + other.widths
		self.z_scales     = self.z_scales     + other.z_scales
		self.positions    = self.positions    + other.positions
		self.orientations = self.orientations + other.orientations
		self.colors1      = self.colors1      + other.colors1
		self.colors2      = self.colors2      + other.colors2
		self.colors3      = self.colors3      + other.colors3
		self.windows1     = self.windows1     + other.windows1
		self.windows2     = self.windows2     + other.windows2
		self.windows3     = self.windows3     + other.windows3

	def pivot(self, pivot_point, rotation, translation):
		self.positions, self.orientations = pivot(self.positions, self.orientations, pivot_point, rotation, translation)

class Objects:
	def __init__(self):
		self.kind         = "Objects"
		self.tags         = []
		self.positions    = []
		self.sizes        = []
		self.orientations = []
		self.radii        = []
		self.asset_ids    = []

	def add(self, tag, asset_id, position, size, orientation, radius):
		self.tags.append(tag)
		self.asset_ids.append(asset_id)
		self.positions.append(position)
		self.sizes.append(size)
		self.orientations.append(orientation)
		self.radii.append(radius)

	def merge(self, other):
		self.tags         = self.tags         + other.tags
		self.asset_ids    = self.asset_ids    + other.asset_ids

		self.positions    = self.positions    + other.positions
		self.sizes        = self.sizes        + other.sizes
		self.orientations = self.orientations + other.orientations
		self.radii        = self.radii        + other.radii

	def pivot(self, pivot_point, rotation, translation):
		self.positions, self.orientations = pivot(self.positions, self.orientations, pivot_point, rotation, translation)

class vertical_facet:
	def __init__(self, idx, min_height, max_height, segment, shape):
		self.kind       = "vertical"
		self.idx        = idx
		self.edge       = segment
		self.min_height = min_height
		self.max_height = max_height
		self.tag        = "default"
		self.has_facade = True
		self.facade     = Complex_Facades(appartment_widths = [], floor_height = 0., xpos = [], zpos = [], preset_list = [])

		self.facet_adjacency = False

		seg3D = np.array([segment[1][0] - segment[0][0], segment[1][1] - segment[0][1], 0.])

		self.center = np.array( [segment[0][0] + seg3D[0]/2., segment[0][1] + seg3D[1]/2., 0.])
		self.width  = np.linalg.norm(seg3D)
		self.bottom_left = np.array( [segment[0][0], segment[0][1], min_height])

		direction = seg3D/self.width
		self.orientation   = np.arctan2(direction[1], direction[0])
		self.direction     = direction
		self.perpendicular = np.cross(direction, vec_up)

		#self.depth = find_closest_intersection(self.center, -self.perpendicular, shape)

		## Determining object intrusion for projected shape
		if self.width != 0.:
			self.projection = oclusion(self.edge, 5., self.width + 5.)

			intersected_area = (shape.intersection(self.projection)).area

			self.oclusion = intersected_area >= 0.01
		self.generated_door = False

class horizontal_facet:
	def __init__(self, level, polygon):
		self.kind       = "horizontal"
		self.level      = level
		self.polygon    = tools.orient(polygon)
		self.tag        = "default"

		self.area       = self.polygon.area
		self.rings      = tools.get_rings(tools.orient(polygon))
		self.adjacents  = []

def generate_pastel_color(white_probability = 0.5, pastel_strength = 0.8):
	if tools.pick_bool(white_probability):
		color = np.ones(3)
	else:
		R = abs(np.random.normal(loc = 0., scale = 2.))
		G = abs(np.random.normal(loc = 0., scale = 0.5))
		B = abs(np.random.normal(loc = 0., scale = 0.3))

		color = np.array([R, G, B])
		color = color*(1. - pastel_strength)/max([R, G, B]) + np.array([pastel_strength, pastel_strength, pastel_strength])
	return color

def generate_color_palette2(main_color, strength, light):
	R, G, B = np.random.random(), np.random.random(), np.random.random()

	result = np.zeros(4, dtype = np.float32)
	result[0] = light + (main_color[0]*strength + R*(1. - strength))*(1. - light)
	result[1] = light + (main_color[1]*strength + G*(1. - strength))*(1. - light)
	result[2] = light + (main_color[2]*strength + B*(1. - strength))*(1. - light)
	result[3] = 1.
	return result

def show_colors(main_color, strength, light):
	import matplotlib.pyplot as plt
	plt.ion()

	image = np.zeros([800, 800, 4], dtype = np.float32)
	for i in range(10):
		i_min, i_max = int(800/10*i), int(800*(i + 1)/10)
		for j in range(10):
			j_min, j_max = int(800/10*j), int(800*(j + 1)/10)
			mask = image[i_min:i_max, j_min:j_max] = generate_color_palette2(main_color, strength, light)
	plt.imshow(image)

def pivot(positions, orientations, pivot_point, rotation, translation):
	i, j, k = np.array([1., 0., 0.]), np.array([0., 1., 0.]), np.array([0., 0., 1.])
	u, v, w = np.cos(rotation)*i + np.sin(rotation)*j, -np.sin(rotation)*i + np.cos(rotation)*j, k

	positions = np.array(positions)
	new_positions = np.zeros([len(positions), 3], dtype = np.float32)

	if len(positions) != 0:
		for i in range(3): positions[:, i] = positions[:, i] - pivot_point[i]

		for i in range(3): new_positions[:, i] = positions[:, 0]*u[i] + positions[:, 1]*v[i] + positions[:, 2]*w[i] + translation[i]

		return list(new_positions), list(np.array(orientations) + rotation*rad2deg)
	else:
		return list(positions), list(orientations)

# def
def oclusion(segment, depth, width):
	seg_length = np.linalg.norm(np.array(segment[1]) - np.array(segment[0]))
	direction  = (np.array(segment[1]) - np.array(segment[0]))/seg_length
	direction  = np.array([direction[0], direction[1], 0.])
	perpendicular = np.cross(direction, vec_up)
	center = (np.array(segment[0]) + np.array(segment[1]))/2.
	point1 = (center - width/2.*direction[:2])
	point2 = (center - width/2.*direction[:2] + depth*perpendicular[:2])
	point3 = (center + width/2.*direction[:2] + depth*perpendicular[:2])
	point4 = (center + width/2.*direction[:2])

	shape = tools.Polygon([point1, point2, point3, point4])
	return shape
