import numpy as np
import modulo
import tools
from importlib import reload

city_mesh = np.load("data/city_mesh.npy", allow_pickle=True).item()
#
# material = "Smooth_Wall"
# print("Triangles: ", city_mesh.submeshes[material].triangles.shape)
# modulo.optimize_submesh(city_mesh.submeshes[material])
#
# city_mesh.submeshes[material].calculate_normals()
# print("Triangles: ", city_mesh.submeshes[material].triangles.shape)
#
# city_mesh.submeshes[material].plot()


# modulo.toggle_debug()
# modulo.optimize_submesh(city_mesh.submeshes[material])
size_before = city_mesh.whole_mesh.triangles.shape
mat_list = city_mesh.materials + []
# mat_list.remove("Smooth_Wall")
for material in mat_list:
	print(material, city_mesh.submeshes[material].triangles.shape)
	city_mesh.submeshes[material].calculate_normals()
	modulo.optimize_submesh(city_mesh.submeshes[material])
	print(material, city_mesh.submeshes[material].triangles.shape)

city_mesh.calculate_whole_mesh()
size_after = city_mesh.whole_mesh.triangles.shape

print("Reduction: ", size_after[0]/size_before[0])

# vertices = np.array([[0., 0., 0.], [1., 0., 0.], [1., 0., 1.], [0., 0., 1.], [2., 0., 0.], [2., 0., 1.]])
# tris     = np.array([[0, 1, 2], [0, 2, 3], [1, 4, 5], [1, 5, 2]])
#
# mesh = tools.SubMesh(vertices = vertices, triangles = tris, colors = np.ones(shape = [6, 4]))
#
# modulo.optimize_submesh(mesh)
