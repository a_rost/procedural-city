#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct height_map {
	float *height, dx, dy, x_min, x_max, y_min, y_max;
	int N, M;
};

struct height_map surface;

void load_height_map(float *array, unsigned int N, unsigned int M, float *extent){
	surface.height = array;
	
	surface.dx = (extent[1] - extent[0])/N;
	surface.dy = (extent[3] - extent[2])/M;
	
	surface.x_min = extent[0];
	surface.x_max = extent[1];
	surface.y_min = extent[2];
	surface.y_max = extent[3];
	surface.N  = N;
	surface.M  = M;
}

unsigned int idx(int i, int j, int M){
	return i*M + j;
}


float get_height(struct height_map surface, float x, float y){
	int i, j, N = surface.N, M = surface.M;
	float f1, f2, f3, f4, g1, g2, dx, dy, x_min, y_min;
	
	dx = surface.dx;
	dy = surface.dy;
	x_min = surface.x_min;
	y_min = surface.y_min;
	
	i = (int)((x - x_min)/dx);
	j = (int)((y - y_min)/dy);

	i = max(i, 0);
	i = min(i, N - 2);
	j = max(j, 0);
	j = min(j, M - 2);
	
	f1 = surface.height[idx(i + 0, j + 0, M)];
	f2 = surface.height[idx(i + 0, j + 1, M)];
	f3 = surface.height[idx(i + 1, j + 0, M)];
	f4 = surface.height[idx(i + 1, j + 1, M)];
	
	g1 = f1 + (f2 - f1)*(y - (y_min + j*dy))/dy;
	g2 = f3 + (f4 - f3)*(y - (y_min + j*dy))/dy;

	return g1 + (g2 - g1)*(x - (x_min + i*dx))/dx;
}


