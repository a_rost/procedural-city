
def mirror_coord(center, x):
	return center - (x - center)

def generate_windows(kind, facing, center, width, min_height, max_height, preset, door = False):
	balcony = False
	if preset == "small1":
		scales = [1., 1.]
		win_height = 1.5
		win_separation = 2.
		n_doors  = 1
		door_width = 1.
		door_material = "wood"
	elif preset == "small2":
		scales = [0.5, 1.]
		win_height = 1.5
		win_separation = 2.
		n_doors  = 1
		door_width = 1.
		door_material = "wood"
	elif preset == "big1":
		scales = [3, 1.]
		win_height = 1.5
		win_separation = 4.
		n_doors  = int(width/win_separation)
		door_width = suggest_size(2, 4, 3, 1, granularity = 0.2)
		door_material = rand_pick(["wood", "metal1"])
	elif preset == "big2":
		scales = [3, 2.]
		win_height = 1.
		win_separation = 4.
		balcony = True
		balcony_width = suggest_size(0.5, 1.5, 1., 0.5, granularity = 0.2)
		balcony_size  = suggest_size(scales[0], 4, 3., 1.)
		n_doors  = int(width/win_separation)
		door_width = suggest_size(2, 4, 3, 1, granularity = 0.2)
		door_material = rand_pick(["wood", "metal1"])
		

	door_height = 2.
	#vec_up = np.array([0, 0, 1])



	return windows
			

def generate_terraces(parcel, Cube, terrace_length):
	wall_height, wall_width = 1., 0.15
	if Cube.roofable == "prolongue":
		local_center = np.array([Cube.local_center[0], Cube.local_center[1], Cube.local_center[2] + Cube.z_scale/2. + 3./2.])
		shed = cube(local_center = local_center, width = Cube.x_scale, length = Cube.y_scale, height = 3.)
		shed.set_materials(mat_up = parcel.materials.mat_roof,
					  mat_front = parcel.materials.mat_windows,
					  mat_back  = parcel.materials.mat_windows,
					  mat_right = parcel.materials.mat_wall,
					  mat_left  = parcel.materials.mat_wall)

		shed.n_terraces = Cube.n_terraces + 1
		shed.nmax_terraces = Cube.nmax_terraces
		shed.roofable = "prolongue" if shed.n_terraces < shed.nmax_terraces else "type1"
		Cube.roofable = "type0"
		parcel.kind.cubes.append(shed)

	elif (Cube.roofable == "type_front" or Cube.roofable == "type_back") and Cube.n_terraces < Cube.nmax_terraces:

		local_center1 = np.array([Cube.local_center[0] - Cube.x_scale/2. + wall_width/2., Cube.local_center[1] - Cube.y_scale/2. + terrace_length/2., Cube.local_center[2] + Cube.z_scale/2. + wall_height/2.])
		local_center2 = np.array([Cube.local_center[0] + Cube.x_scale/2. - wall_width/2., Cube.local_center[1] - Cube.y_scale/2. + terrace_length/2., Cube.local_center[2] + Cube.z_scale/2. + wall_height/2.])
		local_center3 = np.array([Cube.local_center[0], Cube.local_center[1] - Cube.y_scale/2. + wall_width/2., Cube.local_center[2] + Cube.z_scale/2. + wall_height/2.])

		centers = [local_center1, local_center2, local_center3]

		xscales = [0.15, 0.15, Cube.x_scale]
		yscales = [Cube.y_scale, Cube.y_scale, 0.15]
		
		for i in range(3):
			local_center = centers[i]
			wall = cube(local_center = local_center, width = xscales[i], length = yscales[i], height = wall_height)
			wall.set_materials(mat_up = parcel.materials.mat_wall,
					  mat_front = parcel.materials.mat_wall,
					  mat_back  = parcel.materials.mat_wall,
					  mat_right = parcel.materials.mat_wall,
					  mat_left  = parcel.materials.mat_wall)
			if Cube.roofable == "type_back":
				wall.local_center[1] = mirror_coord(Cube.local_center[1], wall.local_center[1])
			parcel.kind.cubes.append(wall)
		
		local_center = np.array([Cube.local_center[0], Cube.local_center[1] - Cube.y_scale/2. + terrace_length/2.,  Cube.local_center[2] + Cube.z_scale/2. + parcel.kind.floor_level])
		floor = quad(local_center = local_center, width = Cube.x_scale - 0.2, length = terrace_length - 0.2)
		floor.set_materials(parcel.materials.mat_terrace)
		if Cube.roofable == "type_back":
			floor.local_center[1] = mirror_coord(Cube.local_center[1], floor.local_center[1])
		floor.set_materials(parcel.materials.mat_terrace)
		parcel.kind.cubes.append(floor)
	
		if terrace_length >= Cube.y_scale:
			local_center = np.array([Cube.local_center[0], Cube.local_center[1] + Cube.y_scale/2. - (Cube.y_scale - terrace_length)/2., Cube.local_center[2] + Cube.z_scale/2. + 3./2.])
			shed.cube(local_center = local_center, width = Cube.x_scale, length = Cube.y_scale, height = 3.)
			shed.set_materials(mat_up = parcel.materials.mat_roof,
						mat_front = parcel.materials.mat_wall,
						mat_back  = parcel.materials.mat_wall,
						mat_right = parcel.materials.mat_wall,
						mat_left  = parcel.materials.mat_wall)
			
			if Cube.roofable == "type_back":
				shed.local_center[1] = mirror_coord(Cube.local_center[1], wall.local_center[1])

			if Cube.n_terraces < Cube.nmax_terraces and Cube.roofable == "type_front": shed.roofable = "type_front" 
			elif Cube.n_terraces < Cube.nmax_terraces and Cube.roofable == "type_back": shed.roofable = "type_back" 
			else: shed.roofable = "type1"
			
			parcel.kind.cubes.append(shed)
		Cube.roofable = "type0"


	def build_yards(self):
		if self.kind.has_frontyard:
			local_center = np.array([self.width/2., self.kind.frontyard_length/2., self.kind.floor_level])
			floor = quad(local_center = local_center, width = self.width, length = self.kind.frontyard_length)
			
			if pick_bool(0.9): floor.set_materials(self.materials.mat_floor) 
			else:              floor.set_materials(self.materials.mat_ground)
			self.kind.cubes.append(floor)
	
		if self.kind.has_backyard:
			local_center = np.array([self.width/2., self.length - self.kind.backyard_length/2., self.kind.floor_level])
			floor = quad(local_center = local_center, width = self.width, length = self.kind.backyard_length)
			floor.set_materials(self.materials.mat_floor)
			if pick_bool(0.7):  floor.set_materials(self.materials.mat_floor) 
			else:               floor.set_materials(self.materials.mat_ground)
			self.kind.cubes.append(floor)


	def get_ncubes(self):
		return len(self.kind.cubes)
	
	def create_shed(self):
		## creates a small building in the backyard
		has_a_shed = np.random.random() <= 0.8
		
		if has_a_shed:
			shed_height  = suggest_size(3, self.kind.height, 3, 2)
			shed_width   = suggest_size(3, parcel.width - 1, 10, 1)

			shed_is_terrace = pick_bool(0.5)
			shed_length  = suggest_size(2, self.kind.backyard_length - 2, 5, 3.) if self.kind.backyard_length >= 3 else self.kind.backyard_length
			separation_x = suggest_size(0, parcel.width - shed_width, (parcel.width - shed_width)/2., 5)

			#will it be a terrace too?
			
			if shed_is_terrace:
				separation = 0
			elif self.kind.backyard_length == shed_length:
				separation = 0
				shed_is_terrace
			else:
				separation  = suggest_size(1, self.kind.backyard_length - shed_length, 3, 5)

			local_center = np.array([separation_x + shed_width/2., parcel.length - self.kind.backyard_length + separation + shed_length/2., shed_height/2.])

			shed = cube(local_center = local_center, width = shed_width, length = shed_length, height = shed_height)
			shed.set_materials(mat_up = self.materials.mat_roof,
					  mat_front = self.materials.mat_wall,
					  mat_back  = self.materials.mat_wall,
					  mat_right = self.materials.mat_wall,
					  mat_left  = self.materials.mat_wall)
			shed.roofable = "type1"
			if shed_is_terrace:
				shed.roofable = "type0"
				shed.mat_up = self.materials.mat_terrace
				local_center1 = np.array([separation_x + 0.15/2., local_center[1], 0.5/2. + shed_height])
				local_center2 = np.array([separation_x + shed_width  - 0.15/2., local_center[1], 0.5/2. + shed_height])
				local_center3 = np.array([local_center[0], parcel.length - self.kind.backyard_length + separation + shed_length - 0.15/2., 0.5/2. + shed_height])
				centers = [local_center1, local_center2, local_center3]

				xscales = [0.15, 0.15, shed_width]
				yscales = [shed_length, shed_length, 0.15]
				
				for i in range(3):
					local_center = centers[i]
					wall = cube(local_center = local_center, width = xscales[i], length = yscales[i], height = 0.5)
					wall.set_materials(mat_up = self.materials.mat_wall,
							mat_front = self.materials.mat_wall,
							mat_back  = self.materials.mat_wall,
							mat_right = self.materials.mat_wall,
							mat_left  = self.materials.mat_wall)
					self.kind.cubes.append(wall)
			self.kind.cubes.append(shed)

	def create_balconyterraces2(self):
		has_balconys = pick_bool(0.5) if self.kind.building_class in ["apparment1", "aparment2", "aparment3"] else False
		terrace_length = suggest_size(2, 7, 2, 2)

			
		if has_balconys:
			n_terraces_max = int((self.kind.size1 - 3)/terrace_length)
			if n_terraces_max < 0: 
				n_terraces_max = 0
				terrace_length = self.kind.size
			else:
				n_terraces     = int(suggest_size(1, n_terraces_max, 1, 1.5, granularity = 1))
	
			for cube in self.kind.cubes:
				if cube.roofable in ["type_front", "type_back", "prolongue"]:
					cube.nmax_terraces = n_terraces_max
					generate_terraces(self, cube, terrace_length)
		else:
			for cube in self.kind.cubes:
				if cube.roofable in ["type_front", "type_back", "prolongue"]:
					cube.roofable = "type1" if self.kind.n_floors <= 3 else "type2"


	def create_balconyterraces(self):
		has_balconys = pick_bool(0.3) if self.kind.building_class in ["apparment1", "aparment2", "aparment3"] else False

		print(self.kind.building_class)

		if has_balconys:
			terrace_length = suggest_size(2, 7, 2, 2)

			n_terraces_max = int((self.kind.size1 - 3)/terrace_length)
			
			if n_terraces_max < 1:
				has_balconys = False
				n_terraces   = 0
			else:
				n_terraces = int(suggest_size(1, n_terraces_max, 1, 1.5, granularity = 1))

			floor_width = self.width
			floor_height= 3.
			
			print("terraces:", n_terraces)
			for i in [0, self.kind.n_repeat]:
				terraces = []
				for j in range(n_terraces):
					floor_length = self.kind.size1 - (j + 1)*terrace_length
					
					local_center = np.array([self.width/2., self.kind.frontyard_length + i*(self.kind.size1 + self.kind.size2) + (j + 1)*terrace_length + floor_length/2., self.kind.height + j*floor_height + floor_height/2.])
				
					shed = cube(local_center = local_center, width = floor_width, length = floor_length, height = floor_height)
					shed.set_materials(mat_up = self.materials.mat_roof,
							mat_front = self.materials.mat_windows,
							mat_back  = self.materials.mat_wall,
							mat_right = self.materials.mat_wall,
							mat_left  = self.materials.mat_wall)
		
					terraces.append(shed)
						

					local_center = np.array([self.width/2., self.kind.frontyard_length + i*(self.kind.size1 + self.kind.size2) + (j + 0.5)*terrace_length + 0.05, self.kind.height + j*floor_height + 0.1])

					floor = quad(local_center = local_center, width = self.width - 0.2, length = terrace_length)
					floor.set_materials(self.materials.mat_terrace)

					terraces.append(floor)
				if i != 0:
					for Cube in terraces:
						Cube.local_center[1] = mirror_coord(self.kind.frontyard_length + i*(self.kind.size1 + self.kind.size2) + self.kind.size3/2., Cube.local_center[1])
				if i == 0:
					self.kind.cubes += terraces
				elif self.kind.ending_block:
					self.kind.cubes += terraces

	def transform_local2global(self):
		for cube in self.kind.cubes:
			local = cube.local_center
			cube.center = local[0]*self.local_i + local[1]*self.local_j + local[2]*self.local_k + self.left_corner

	def clean_cubes(self):
		cubes = self.kind.cubes
		for cube in cubes:
			if cube.x_scale*cube.y_scale*cube.z_scale == 0.:
				cubes.remove(cube)
	
	def recenter(self, center):
		self.left_corner += center
	
	def give_name(self):
		global n_appartments
		if self.kind.building_class in ["aparment1", "aparment2", "aparment3"]:
			self.name = "Appartment" + str(n_appartments)
			n_appartments += 1
		elif self.kind.building_class in ["deposit1"]:
			self.name = "Deposit" + str(n_appartments)
			n_appartments += 1

	def create_roof(self):
		for cube in self.kind.cubes:
			generate_roof(self, cube, 3., cube.roofable)
		





class apparment1:
	def __init__(self, parcel):
		global n_appartments
		parcel.name = "Appartment" + str(n_appartments)
		n_appartments += 1
		self.materials = parcel.materials
		self.building_class = "aparment1" 
		self.n_floors     = suggest_size(2, 10, 4, 3.5)
		self.height       = self.n_floors*3   + np.random.random()*2.
		self.has_backyard = np.random.random() <= 0.8
		self.has_frontyard= np.random.random() <= 0.1

		self.floor_level = suggest_size(0.1, 0.2, 0.1, 0.5, granularity = 0.05)
		
		if parcel.depth == 0:
			self.has_backyard  = False
			self.has_frontyard = False

		self.backyard_length  = np.random.randint(min(0.25*parcel.length, 15.)) if self.has_backyard  else 0.
		self.frontyard_length = np.random.randint(min(0.25*parcel.length, 10.)) if self.has_frontyard else 0.

		#if self.frontyard_length < 5:
			#self.frontyard_length = 5
		if parcel.length - self.backyard_length - self.frontyard_length > 30.:
			self.has_backyard = True
			self.backyard_length = parcel.length - self.frontyard_length - 30.

		self.size1 = parcel.length - self.frontyard_length - self.backyard_length
		self.n_repeat = 1
		self.ending_block = False

		local_center = np.array([parcel.width/2., self.frontyard_length + (parcel.length - self.frontyard_length - self.backyard_length)/2., self.height/2.])

		main_building = cube(local_center = local_center, width = parcel.width, length = parcel.length - self.frontyard_length - self.backyard_length, height = self.height)

		main_building.roofable = "type1" if self.n_floors <= 3 else "type3"
		main_building.n_terraces = 0
		self.cubes = [main_building]

		win_preset = "big2" if np.random.random() <= 0.7 else "big1"
		windows = generate_windows(self, "front", np.array([parcel.width/2., self.frontyard_length, self.height/2.]), parcel.width, 3., self.height, win_preset, door = True)
		self.cubes += windows
		windows = generate_windows(self, "back", np.array([parcel.width/2., parcel.length - self.backyard_length, self.height/2.]), parcel.width, 6., self.height, win_preset, door = False)
		self.cubes += windows

class deposit1:
	#global n_deposits
	def __init__(self, parcel):
		global n_deposits
		parcel.name = "Deposit" + str(n_deposits)
		n_deposits += 1
		
		parcel.materials.mat_wall    = "white"
		parcel.materials.mat_windows = "white"
		self.materials = parcel.materials
		self.building_class = "deposit1" 
		self.n_floors     = suggest_size(1, 3, 1, 1)
		self.height       = self.n_floors*3   + np.random.random()*2.
		self.has_backyard = np.random.random() <= 0.3
		self.has_frontyard= False

		self.floor_level = suggest_size(0.1, 0.2, 0.1, 0.5, granularity = 0.05)
		
		self.backyard_length  = np.random.randint(min(0.25*parcel.length, 10.)) if self.has_backyard  else 0.
		self.frontyard_length = 0.

		self.size1 = parcel.length - self.frontyard_length - self.backyard_length
		self.n_repeat = 1
		self.ending_block = False

		local_center = np.array([parcel.width/2., self.frontyard_length + (parcel.length - self.frontyard_length - self.backyard_length)/2., self.height/2.])

		main_building = cube(local_center = local_center, width = parcel.width, length = parcel.length - self.frontyard_length - self.backyard_length, height = self.height)
		main_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_windows, 
							  mat_back  = self.materials.mat_windows,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)

		main_building.roofable = "type0"
		self.cubes = [main_building]
		
		local_center = np.array([parcel.width/2., self.frontyard_length + (parcel.length - self.frontyard_length - self.backyard_length)/2., self.height])
		roof = cylinder(local_center = local_center, width = parcel.width, length = parcel.length - self.frontyard_length - self.backyard_length, height = parcel.width)
		roof.set_materials(self.materials.mat_metal)
		self.cubes.append(roof)
		windows = generate_windows(self, "front", np.array([parcel.width/2., self.frontyard_length, self.height/2.]), parcel.width, 3., 3., "big1", door = True)
		self.cubes += windows
		if self.has_backyard:
			windows = generate_windows(self, "back", np.array([parcel.width/2., parcel.length - self.backyard_length, self.height/2.]), parcel.width, 3., 3., "big1", door = True)
			self.cubes += windows
			


			
class apparment2:
	def __init__(self, parcel):
		global n_appartments
		parcel.name = "Appartment" + str(n_appartments)
		n_appartments += 1
		self.materials = parcel.materials
		self.building_class = "aparment2" 
		self.n_floors     = suggest_size(2, 10, 4, 3.5)
		self.height       = self.n_floors*3   + np.random.random()*2.

		fit_in_parcel = np.random.random() <= 0.0
		ending_block  = np.random.random() <= 0.8
		
		has_frontyard = np.random.random() <= 0.1
		
		frontyard_length = suggest_size(2, 10, 3, 3.5) if has_frontyard else 0.

		if fit_in_parcel:
			pass
		else:
			size1 = suggest_size(7, 10, 7, 3)
			size2 = suggest_size(2, size1, 2, 3)
			width2 = suggest_size(parcel.width - 3, parcel.width, 2, 1) 
			size3 = size1 if ending_block else 0.

			if frontyard_length + size1 + size2 + size3 >= parcel.length:
				ending_block = False
				size3 = 0.

			n_repeat = min(int(min(parcel.length - frontyard_length - size3, 40.)/(size1 + size2)), 3)
			backyard_length = parcel.length - frontyard_length - size3 - (size1 + size2)*n_repeat

		has_backyard = backyard_length != 0.

		cubes = []
		
		self.ending_block = ending_block
		self.size1 = size1
		self.size2 = size2
		self.size3 = size3
		self.n_repeat = n_repeat
		self.backyard_length = backyard_length
		self.frontyard_length = frontyard_length
		self.has_backyard  = has_backyard
		self.has_frontyard = has_frontyard
		self.floor_level = suggest_size(0.1, 0.2, 0.1, 0.5, granularity = 0.05)
		invert = np.random.random() <= 0.5
		for i in range(n_repeat):
			local_center = np.array([parcel.width/2., self.frontyard_length + i*(size1 + size2) + size1/2., self.height/2.])

			main_building = cube(local_center = local_center, width = parcel.width, length = size1, height = self.height)
			main_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_windows, 
							  mat_back  = self.materials.mat_windows,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			main_building.roofable = "type1"
			main_building.n_terraces = 0
			cubes.append(main_building)

			windows = generate_windows(self, "back", local_center + vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/2.), parcel.width - width2, 3., self.height, "small1", door = False)
			cubes += windows
			if i > 0:
				windows = generate_windows(self, "front", local_center - vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/2.), parcel.width - width2, 3., self.height, "small1", door = False)
				cubes += windows

			local_center = np.array([width2/2., self.frontyard_length + i*(size1 + size2) + size1 + size2/2., self.height/2.])

			sec_building = cube(local_center = local_center, width = width2, length = size2, height = self.height)
			sec_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_wall, 
							  mat_back  = self.materials.mat_wall,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			sec_building.roofable = "type2"
			sec_building.n_terraces = 0
			cubes.append(sec_building)

			windows = generate_windows(self, "right", local_center + vec_right*width2/2., size2, 3., self.height, "small2", door = True)
			cubes += windows


			local_center = np.array([parcel.width - (parcel.width - width2)/2., self.frontyard_length + i*(size1 + size2) + size1 + size2/2., self.floor_level])
			floor = quad(local_center = local_center, width = parcel.width - width2, length = size2)
			floor.set_materials(self.materials.mat_floor)
			cubes.append(floor)
		if ending_block:
			local_center = np.array([parcel.width/2., self.frontyard_length + n_repeat*(size1 + size2) + size3/2., self.height/2.])

			main_building = cube(local_center = local_center, width = parcel.width, length = size3, height = self.height)
			main_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_windows, 
							  mat_back  = self.materials.mat_windows,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			main_building.roofable = "type1"
			main_building.n_terraces = 0
			if self.backyard_length >= 2.:
				windows = generate_windows(self, "back", local_center + vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/2.), parcel.width - width2, 3., self.height, "small1", door = False)
				cubes += windows

			cubes.append(main_building)
		win_preset = "big2" if np.random.random() <= 0.7 else "big1"
		windows = generate_windows(self, "front", np.array([parcel.width/2., self.frontyard_length, self.height/2.]), parcel.width, 3., self.height, win_preset, door = True)
		cubes += windows
		
		if win_preset == "big2" and backyard_length >= 2.:
			win_preset = "big2"
		else:
			win_preset = "big1"

		if self.backyard_length >= 2.:
			if ending_block:
				windows = generate_windows(self, "back", np.array([parcel.width/2., parcel.length - self.backyard_length, self.height/2.]), parcel.width, 6., self.height, win_preset, door = False)
			else:
				windows = generate_windows(self, "back", np.array([width2/2., parcel.length - self.backyard_length, self.height/2.]), width2, 6., self.height, win_preset, door = False)
			cubes += windows
		self.cubes = cubes
		invert_building(parcel, self, invert = invert)


class apparment3:
	def __init__(self, parcel):
		global n_appartments
		parcel.name = "Appartment" + str(n_appartments)
		n_appartments += 1
		self.materials = parcel.materials
		self.building_class = "aparment3" 
		self.n_floors     = suggest_size(2, 10, 4, 3.5)
		self.height       = self.n_floors*3   + np.random.random()*2.

		ending_block  = np.random.random() <= 0.8
		
		has_frontyard = np.random.random() <= 0.1
		
		frontyard_length = suggest_size(2, 10, 3, 3.5) if has_frontyard else 0.


		size1 = suggest_size(7, 10, 7, 3)
		size2 = suggest_size(2, size1, 2, 3)
		width2 = suggest_size(parcel.width/3., parcel.width/2., parcel.width/3., 1) 
		size3 = size1 if ending_block else 0.

		if frontyard_length + size1 + size2 + size3 >= parcel.length:
			ending_block = False
			size3 = 0.

		n_repeat = min(int(min(parcel.length - frontyard_length - size3, 40.)/(size1 + size2)), 3)
		backyard_length = parcel.length - frontyard_length - size3 - (size1 + size2)*n_repeat

		has_backyard = backyard_length != 0.

		cubes = []

		self.ending_block = ending_block
		self.size1 = size1
		self.size2 = size2
		self.size3 = size3
		
		self.n_repeat = n_repeat
		self.backyard_length  = backyard_length
		self.frontyard_length = frontyard_length
		self.has_backyard  = has_backyard
		self.has_frontyard = has_frontyard
		self.floor_level = suggest_size(0.1, 0.2, 0.1, 0.5, granularity = 0.05)



		for i in range(n_repeat):
			local_center = np.array([parcel.width/2., self.frontyard_length + i*(size1 + size2) + size1/2., self.height/2.])

			main_building = cube(local_center = local_center, width = parcel.width, length = size1, height = self.height)
			main_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_windows, 
							  mat_back  = self.materials.mat_windows,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			main_building.roofable = "type1"
			main_building.n_terraces = 0
			cubes.append(main_building)

			windows = generate_windows(self, "back", local_center + vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
			cubes += windows
			windows = generate_windows(self, "back", local_center + vec_back*size1/2. - vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
			cubes += windows
			if i > 0:
				windows = generate_windows(self, "front", local_center - vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
				cubes += windows
				windows = generate_windows(self, "front", local_center - vec_back*size1/2. - vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
				cubes += windows

			local_center = np.array([parcel.width/2., self.frontyard_length + i*(size1 + size2) + size1 + size2/2., self.height/2.])

			sec_building = cube(local_center = local_center, width = width2, length = size2, height = self.height)
			sec_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_wall, 
							  mat_back  = self.materials.mat_wall,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			sec_building.roofable = "type2"
			sec_building.n_terraces = 0
			cubes.append(sec_building)

			local_center = np.array([parcel.width - (parcel.width - width2)/4., self.frontyard_length + i*(size1 + size2) + size1 + size2/2., self.floor_level])
			floor = quad(local_center = local_center, width = (parcel.width - width2)/2., length = size2)
			floor.set_materials(self.materials.mat_floor)
			cubes.append(floor)

			local_center = np.array([(parcel.width - width2)/4., self.frontyard_length + i*(size1 + size2) + size1 + size2/2., self.floor_level])
			floor = quad(local_center = local_center, width = (parcel.width - width2)/2., length = size2)
			floor.set_materials(self.materials.mat_floor)
			cubes.append(floor)

		if ending_block:
			local_center = np.array([parcel.width/2., self.frontyard_length + n_repeat*(size1 + size2) + size3/2., self.height/2.])

			main_building = cube(local_center = local_center, width = parcel.width, length = size3, height = self.height)
			main_building.set_materials(mat_up = self.materials.mat_roof, 
							  mat_front = self.materials.mat_windows, 
							  mat_back  = self.materials.mat_windows,
							  mat_right = self.materials.mat_wall,
							  mat_left  = self.materials.mat_wall)
			main_building.roofable = "type1"
			main_building.n_terraces = 0
			cubes.append(main_building)
			windows = generate_windows(self, "front", local_center - vec_back*size1/2. + vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
			cubes += windows
			windows = generate_windows(self, "front", local_center - vec_back*size1/2. - vec_right*(parcel.width/2. - (parcel.width - width2)/4.), (parcel.width - width2)/2., 3., self.height, "small1", door = False)
			cubes += windows

		win_preset = "big2" if np.random.random() <= 0.7 else "big1"
		windows = generate_windows(self, "front", np.array([parcel.width/2., self.frontyard_length, self.height/2.]), parcel.width, 3., self.height, win_preset, door = True)
		cubes += windows
		
		if win_preset == "big2" and backyard_length >= 2.:
			win_preset = "big2"
		else:
			win_preset = "big1"

		if self.backyard_length >= 2.:
			if ending_block:
				windows = generate_windows(self, "back", np.array([parcel.width/2., parcel.length - self.backyard_length, self.height/2.]), parcel.width, 6., self.height, win_preset, door = False)
			else:
				windows = generate_windows(self, "back", np.array([parcel.width/2., parcel.length - self.backyard_length, self.height/2.]), width2, 6., self.height, win_preset, door = False)
			cubes += windows

		self.cubes = cubes


