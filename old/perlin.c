#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int mod(int a, int b){
    int r = a % b;
    return r < 0 ? r + b : r;
}

int idx(int p_o, int q_o, int r_o, unsigned int M){
	
	unsigned int p, q, r;
	
	p = mod(p_o, M);
	q = mod(q_o, M);
	r = mod(r_o, M);
	
	return p*M*M + q*M + r;
}


float function(float x){
	return powf(1.f - fabsf(x), 1);
}
float determine_dot(float position[3], float *random_gradient, int p, int q, int r, unsigned int M, float *weight){
	float grid_pos[3], grad_grid = 1.f/M, grad[3], dot, u[3];
	
	grid_pos[0] = grad_grid*p - position[0];
	grid_pos[1] = grad_grid*q - position[1];
	grid_pos[2] = grad_grid*r - position[2];
	
	u[0] = grid_pos[0]/grad_grid;
	u[1] = grid_pos[1]/grad_grid;
	u[2] = grid_pos[2]/grad_grid;

	grad[0] = random_gradient[3*(idx(p, q, r, M)) + 0];
	grad[1] = random_gradient[3*(idx(p, q, r, M)) + 1];
	grad[2] = random_gradient[3*(idx(p, q, r, M)) + 2];

	dot = grad[0]*grid_pos[0] + grad[1]*grid_pos[1] + grad[2]*grid_pos[2];
	*weight = function(u[0])* function(u[1])* function(u[2]);
	return dot*(*weight);
}

void create_perlin(float *random_gradient, unsigned int gradient_resolution, float *grid, unsigned int grid_resolution){
	float position[3], grad_grid = 1.f/gradient_resolution, grid_size = 1.f/grid_resolution, suma, weight, *weights;

	int p, q, r;
	weights = (float *) malloc(8*sizeof(float));
	
	for(unsigned int i = 0; i < grid_resolution; i++){
		for(unsigned int j = 0; j < grid_resolution; j++){
			for(unsigned int k = 0; k < grid_resolution; k++){
				position[0] = grid_size*i;
				position[1] = grid_size*j;
				position[2] = grid_size*k;
				
				p = (int) (position[0]/grad_grid);
				q = (int) (position[1]/grad_grid);
				r = (int) (position[2]/grad_grid);

				suma = 0.f;
				suma += determine_dot(position, random_gradient, p + 0, q + 0, r + 0, gradient_resolution, &weights[0]);
				suma += determine_dot(position, random_gradient, p + 0, q + 0, r + 1, gradient_resolution, &weights[1]);
				suma += determine_dot(position, random_gradient, p + 0, q + 1, r + 0, gradient_resolution, &weights[2]);
				suma += determine_dot(position, random_gradient, p + 0, q + 1, r + 1, gradient_resolution, &weights[3]);
				suma += determine_dot(position, random_gradient, p + 1, q + 0, r + 0, gradient_resolution, &weights[4]);
				suma += determine_dot(position, random_gradient, p + 1, q + 0, r + 1, gradient_resolution, &weights[5]);
				suma += determine_dot(position, random_gradient, p + 1, q + 1, r + 0, gradient_resolution, &weights[6]);
				suma += determine_dot(position, random_gradient, p + 1, q + 1, r + 1, gradient_resolution, &weights[7]);
				
				weight = weights[0] + weights[1] + weights[2] + weights[3] + weights[4] + weights[5] + weights[6] + weights[7];
				grid[idx(i, j, k, grid_resolution)] = suma/weight;
			}
		}
		
		
	}
	free(weights);
}


static inline float gaussian(int x, int y, int z, float sigma){
	float arg = -((x*x + y*y + z*z)/2.f/(sigma*sigma));
	return expf(arg);
}
void covolve(float *matrix, unsigned int resolution, float factor, float *result){
	float norm = 0., suma;
	int limit = (int) (3*factor);
	
	for(int i = -limit; i <= limit; i++){
		for(int j = -limit; j <= limit; j++){
			for(int k = -limit; k <= limit; k++)
				norm += gaussian(i, j, k, factor); 
		}
	}
	
	
	for(int i = 0; i < resolution; i++){
		for(int j = 0; j < resolution; j++){
			for(int k = 0; k < resolution; k++){
				suma = 0.f;
				for(int p = -limit; p <= limit; p++){
					for(int q = -limit; q <= limit; q++){
						for(int r = -limit; r <= limit; r++)
							suma += gaussian(p, q, r, factor)*matrix[idx(i + p, j + q, k + r, resolution)];
					}
					
				}
				result[idx(i, j, k, resolution)] = suma/norm;
			}
		}
	}
}

//gcc -c -Ofast -fPIC perlin.c -o perlin.o -lm -fopenmp
//gcc -shared perlin.o -o perlin.so -lm -fopenmp
