import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import tools
import manzana
import presets

plt.ion()




def set_building_kind(parcel):
	if parcel.length >= 30. and np.random.random() <= 0.3 and parcel.depth != 0:
		parcel.kind = deposit1(parcel)
	elif np.random.random() <= 0.3 or parcel.depth == 0:
		parcel.kind = apparment1(parcel)
	elif parcel.width < 10.:
		parcel.kind = apparment2(parcel)
	else:
		parcel.kind = apparment3(parcel)

	parcel.generate_walls()
	parcel.create_balconyterraces()
	parcel.build_yards()
	parcel.create_shed()
	#parcel.transform_local2global()
	parcel.create_roof()
	parcel.create_roof()
	parcel.clean_cubes()
	#parcel.give_name()








for parcel in parcels:
	presets.generate_appartment1(parcel)
	presets.generate_walls(parcel)
	presets.generate_terraces(parcel)
	presets.generate_roof(parcel)






write_cubes(parcels)


