import numpy as np
import tools

		


np.random.seed(100)
n_appartments = 0
n_deposits    = 0
n_offices     = 0
n_stores      = 0
n_generics    = 0
all_parcels   = []




class Cylinder:
	def __init__(self, local_center, width, length, height):
		self.primitive = "Cylinder"
		self.tag       = "roof"

		self.x_scale = width
		self.y_scale = length
		self.z_scale = height
		self.local_center  = local_center

		self.top_type  = "default"

	def set_materials(self, material):
		self.mat  = material

class Quad:
	def __init__(self, local_center, width, length, tag = "floor"):
		self.primitive = "Quad"
		self.tag       = tag

		self.x_scale = width
		self.y_scale = length
		self.z_scale = 1.
		self.local_center  = local_center

		self.top_type   = "default"

	def set_materials(self, material):
		self.mat   = material


class WaterTank:
	def __init__(self, local_center, width, height, material):
		self.primitive = "WaterTank"
		self.tag       = "Tank"
		self.mat       = material

		self.x_scale = width
		self.y_scale = width
		self.z_scale = height
		self.local_center  = local_center








def generate_entrance(building, center, tot_width, facing, tipo = "appartment"):
	objects = []
	if tipo == "appartment":
		if facing == "front":
			n_commerces = max(int(tot_width/4.), 2)
			width = tot_width/n_commerces
			door_idx = np.random.randint(n_commerces)
			
			for i in range(n_commerces):
				local_center = np.array([center[0] - tot_width/2. + width*i + width/2., center[1], 1.])
				if i == door_idx:
					material = "glass"
				else:
					material = "blind"
				win = Window(local_center = local_center, facing = "front", width = width*0.7, height = 2., material = material)
				objects.append(win)
		else:
			local_center = np.array([center[0], center[1], 1.])
			material = "glass"
			win = Window(local_center = local_center, facing = "back", width = 2., height = 2., material = material)
			objects.append(win)
	elif tipo == "deposit":
		n_commerces = max(int(tot_width/5.), 1)
		width  = tot_width/n_commerces
		
		for i in range(n_commerces):
			local_center = np.array([center[0] - tot_width/2. + width*i + width/2., center[1], 1.])

			material = "blind"
			win = Window(local_center = local_center, facing = facing, width = width*0.9, height = 2., material = material)
			objects.append(win)
		
	building.objects += objects

def generate_facade(building, center, tot_width, min_floor, max_floor, facing):
	has_balconys = tools.pick_bool(0.4)
	if facing == "back" and building.backyard_length <= 2.:
		has_balconys = False
		
	if facing == "front":
		preset = tools.rand_pick(["balcony1", "balcony2"]) if has_balconys else tools.rand_pick(["normal1", "normal2", "normal3"])
	else:
		preset = tools.rand_pick(["balcony1", "balcony2"]) if has_balconys else tools.rand_pick(["normal1", "normal2"])
		
	ncols = max(int(tot_width/4.), 2)
	#nfloors = int(height/3.)
	width = tot_width/ncols
	
	facade_objects = []
	for i in range(min_floor, max_floor):
		for j in range(ncols):
			local_center = np.array([center[0] - tot_width/2. + j*width + width/2., center[1], i*3. + 1.5])
			facade_objects += create_individual_facade(building, width, local_center, facing, i, preset)
	building.objects += facade_objects

	
def generate_roof(parcel):
	####!!! REVISAR!!!
	wall_height = 0.4
	wall_width  = 0.15

	building = parcel.building
	objects = []
	for primitive in building.objects:
		if primitive.tag == "main_block":
			if primitive.face_tags["tag_top"] == "roof1":
				wall_height = 0.4
				

			elif primitive.face_tags["tag_top"] == "roof2":
				wall_height = 0.2
				local_center = np.array([primitive.local_center[0], primitive.local_center[1], primitive.local_center[2] + primitive.z_scale/2. + height/2.])
				shed = Cube(local_center = local_center, width = primitive.x_scale, length = primitive.y_scale, height = height)
				shed.set_materials(mat_top = building.materials.mat_roof,
							mat_front = building.materials.mat_wall,
							mat_back  = building.materials.mat_wall,
							mat_right = building.materials.mat_wall,
							mat_left  = building.materials.mat_wall)
				shed.face_tags["tag_top"] = "roof1"
				objects.append(shed)
			elif primitive.face_tags["tag_top"] == "roof3":
				wall_height = 0.2
				local_center = np.array([primitive.local_center[0], primitive.local_center[1], primitive.local_center[2] + primitive.z_scale/2. + height/2.])
				shed = Cube(local_center = local_center, width = primitive.x_scale*0.3, length = primitive.y_scale*0.3, height = height)
				shed.set_materials(mat_top = building.materials.mat_roof,
							mat_front = building.materials.mat_wall,
							mat_back  = building.materials.mat_wall,
							mat_right = building.materials.mat_wall,
							mat_left  = building.materials.mat_wall)
				shed.face_tags["tag_top"] = "roof1"
				objects.append(shed)
			local_center1 = np.array([primitive.local_center[0] - primitive.x_scale/2. + wall_width/2., primitive.local_center[1], primitive.local_center[2] + primitive.z_scale/2. + wall_height/2.])
			local_center2 = np.array([primitive.local_center[0] + primitive.x_scale/2. - wall_width/2., primitive.local_center[1], primitive.local_center[2] + primitive.z_scale/2. + wall_height/2.])
			local_center3 = np.array([primitive.local_center[0], primitive.local_center[1] + primitive.y_scale/2. - wall_width/2., primitive.local_center[2] + primitive.z_scale/2. + wall_height/2.])
			local_center4 = np.array([primitive.local_center[0], primitive.local_center[1] - primitive.y_scale/2. + wall_width/2., primitive.local_center[2] + primitive.z_scale/2. + wall_height/2.])
			centers = [local_center1, local_center2, local_center3, local_center4]

			xscales = [0.15, 0.15, primitive.x_scale, primitive.x_scale]
			yscales = [primitive.y_scale, primitive.y_scale, 0.15, 0.15]
		
			for i in range(4):
				local_center = centers[i]
				wall = Cube(local_center = local_center, width = xscales[i], length = yscales[i], height = wall_height, tag = "wall")
				wall.set_materials(mat_top = building.materials.mat_wall,
						mat_front = building.materials.mat_wall,
						mat_back  = building.materials.mat_wall,
						mat_right = building.materials.mat_wall,
						mat_left  = building.materials.mat_wall)
				objects.append(wall)

	primitive.face_tags["tag_top"] = "default"
	building.objects += objects

def generate_sheds(building, parcel):
	return 0.




def generate_appartment1(parcel):
	building = Building()




	local_center = np.array([parcel.width/2., building.frontyard_length + (parcel.length - building.frontyard_length - building.backyard_length)/2., n_floors*3./2.])
	main_building = Cube(local_center = local_center, width = parcel.width, length = parcel.length - building.frontyard_length - building.backyard_length, height = n_floors*3., tag = "main_block", face_tags = {"tag_top": "roof1", "tag_front": "default", "tag_back": "default", "tag_bottom": "default", "tag_right": "default", "tag_left": "default"})

	main_building.set_materials(mat_top = building.materials.mat_roof, 
							  mat_front = building.materials.mat_wall, 
							  mat_back  = building.materials.mat_wall,
							  mat_right = building.materials.mat_wall,
							  mat_left  = building.materials.mat_wall)

	center = [local_center[0], building.frontyard_length, 0.]
	#generate_window_column(building, center, "front", parcel.width, 3., n_floors*3., preset = window_presets[0], door = False)
	 #generate_facade(building, parcel, n_floors*3)

	generate_facade(building, [parcel.width/2., building.frontyard_length, n_floors*1.5], parcel.width, 1, n_floors, "front")
	if building.backyard_length >= 2.:
		generate_facade(building, [parcel.width/2., parcel.length - building.backyard_length, n_floors*1.5], parcel.width, 1, n_floors, "back")
	generate_entrance(building, [parcel.width/2., building.frontyard_length,                 0.], parcel.width, "front")
	generate_entrance(building, [parcel.width/2., parcel.length - building.backyard_length,  0.], parcel.width, "back")
	building.objects.append(main_building)
	parcel.building = building

