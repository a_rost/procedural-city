import tools
import numpy as np
import shape_generation as shape_gen
from roofs import determine_rest_points


def shape_info(width, length):
	min_sizeX = max(width/2., 3.0)
	max_sizeX = max(width, min_sizeX)

	min_sizeY = max(length/3., 5.0)
	max_sizeY = max(min(length*0.8, length - 2), min_sizeY)

	mean_sizeX, mean_sizeY = width, length*0.7
	std_sizeX, std_sizeY   = width/6., length/6.

	std_locX, std_locY = width/2., length/4

	return min_sizeX, max_sizeX, mean_sizeX, std_sizeX, min_sizeY, max_sizeY, mean_sizeY, std_sizeY, std_locX, std_locY

def correct_shape(shape, back_offset):
	return 0

def correct_shape(shape, target_position, side = -1):
	if shape.type == "Polygon":
		rings = list(tools.get_rings(shape))

		exterior = np.array(list(rings[0]))

		if side == -1:
			target_offset = exterior[:,1].min()
		else:
			target_offset = exterior[:,1].max()

		mask_offset = exterior[:, 1] == target_offset
		line_extent = [exterior[mask_offset, 0].min(), exterior[mask_offset, 0].max()]
		if line_extent[1] - line_extent[0] < 2.0:
			line_extent[0] = line_extent[0] - 2.
			line_extent[1] = line_extent[1] + 2.

		if side == -1:
			attachment = tools.create_box_from_extent([line_extent[0], line_extent[1], target_position-0.001, target_offset + 0.001])
		else:
			attachment = tools.create_box_from_extent([line_extent[0], line_extent[1], target_offset-0.001, target_position + 0.001])

		new_shape = tools.orient(tools.unary_union([shape, attachment]))

		rings = list(tools.get_rings(new_shape))
		exterior = np.array(list(rings[0]))
		offset = abs(target_position - exterior[:, 1].min()) if side == -1 else abs(target_position - exterior[:, 1].max())
		if offset >= 0.01:
			print("Mierda:")
			print(shape)
			print(new_shape)
			print("side: ", side)
			print("target: ", target_position)
			print("attachment", attachment)
		return new_shape
		#return tools.Polygon(new_exterior, holes = rings[1:])
	elif shape.type == "MultiPolygon":
		result = []
		for individual_shape in shape:
			result.append(correct_shape(individual_shape, target_position, side))
		return tools.MultiPolygon(result)
	else:
		return tools.Polygon()

debug_shapes, debug_shapes2 = 0, 0
def generate_random_building(main_shape, front, terrain, nlevels = 3):
	global debug_shapes, debug_shapes2

	debug_shapes, debug_shapes2 = [], []
	length, width, orientation, coords, centroid = tools.get_orientation(main_shape)
	extent, center, width, length = tools.determine_extent(main_shape)


	shape_collection = [tools.orient(terrain), tools.orient(terrain)]
	current_shape = main_shape
	front_shape   = front.buffer(5., join_style = 2, cap_style = 2).intersection(main_shape)
	front_extent, front_center, front_width, front_length = tools.determine_extent(front_shape)

	for i in range(nlevels - 2):
		N = np.int32(tools.suggest_value(2., 5, 3*length/20., 1, 1))
		success, shape = shape_gen.generate_shape2(current_shape, shape_info, N = N, orientation = orientation)
		debug_shapes2.append(shape)

		if success:
			if i == 0:
				shape = correct_shape(shape, front_extent[2])
				shape = correct_shape(shape, extent[3], side = 1)
				shape = tools.orient(tools.unary_union([shape, front_shape]))

			shape_collection.append(shape)
			current_shape = shape
		else:
			shape = tools.orient(tools.unary_union([current_shape, front_shape]))
			shape_collection.append(shape)
			current_shape = shape
		debug_shapes = shape_collection
	return shape_collection


def generate_cosa(terrain, level0, nlevels = 3, floor_height = 3.0, orientation = 0.):
	if terrain.type in ["Polygon", "MultiPolygon"]:
		extent, center, width, length = tools.determine_extent(terrain)

		shape_collection, levels = [], []
		current_shape = terrain
		level = level0

		N = np.int32(tools.suggest_value(2., 5, 3*length/20., 1, 1))
		for i in range(nlevels):
			success, shape = shape_gen.generate_shape(current_shape, shape_info, N = N, passages_check = True, orientation = orientation)
			if success:
				shape = correct_shape(shape, extent[2])

				shape_collection.append(shape)
				level += tools.suggest_value(1, 3, 2, 1, 1)*floor_height
				levels.append(level)
				current_shape = shape
		return shape_collection, levels
	else:
		raise "Error"

def generate_patios(terrain, level0, nlevels = 3, floor_height = 3.0, orientation = 0.):
	ring = tools.get_rings(tools.orient(recipe.main_shape))[0]

	idx = 0
	for j in range(len(ring)):
		segment = ring[j]
		if np.sqrt((segment[0])**2 + (segment[1])**2) <= 0.001:
			idx = j
	ring = np.roll(ring, -idx, axis = 0)

	indexes = np.unique(ring, axis = 0, return_index=True)[1]
	indexes.sort()
	ring = list(ring[indexes][1:])
	ring.append(np.array([0., 0.]))

	linestring = tools.LineString(ring).coords

	shape_gen.place_yards


def appartment_back_shapes(recipe, parcel):
	back_length = parcel.length - recipe.back_offset

	shape_collection, levels = [], []
	#back_shape
	if recipe.back_structures == "back_yard" and recipe.back_shape.type in ["Polygon", "MultiPolygon"] and recipe.back_length > 4.0:
		### Complejizar mas tarde
		N = int(tools.suggest_value(1, 3, 1, 1, 1))
		back_level1, back_level2 = 0, int(tools.suggest_value(1, 2, 1, 0.5, 1)*recipe.floor_height)

		# shape_collection, levels = generate_cosa(recipe.back_shape, 1, N, recipe.floor_height, parcel.orientation)
		shape_collection = [recipe.back_shape]
		levels = [back_level2]
	elif recipe.back_structures == "back_yard" and recipe.back_shape.type in ["Polygon", "MultiPolygon"]:
		back_level1, back_level2 = 0, int(tools.suggest_value(1, 2, 1, 0.5, 1)*recipe.floor_height)
		shape_collection = [recipe.back_shape]
		levels = [back_level2]

	elif recipe.back_structures == "parking" and recipe.back_shape.type in ["Polygon", "MultiPolygon"]:
		back_level1, back_level2 = 0., tools.suggest_value(1, 2, 1, 0.5, 1)*recipe.floor_height
		shape_collection = [recipe.back_shape, recipe.back_shape]
		levels = [back_level1, back_level2]

	elif recipe.back_structures == "commerce" and recipe.back_shape.type in ["Polygon", "MultiPolygon"]:
		#if tools.pick_bool(0.3):

		back_level = int(tools.suggest_value(1, recipe.floors, recipe.floors/2., 4, 1))*recipe.floor_height
		N = int(tools.suggest_value(1, 3, 1, 1, 1))
		ring = tools.get_rings(recipe.back_shape)[0]

		yard_width  = tools.suggest_value(parcel.width/4., parcel.width/2., parcel.width/3., 1., 0.1)
		yard_length = tools.suggest_value(yard_width, 3*yard_width, yard_width*2, 1., 0.1)
		nrep = max( np.int(0.25*recipe.back_shape.area/(yard_length*yard_width)), 1)

		yards = shape_gen.place_yards(ring, yard_width, yard_length, 10., nrep, 0., flip = False)
		# shape_collection = [recipe.back_shape, tools.Polygon()]
		# shape_collection = [recipe.back_shape, recipe.back_shape.buffer(-1)]
		shape_collection = [recipe.back_shape, recipe.back_shape]
		levels = [0., back_level]
		# shape_collection, levels = generate_cosa(recipe.back_shape, back_level, N, recipe.floor_height, parcel.orientation)


	elif recipe.back_structures in ["terrace", "continuation"] and recipe.back_shape.type in ["Polygon", "MultiPolygon"]:
		back_level = int(tools.suggest_value(1, recipe.floors, recipe.floors/2., 4, 1))*recipe.floor_height
		# N = int(tools.suggest_value(1, 3, 1, 1, 1))

		# shape_collection, levels = generate_cosa(recipe.back_shape, back_level, N, recipe.floor_height, parcel.orientation)
		ring = tools.get_rings(recipe.back_shape)[0]

		yard_width  = tools.suggest_value(parcel.width/4., parcel.width/2., parcel.width/3., 1., 0.1)
		yard_length = tools.suggest_value(yard_width, 3*yard_width, yard_width*2, 1., 0.1)
		nrep = max( np.int(0.25*recipe.back_shape.area/(yard_length*yard_width)), 1)

		yards = shape_gen.place_yards(ring, yard_width, yard_length, 10., nrep, 0., flip = False)
		shape_collection = [recipe.back_shape, recipe.back_shape.difference(yards)]
		levels = [0., back_level]

	print("back heights", levels)

	return shape_collection, levels


