import numpy as np
from shapely.geometry import Polygon, MultiPoint
from shapely.ops import unary_union
from shapely.ops import snap
import shapely
import tools
from tools import deg2rad, rad2deg, vec_right, vec_left, vec_up, vec_down, vec_front, vec_back
import roofs
import back_structures as back
import shape_generation as shape_gen
import geometry_generation as geo_gen
import time
from classes import facade_model, building_presets, Trees, Complex_Facades, Facades, Objects, vertical_facet, horizontal_facet, replace_material

DEBUG = False

optimize_facades = True


def generate_block_structures(manzana_shape, sidewalk_shape, street_shape):
	trees   = Trees()
	objects = Objects()

	sidewalk_shape = manzana_shape.buffer(3).difference(manzana_shape)
	street_shape   = (street_shape.difference(sidewalk_shape)).difference(manzana_shape.buffer(1e-8))

	sidewalk_height, sidewalk_width, tree_linear_density, tree_mean_height, corner_padding = 0.15, 3., 5/100., 10., 10.

	tree_ring   = manzana_shape.buffer(sidewalk_width - 0.7, join_style = 2)
	# lamp_ring   = tools.get_rings(manzana_shape.buffer(sidewalk_width - 0.7, join_style = 2))[0]

	sidewalk_mesh = geo_gen.extrude_shape(sidewalk_shape, 0., sidewalk_height, "Sidewalk")
	street_mesh   = geo_gen.meshify_shape(street_shape, 0., "Asphalt", color = [1]*4)

	# X, Y = info["side_x"], info["side_y"]
	# undistorted_manzana = np.
	if False:
		orientations = [0., 90., 180., 270.]
		ring = [[0., 0.], [X, 0.], [X, Y], [0., Y], [0., 0.]]
		for i in range(len(ring) - 1):

			corner1, corner2 = np.array([ring[i][0], ring[i][1], sidewalk_height]), np.array([ring[i + 1][0], ring[i + 1][1], sidewalk_height])
			cuadra_size = np.linalg.norm(corner2 - corner1)

			direction = (corner2 - corner1)/cuadra_size
			ntrees = int((cuadra_size - 2*corner_padding)*tree_linear_density) + 1

			onedimentionalpos = np.linspace(0., 1., ntrees)*(np.random.random(ntrees)*0.2 + 0.9)
			tree_vec =  corner2 - corner_padding*direction - (corner1 + corner_padding*direction)
			trees_positions = np.zeros([ntrees, 3])

			for k in range(3): trees_positions[:, k] = (corner1 + corner_padding*direction)[k] + tree_vec[k]*onedimentionalpos

			#trees_positions = np.linspace(corner1 + corner_padding*direction, corner2 - corner_padding*direction, ntrees)
			for j in range(len(trees_positions)):

				tree_height = tools.suggest_value(6., 20., 10., 4., 0.1)

				# trees.add(trees_positions[j], tree_height, tree_height/2., tag = "default")
				# distorted_position
				distorted_position = transform_coords([trees_positions[j, 0]], [trees_positions[j, 1]])
				objects.add("cantero", 0, distorted_position, 1., orientations[i], 1.)

	return sidewalk_mesh + street_mesh, objects, trees


def plot_facade_mesh(mesh):
	import matplotlib.pyplot as plt
	plt.ion()
	verts = mesh.whole_mesh.vertices

	fig, axs = plt.subplots(ncols = 3, nrows = 1)
	axs[0].plot(verts[:, 0], verts[:, 1], ".")
	axs[1].plot(verts[:, 0], verts[:, 2], ".")
	axs[2].plot(verts[:, 1], verts[:, 2], ".")
	axs[0].set_xlabel("X")
	axs[0].set_ylabel("Y")
	axs[1].set_xlabel("X")
	axs[1].set_ylabel("Z")
	axs[2].set_xlabel("Y")
	axs[2].set_ylabel("Z")


def load_model(path, center = False):
	result = tools.Mesh()

	archivo = open(path, "r")

	value = True
	while(True):
		mat_name = archivo.readline().strip('\n')
		if mat_name == "end":
			break

		nvert = int(archivo.readline().strip('\n'))
		vertices = np.zeros([nvert, 3], np.float32)
		uvs      = np.zeros([nvert, 2], np.float32)
		colors   = np.ones([nvert, 4], np.float32)

		for i in range(nvert):
			vertices[i, 0] = np.float32(archivo.readline().strip('\n'))
			vertices[i, 2] = np.float32(archivo.readline().strip('\n'))
			vertices[i, 1] = np.float32(archivo.readline().strip('\n'))

		for i in range(nvert):
			uvs[i, 0] = np.float32(archivo.readline().strip('\n'))
			uvs[i, 1] = np.float32(archivo.readline().strip('\n'))

		ntriang = int(archivo.readline().strip('\n'))

		triangles = np.zeros([ntriang, 3], np.int32)
		for i in range(ntriang):
			triangles[i, 0] = np.int32(archivo.readline().strip('\n'))
			triangles[i, 2] = np.int32(archivo.readline().strip('\n'))
			triangles[i, 1] = np.int32(archivo.readline().strip('\n'))

		submesh = tools.SubMesh(vertices = vertices, triangles = triangles, uvs = uvs, colors = colors, material = mat_name)

		result += submesh.get_mesh()

	archivo.close()

	if center:
		result.calculate_whole_mesh()
		vertices = result.whole_mesh.vertices
		if len(vertices) != 0:
			extent = [vertices[:, 0].min(), vertices[:, 0].max(), vertices[:, 1].min(), vertices[:, 1].max()]

			for material in result.submeshes.keys():
				mesh = result.submeshes[material]
				vertices = mesh.vertices
				vertices[:, 0] += -(extent[1] + extent[0])/2.
				vertices[:, 1] += -(extent[3] + extent[2])/2.


				result.submeshes[material].vertices = vertices
			result.calculate_whole_mesh()
	return result

facade_assets = {}

def load_all_facades():
	global facade_assets
	from os import listdir

	path = "Assets/Facades/"
	filenames = ["front", "back", "balcony", "yards", "yard_door", "yard_door", "yard_door", "service_door", "service_window", "offices_front"]

	aux = ["front", "back", "balcony", "yard", "terrace", "PB", "parking", "door", "window", "offices"]

	for i in range(len(aux)):
		facade_group = []
		if "offices" in filenames[i]:
			foldername = path + filenames[i]
		else:
			foldername = path + "appartment_" + filenames[i]

		for item in listdir(foldername):
			objeto      = facade_model()
			objeto.mesh = load_model(foldername + "/" + item)

			objeto.mesh += tools.SubMesh(material = "window_reference").get_mesh()
			objeto.mesh += tools.SubMesh(material = "color1").get_mesh()
			objeto.mesh += tools.SubMesh(material = "color2").get_mesh()
			objeto.mesh += tools.SubMesh(material = "color3").get_mesh()

			objeto.identify_windows("window_reference")
			objeto.get_shapes()
			facade_group.append(objeto)

		facade_assets[aux[i]] = facade_group


def load_all_models():
	from os import listdir

	model_assets = []
	model_types  = {}
	path = "Assets/Models/"

	count = 0
	for kind in listdir(path):
		object_group = []


		for item in listdir(path + kind):

			mesh = load_model(path + kind + "/" + item, center = True)

			model_assets.append(mesh)

			if kind == "Various":
				model_types[item.replace(".txt", "")] = np.array([count], dtype = np.int32)
			else:
				object_group.append(count)
			count += 1
		if kind != "Various":
			model_types[kind] = np.array(object_group, dtype = np.int32)
	return model_assets, model_types

class models_catalogue:
	def det_radii(self):
		radii, widths, depths = [], [], []
		for mesh in self.meshes:
			verts = mesh.whole_mesh.vertices
			if len(verts) != 0:
				r_max  = np.sqrt(verts[:, 0]**2 + verts[:, 1]**2).max()
				x_max  = abs(verts[:, 0]).max()
				y_max  = abs(verts[:, 1]).max()
			else:
				r_max = x_max = y_max = 0.


			radii.append(r_max)
			widths.append(x_max*2)
			depths.append(y_max*2)

		return np.array(radii, dtype = np.float32), np.array(widths, dtype = np.float32), np.array(depths, dtype = np.float32)

	def set_properties(self, kind, priority = 100, spawn = [], probability = 0., Nmean = 0, Nmax = 0, mode = "same", spawnmode = 0, auto = False, generate_uvs = False):
		self.priority[kind]    = priority
		self.spawn[kind]       = spawn
		self.probability[kind] = probability
		self.Nmean[kind]       = Nmean
		self.Nmax[kind]        = Nmax
		self.mode[kind]        = mode
		self.spawnmode[kind]   = spawnmode
		self.auto[kind]        = auto

		if generate_uvs:
			asset_ids = self.types[kind]
			for idx in asset_ids:
				for material in self.meshes[idx].materials:
					self.meshes[idx].submeshes[material].uvs = tools.wrap_uvs_method1(self.meshes[idx].submeshes[material])

	def generate_objects(self, facet, building, free_area):
		objects = Objects()

		priorities = np.array(list(self.priority.values()))

		order = np.arange(self.nmodels)[np.argsort(priorities)]
		kind_list = list(self.auto.keys())

		for i in range(len(kind_list)):
			kind = kind_list[order[i]]

			if self.auto[kind]:
				if tools.pick_bool(self.probability[kind]) and facet.tag in self.spawn[kind]:
					things              = Objects()
					things.asset_ids    = []

					level = facet.level

					N = int(tools.suggest_value(0., self.Nmax[kind], self.Nmean[kind], self.Nmean[kind]**0.5, 1.0))
					M = len(self.types[kind])

					model_id = np.random.randint(M)
					asset_id = self.types[kind][model_id]

					radius, depth, width = self.radii[asset_id], self.depths[asset_id], self.widths[asset_id]

					positions, orientations = [], []

					if self.spawnmode[kind] == 0:
						density = self.Nmean[kind]/facet.polygon.area
						model_ids = list(np.random.randint(0, M, N))
						for j in model_ids:
							asset_id = self.types[kind][j]

							radius, depth, width = self.radii[asset_id], self.depths[asset_id], self.widths[asset_id]
							current_pos, current_orien, occupied_space = spread_within_area(density, "random", free_area, level, radius, 2*radius, number_limit = 1, limit_mode = "random")
							if len(current_pos) != 0:
								positions    += current_pos
								orientations += current_orien
								things.asset_ids += [j]

								free_area = free_area.difference(occupied_space)


					elif self.spawnmode[kind] == 2:
						positions, orientations, occupied_space = spread_along_perimeter(facet.polygon, free_area, level, N, width, depth)
						free_area = free_area.difference(occupied_space)

					elif self.spawnmode[kind] == 1:
						density = self.Nmean[kind]/facet.polygon.area
						positions, orientations, occupied_space = spread_within_area(density, "uniform", free_area, level, radius, 2*radius, number_limit = self.Nmax[kind], limit_mode = "distance")
						free_area = free_area.difference(occupied_space)


					N = len(positions)

					things.tags         = [kind]*N
					if self.spawnmode[kind] != 0:
						things.asset_ids    = [model_id]*N

					#else:
						#things.asset_ids    = list(np.random.randint(len(self.types[kind]), size = N))

					things.positions    = positions
					things.sizes        = [1.0]*N
					things.radii        = [1.0]*N
					things.orientations = orientations
					objects.merge(things)
		return objects


	def __init__(self):
		self.meshes, self.types = load_all_models()
		self.radii, self.widths, self.depths  = self.det_radii()

		self.nmodels = len(self.radii)
		self.priority, self.spawn, self.probability, self.Nmean, self.Nmax, self.mode, self.spawnmode, self.auto = {}, {}, {}, {}, {}, {}, {}, {}

		for item in self.types.keys():
			self.set_properties(item)
		self.set_properties("ACs",         0.2, ["small_yard", "balcony", "commerce_roof", "normal_roof", "commerce_yard"],   0.9, 2.5, 1, "same", 1, True)
		self.set_properties("Industrial_AC", 0., ["commerce_roof", "normal_roof", "commerce_yard"],   0.9, 2.5, 4, "same", 1, True)
		self.set_properties("Asadores",    1, ["terrace", "yard", "commerce_yard"], 0.9, 1.0, 1, "same", 2, True, generate_uvs = True)
		self.set_properties("Quincho",     2, ["terrace", "yard"], 0.3, 1.0, 1, "same", 2, True, generate_uvs = True)
		self.set_properties("Solar_Panel", 3, ["commerce_roof"],   0.9, 2.5, 3, "same", 1, True)
		# self.set_properties("Trash_Cans",  4, ["commerce_yard"],   0.6, 2.5, 3, "same", 0, True)
		self.set_properties("Ventiluz",    5, ["commerce_roof", "terrace"], 0.5, 1.0, 3, "same", 1, True, generate_uvs = True)

		self.set_properties("Benches",     6, ["terrace", "yard", "commerce_yard"], 0.9, 2.5, 4, "same", 2, True, generate_uvs = True)
		self.set_properties("Farol",       7, ["terrace", "yard", "commerce_yard"], 0.6, 1.0, 1, "same", 2, True)
		self.set_properties("Pelopincho",  8, ["terrace", "yard"], 0.5, 1.0, 1, "same", 1, True, generate_uvs = True)
		self.set_properties("Flower_Pot",  9, ["terrace", "yard", "commerce_yard"], 0.8, 2.0, 4, "same", 2, True, generate_uvs = True)
		self.set_properties("Tables",     10, ["terrace", "yard", "commerce_yard"], 0.7, 2.5, 3, "same", 1, True, generate_uvs = True)
		self.set_properties("Garbage",    11, ["terrace", "yard", "commerce_roof"], 0.7, 2.0, 3, "rand", 0, True)
		self.set_properties("Tree_Garden",15.5, ["terrace", "yard", "commerce_yard"], 0.9, 1.0, 1, "rand", 2, True)
		#self.set_properties("Structures", 16.5, ["yard"], 0.9, 1.0, 1, "rand", 0, True)



model_assets = models_catalogue()
load_all_facades()

def mesh_decompose(submesh, mesh_dict):
	for item in mesh_dict.keys():
		mesh_dict[item] = np.int32(mesh_dict[item])

	vertices_list, triangles_list, colors_list, uvs_list = [], [], [], []
	#vertices_idxs = np.zeros(len(submesh.vertices), dtype = np.int32)

	for item in mesh_dict.keys():
		vertices = submesh.vertices[mesh_dict[item]]
		colors   = submesh.colors[mesh_dict[item]]
		uvs      = submesh.uvs[mesh_dict[item]]

		ntriang   = int(len(mesh_dict[item])/3)
		triangles = np.reshape(np.arange(len(mesh_dict[item])), [ntriang, 3])

		vertices_list.append(vertices)
		triangles_list.append(triangles)
		uvs_list.append(uvs)
		colors_list.append(colors)

	mesh = tools.Mesh(vertices_list, triangles_list, uvs_list, colors_list, materials_list = list(mesh_dict.keys()))
	return mesh

def generate_facade_mesh(facade_list, high_poly = True):
	facade_mesh = tools.Mesh()

	for i in range(len(facade_list.tags)):
		# print(facade_list.x_scales[i])
		height      = facade_list.z_scales[i]
		avail_width = facade_list.widths[i]
		position    = facade_list.positions[i]
		orientation = facade_list.orientations[i]*np.pi/180. + np.pi/2.
		asset_name  = facade_list.tags[i]
		asset       = facade_assets[asset_name][np.mod(facade_list.asset_ids[i], len(facade_assets[asset_name]))]
		asset_height= asset.height
		asset_width = asset.width
		# factor      = asset.max_width/10.
		# v_fisico = v_asset*factor
		# factor = x_scale/10.
		# v_fisico = x_scale <= width*10/max_width


		# width          = min(facade_list.x_scales[i],     avail_width*10/asset.max_width)
		scale_factor_x = min(facade_list.x_scales[i]/10., avail_width/asset.max_width)

		individual_facade_mesh = tools.Mesh()

		color1, color2, color3 = facade_list.colors1[i], facade_list.colors2[i], facade_list.colors3[i]

		parte1 = asset.mesh.submeshes["color1"]
		parte2 = asset.mesh.submeshes["color2"]
		parte3 = asset.mesh.submeshes["color3"]
		parte1.material = "Smooth_Wall"
		parte2.material = "Smooth_Wall"
		parte3.material = "Smooth_Wall"

		for j in range(4):
			parte1.colors[:, j] = color1[j]
			parte2.colors[:, j] = color2[j]
			parte3.colors[:, j] = color3[j]

		if high_poly:
			individual_facade_mesh += parte1.get_mesh() + parte2.get_mesh() + parte3.get_mesh()

		parte_win = asset.mesh.submeshes["window_reference"]
		mat_dict  = {"Yard_Door": [], "Offices2": [], "Offices_Interior": [], "Normal_Interior": [], "Curtains_Interior": [], "Blind_Interior": []}
		for k in range(len(asset.windows)):
			window  = asset.windows[k]
			x_scale = asset.win_scales[k][0]
			z_scale = asset.win_scales[k][1]

			#verices = parte_win.mesh
			if   np.mod(k, 3) == 0: win_mat = facade_list.windows1[i]
			elif np.mod(k, 3) == 1: win_mat = facade_list.windows2[i]
			elif np.mod(k, 3) == 2: win_mat = facade_list.windows3[i]

			if win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
				mat_dict["Blind_Interior"] += window

			elif win_mat == "curtains":
				mat_dict["Curtains_Interior"] += window

			elif win_mat == "glass":
				mat_dict["Normal_Interior"] += window

			elif win_mat == "normal":
				mat_dict["Normal_Interior"] += window

			aperture = 0. if np.random.random() <= 0.4 else np.random.random()
			R, G, B  = np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5

			# actual_size_x = width/asset_width*x_scale
			actual_size_x = scale_factor_x*x_scale
			actual_size_z = height/asset_height*z_scale

			parte_win.colors[window, 2] = actual_size_x/5.0
			parte_win.colors[window, 3] = actual_size_z/5.0

			if win_mat in ["glass", "curtains"]:
				parte_win.colors[window, 0] = R
				parte_win.colors[window, 1] = aperture*0.5

			elif win_mat == "normal":
				parte_win.colors[window, 0] = np.random.random()
				parte_win.colors[window, 1] = np.random.random()

			elif win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
				if   win_mat == "blind_wood":    parameter = 0.0
				elif win_mat == "blind_metal":   parameter = 0.5
				else:                            parameter = 1.0

				parte_win.colors[window, 0] = parameter
				parte_win.colors[window, 1] = aperture

		parte_win = mesh_decompose(parte_win, mat_dict)

		individual_facade_mesh += parte_win

		if high_poly:
			for other in asset.mesh.materials:
				if not(other in ["color1", "color2", "color3", "window_reference"]):
					individual_facade_mesh += asset.mesh.submeshes[other].get_mesh()

		vec_i = np.array([ np.cos(orientation), np.sin(orientation), 0.])
		vec_j = np.array([-np.sin(orientation), np.cos(orientation), 0.])

		for material in individual_facade_mesh.materials:
			verts  = np.copy(individual_facade_mesh.submeshes[material].vertices)
			nverts = len(verts)

			rotated_verts = np.zeros(verts.shape)

			rotated_verts[:, 0] = position[0] + verts[:, 0]*vec_i[0] + verts[:, 1]*vec_j[0]*scale_factor_x + verts[:, 2]*vec_up[0]*height/asset_height
			rotated_verts[:, 1] = position[1] + verts[:, 0]*vec_i[1] + verts[:, 1]*vec_j[1]*scale_factor_x + verts[:, 2]*vec_up[1]*height/asset_height
			rotated_verts[:, 2] = position[2] + verts[:, 0]*vec_i[2] + verts[:, 1]*vec_j[2]*scale_factor_x + verts[:, 2]*vec_up[2]*height/asset_height

			individual_facade_mesh.submeshes[material].vertices = rotated_verts
		facade_mesh += individual_facade_mesh

	if high_poly:
		facade_mesh.submeshes["Smooth_Wall"].uvs = tools.wrap_uvs_method1(facade_mesh.submeshes["Smooth_Wall"])

	return facade_mesh

def pick_one(lista, asset_id):
	N = len(lista)
	return lista[np.mod(asset_id, N)]

def generate_objects_mesh(objects_list):
	objects_mesh = tools.Mesh()

	for i in range(len(objects_list.tags)):
		position    = objects_list.positions[i]
		orientation = objects_list.orientations[i]
		asset_name  = objects_list.tags[i]
		asset_id    = objects_list.asset_ids[i]

		model = model_assets.meshes[model_assets.types[asset_name][asset_id]]

		individual_object_mesh = tools.Mesh() + model

		vec_i = np.array([ np.cos(orientation), np.sin(orientation), 0.])
		vec_j = np.array([-np.sin(orientation), np.cos(orientation), 0.])

		for material in individual_object_mesh.materials:
			verts  = np.copy(individual_object_mesh.submeshes[material].vertices)
			nverts = len(verts)

			rotated_verts = np.zeros(verts.shape)

			rotated_verts[:, 0] = position[0] + verts[:, 0]*vec_i[0] + verts[:, 1]*vec_j[0] + verts[:, 2]*vec_up[0]
			rotated_verts[:, 1] = position[1] + verts[:, 0]*vec_i[1] + verts[:, 1]*vec_j[1] + verts[:, 2]*vec_up[1]
			rotated_verts[:, 2] = position[2] + verts[:, 0]*vec_i[2] + verts[:, 1]*vec_j[2] + verts[:, 2]*vec_up[2]

			individual_object_mesh.submeshes[material].vertices = rotated_verts
		objects_mesh += individual_object_mesh
	return objects_mesh

debug_info = 0
def generate_terraces(building, recipe):
	terrace_walls = tools.Mesh()
	presets = recipe.presets
	
	generate_rim     = tools.pick_bool(0)
	generate_railig  = tools.pick_bool(0.0)
	generate_techito = tools.pick_bool(0.4)
	techito_slope    = tools.weigh_output([0.5, 0.3, 0.], [10, 30, 30])
	techito_style    = tools.weigh_output([3, 2, 1], [30, 20, 20])
	for facet in building.facets:

		if facet.kind == "horizontal" and facet.tag in ["terrace", "normal_roof", "lower_rooftop", "zinc_roof", "commerce_roof", "higher_rooftop", "balcony", "commerce_yard"] and facet.level != 0.:
			rings = facet.rings
			if   facet.tag in ["terrace", "balcony", "commerce_yard"]  and facet.level >= 3*recipe.floor_height: wall_height = 1.
			#elif facet.tag == "terrace"    and facet.level >= 1*recipe.floor_height: wall_height = 1.5
			#elif facet.tag == "small_yard" and facet.level >= 1*recipe.floor_height: wall_height = 1.5
			elif facet.tag == "small_yard" and facet.level >= 1*recipe.floor_height: wall_height = 1.
			elif facet.tag in ["small_yard", "terrace", "commerce_yard"] and facet.level <= 1*recipe.floor_height: wall_height = 3.
			elif facet.tag == "zinc_roof": wall_height = 0.3
			elif facet.tag in ["higher_rooftop"]: wall_height = 0.1
			#elif facet.tag == "shed_top":  wall_height = 0.3
			else:                          wall_height = 0.2

			current_level = facet.level

			# j = np.where(building.levels == current_level)[0][0]
			# built_shape = building.built_shapes[j]
			# whole_shape = building.whole_shapes[j]

			whole_shape = facet.whole_shape
			built_shape = facet.built_shape
			
			medianera_shape   = (building.medianera.buffer(0.15, join_style = 2).intersection(whole_shape)).difference(built_shape.buffer(10e-6, join_style = 2))

			terrace_perimeter = whole_shape.difference(whole_shape.buffer(-0.15, join_style = 2))
			wall_shape        = terrace_perimeter.intersection(facet.polygon)
			wall_shape        = (wall_shape.buffer(-1e-5, join_style = 2)).buffer(1e-5, join_style = 2)

			if facet.tag in ["terrace", "small_yard", "balcony"]:
				high_wall_shape = wall_shape.intersection(medianera_shape)
				low_wall_shape  = wall_shape.difference(high_wall_shape.buffer(10e-8, join_style = 2))
				if low_wall_shape.area > 0.:
					if generate_railig:
						terrace_walls += geo_gen.generate_railing(whole_shape, built_shape, facet.polygon, low_wall_shape, current_level, "Smooth_Wall", "Smooth_Wall")
					else:
						terrace_walls += geo_gen.extrude_shape(low_wall_shape, current_level, current_level + wall_height, "Smooth_Wall", presets.main_color)

				if high_wall_shape.area > 0.:
					terrace_walls += geo_gen.extrude_shape(high_wall_shape, current_level, current_level + 2.0, "Smooth_Wall", presets.main_color)

			else:
				if wall_shape.area > 0.:
					terrace_walls += geo_gen.extrude_shape(wall_shape, current_level, current_level + wall_height, "Smooth_Wall", presets.main_color)

			if facet.tag in ["terrace", "yard", "balcony"] and generate_techito:
				techito, value = geo_gen.generate_techito(facet.polygon, built_shape, current_level + 2.9, presets.mat_roof2, width = 1.5, slope = techito_slope, join_style = techito_style)
				if facet.polygon.area/(techito.whole_mesh.calculate_area()*0.5) >= 4. and value:
					terrace_walls += techito

			if generate_rim and facet.tag in ["normal_roof", "lower_rooftop", "commerce_roof", "balcony"]:
				rim = geo_gen.generate_rim(facet.polygon, recipe.parcel_shape, 0.1, facet.level - 0.3, 0.14, "Smooth_Wall")
				terrace_walls += rim
	return terrace_walls

def generate_roof(building, recipe):
	global debug_info
	roof_meshes = tools.Mesh()
	presets     = recipe.presets
	for facet in building.facets:
		if facet.kind == "horizontal" and facet.tag in ["zinc_roof", "complex_roof", "shed_top"] and facet.level != 0.:
			# available_space = (facet.whole_shape.difference(facet.built_shape))
			available_space = (recipe.parcel_shape).difference(facet.built_shape)
			roof_mesh = tools.Mesh()

			if facet.tag == "zinc_roof" or facet.tag in ["shed_top"] and tools.pick_bool(0.5):
				roof_type = "curved" if tools.pick_bool(0.6) else "v_shaped"
				height1 = np.random.random()*2.    ## Base height
				height2 = 1.       ## Curved roof height
				
				shape = facet.polygon.difference(building.medianera.buffer(0.15, join_style = 2))
				if roof_type == "curved":
					debug_info = shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, "twosided"
					roof_mesh = geo_gen.generate_general_roof(shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, kind = "curved")[0]

					args = "1", shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, "curved"
				else:
					debug_info = shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, "twosided"
					roof_mesh = geo_gen.generate_general_roof(shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, kind = "twosided")[0]
					args = "2", shape, available_space, facet.level, height1, presets.mat_metal, presets.mat_wall, "twosided"

			elif facet.tag in ["complex_roof"]:
				if facet.tag == "complex_roof":
					kind = tools.weigh_output(["linear", "quadratic", "type1", "type2"], [1, 1, 2, 2])
				else:
					kind = tools.weigh_output(["linear", "type1", "type2"], [1, 2, 2])

				i = np.where(building.levels == facet.level)[0][0]
				whole_shape = building.whole_shapes[i]

				success = False
				if kind in ["linear", "quadratic"]:
					terrace_perimeter = whole_shape.difference(whole_shape.buffer(-0.15, join_style = 2))
					wall_shape        = terrace_perimeter.intersection(facet.polygon)

					shape = facet.polygon.difference(wall_shape)
					
					meshes, success = geo_gen.generate_complex_roof(shape, whole_shape, facet.level, facet.level + 1.5, presets.mat_roof2, presets.mat_wall, dx = 0.2, kind = kind, nlevels = 20)

					args = "3", shape, whole_shape, facet.level, facet.level + 1.5, presets.mat_roof2, presets.mat_wall, 0.2, kind, 20
					if success:
						roof_mesh = meshes
					print("success with complex:", success)
				if kind == "type1" or success == False:
					roof_mesh = geo_gen.generate_general_roof(facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, kind = "twosided", orientation = np.pi/2.)[0]

					args = "4", facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, "twosided", np.pi/2.
				else:
					roof_mesh = geo_gen.generate_general_roof(facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, kind = "twosided", orientation = 0.)[0]
					args = "5", facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, "twosided", 0.
			else:
				#meshes, success = tools.generate_bellow_roof(facet.polygon, facet.level, 0.3, presets.mat_roof2, presets.mat_wall)
				roof_mesh, success = geo_gen.generate_general_roof(facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, kind = "wave")
				args = "6", facet.polygon, available_space, facet.level, 1., presets.mat_roof2, presets.mat_wall, "wave"

				# roof_meshes += meshes
			if roof_mesh.whole_mesh.calculate_area() == 0.:
				# pass
				debug_info = args
			roof_meshes += roof_mesh

	return roof_meshes
#def generate_roof_st

def generate_medianera(building):
	wall_height = 3. + np.random.random()*0.1
	
	#shape = (building.medianera.parallel_offset(0.15, "left")).buffer(0.15, join_style = 2, cap_style = 0)
	shape = (building.medianera.buffer(0.15, join_style = 2).intersection(building.whole_shapes[0])).difference(building.built_shapes[0].buffer(1e-6, join_style = 2))

	if shape.area != 0.:
		mesh  = geo_gen.extrude_shape(shape, 0., wall_height, "Smooth_Wall")

		if tools.pick_bool(0.4):
			mesh += geo_gen.generate_glass(shape, wall_height)
	else:
		mesh  = tools.Mesh()

	return mesh

def limit_pool_size(envelope):
	ring = tools.get_rings(envelope)[0]
	seg1 = np.array(ring[1]) - np.array(ring[0])
	seg2 = np.array(ring[2]) - np.array(ring[1])
	
	L1, L2 = np.linalg.norm(seg1), np.linalg.norm(seg2)
	
	center = np.array([(ring[0][0] + ring[1][0] + ring[2][0] + ring[3][0])/4., (ring[0][1] + ring[1][1] + ring[2][1] + ring[3][1])/4.])
	dir1, dir2 = seg1/L1, seg2/L2
	
	if L1 >= L2:
		L1 = min(L1, 10.)
		L2 = min(L2, 4.)
	else:
		L1 = min(L1, 4.)
		L2 = min(L2, 10.)
	
	coord1, coord2, coord3, coord4 = center - L1*dir1/2. - L2*dir2/2., center + L1*dir1/2. - L2*dir2/2., center + L1*dir1/2. + L2*dir2/2., center - L1*dir1/2. + L2*dir2/2.
	return tools.Polygon([coord1, coord2, coord3, coord4, coord1])

def distribute_spaced_points(N, extent, dist_min):
	ntrials, X, Y = N*100, [], []
	for i in range(ntrials):
		x = np.random.random()*(extent[1] - extent[0]) + extent[0]
		y = np.random.random()*(extent[3] - extent[2]) + extent[2]

		add_point = True
		for j in range(len(X)):
			xj, yj = X[j], Y[j]
			dist = ((x - xj)**2 + (y - yj)**2)**0.5
					
			if dist < dist_min:
				add_point = False
				break
		if add_point:
			X.append(x)
			Y.append(y)
	
	return X, Y

def spread_within_area(density, mode, shape, level, tolerance, spacing, number_limit = 0, limit_mode = "distance"):
	output_positions, positions_2D, orientation, success = [], [], [], False
	spawn_area = shape.buffer(-tolerance)

	npoints = 0
	if spawn_area.area != 0.:
		bounds = spawn_area.envelope
		envelope_coord_array = np.array(tools.get_rings(bounds)[0])
		extent = [envelope_coord_array[:,0].min(), envelope_coord_array[:,0].max(), envelope_coord_array[:,1].min(), envelope_coord_array[:,1].max()]

		if mode == "uniform":
			separation = np.sqrt(1./density)
			n, m = max(int((extent[1] - extent[0])/separation), 1), max(int((extent[3] - extent[2])/separation), 1)

			separation_x = (extent[1] - extent[0])/n
			separation_y = (extent[3] - extent[2])/m

			x_space = np.linspace(extent[0] + separation_x/2., extent[1] - separation_x/2., n)
			y_space = np.linspace(extent[2] + separation_y/2., extent[3] - separation_y/2., m)
			
			#x_space += -x_space.mean() + (extent[0] + extent[1])*0.5
			#y_space += -y_space.mean() + (extent[2] + extent[3])*0.5
			
			X, Y = np.meshgrid(x_space, y_space)
			X, Y = X.flatten(), Y.flatten()

		elif mode == "random":
			npoints = int(bounds.area*density)
			X, Y = distribute_spaced_points(npoints, extent, spacing)

		npoints = 0

		for i in range(len(X)):
			point = tools.Point([X[i], Y[i]])
			if point.within(spawn_area):
				output_positions.append(np.array([X[i], Y[i], level]))
				positions_2D.append([X[i], Y[i]])
				npoints += 1

		orientation = list(np.zeros(npoints)) if mode == "uniform" else list(np.random.random(npoints)*2*np.pi)

		if number_limit != 0 and npoints > number_limit and npoints != 0:
			array_positions, array_orientations = np.array(output_positions), np.array(orientation)

			if limit_mode == "distance":
				center = np.array(shape.centroid.coords[0])

				distances_to_center = np.sqrt((array_positions[:, 0] - center[0])**2 + (array_positions[:, 1] - center[1])**2)
				idxs = np.argsort(distances_to_center)

			elif limit_mode == "random":
				idxs = np.random.permutation(np.arange(npoints))

			output_positions = list(array_positions[idxs][0: number_limit])
			orientation      = list(array_orientations[idxs][0: number_limit])
			npoints = number_limit

	occupied_space = MultiPoint(positions_2D).buffer(tolerance)

	return output_positions, orientation, occupied_space

def spread_along_perimeter(shape, free_space, level, N, width, depth):
	ntries = 5

	count, positions, orientations, total_space = 0, [], [], Polygon()
	shape = tools.orient(shape)
	for i in range(N*ntries):
		t = np.random.random()
		success, position, direction = tools.get_position_direction(shape, t)


		perp = np.array([-direction[1], direction[0]])
		position   = position + perp*(depth/2. + 0.15)
		orientation = np.arctan2(direction[1], direction[0])
		occupied_space = tools.create_box(position, [width, depth], orientation)

		if (occupied_space.intersection(free_space)).area >= width*depth*0.999:
			positions.append(np.array([position[0], position[1], level]))
			orientations.append(orientation)
			free_space = free_space.difference(occupied_space)
			total_space = unary_union([total_space, occupied_space])
			count += 1
			if count == N:
				break

	return positions, orientations, total_space

def generate_yard_meshes(facet, free_area):
	meshes, current_objects = tools.Mesh(), Objects()

	if facet.polygon.area >= 50. and facet.tag == "yard":
		level = facet.level
		extent, center, width, length = tools.determine_extent(facet.polygon)

		if length > 20:
			grounded_yard, make_trees, make_bushes = tools.pick_bool(0.9), tools.pick_bool(0.8), tools.pick_bool(0.5)
		else:
			grounded_yard, make_trees, make_bushes = tools.pick_bool(0.9), tools.pick_bool(0.8), tools.pick_bool(0.5)

		if grounded_yard:
			ground_length = tools.suggest_value(2., length, 10, 5., 0.1)
			ground_shape  = tools.create_box([width/2., extent[3] - ground_length/2.], [width, ground_length], 0.)
			ground_shape  = ground_shape.intersection(free_area)

			border           = tools.orient((ground_shape.buffer(0.1).difference(ground_shape)).intersection(facet.polygon))
			ground_surface   = geo_gen.meshify_shape(ground_shape, level + 0.20, "Grass", [0., 0., 0., 1.])
			grass            = geo_gen.generate_grass(ground_shape, level, density = 6.)
			border_surface   = geo_gen.extrude_shape(border.buffer(10e-8), level, level + 0.25, "Smooth_Wall", color1 = [1., 1., 1., 1.], color2 = [1., 1., 1., 1.])
			meshes += ground_surface + border_surface + grass
			free_area = free_area.difference(ground_shape)

			treeable_area = ground_shape

		if grounded_yard and tools.pick_bool(1.0):
			bushes_shape = (facet.polygon.difference(facet.polygon.buffer(-0.2))).intersection(ground_shape.buffer(0.1))
			bushes_shape = (bushes_shape.buffer(-0.02, join_style = 2).buffer(0.02, join_style = 2)).intersection(ground_shape)

			free_area    = free_area.difference(bushes_shape)
			frame        = geo_gen.extrude_rings(bushes_shape, level + 0.2, level + 3.0, "Plant", [1]*4)
			bushes_mesh  = geo_gen.cover_surface2(frame, 30, material = "Plant", size = 0.5, angle_max = 10.)

			meshes      += bushes_mesh
			treeable_area = ground_shape.difference(bushes_shape)

		if make_bushes and grounded_yard:
			bushes_shape = (facet.polygon.difference(facet.polygon.buffer(-0.7))).intersection(ground_shape)
			bushes_shape = (bushes_shape.buffer(-0.2, join_style = 2).buffer(0.2, join_style = 2)).intersection(ground_shape)

			free_area    = free_area.difference(bushes_shape)
			bushes_mesh  = geo_gen.generate_bushes(bushes_shape, level + 0.2, density = 50., size = 0.2)
			meshes      += bushes_mesh
			treeable_area = ground_shape.difference(bushes_shape)

		else:
			treeable_area = free_area

		if make_trees:
			nassets = len(model_assets.types["Trees"])

			tree_positions, orientation, occupied_space = spread_within_area(2, "random", treeable_area, level, 1.5, 1.5, number_limit = 4, limit_mode = "random")

			free_area.difference(occupied_space)
			ntrees = len(tree_positions)

			backyard_trees = Objects()

			backyard_trees.positions    = tree_positions
			backyard_trees.asset_ids    = list(np.random.randint(0, nassets, ntrees))
			backyard_trees.sizes        = [1.0]*ntrees

			backyard_trees.radii        = list(np.random.random(ntrees)*(3. - 1.5) + 1.5)
			backyard_trees.tags         = ["Trees"]*ntrees
			backyard_trees.orientations = orientation
			current_objects.merge(backyard_trees)

		if make_trees and grounded_yard == False:
			canteros              = Objects()
			canteros.tags         = ["Cantero"]*ntrees
			canteros.asset_ids    = [0]*ntrees
			canteros.positions    = backyard_trees.positions
			canteros.sizes        = [1.0]*ntrees
			canteros.orientations = [0.]*ntrees
			canteros.radii        = [0.7]*ntrees
			current_objects.merge(canteros)
	return meshes, current_objects, free_area

def populate_facets(building, recipe):
	things = Objects()
	trees  = Trees()
	meshes = tools.Mesh()
	presets = recipe.presets
	
	watertank_total = 0
	vent_type = np.random.randint(len(model_assets.types["Vents"]))
	tank_type = np.random.randint(len(model_assets.types["Water_Tanks"]))

	for facet in building.facets:
		if facet.kind == "horizontal":
			level = facet.level
			i = np.where(building.levels == level)[0][0]

			built_shape = building.built_shapes[i]

			has_pool = False
			if facet.tag in ["balcony", "small_yard"]:
				facade_separation = 0.5
			else:
				facade_separation = 1.0
			free_area = facet.polygon.difference(built_shape.buffer(facade_separation, join_style = 2))    ### A bit of space from facade
			free_area = free_area.buffer(-0.15, join_style = 2)                             ### Walls excluded

			Pool_area = tools.Polygon([])
			current_objects = Objects()
			### Pool
			if recipe.wealth == "rich" and np.random.random() <= 1./3.:
				if facet.tag in ["terrace", "yard"]:
					poolable_area = facet.polygon.buffer(-1.5)
					
					if poolable_area.type == "Polygon":
						areas = [poolable_area]
					elif poolable_area.type == "MultiPolygon":
						areas = [shape.area for shape in poolable_area]
						areas = [poolable_area[np.argmax(areas)]]
					else:
						areas = []
					for poolable_area in areas:
						if poolable_area.area != 0.:
							pool_envelope = poolable_area.minimum_rotated_rectangle

							pool_envelope = limit_pool_size(pool_envelope)

							Pool_area     = pool_envelope.intersection(poolable_area)
							water_shape   = Pool_area.buffer(-1.5).buffer(1.)

							border_width  = tools.suggest_value(0.2, 1.5, 0.5, 0.5, 0.1)
							border_height = tools.suggest_value(0.2, 0.5, 0.2, 0.2, 0.01)
							pool_mat      = tools.weigh_output(["Pool_Side", "Vertex_Shinny", "Smooth", "Smooth2", "Wooden_Tiles"], [30, 15, 10, 10, 5])
							if water_shape.area >= 2.*5.:
								has_pool = True
								level = facet.level if facet.level != 0. else 0.15
								pool_bounds      = (water_shape.buffer(border_width)).difference(water_shape)
								water_surface    = geo_gen.meshify_shape(water_shape, level + border_height - 0.1, "Dirty_Water", [0., 0., 0., 1.])
								walkable_surface = geo_gen.extrude_shape(pool_bounds, level, level + border_height, pool_mat, color1 = [1., 1., 1., 1.], color2 = [1., 1., 1., 1.])
								
								free_area = free_area.difference(Pool_area)
								meshes += water_surface + walkable_surface

			### Backyards and trees
			yard_mesh, yard_objects, free_area = generate_yard_meshes(facet, free_area)
			meshes += yard_mesh
			current_objects.merge(yard_objects)


			### Filter for pool
			if has_pool and tools.pick_bool(0.8):
				position, orientation, occupied_space = spread_along_perimeter(facet.polygon, free_area.intersection(Pool_area.buffer(2.0)), level, 1, 1.0, 1.0)
				free_area = free_area.difference(occupied_space)

				N = len(position)
				filters              = Objects()
				filters.tags         = ["Pool_Filter"]*N
				filters.asset_ids    = [0]*N
				filters.positions    = position
				filters.sizes        = [1.0]*N
				filters.radii        = [2.0]*N
				filters.orientations = orientation
				current_objects.merge(filters)


			### Ventiluz
			if facet.tag in ["commerce_roof"] and facet.level >= 1*3.0 and tools.pick_bool(0.5) and facet.polygon.area >= 30.:
				constrains = free_area.buffer(-1.0)
				if constrains.area != 0.:
					coords = list(constrains.centroid.coords)[0]
					#coords = [coords[0], coords[1]]

					if tools.pick_bool(0.3):
						size_x = size_y = tools.suggest_value(1., 5., 4., 1.)
						orientation = np.pi/4.
					else:
						size_x, size_y = tools.suggest_value(1., 5., 4., 1.), tools.suggest_value(1., 5., 4., 1.)
						orientation = 0.

					ventiluz_area = tools.create_box(coords, [size_x, size_y], orientation).intersection(constrains)

					ventiluz_mesh, success = geo_gen.generate_ventiluz(ventiluz_area, facet.level)

					if success:
						meshes += ventiluz_mesh
						free_area = free_area.difference(ventiluz_area.buffer(0.15, join_style = 2))

			if facet.tag in ["commerce_roof", "lower_rooftop"] and tools.pick_bool(0.4):
				shape = roofs.create_imperfections(free_area, free_area)
				meshes += geo_gen.extrude_shape(shape, facet.level, facet.level + 0.1, presets.mat_roof)

			### Ventilation
			if (facet.tag in ["terrace", "normal_roof", "commerce_roof", "complex_roof", "zinc_roof"]) and (facet.area >= 80.) or facet.tag == "lower_rooftop":
				if facet.tag == "lower_rooftop":
					density, number_limit, tolerance = 8/facet.area, 3, 0.7
				else:
					density, number_limit, tolerance = 1./9, 1, 0.7

				asset_id = model_assets.types["Vents"][vent_type]
				radius = model_assets.radii[asset_id]

				#spread_within_area(density, mode, shape, tolerance, spacing, number_limit = 0, limit_mode = "distance")
				vent_positions, orientations, occupied_space = spread_within_area(density, "uniform", free_area, level, radius, 2*radius, number_limit = number_limit, limit_mode = "distance")

				free_area = free_area.difference(occupied_space)


				nvent                     = len(vent_positions)
				ventilations              = Objects()
				ventilations.tags         = ["Vents"]*nvent
				ventilations.asset_ids    = [vent_type]*nvent
				ventilations.positions    = vent_positions
				ventilations.sizes        = [1.0]*nvent if facet.tag != "complex_roof" else [1.0]*nvent
				ventilations.orientations = orientations
				ventilations.radii        = [1.0]*nvent
				current_objects.merge(ventilations)


			### Water tank
			if facet.tag in ["lower_rooftop", "higher_rooftop"] and watertank_total < 3:
				volume = recipe.floors*3*recipe.main_shape.area*24/7000.
				tank_height = tools.suggest_value(2.5, 4., 3., 1., 0.2)
				success, tank_mesh = roofs.generate_watertank(facet.polygon, volume, level, height = tank_height, material = recipe.presets.mat_wall, color = presets.main_color)
				if success == False:
					asset_id = model_assets.types["Water_Tanks"][tank_type]
					radius = model_assets.radii[asset_id]

					tank_positions, orientations, occupied_space = spread_within_area(0.25, "uniform", free_area, level, radius, 2*radius, number_limit = 2, limit_mode = "distance")

					free_area          = free_area.difference(occupied_space)
					ntanks             = len(tank_positions)
					tanks              = Objects()
					tanks.tags         = ["Water_Tanks"]*ntanks
					tanks.asset_ids    = [tank_type]*ntanks
					tanks.positions    = tank_positions
					tanks.sizes        = [1.0]*ntanks
					tanks.radii        = [1.0]*ntanks
					tanks.orientations = orientations
					current_objects.merge(tanks)
					watertank_total += ntanks
				else:
					watertank_total += 1
					meshes += tank_mesh


			various = model_assets.generate_objects(facet, building, free_area)
			current_objects.merge(various)


			things.merge(current_objects)
	return meshes, things, trees

def generate_appartment_facades(building, recipe):
	facades = Facades()
	presets = recipe.presets
	facet_count = 0
	for facet in building.facets:
		if facet.kind == "vertical" and facet.tag in ["facade1", "facade2", "small_windows1"]:
			if facet.tag == "facade1":
				model_width = 6

			elif facet.tag == "facade2":
				model_width = 6

			else:
				model_width = 3

			ncols = max(min(int(facet.width/model_width), 3), 1)

			appartment_width = facet.width/ncols


			height = facet.max_height - facet.min_height
			max_floor = int((height + 0.001)/recipe.floor_height)

			xpos, zpos, preset_list, widths = [], [], [], []
			for i in range(0, max_floor):
				generate_facade, iterate_columns, facade_width = False, False, appartment_width
				if i == 0 and facet.adjacent != "default":
					adjacent      = facet.adjacent
					area_adjacent = adjacent.polygon.area
					hfacet_width  = area_adjacent/facet.width

					### Case balcony
					if facet.tag in ["facade1", "facade2"] and hfacet_width <= 3.5 and adjacent.tag == "balcony":
						preset, kind = presets.facade_main_balcony, 0
						facet.generated_door = True
						generate_facade      = True
						iterate_columns      = True

					### Case yard or terrace
					elif facet.tag in ["facade1", "facade2"] and adjacent.tag in ["yard", "terrace"]:
						preset, kind = presets.facade_terrace, 1
						facet.generated_door = True
						generate_facade      = True
						facade_width         = facet.width

					### Case zinc roof
					elif facet.tag in ["facade1", "facade2"] and adjacent.tag in ["zinc_roof"]:
						preset, kind = presets.facade_terrace, 1
						facet.generated_door = False
						generate_facade      = False
						facade_width         = facet.width

				### Case on street
				elif i == 0 and facet.min_height == 0. and facet.tag == "facade1":
					preset, kind = presets.facade_PB, 2
					facet.generated_door = True
					generate_facade      = True
					facade_width         = facet.width
						
				else:
					### Case facade with or without attached balcony
					if facet.tag == "facade1":
						preset, kind = presets.facade_main_normal1, 3
						generate_facade      = True
						iterate_columns      = True

					### Case facade without attached balcony
					elif facet.tag == "facade2":
						preset, kind = presets.facade_main_normal2, 4
						generate_facade      = True
						iterate_columns      = True

					### Case wall facing small yards
					elif facet.tag == "small_windows1":
						preset, kind = presets.facade_small_yard, 5
						generate_facade      = True
						iterate_columns      = True

				if facet.min_height >= recipe.floors*recipe.floor_height: generate_facade = False

				if generate_facade:
					if facet.tag in ["small_windows1"]:
						facade_width = 10.

					if iterate_columns:
						for j in range(ncols):
							# xpos.append(j*appartment_width + appartment_width/2. - facet.width/2.)

							preset_list.append(preset)
							widths.append(appartment_width)
							xpos.append(j*appartment_width + appartment_width/2.)
							zpos.append(i*recipe.floor_height + recipe.floor_height*0.5)

							local_center    = facet.center + (j*appartment_width + appartment_width/2. - facet.width/2.)*facet.direction
							local_center[2] = facet.min_height + i*recipe.floor_height + recipe.floor_height*0.5
							facades.add(preset, local_center, facade_width, appartment_width, recipe.floor_height, facet.orientation*rad2deg)
					else:
						preset_list.append(preset)
						widths.append(appartment_width)
						xpos.append(facet.width/2.)
						zpos.append(recipe.floor_height*0.5)

						local_center    = facet.center
						local_center[2] = facet.min_height + i*recipe.floor_height + recipe.floor_height*0.5
						facades.add(preset, local_center, facade_width, appartment_width, recipe.floor_height, facet.orientation*rad2deg)

			if len(xpos) != 0:
				single_facade    = Complex_Facades(widths, recipe.floor_height, xpos, zpos, preset_list)
				facet.facade.merge(single_facade)
			else:
				single_facade    = Complex_Facades([-1], recipe.floor_height, [-1], [-1], [presets.facade_main_normal1])
				facet.facade.merge(single_facade)

		elif facet.kind == "vertical":
			single_facade    = Complex_Facades([-1], recipe.floor_height, [-1], [-1], [presets.facade_main_normal1])
			facet.facade.merge(single_facade)


			facet_count += 1
	return facades

def check_facades(facades):
	nfacades = len(facades.positions)
	for i in range(nfacades):
		pos_i = facades.positions[i]
		for j in range(nfacades):
			pos_j = facades.positions[j]
			dist = ((pos_i[0] - pos_j[0])**2 + (pos_i[1] - pos_j[1])**2 + (pos_i[2] - pos_j[2])**2)**0.5
			
			if dist < 1. and i != j:
				pass

def generate_appartment_doors(building, recipe):
	facades = Facades()
	presets = recipe.presets
	for facet in building.facets:
		if facet.kind == "horizontal" and facet.tag in ["small_yard", "commerce_yard", "yard", "terrace", "normal_roof", "commerce_roof", "complex_roof", "balcony", "commerce_garden"]:
			has_access, widths = False, []
			for i in range(len(facet.adjacents)):
				v_facet = facet.adjacents[i]
				widths.append(v_facet.width)
				if v_facet.generated_door:
					has_access = True

			for i in range(len(facet.adjacents)):
				v_facet, generate_doors = facet.adjacents[i], False

				local_center    = v_facet.center
				local_center[2] = v_facet.min_height + recipe.floor_height*0.5
				
				### Entrance to small yard
				if facet.tag in ["small_yard", "commerce_yard", "commerce_roof", "balcony"]:
					asset_width = 10.
					if v_facet.width == max(widths) and has_access == False:
						preset = presets.service_door
						generate_doors = True
						has_access     = True

					elif v_facet.width >= 3.:
						preset = presets.service_window
						generate_doors = True

				elif facet.tag in ["terrace", "yard"]:
					asset_width = min(10., v_facet.width)
					#asset_width = 10
					if v_facet.width == max(widths) or v_facet.width >= 6.:
						preset = presets.facade_terrace
						generate_doors = True
					elif v_facet.width >= 4.:
						preset = presets.service_door
						has_access     = True
						generate_doors = True
						asset_width = 10.
					elif v_facet.width >= 2.:
						preset = presets.service_window
						has_access     = True
						generate_doors = True
						asset_width = 10.

				elif facet.tag in ["normal_roof"]:
					#asset_width = min(10., v_facet.width)
					asset_width = 10
					if v_facet.width == max(widths):
						preset = presets.service_door
						generate_doors = True
						#asset_width = 10.
					elif v_facet.width >= 2.5:
						preset = presets.service_window
						has_access     = True
						generate_doors = True
						#asset_width = 10.

				elif facet.tag in ["complex_roof"]:
					asset_width = 10

					if v_facet.width >= 2.5:
						preset = presets.service_window
						has_access     = True
						generate_doors = True

				if v_facet.generated_door:
					generate_doors = False

				v_height = v_facet.max_height - v_facet.min_height
				if v_height <= 2.:
					generate_doors = False

				if generate_doors:
					widths = [v_facet.width]
					xpos   = [v_facet.width/2.]
					zpos   = [recipe.floor_height*0.5]
					preset_list = [preset]

					single_facade    = Complex_Facades(widths, recipe.floor_height, xpos, zpos, preset_list)
					v_facet.facade.merge(single_facade)

					facades.add(preset, local_center, asset_width, v_facet.width, min(v_facet.max_height - v_facet.min_height, recipe.floor_height), v_facet.orientation*rad2deg)
	return facades

############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################


def gather_vertical_facets(built_shapes, levels):
	facet_sets, edges = [], []
	n_levels = len(levels)
	count = 0
	for i in np.arange(n_levels - 2, -1, -1):
		rings = tools.get_rings(built_shapes[i])
			
		for ring in rings:
			nseg = len(ring)
			for k in range(nseg - 1):
				segment = ring[k: k + 2]
				#segment = [segment[1], segment[0]]
				if segment in edges:
					j = edges.index(segment)

					facet_sets[j].min_height     = levels[i]
					facet_sets[j].bottom_left[2] = levels[i]
				else:
					vfacet = vertical_facet(idx = count, segment = segment, min_height = levels[i], max_height = levels[i + 1], shape = built_shapes[i])
					if vfacet.width != 0.:
						facet_sets.append(vfacet)
						edges.append(segment)
						count += 1
	return facet_sets

def gather_horizontal_facets(whole_shapes, built_shapes, levels):
	n_levels = len(levels)
	facets = []
	for i in range(n_levels):
		#polygons_level = tools.list_polygons(   whole_shapes[i].difference(built_shapes[i]) )
		polygons_level = tools.list_polygons(   whole_shapes[i].difference(built_shapes[i].buffer(1e-8, cap_style = 2, join_style = 2) ))
		#snap(ls_red, ls_blue, 1e-8))
		for k in range(len(polygons_level)):
			if polygons_level[k].area > 0.001:
				level = 0.0 if levels[i] == 0. else levels[i]
				facet = horizontal_facet(level,  polygons_level[k])

				j = np.where(levels == level)[0][0]
				facet.built_shape = built_shapes[j]
				facet.whole_shape = whole_shapes[j]

				facets.append(facet)
	return facets

def find_adjacent_facet(building, v_facet):
	edge_center = tools.Point([(v_facet.edge[0][0] + v_facet.edge[1][0])/2., (v_facet.edge[0][1] + v_facet.edge[1][1])/2.])
	
	adjacent = "default"
	for item in building.facets:
		if item.kind == "horizontal":
			if item.level == v_facet.min_height:
				if item.polygon.distance(edge_center) <= 0.00001:
					adjacent = item
	return adjacent

def process_facets(building):
	nfacets   = len(building.facets)
	whole_shapes = building.whole_shapes

	roof_shapes, roof_levels = roofs.determine_roofs(whole_shapes)
	nroofs = len(roof_shapes)

	## Precomputing quantities
	levels = []
	for i in range(nfacets):
		facet = building.facets[i]
		if facet.kind == "vertical": 
			segment = facet.edge
			center = [(segment[0][0] + segment[1][0])/2., (segment[0][1] + segment[1][1])/2.]
			
			if tools.point_in_ring(center, building.medianera, criteria = 0.1):
				facet.tag = "medianera"
			else:
				facet.tag = "default"

			facet.adjacent = find_adjacent_facet(building, facet)
			if facet.adjacent != "default":
				facet.adjacent.adjacents.append(facet)

		elif facet.kind == "horizontal":
			facet.dist_2_top = -1.0
			for j in range(nroofs):
				roof_shape = roof_shapes[j]
				roof_level = roof_levels[j]

				if facet.polygon.intersection(roof_shape) != 0.:
					facet.dist_2_top = building.levels[roof_level] - facet.level

def classify_appartment_facets(building, recipe):
	presets   = recipe.presets
	nfacets   = len(building.facets)

	## Actual classification
	for i in range(nfacets):
		facet = building.facets[i]

		if facet.kind == "horizontal":
			centroid = facet.polygon.centroid

			n_level = facet.level/recipe.floor_height
			if facet.dist_2_top == 0.0:
				facet.tag, facet.material = "lower_rooftop",  presets.mat_roof                           # simple roof, without anything on top
			# elif facet.dist_2_top <= 2.5:
				# facet.tag, facet.material = "lower_rooftop", presets.mat_roof

			elif recipe.building_type == "offices":
				if facet.level >= 1.0*recipe.floor_height and facet.dist_2_top >= recipe.floor_height:
					facet.tag, facet.material = "commerce_yard", presets.mat_terrace

				elif facet.level >= 3*recipe.floor_height and facet.area >= 25 and facet.area <= 200 and tools.pick_bool(0.2):
					facet.tag, facet.material = "complex_roof", presets.mat_metal

				elif facet.area <= 6*6 and facet.dist_2_top >= recipe.floor_height:
					facet.tag, facet.material = "commerce_yard", presets.mat_terrace

				elif facet.level >= 1*recipe.floor_height:
					facet.tag, facet.material = "commerce_roof", presets.mat_roof

				else:
					facet.tag, facet.material = "commerce_yard", presets.mat_terrace


			elif centroid.coords[0][1] >= recipe.back_offset:
				### Back surfaces
				if recipe.back_structures == "back_yard":
					if facet.level >= 1.0*recipe.floor_height*0 + 2:
						facet.tag, facet.material = "shed_top", presets.mat_roof
					else:
						facet.tag, facet.material = "yard", presets.mat_ground

				elif recipe.back_structures == "parking" and facet.level >= 0.5*recipe.floor_height:
					facet.tag, facet.material = "zinc_roof", presets.mat_metal

				elif recipe.back_structures == "commerce":
					if facet.level >= 1.0*recipe.floor_height:
						facet.tag, facet.material = "commerce_roof", presets.mat_roof
					elif facet.dist_2_top >= recipe.floor_height:
						facet.tag, facet.material = "commerce_yard", presets.mat_terrace
					else:
						facet.tag, facet.material = "commerce_yard", presets.mat_terrace

				elif recipe.back_structures == "terrace":
					if facet.level >= 1.0*recipe.floor_height and facet.dist_2_top >= recipe.floor_height:
						facet.tag, facet.material = "terrace", presets.mat_terrace
					elif facet.area <= 4*4 and facet.dist_2_top >= recipe.floor_height:
						facet.tag, facet.material = "small_yard", presets.mat_terrace
					else:
						facet.tag, facet.material = "yard", presets.mat_terrace

				elif recipe.back_structures == "continuation":
					if facet.level >= 1.0*recipe.floor_height*0 + 2:
						facet.tag, facet.material = "normal_roof", presets.mat_roof
					else:
						facet.tag, facet.material = "yard", presets.mat_floor

			else:
				if facet.area <= 21. and facet.level > 2*recipe.floor_height and facet.dist_2_top >= recipe.floor_height:
					facet.tag, facet.material = "balcony", presets.mat_terrace

				elif facet.area <= 16. and facet.level <= 2*recipe.floor_height and facet.dist_2_top >= recipe.floor_height:
					facet.tag, facet.material = "small_yard", presets.mat_terrace

				elif facet.level < 0.5*recipe.floor_height:
					facet.tag, facet.material = "yard", presets.mat_floor

				elif recipe.terraceable and facet.level >= 2*recipe.floor_height and facet.area >= 16.0 and facet.dist_2_top >= recipe.floor_height:
					facet.tag, facet.material = "terrace", presets.mat_terrace

				elif facet.level >= 3*recipe.floor_height and facet.area <= 200 and recipe.wealth == "rich" and tools.pick_bool(0.15):
					facet.tag, facet.material = "complex_roof", presets.mat_roof

				else:
					facet.tag, facet.material = "normal_roof", presets.mat_roof 

			# print(recipe.building_type + ", " + "facet tag: " + facet.tag)
		elif facet.kind == "vertical":
			facet.material = presets.mat_wall
			if facet.tag == "medianera":
				pass
			elif facet.width >= 6. and facet.oclusion == False and facet.min_height < recipe.floors*recipe.floor_height:
				facet.tag = "facade1"

				if building.is_corner and building.corner_direction == "left":
					if np.dot(facet.perpendicular, vec_right) >= 0.9:
						facet.tag = "facade2"
					elif np.dot(facet.perpendicular, vec_up) >= 0.9:
						facet.tag = "facade2"

				elif building.is_corner and building.corner_direction == "right":
					if np.dot(facet.perpendicular, vec_left) >= 0.9:
						facet.tag = "facade2"
					elif np.dot(facet.perpendicular, vec_up) >= 0.9:
						facet.tag = "facade2"

				elif np.dot(facet.perpendicular, vec_up) >= 0.9:
					facet.tag = "facade2"
				
			elif facet.width >= 1.5:
				facet.tag = "small_windows1"
			else: 
				facet.tag = "default"

def create_mesh(building, recipe, high_poly = True):
	facets = building.facets
	presets = recipe.presets
	
	mesh = tools.Mesh()
	
	for i in range(building.n_levels - 1):
		shape = building.built_shapes[i]

		mesh += geo_gen.extrude_rings(shape, building.levels[i], building.levels[i + 1], presets.mat_wall, presets.main_color)

	for facet in facets:
		if facet.kind == "horizontal":
			if facet.polygon.is_empty == False:
				level = facet.level if facet.level != 0. else 0.15
				if facet.tag in ["terrace", "small_yard", "yard", "balcony"]:
					mesh += geo_gen.meshify_shape(facet.polygon, level, presets.mat_terrace, presets.terrace_color, organicity = 0.0)
				elif (facet.tag in ["zinc_roof", "complex_roof", "shed_top"]) == False:
					mesh += geo_gen.meshify_shape(facet.polygon, level, presets.mat_roof,    presets.terrace_color, organicity = 0.0)

	return mesh

class building_collection:
	#This is valid data structure
	def __init__(self):
		self.objects  = Objects()
		self.mesh     = tools.Mesh()
		self.trees    = Trees()
		self.facades  = Facades()
		#self.presets  = materials()

class building_shape:
	def __init__(self, built_shapes, whole_shapes, levels, is_corner, corner_direction, medianera):
		self.built_shapes   = built_shapes
		self.whole_shapes   = whole_shapes
		self.levels         = np.array(levels, dtype = np.float32)
		self.n_levels       = len(levels)
		self.is_corner      = is_corner
		self.corner_direction = corner_direction
		self.medianera      = medianera

	def determine_facets(self):
		self.facets  = gather_vertical_facets(self.built_shapes, self.levels)
		self.facets  = self.facets + gather_horizontal_facets(self.whole_shapes, self.built_shapes, self.levels)




class appartment_recipe:
	def __init__(self, kind, parcel, floors, size = 30., yard_kind = "left", yard_width = 0., yard_length = 0., yard_sep = 0., yard_offset = 0., nrep = 1):
		if kind in ["type1", "type2", "type3", "type4" , "type5"]:
			self.building_type = "appartments"
		else:
			self.building_type = "offices"
		
		#building_type = "offices"
		self.parcel_shape       = parcel.parcel_shape
		self.kind               = kind
		self.variable_yard_size = tools.pick_bool(0.3) if kind != "type4" else False
		self.wealth             = "rich" if tools.pick_bool(parcel.properties[0]) else "poor"
		self.dirtyness          = tools.suggest_value(0., 1., 1. - parcel.properties[0], 0.2, 0.01)

		self.presets            = building_presets(self.building_type, self.wealth, self.dirtyness)
		
		self.terraceable        = tools.pick_bool(0.8) if self.wealth == "rich" else tools.pick_bool(0.3)
		self.balconyzable       = tools.pick_bool(0.8) if self.wealth == "rich" else tools.pick_bool(0.6)
		self.symmetric_balconies= tools.pick_bool(0.25) if kind != "type4" else False                   ## In case it has terraces
		self.n_balconies        = int(tools.suggest_value(1, 4, 2, 1, 1)) if self.balconyzable else 0 ## In case it has terraces

		if self.balconyzable: self.terraceable = True
		if self.building_type == "offices":
			# self.terraceable  = False
			self.balconyzable = False

		if kind in ["type5", "type6"] and self.balconyzable: 
			self.n_balconies        = 2
			self.variable_yard_size = False

		if self.building_type == "appartments":
			self.back_structures    = tools.weigh_output(["back_yard", "parking", "commerce", "terrace", "continuation"], [10., 15., 10., 5., 3.])
			# if
		else:
			self.back_structures    = tools.weigh_output(["commerce", "continuation", "parking"], [10, 5, 3])

		self.floors             = floors
		self.floor_height       = tools.suggest_value(2.9, 3.3, 3., 0.15, 0.01)
		
		self.has_elevator = True if floors > 4 else False

		self.main_block_size    = size if parcel.length - size <= 35. else parcel.length - 35.
		if parcel.length - self.main_block_size < 3.: self.main_block_size = parcel.length - 3.

		self.main_block_width = tools.suggest_value(10., parcel.width - 1.5*2, parcel.width - 2*2., 2, 0.2) if kind in ["office3"] else parcel.width
		if parcel.is_corner: self.main_block_size = parcel.length

		back_offset = self.main_block_size

		self.yards = {"kind": yard_kind, "width": yard_width, "length": yard_length, "separation": yard_sep + yard_length/2., "repetitions": nrep, "offset": yard_offset}

		back_length = parcel.length - back_offset
		if back_length <= 1: self.back_structures = "commerce"
		elif back_length <= 5: self.back_structures = ["back_yard", "commerce"][tools.weigh_decision([8., 3.])]


		elif back_length >= 15: self.back_structures = ["back_yard", "parking"][tools.weigh_decision([1 + 6*parcel.properties[1], 4.])]
		# self.back_structures = "parking"
		print("first decisons: ", self.back_structures)
		self.back_offset = back_offset
		self.back_length = back_length
		# tools.plot_polygon(geo_gen.create_box([parcel.middle, back_offset + back_length/2.], [parcel.width, back_length],          0.))
		# tools.plot_polygon(parcel.parcel_shape)

		self.back_shape  = tools.create_box([parcel.middle, back_offset + back_length/2.], [parcel.width, back_length],          0.).intersection(parcel.parcel_shape)
		self.main_shape = (parcel.parcel_shape).difference(self.back_shape.buffer(1e-8, join_style = 2))

		self.back_shape = self.back_shape.buffer(0.1, join_style = 2).intersection(parcel.parcel_shape)
		self.main_block_size, self.main_block_width, orientation, coords, centroid = tools.get_orientation(self.main_shape)

		self.front_shape = parcel.front.buffer(tools.suggest_value(3, 6, 4, 1, 0.1), join_style = 2).intersection(parcel.parcel_shape)
		# self.main_shape  = tools.create_box([parcel.middle, self.main_block_size/2.], [parcel.width, self.main_block_size], 0.).intersection(parcel.parcel_shape)

	def summary(self):
		print("Kind: " + self.building_type + " " + self.kind + " Wealth: " + self.wealth + " Dirtyness: " + str(self.dirtyness))
		print("Main size: " + str(self.main_block_size) + " floors: " + str(self.floors) + " of " + str(self.floor_height) + "m")
		print("Variable yard size: ", self.variable_yard_size)
		print("Terraceable: " + str(self.terraceable) + " balconies: " + str(self.balconyzable) + " (" + str(self.n_balconies) + ") Symmetric? : " + str(self.symmetric_balconies))
		print("Back: " + self.back_structures + " Size: " + str(self.back_length))


def appartment_yard_shapes(recipe, parcel):
	global plot_count
	#determining perimeter where yards will generate
	ring = tools.get_rings(tools.orient(recipe.parcel_shape))[0]

	idx = 0
	for j in range(len(ring)):
		segment = ring[j]
		if np.sqrt((segment[0])**2 + (segment[1])**2) <= 0.001:
			idx = j
	ring = np.roll(ring, -idx, axis = 0)

	indexes = np.unique(ring, axis = 0, return_index=True)[1]
	indexes.sort()
	ring = list(ring[indexes][1:])
	ring.append(np.array([0., 0.]))

	linestring = tools.LineString(ring).coords


	#generating yards
	nfloors, floor_height = recipe.floors, recipe.floor_height
	shapes, levels = [], []

	if   recipe.back_structures == "back_yard":    min_level = [0, 1, 2][tools.weigh_decision([1, 3, 2])]
	elif recipe.back_structures == "terrace":      min_level = [0, 1, 2][tools.weigh_decision([1, 3, 2])]
	elif recipe.back_structures == "continuation": min_level = [0, 1, 2][tools.weigh_decision([1, 3, 2])]
	elif recipe.back_structures == "parking":      min_level = [0, 1, 2][tools.weigh_decision([0, 3, 2])]
	elif recipe.back_structures == "commerce":     min_level = [0, 1, 2][tools.weigh_decision([0, 3, 2])]

	yard_info = recipe.yards

	if recipe.variable_yard_size == False:
		n_yard_levels = 1
	else:
		nbalconies = 2 if recipe.kind in ["type1", "type2", "type3", "type4"] and recipe.variable_yard_size else 1
		n_yard_levels = 1 + nbalconies

	for i in range(n_yard_levels):

		############ Yard size determination
		if recipe.variable_yard_size:
			yard_length2  = yard_info["length"] + i*2.5

		else:
			yard_length2 = yard_info["length"]


		############# Yard shape creation
		if yard_info["kind"] == "right":
			result = shape_gen.place_yards(linestring, yard_info["width"], yard_length2, yard_info["separation"], yard_info["repetitions"], yard_info["offset"])
		elif yard_info["kind"] == "left":
			result = shape_gen.place_yards(linestring, yard_info["width"], yard_length2, yard_info["separation"], yard_info["repetitions"], yard_info["offset"], flip = True)

		elif yard_info["kind"] == "I":
			yards1 = shape_gen.place_yards(linestring,                    yard_info["width"], yard_length2, yard_info["separation"], yard_info["repetitions"], yard_info["offset"], flip = False)
			yards2 = shape_gen.place_yards(linestring,                    yard_info["width"], yard_length2, yard_info["separation"], yard_info["repetitions"], yard_info["offset"], flip = True)
			result = unary_union([yards1, yards2])

		else:
			result = tools.Polygon([])

		if i == 0:
			levels.append(min_level*floor_height)
			shapes.append(result)
		elif recipe.variable_yard_size:
			levels.append((nfloors + nbalconies - i -1)*floor_height)
			shapes.append(result)

	return shapes, levels



def appartment_main_shapes(recipe, parcel):
	nfloors, floor_height  = recipe.floors, recipe.floor_height

	trim_corner = False
	if parcel.is_corner and parcel.corner_direction == "left" and tools.pick_bool(0.5):
		trim_corner = True
		piece_center, piece_orientation = [0., 0.], 45.*deg2rad

	elif parcel.is_corner and parcel.corner_direction == "right" and tools.pick_bool(0.5):
		trim_corner = True
		piece_center, piece_orientation = [parcel.width, 0.], -45.*deg2rad

	if trim_corner:
		d = 4.
		L1 = 2**0.5*d
		L2 = 2**0.5*d*2
		piece = tools.create_box(piece_center, [L1, L2], piece_orientation)
		
	#levels = tools.generate_random_levels(nfloors, 4)
	levels, shapes = [0], [parcel.parcel_shape.difference(recipe.back_shape.buffer(1e-8, join_style = 2))]


	if tools.pick_bool(0.0):
		# size   = np.array([parcel.width,  recipe.main_block_size])
		# center = np.array([parcel.middle, size[1]/2.])
  #
		# block = tools.create_box(center, size, rotation = parcel.orientation)

		shapes.append(recipe.main_shape)
		levels.append(nfloors*floor_height)

	else:
		# size   = np.array([parcel.width,  recipe.main_block_size])
		# center = np.array([parcel.middle, size[1]/2.])
  #
		# block = tools.create_box(center, size, rotation = parcel.orientation)

		levels = list(tools.generate_random_levels(nfloors, 3)*floor_height)
		# shapes = back.generate_random_building(recipe.main_shape, parcel.front, parcel.parcel_shape, nlevels = len(levels))
		# new_main_shape = recipe.main_shape.buffer(0.1, join_style = 2).intersection(parcel.parcel_shape)
		shapes = back.generate_random_building(recipe.main_shape, parcel.front, recipe.main_shape, nlevels = len(levels))

	shapes_yard, levels_yard, front_offset, back_offset = [], [], 0., 0.
	if recipe.balconyzable:
		nfloors_final = int(max(levels)/recipe.floor_height)
		nbalconies = min(nfloors_final - 1, recipe.n_balconies)

		for i in range(nbalconies):
			if recipe.symmetric_balconies:
				front_offset = (i + 1)*1.7
				back_offset  = (i + 1)*1.7

			else:
				front_offset = (i + 1)*1.7
				back_offset  = 0.

			size   = np.array([parcel.width,  recipe.main_block_size  - front_offset - back_offset])
			center = np.array([parcel.middle, front_offset + size[1]/2.])

			block = tools.create_box(center, size, rotation = parcel.orientation)
			if trim_corner:
				block = block.difference(piece)
			yard_shape = recipe.main_shape.buffer(10e-8, join_style = 2).difference(block)

			shapes_yard.append(yard_shape)
			# levels_yard.append((nfloors_final - (1))*recipe.floor_height)
			levels_yard.append((nfloors_final - (nbalconies - i))*recipe.floor_height)

	return shapes, levels, shapes_yard, levels_yard

objeto = 12

def determine_slenderness(whole_shapes, levels):
	nlevels = len(levels)
	slenderness = []
	for i in range(1, nlevels):
		current_shape = whole_shapes[i]
		height = levels[i] - levels[i - 1]
		if current_shape.area != 0.:
			if current_shape.type == "Polygon":
				L = (current_shape.area)**0.5

				slenderness.append(height/L)
			elif current_shape.type == "MultiPolygon":
				for shape in current_shape:
					L = (shape.area)**0.5

					slenderness.append(height/L)
	return slenderness

def topology_check(shapes, distance):
	count = []
	for shape in shapes:
		current_shape = shape.buffer(-distance, join_style = 2)
		if current_shape.area != 0.:
			if current_shape.type == "Polygon":
				count.append(1)
			elif current_shape.type == "MultiPolygon":
				count.append(len(current_shape))
			else:
				count.append(0)
		else:
			count.append(0)
	return count

def check_validity(count1, count2):
	if len(count1) != len(count2):
		validity = False
	else:
		if count1 == count2:
			validity = True
		else:
			validity = False
	return validity

levels_debug, shapes_debug = 0, 0
def build_appartment_shapes(recipe, parcel):
	global levels_debug, shapes_debug
	yard_level = int(tools.suggest_value(1, 2, 1, 0.5, 1))
	nlevels = recipe.floors

	shapes_to_unite, shapes_to_add, shapes_to_substract, levels = [], [], [], []
	
	shapes_back, levels_back                             = back.appartment_back_shapes(recipe, parcel)
	shapes_main, levels_main, shapes_yard1, levels_yard1 = appartment_main_shapes(recipe, parcel)
	shapes_yard, levels_yard                             = appartment_yard_shapes(recipe, parcel)

	shapes_unite     = shapes_back + shapes_main
	shapes_substract = shapes_yard + shapes_yard1
	levels_unite     = levels_back + levels_main
	levels_substract = levels_yard + levels_yard1

	if DEBUG:
		fig, axs = tools.plt.subplots(ncols = 2, nrows = 1)

		tools.plot_polygon(parcel.parcel_shape, axs = axs[0], linewidth = 2)
		tools.plot_polygon(shapes_substract, axs = axs[0])

		tools.plot_polygon(parcel.parcel_shape, axs = axs[1], linewidth = 2)
		tools.plot_polygon(shapes_unite, axs = axs[1])

	shapes_debug = [[shapes_back, shapes_main], [shapes_yard, shapes_yard1]]
	levels_debug = [[levels_back + levels_main], [levels_yard, levels_yard1]]

	whole_shapes, built_shapes, levels = shape_gen.better_combine(parcel.parcel_shape, shapes_unite, levels_unite, shapes_substract, levels_substract)
	whole_shapes, built_shapes, levels = roofs.generate_roofs(whole_shapes, built_shapes, levels)
	count1 = topology_check(whole_shapes, 0.)
	whole_shapes, built_shapes, levels = shape_gen.correct_shapes(whole_shapes, levels)
	count2 = topology_check(whole_shapes,  1.0)
	count3 = topology_check(whole_shapes, -1.0)

	slenderness = determine_slenderness(whole_shapes, levels)
	if max(slenderness) >= 3.5: validity = False
	else: validity = True

	validity *= check_validity(count1, count2)
	validity *= check_validity(count2, count3)

	if validity:
		print("Count")
		print(count1, count2)

	return whole_shapes, built_shapes, levels, validity

def create_model_appartment():
	import roofs

	level0 = tools.create_box([5., 20.], [10, 40.], 0.)
	level1 = tools.create_box([5., 15.], [10, 30.], 0.)

	level2_a = tools.create_box([5., 5.], [10, 10.], 0.)
	level2_b = tools.create_box([5., 25.], [10, 10.], 0.)

	level2 = unary_union([level2_a, level2_b])

	level3_a = tools.create_box([1.25, 5.], [2.5, 10.], 0.)
	level3_b = tools.create_box([10 - 1.25, 5.], [2.5, 10.], 0.)

	level3 = unary_union([level3_a, level3_b])

	whole_shapes = [level0, level1, level2, level3]
	built_shapes = [level1, level2, level3, tools.Polygon()]
	levels       = [0, 4.2, 8.4, 12.5]

	rooves, levels_idx = roofs.determine_roofs(whole_shapes)

	new_whole, new_built, new_levels = roofs.generate_roofs(whole_shapes, built_shapes, levels)
	return new_whole, new_built, new_levels

timer1, timer2, timer3, timer4, timer5, timer6, timer7, timer8, timer9, timer10, app_count = 0.0, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0

def generate_facade_mesh2(shape_info):
	mesh = tools.Mesh()
	for item in shape_info.facets:
		if item.kind == "vertical":
			mesh += item.facade.generate_mesh(facade_assets, item)
	return mesh

def plot_hfacets(shape_info):
	fig, axs = tools.plt.subplots(ncols= 1, nrows = 1)
	axs.set_title("Facet classification")
	for facet in shape_info.facets:
		if facet.kind == "horizontal":
			shape = facet.polygon
			tools.plot_polygon(shape, axs = axs)
			axs.annotate(facet.tag, list(shape.centroid.coords[0]))

def generate_shape_information(recipe, parcel):
	success, count = False, 0
	while(success == False and count < 10):
		try:
			shapes_success, shapes_trials = False, 0
			while(shapes_success == False and shapes_trials < 20):
				whole_shapes, built_shapes, levels, validity = build_appartment_shapes(recipe, parcel)

				max_reached = max(levels)
				shape_info = building_shape(built_shapes, whole_shapes, levels, parcel.is_corner, parcel.corner_direction, parcel.medianera)
				if levels.max() == max_reached and levels.max() >= recipe.floors*recipe.floor_height and validity:
					shapes_success = True
				shapes_trials += 1

			success = True
		except Exception as e:
			print(e)
			success = False
			count   += 1
	return shape_info

def generate_structure(recipe, shape_info, lod = 0):
	#Three lod, 0: maximum detail. 1: rough shapes (walls and bricks, etc.) and windows, 2: only rough shapes
	optimize_facades = True
	high_poly = True if lod == 0 else False

	shape_info.determine_facets()
	process_facets(shape_info)
	classify_appartment_facets(shape_info, recipe)

	building_mesh  = create_mesh(shape_info, recipe, high_poly)
	roof_mesh      = generate_roof(shape_info, recipe)

	mesh = building_mesh + roof_mesh

	tools.modulo.optimize_mesh(mesh)
	if lod < 2:
		facades = generate_appartment_facades(shape_info, recipe)
		doors   = generate_appartment_doors(shape_info, recipe)

		if optimize_facades:
			facades_mesh   = generate_facade_mesh(facades, high_poly)
			facades_mesh  += generate_facade_mesh(doors, high_poly)
		else:
			facades_mesh   = generate_facade_mesh2(shape_info)

		medianera_mesh = generate_medianera(shape_info)
		terraces_mesh  = generate_terraces(shape_info, recipe)

		mesh += medianera_mesh + facades_mesh + terraces_mesh

		if lod == 0:
			object_meshes, things, trees = populate_facets(shape_info, recipe)
			objects_mesh   = generate_objects_mesh(things)


			mesh += object_meshes + objects_mesh
	return mesh

def generate_appartment(recipe, parcel, only_meshes = False):
	global timer1, timer2, timer3, timer4, timer5, timer6, timer7, timer8, timer9, timer10, app_count
	print("Generating " + recipe.kind + " appartment")
	building = building_collection()

	success, count = False, 0
	while(success == False and count < 10):
		# if True:
		try:
			time1 = time.time()
			shapes_success, shapes_trials = False, 0
			while(shapes_success == False and shapes_trials < 20):
				building = building_collection()

				whole_shapes, built_shapes, levels, validity = build_appartment_shapes(recipe, parcel)

				max_reached = max(levels)
				shape_info = building_shape(built_shapes, whole_shapes, levels, parcel.is_corner, parcel.corner_direction, parcel.medianera)
				if levels.max() == max_reached and levels.max() >= recipe.floors*recipe.floor_height and validity:
					shapes_success = True
				shapes_trials += 1

			print("Trials: " + str(shapes_trials))
			if shapes_success == False:
				print("Couldn't generate")
			if shapes_success:
				pass
				# print("Yay, " + str(count) + " trials")

			time2 = time.time()

			shape_info.determine_facets()
			time3 = time.time()

			process_facets(shape_info)
			time4 = time.time()

			classify_appartment_facets(shape_info, recipe)
			time5 = time.time()

			if DEBUG:
				plot_hfacets(shape_info)
			medianera_mesh = generate_medianera(shape_info)
			time6 = time.time()
			building_mesh  = create_mesh(shape_info, recipe)
			time7 = time.time()
			terraces_mesh  = generate_terraces(shape_info, recipe)
			time8 = time.time()
			roof_mesh      = generate_roof(shape_info, recipe)
			time9 = time.time()

			object_meshes, things, trees = populate_facets(shape_info, recipe)
			time10 = time.time()

			building.mesh += building_mesh + medianera_mesh + terraces_mesh + roof_mesh + object_meshes
			time11 = time.time()

			timer1 += time2  - time1
			timer2 += time3  - time2
			timer3 += time4  - time3
			timer4 += time5  - time4
			timer5 += time6  - time5
			timer6 += time7  - time6
			timer7 += time8  - time7
			timer8 += time9  - time8
			timer9 += time10 - time9
			timer10+= time11 - time10

			if only_meshes == False:
				facades = generate_appartment_facades(shape_info, recipe)
				doors   = generate_appartment_doors(shape_info, recipe)

				if optimize_facades:
					facades_mesh   = generate_facade_mesh(facades)
					facades_mesh  += generate_facade_mesh(doors)
				else:
					facades_mesh   = generate_facade_mesh2(shape_info)

				objects_mesh   = generate_objects_mesh(things)

				building.mesh += objects_mesh + facades_mesh

				building.facades.merge(doors)
				building.facades.merge(facades)
				building.objects.merge(things)
				building.trees.merge(trees)
			success = True
		except Exception as e:
		# else:
			print(e)
			success = False
			count   += 1
	return building

def print_perf():
	print("Time shape calculation: ", timer1)
	print("Facet determination   : ", timer2)
	print("Facet processing      : ", timer3)
	print("Facet classification  : ", timer4)
	print("Medianera generation  : ", timer5)
	print("Building mesh         : ", timer6)
	print("Terraza mesh          : ", timer7)
	print("Techo generation      : ", timer8)
	print("Facet population      : ", timer9)
	print("Mesh combination      : ", timer10)

def generate_appartment1(parcel):
	# Simple C-shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 25, floors, sigma_floors))
	size        = tools.suggest_value(20, 40, 25, 5, 0.2)
	yard_kind   = tools.rand_pick(["left", "right"])
	yard_width  = tools.suggest_value(width/3., width/2., width/2., 3, 0.25)
	yard_length = tools.suggest_value(size/3.,  0.5*size, 0.3*size, 4, 0.25)
	yard_offset = tools.suggest_value(0., 3., 0., 4, 0.5) + size/2.
	nrep        = 2
	properties  = appartment_recipe("type1", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties

def generate_appartment2(parcel):
	# Simple I-shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 25, floors, sigma_floors))
	size        = tools.suggest_value(20, 40, 30, 5, 0.2)
	yard_kind   = "I"
	
	min_width   = (width - 3)/2.
	yard_width  = tools.suggest_value(width/4., min(min_width, width/3.), width/3., 3, 0.1)
	yard_length = tools.suggest_value(size/3., 0.3*size, 0.3*size, 4, 0.25)
	yard_offset = tools.suggest_value(0., 3., 0., 4, 0.5) + size/2.
	nrep        = 2
	properties  = appartment_recipe("type2", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties

def generate_appartment3(parcel):
	# Simple squared shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 35, floors, sigma_floors))
	size        = tools.suggest_value(20, 25, 20, 5, 0.2)
	yard_kind   = "I"
	yard_width  = yard_length = yard_offset = nrep = 0
	nrep        = 0
	properties  = appartment_recipe("type3", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties

def generate_appartment4(parcel):
	# Simple inverted c-shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 25, floors, sigma_floors))
	size        = tools.suggest_value(20, min(30, parcel.length), 25, 5, 0.2)
	#size        = 
	yard_kind   = "left"
	yard_width  = tools.suggest_value(size/3., size/2., size/3., 3, 0.25)
	yard_length = tools.suggest_value(width/3., 0.4*width, 0.4*width, 4, 0.25)
	yard_offset = size + width/2.

	nrep        = 2
	properties  = appartment_recipe("type4", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties

def generate_appartment5(parcel):
	# Repeating left/right pattern building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 13, floors, sigma_floors))
	yard_width  = tools.suggest_value(3., (width - 3.)/2., width/3., 3, 0.25)
	yard_length = tools.suggest_value(4., 6., 3., 2, 0.25)
	yard_sep    = tools.suggest_value(yard_length*2.5, 4.*yard_length, yard_length*2.5, 6, 0.25) 
	yard_offset = tools.suggest_value(yard_sep, yard_sep*1.5, yard_sep, 4, 0.5) + yard_length/2.

	nrep        = max(min(int((parcel.length - yard_offset)/(yard_length + yard_sep)), 4), 1) + 1

	size        = min(min(parcel.length, yard_offset + (yard_length + yard_sep)*nrep), 40.)
	print("type5 appartment, size: " +str(size))
	yard_kind   = tools.rand_pick(["left", "right"])
	properties  = appartment_recipe("type5", parcel, nfloors, size, yard_kind, yard_width, yard_length, yard_sep, yard_offset, nrep)
	return properties

def generate_appartment6(parcel):
	# Repeating I pattern building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 13, floors, sigma_floors))
	yard_width  = tools.suggest_value(2., (width - 3.)/2., width/3., 3, 0.25)
	yard_length = tools.suggest_value(3.5, 6., 3., 2, 0.25)
	yard_sep    = tools.suggest_value(yard_length*2.5, 4*yard_length, yard_length*2.5, 3, 0.25) 
	yard_offset = tools.suggest_value(yard_sep, yard_sep*1.5, yard_sep, 4, 0.5) + yard_length/2.

	nrep        = max(min(int((parcel.length - yard_offset)/(yard_length + yard_sep)), 4), 1)

	size        = min(min(parcel.length, yard_offset + (yard_length + yard_sep)*nrep), 40.)
	print("type6 appartment, size: " +str(size))

	yard_kind   = tools.rand_pick(["I"])
	properties  = appartment_recipe("type6", parcel, nfloors, size, yard_kind, yard_width, yard_length, yard_sep, yard_offset, nrep)
	return properties


def generate_office1(parcel):
	# Simple C-shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 40, floors, sigma_floors))
	size        = tools.suggest_value(20, 40, 25, 5, 0.2)
	yard_kind   = tools.rand_pick(["left", "right"])
	
	yard_width  = tools.suggest_value(width/3., min(width/2., width - 6.), width/2., 3, 0.25)
	yard_length = tools.suggest_value(size/3.,  0.5*size, 0.3*size, 4, 0.25)
	yard_offset = tools.suggest_value(0., 3., 0., 4, 0.5) + size/2.
	nrep        = 2
	properties  = appartment_recipe("office1", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties

def generate_office2(parcel):
	# Simple squared shaped building
	floors, sigma_floors = parcel.properties[2], parcel.properties[3]*parcel.properties[2]
	width       = parcel.avg_width
	nfloors     = int(tools.suggest_value(4, 40, floors, sigma_floors))
	size        = tools.suggest_value(20, min(30, parcel.length - 4), 20, 5, 0.2)
	yard_kind   = "None"
	yard_width  = 0.
	yard_length = 0.
	yard_offset = 0.
	nrep        = 1
	properties  = appartment_recipe("office2", parcel, nfloors, size, yard_kind, yard_width, yard_length, 0., yard_offset, nrep)
	return properties



def gauss(x, std):
	return np.exp(-(x**2)/(2*std**2))/np.sqrt(std)

#def parcel_profile()
def building_probability(parcel):
	probs = np.zeros(8)
	functions = [generate_appartment1, generate_appartment2, generate_appartment3, generate_appartment4, generate_appartment5, generate_appartment6, generate_office1, generate_office2]
	w_mean, w_std = [9., 13., 4., 9., 8., 11., 15., 15.], [3., 3., 3., 3., 3., 3., 3., 3.]
	l_mean, l_std = [30., 30., 10., 20., 40., 40., 30., 20.], [10., 10., 15., 20., 10., 10., 10., 7.]
	multiplier    = [1., 1., 0., 0.0, 1.0, 1.0, .2, 0.0]
	
	for i in range(len(probs)):
		probs[i] = gauss(w_mean[i] - parcel.width, w_std[i])*gauss(l_mean[i] - parcel.length, l_std[i])*multiplier[i]

	return functions[tools.weigh_decision(probs)]

def manage_buildings(parcel):
	if parcel.is_corner:
		function = generate_appartment4
	elif parcel.depth == 0:
		function = generate_appartment4
	else:
		function = building_probability(parcel)

	return function(parcel)


def determine_closest(shape1, shape2):
	return (shape1.exterior).distance(shape2.exterior)


def transform_to_linestring(shape):
	rings = tools.get_rings(shape)
	
	result = []
	for ring in rings:
		result.append(tools.LineString(ring))
	return tools.MultiLineString(result)



		
	


