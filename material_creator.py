import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import modulo
plt.ion()

#path = "/run/media/agustin/Projects/unity-game/Assets/Materials/Textures/"
#path = "/home/agustin/unity-game/Assets/Materials/Textures/"
path = "/home/agustin/unity-game_HDRP/Assets/Materials/Textures/"
path = "textures/"

class material:
	def __init__(self, name, heightmap, albedo, metallic, extent, calculate_AO = False):
		self.name      = name
		self.heightmap = heightmap
		self.albedo    = albedo
		self.metallic = metallic
		self.extent    = extent
		self.normalmap = det_normals(heightmap, extent)
		self.calculate_AO = calculate_AO
		if calculate_AO:
			AO = modulo.estimate_ambient_occlusion(heightmap, extent, max_dist = "fraction")
			#self.ambient_occlusion = np.zeros(self.normalmap.shape, dtype = np.float32)
			self.metallic[:, :, 1] = AO
			
			#self.ambient_occlusion = modulo.estimate_ambient_occlusion(heightmap, extent)

	def show(self):
		fig, axs = plt.subplots(ncols = 4, nrows = 1)
		axs[0].imshow(self.heightmap.T, origin = "lower", extent = self.extent)
		axs[1].imshow(self.normalmap[:, :, 0].T, origin = "lower", extent = self.extent)
		axs[2].imshow(self.normalmap[:, :, 1].T, origin = "lower", extent = self.extent)
		axs[3].imshow(self.normalmap[:, :, 2].T, origin = "lower", extent = self.extent)

	def save(self):

		plt.imsave(path + self.name + "_metallic.png",  self.metallic)
		plt.imsave(path + self.name + "_albedo.png",    self.albedo)
		plt.imsave(path + self.name + "_height.png",    self.heightmap, cmap="gray")
		plt.imsave(path + self.name + "_normal.png",    0.5*(self.normalmap + 1.))
		#if self.calculate_AO:
			#plt.imsave(path + self.name + "_AO.png",    self.ambient_occlusion)
			

class optimized_material:
	def __init__(self, name, heightmap, darkness, metallic, shinnyness):
		size = heightmap.shape
		information = np.zeros([size[0], size[1], 3], dtype = np.float32)

		self.name      = name
		information[:, :, 0] = heightmap
		information[:, :, 1] = metallic
		information[:, :, 2] = darkness
		information[:, :, 3] = shinnyness

		self.information = information

		self.normalmap = det_normals(heightmap, extent)

	def save(self):
		#path = "/run/media/agustin/Projects/Materials/Assets/Textures/"
		path = "/home/agustin/unity-game_HDRP/Assets/Materials/Textures/"

		plt.imsave(path + self.name + "_information.png",  self.information)
		plt.imsave(path + self.name + "_normal.png",    0.5*(self.normalmap + 1.))


def det_normals(heights, extent):
	N, M = len(heights), len(heights[0])
	dx, dy = (extent[1] - extent[0])/N, (extent[3] - extent[2])/M
	grad = np.gradient(heights)
	
	vectors = np.zeros([N, M, 3])
	normals = np.zeros([N, M, 3])
	
	vectors[:,:,0] = -grad[1]/dx
	vectors[:,:,1] = grad[0]/dy
	vectors[:,:,2] = 1.
	
	norm = np.linalg.norm(vectors, axis = 2)
	for i in range(3): normals[:,:,i] = vectors[:,:,i]/norm
	
	return normals

def multilayer_noise(size, resolution, scales, amplitudes, cutoff = 0.0):
	result = np.zeros([resolution, resolution], dtype = np.float32)
	for i in range(len(scales)):
		seed = np.random.randint(1000)
		if scales[i] == 0.001:
			result += np.random.random([resolution, resolution])*0.5*amplitudes[i]
		else:
			result += modulo.perlin2D(size, scales[i], resolution)*amplitudes[i]
	if cutoff != 0.0:
		result[result <= np.percentile(result, 100 - cutoff)] = np.percentile(result, 100 - cutoff)
	result = (result - result.min())/(result.max() - result.min())
	return result

def generate_tile_normal(tolerance):
	theta = np.random.random()*tolerance
	phi   = np.random.random()*2*np.pi
	vector = np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])
	return vector





def create_poor_bricks1():
	M, size = 1024, [2., 2.]
	N1, N2 = 9, 28
	ratio = N2/N1
	tile_size = size[0]/N1
	border_size = tile_size*0.06

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M
	offset     = int(tile_size/2./dx)

	offset = np.zeros([N1, N2], dtype = np.int32)
	sigma1, sigma2  = 0.04, 0.005
	for m in range(N2):
		row_displacement = np.random.random()*sigma1 - sigma1/2.
		if m%2 == 0:
			row_displacement += tile_size/2.

		for n in range(N1):
			brick_displacement = np.random.random()*sigma2 - sigma2/2.
			displacement = brick_displacement + row_displacement
			offset[n, m]  = int(displacement/dx)

	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j

			n, m = int(x/tile_size), int(y*ratio/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size/ratio

			if eta >= border_size and sita >= border_size:
				z = 0.01

			else:
				z = -0.01

			heightmap[j, (i + offset[n, m])%M] = z

	image = gaussian_filter(heightmap, 0.005/dx, mode = "wrap")

	noise =  multilayer_noise(size, M, [0.06, 0.02, 0.008, 0.001], [1., 1.5, 1.5, 1.5])
	
	heightmap = image + noise*0.01

	mask_bricks = heightmap >= 0.005
	heightmap   *= 0.

	holes  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.], cutoff = 30.)
	bumps  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 1.5], cutoff = 5.)
	
	noise1 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	noise2 =  multilayer_noise(size, M, [0.02, 0.01, 0.001], [1., 1., 0.5])
	heightmap[ mask_bricks] = (heightmap - holes*0.005 + noise1*0.001 + bumps*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap + noise1*0.005 + bumps*0.005)[~mask_bricks]

	##Noise Albedo
	mask_noise = (~mask_bricks)|(bumps > 0.)
	albedo[ mask_bricks] = np.array([0.8, 0.45, 0.25, 1.])
	albedo[mask_noise] = np.array([ 1.0, 1.0, 1.0, 1.])
	albedo[:, :, 3]    = multilayer_noise(size, M, [0.2, 0.1, 0.05, 0.02], [4., 3., 2., 1.])
	noise1 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.5], cutoff = 50.)*0.4
	noise2 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.0])*0.5
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		
		albedo[mask_noise, i] = albedo[mask_noise, i]*(1. - noise2)[mask_noise]

	return heightmap, albedo, metallic


if False:
	height, albedo, metallic = create_poor_bricks1()
	bricks = material("poor_bricks1", height, albedo, metallic, [0., 2., 0., 2.], True)
	bricks.save()



def create_tiles_heighmap(disalignment):
	M, size, N = 1024, [2., 2.], 10
	tile_size = size[0]/N

	heightmap = np.zeros([M, M], dtype = np.float32)
	albedo    = np.zeros([M, M, 4], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	border_size = tile_size*0.02
	params = np.zeros([N, N, 4], dtype = np.float32)
	for i in range(N):
		for j in range(N):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size*(j + 0.5)
			darkness = 1. - np.random.random()*0.1
			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = darkness
	
	dx         = size[0]/M
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(x/tile_size), int(y/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				
				z = a*x + b*y + c

				darkness = params[n, m, 3]
				#albedo[i, j] = np.array([0.60*darkness, 0.2*darkness, 0.00*darkness, 1.])
				
			else:
				z = -0.005

			heightmap[i, j] = z

	
	mask_tiles = heightmap != -0.005
	

	##Smoothness
	metallic[ mask_tiles, 3] = 0.6
	metallic[~mask_tiles, 3] = 0.0

	##Metallic
	metallic[ mask_tiles, 0] = 0.0
	metallic[~mask_tiles, 0] = 0.0


	##Noise Smoothness
	noise =  multilayer_noise(size, M, [0.03, 0.01, 0.001], [1., 1., 1.], cutoff = 20)

	metallic[ mask_tiles, 3] = (0.1*noise + 0.6*(1. - noise))[mask_tiles]
	
	##Noise Bump
	noise =  multilayer_noise(size, M, [0.01, 0.008, 0.001], [1., 1., 1.])
	heightmap += noise*0.0005

	noise_large =  multilayer_noise(size, M, [0.2], [1.])
	
	albedo[mask_tiles, 0] = 1.
	albedo[:, :, 1]         = noise_large
	albedo[:, :, 2]         = noise
	
	albedo[:, :, 3]         = (heightmap - heightmap.min())/(heightmap.max() - heightmap.min())
	
	return heightmap, albedo, metallic


height, albedo, metallic = create_tiles_heighmap(10)
tiles = material("tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()






def create_wooden_tiles(disalignment):
	M, size, N1, N2 = 1024, [2., 2.], 2, 50
	sizeX, sizeY = size[0]/N1, size[1]/N2

	heightmap = np.zeros([M, M], dtype = np.float32)
	albedo    = np.zeros([M, M, 4], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	borderX, borderY = sizeX*0.000, sizeY*0.1
	params = np.zeros([N1, N2, 4], dtype = np.float32)
	for i in range(N1):
		for j in range(N2):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*sizeX*(i + 0.5) - b*sizeY*(j + 0.5)
			darkness = 1. - np.random.random()*0.1
			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = darkness

	dx         = size[0]/M
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j

			n, m = int(x/sizeX), int(y/sizeY)
			eta, sita = x - n*sizeX, y - m*sizeY
			if eta >= borderX and sita >= borderY:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]

				z = a*x + b*y + c

				darkness = params[n, m, 3]
				albedo[i, j] = np.array([darkness, darkness, darkness, 1.])

			else:
				z = -0.005

			heightmap[i, j] = z


	mask_tiles = heightmap != -0.005


	##Smoothness
	metallic[ mask_tiles, 3] = 0.5
	metallic[~mask_tiles, 3] = 0.0

	##Metallic
	metallic[ mask_tiles, 0] = 0.0
	metallic[~mask_tiles, 0] = 0.0


	##Noise Smoothness
	noise =  multilayer_noise(size, M, [0.3, 0.1, 0.03, 0.01], [4., 3., 2., 1.], cutoff = 50)

	metallic[ mask_tiles, 3] = (0.1*noise + 0.6*(1. - noise))[mask_tiles]

	##Noise Bump
	noise =  multilayer_noise(size, M, [0.01, 0.008, 0.001], [1., 1., 1.])
	heightmap += noise*0.0005

	noise_large =  multilayer_noise(size, M, [0.2], [1.])

	#albedo[mask_tiles, 0] = 1.
	#albedo[:, :, 1]         = noise_large
	#albedo[:, :, 2]         = noise

	#albedo[:, :, 3]         = (heightmap - heightmap.min())/(heightmap.max() - heightmap.min())

	return heightmap, albedo, metallic


height, albedo, metallic = create_wooden_tiles(5)
tiles = material("wooden_tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()


def create_chess_tiles(disalignment):
	M, size, N = 1024, [2., 2.], 10
	tile_size = size[0]/N

	heightmap = np.zeros([M, M], dtype = np.float32)
	albedo    = np.zeros([M, M, 4], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	border_size = tile_size*0.02
	params = np.zeros([N, N, 4], dtype = np.float32)
	for i in range(N):
		for j in range(N):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size*(j + 0.5)
			darkness = 1. - np.random.random()*0.1
			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = ((i + j)%2)*0.9 + 0.1
	
	dx         = size[0]/M
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(x/tile_size), int(y/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				
				z = a*x + b*y + c

				darkness = params[n, m, 3]
				albedo[i, j] = darkness*np.array([0.75, 0.75, 0.75, 1.])
				
			else:
				z = -0.005
				albedo[i, j] = np.array([0.9, 0.9, 0.9, 1.])

			heightmap[i, j] = z

	
	mask_tiles = heightmap != -0.005
	
	heightmap[~mask_tiles] = heightmap[mask_tiles].mean() - 0.01

	heightmap = gaussian_filter(heightmap, 0.001/dx, mode = "wrap")
	
	##Smoothness
	metallic[ mask_tiles, 3] = 0.4
	metallic[~mask_tiles, 3] = 0.0

	##Noise Smoothness
	noise =  multilayer_noise(size, M, [0.03, 0.01, 0.001], [1., 1., 1.], cutoff = 20)

	metallic[ mask_tiles, 3] = (0.1*noise + 0.4*(1. - noise))[mask_tiles]

	for i in range(3): albedo[:, :, i] = albedo[:, :, i]*(1. - noise*0.1)
	albedo[:, :, 3] = 1.0

	##Noise Bump
	noise =  multilayer_noise(size, M, [0.01, 0.008, 0.001], [1., 1., 1.])
	heightmap += noise*0.0005

	return heightmap, albedo, metallic


height, albedo, metallic = create_chess_tiles(10.)
tiles = material("chess_tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()


def create_irregularities():
	N, size = 1024, [20., 20.]
	noise =  multilayer_noise(size, N, [4., 2.0, 1.0, 0.5], [1., 3., 3., 1.])
	heightmap = noise*0.1
	normalmap = det_normals(heightmap, [0., size[0], 0., size[1]])
	plt.imsave(path + "floor_irregularities_normal.png",    0.5*(normalmap + 1.))

create_irregularities()

def create_plastic_roof():
	M, size = 1024, [2., 2.]
	heightmap = np.zeros([M, M], dtype = np.float32)

	x_space = np.linspace(0., 1., M + 1)[0:M]

	X, Y = np.meshgrid(x_space, x_space)

	heightmap = np.sin(Y*2*np.pi/0.1)*0.025
	
	#noise =  modulo.perlin([1., 1.], 0.01, 1./M, seed = 20)*1.5 + modulo.perlin([1., 1.], 0.004, 1./M, seed = 10) + np.random.random([M, M])*0.5 - 0.25
	noise =  multilayer_noise(size, M, [0.01, 0.004, 0.001], [1.5, 1., 0.5], cutoff = 40.)
	#noise = (noise - noise.min())/(noise.max() - noise.min())
	
	heightmap += noise*0.005

	albedo = np.ones([M, M, 4], dtype = np.float32)
	albedo[: , :]  = np.array([ 0.66,  0.66,  0.4, 1.])
	
	###Noise Albedo
	#noise =  modulo.perlin([1., 1.], 0.1, 1./M, seed = 20)*1.5 + modulo.perlin([1., 1.], 0.03, 1./M, seed = 10)  + modulo.perlin([1., 1.], 0.01, 1./M, seed = 10) + np.random.random([M, M])*0.5 - 0.25
	noise =  multilayer_noise(size, M, [0.1, 0.03, 0.01, 0.001], [1.5, 1., 1., 0.5], cutoff = 40.)
	#noise[noise <= np.percentile(noise, 40.)] = np.percentile(noise, 40.)
	#noise = (noise - noise.min())/(noise.max() - noise.min())

	color_noise = noise*0.3
	for i in range(3):
		albedo[:, :, i] = albedo[:, :, i]*(1. - color_noise)
	
	
	###Smoothness
	metallic = np.zeros([M, M, 4], dtype = np.float32)
	metallic[:, :, 3] = (0.0*color_noise + 0.4*(1. - color_noise))

	###Metallic
	metallic[:, 0] = 0.0
	
	return heightmap, albedo, metallic

def create_rusty_roof():
	M, size = 1024, [2., 2.]
	heightmap = np.zeros([M, M], dtype = np.float32)

	x_space = np.linspace(0., size[0], M, endpoint = False)

	X, Y = np.meshgrid(x_space, x_space)

	heightmap = np.sin(Y*2*np.pi/0.1)*0.015
	
	noise =  multilayer_noise(size, M, [0.01, 0.008, 0.001], [.5, 1., 2.])
	
	heightmap += noise*0.001

	##Noise Albedo
	noise =  multilayer_noise(size, M, [0.5, 0.1, 0.03, 0.008], [1.5, 1., 1., 1.], cutoff = 40.)

	rust_color = np.array([0.32, 0.16, 0.16])
	zinc_color = np.array([0.54, 0.54, 0.54])

	albedo = np.ones([M, M, 4], dtype = np.float32)
	for i in range(3):
		albedo[:, :, i] = noise*rust_color[i] + zinc_color[i]*(1. - noise)
	
	##Smoothness
	metallic = np.zeros([M, M, 4], dtype = np.float32)
	metallic[:, :, 3] = (0.0*noise + 0.56*(1. - noise))
	metallic[:, :, 0] = (0.0*noise + 1.0*(1. - noise))

	heightmap += noise*0.005
	
	return heightmap, albedo, metallic


#def create_clean_roof():
	#M = 1000
	#heightmap = np.zeros([M, M], dtype = np.float32)

	#x_space = np.linspace(0., 2., M + 1)[0:M]

	#X, Y = np.meshgrid(x_space, x_space)

	#heightmap = np.sin(Y*2*np.pi/0.1)*0.015
	
	#noise =  modulo.perlin([2., 2.], 0.01, 2./M, seed = 20)*1.5 + modulo.perlin([2., 2.], 0.008, 2./M, seed = 10) + np.random.random([M, M])*0.5 - 0.25
	#noise = (noise - noise.min())/(noise.max() - noise.min())
	
	#heightmap += noise*0.002

	
	###Noise Albedo
	#noise =  modulo.perlin([2., 2.], 0.5, 2./M, seed = 20)*1.5 + modulo.perlin([2., 2.], 0.1, 2./M, seed = 10) + modulo.perlin([2., 2.], 0.03, 2./M, seed = 10) + modulo.perlin([2., 2.], 0.008, 2./M, seed = 10)
	#noise[noise <= np.percentile(noise, 90.)] = np.percentile(noise, 90.)
	#noise = (noise - noise.min())/(noise.max() - noise.min())

	#rust_color = np.array([0.32, 0.16, 0.16])
	#zinc_color = np.array([0.54, 0.54, 0.54])

	#albedo = np.ones([M, M, 4], dtype = np.float32)
	#for i in range(3):
		#albedo[:, :, i] = noise*rust_color[i] + zinc_color[i]*(1. - noise)
	
	
	###Smoothness
	#metallic = np.zeros([M, M, 4], dtype = np.float32)
	#metallic[:, :, 3] = (0.0*noise + 0.56*(1. - noise))
	#metallic[:, :, 0] = (0.0*noise + 1.0*(1. - noise))

	#heightmap += noise*0.005
	
	#return heightmap, albedo, metallic


#def create_blind_material():
	#M, size = 128, [1., 1.]

	#heightmap = np.zeros([M, M], dtype = np.float32)

	#x_space = np.linspace(0., size[0], M, endpoint = False)

	#X, Y = np.meshgrid(x_space, x_space)

	#heightmap = (1. - np.sin(Y*2*np.pi/0.1)**40)*0.01
	
	#noise =  multilayer_noise(size, M, [0.01, 0.001], [0.1, 0.5])
	
	#heightmap += noise*0.0005

	###Noise Albedo
	#noise =  multilayer_noise(size, M, [0.03, 0.01, 0.001], [1., 1., 1.5], cutoff = 10.)

	#dirty_color = np.array([0.8, 0.8, 0.8])
	#clean_color = np.array([1.0, 1.0, 1.0])

	#albedo = np.ones([M, M, 4], dtype = np.float32)
	#for i in range(3):
		#albedo[:, :, i] = noise*dirty_color[i] + clean_color[i]*(1. - noise)
	
	###Smoothness
	#metallic = np.zeros([M, M, 4], dtype = np.float32)
	#metallic[:, :, 3] = (0.2*noise + 0.5*(1. - noise))
	#metallic[:, :, 0] = 0.

	#heightmap += noise*0.001
	
	#return heightmap, albedo, metallic

def create_curtains_material():
	M, size = 1024, [1., 1.]

	heightmap = np.zeros([M, M], dtype = np.float32)

	x_space = np.linspace(0., size[0], M, endpoint = False)

	X, Y = np.meshgrid(x_space, x_space)

	heightmap = np.sin(X*2*np.pi/0.1)*0.03
	noise =  multilayer_noise(size, M, [0.001], [1.0])
	
	heightmap += noise*0.0005

	##Noise Albedo
	noise =  multilayer_noise(size, M, [0.008, 0.001], [0.1, 1.0])

	dirty_color = np.array([0.8, 0.8, 0.8])
	clean_color = np.array([1.0, 1.0, 1.0])

	albedo = np.ones([M, M, 4], dtype = np.float32)
	for i in range(3):
		albedo[:, :, i] = noise*dirty_color[i] + clean_color[i]*(1. - noise)
	
	albedo[:, int(M/2):M] = 0.0
	
	mask = (heightmap + 0.03)/0.061
	for i in range(3):
		albedo[:, :, i] *= (1 - 0.5*mask)
	###Smoothness
	metallic = np.zeros([M, M, 4], dtype = np.float32)
	metallic[:, :, 3] = 0.4
	metallic[:, :, 0] = 0.

	return heightmap, albedo, metallic


height, albedo, metallic = create_curtains_material()
window = material("curtains", height, albedo, metallic, [0., 1., 0., 1.], True)
window.save()


def create_frame_window(frame_thickness = 0.05):
	M, size = 1024, [1., 1.]
	x_space = np.linspace(0, size[0], M)
	y_space = np.linspace(0, size[0], M)
	X, Y = np.meshgrid(x_space, y_space)
	
	mask = (X <= frame_thickness)|(X >= size[0] - frame_thickness)|(Y <= frame_thickness)|(Y >= size[0] - frame_thickness)
	
	heightmap = np.zeros([M, M])
	heightmap[mask] = 0.01
	albedo   = np.zeros([M, M, 4])
	metallic = np.zeros([M, M, 4])
	#albedo[mask, 3] = 1.0
	albedo[mask, 0:4] = 1.0
	metallic[mask, 0] = 1.0
	metallic[mask, 3] = 0.5
	#metallic[mask, 
	
	return heightmap, albedo, metallic


height, albedo, metallic = create_frame_window()
window = material("frame_window", height, albedo, metallic, [0., 1., 0., 1.], True)
window.save()

def create_bricks_heighmap():
	M, size = 1024, [2., 2.]
	disalignment, N1, N2 = 10., 10, 20
	tile_size = size[0]/N1
	border_size = tile_size*0.05

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	params = np.zeros([N1, N2, 4], dtype = np.float32)
	for i in range(N1):
		for j in range(N2):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size/2.*(j + 0.5)

			darkness = 1. - np.random.random()*0.1

			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = darkness

	dx         = size[0]/M
	offset     = int(tile_size/2./dx)
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(x/tile_size), int(y*2/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size/2.
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				
				z = a*x + b*y + c
				darkness = params[n, m, 3]
				color    = np.array([0.6*darkness, 0.2*darkness, 0.*darkness, 1.])
			else:
				z = -0.01
				color    = np.array([ 0.6,  0.6,  0.6, 1.])
			if m%2 == 0:
				heightmap[j, i] = z
				albedo[j, i]    = color
			else:
				heightmap[j, (i + offset)%M] = z
				albedo[j, (i + offset)%M]    = color

	#heightmap = heightmap.T
	mask_bricks = heightmap != -0.01
	
	##Smoothness
	metallic[ mask_bricks, 3] = 0.4
	metallic[~mask_bricks, 3] = 0.0

	##Metallic
	metallic[ mask_bricks, 0] = 0.0
	metallic[~mask_bricks, 0] = 0.0

	##Noise Bump
	holes =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.], cutoff = 2)
	noise =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1., 1.])
	
	heightmap[ mask_bricks] = (heightmap - holes*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap - noise*0.005)[~mask_bricks]

	##Noise Smoothness
	metallic[ mask_bricks, 3] = (0.1*holes + 0.4*(1. - holes))[mask_bricks]
	
	##Noise Albedo
	noise1 = 0.6*holes + multilayer_noise(size, M, [0.03, 0.01, 0.001], [0.5, 0.7, 2.], cutoff = 30.)*0.3
	noise2 = multilayer_noise(size, M, [0.02, 0.008, 0.001], [1., 1., 1.])*0.3
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		albedo[~mask_bricks, i] = albedo[~mask_bricks, i]*(1. - noise2)[~mask_bricks]

	return heightmap, albedo, metallic

def create_rough_bricks_heighmap():
	M, size = 1024, [2., 2.]
	N1, N2 = 9, 28
	ratio = N2/N1
	tile_size = size[0]/N1
	border_size = tile_size*0.05

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M
	offset     = int(tile_size/2./dx)

	offset = np.zeros([N1, N2], dtype = np.int32)
	sigma1, sigma2  = 0.03, 0.005
	for m in range(N2):
		row_displacement = np.random.random()*sigma1 - sigma1/2.
		if m%2 == 0:
			row_displacement += tile_size/2.

		for n in range(N1):
			brick_displacement = np.random.random()*sigma2 - sigma2/2.
			displacement = brick_displacement + row_displacement
			offset[n, m]  = int(displacement/dx)

	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j

			n, m = int(x/tile_size), int(y*ratio/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size/ratio

			if eta >= border_size and sita >= border_size:
				z = 0.01

			else:
				z = -0.01

			heightmap[j, (i + offset[n, m])%M] = z

	image = gaussian_filter(heightmap, 0.005/dx, mode = "wrap")

	noise =  multilayer_noise(size, M, [0.06, 0.02, 0.008, 0.001], [1., 1.5, 1.5, 1.5])
	
	heightmap = image + noise*0.01

	mask_bricks = heightmap >= 0.005

	#heightmap[mask_bricks] += 0.002
	heightmap[heightmap >= 0.013] = 0.013

	holes  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.], cutoff = 30.)
	noise1 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	noise2 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	heightmap[ mask_bricks] = (heightmap - holes*0.005 + noise1*0.001)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap + noise2*0.005)[~mask_bricks]

	##Smoothness
	metallic[ mask_bricks, 3] = 0.1
	metallic[~mask_bricks, 3] = 0.0

	##Metallic
	metallic[ mask_bricks, 0] = 0.0
	metallic[~mask_bricks, 0] = 0.0

	##Noise Albedo
	albedo[ mask_bricks] = np.array([0.8, 0.4, 0.25, 1.])
	albedo[~mask_bricks] = np.array([ 1.0,  1.0,  1.0, 1.])

	noise1 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.5], cutoff = 60.)*0.6
	noise1[holes != 0.] = 0.
	noise2 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.5])*0.5
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		
		albedo[~mask_bricks, i] = albedo[~mask_bricks, i]*(1. - noise2)[~mask_bricks]

	return heightmap, albedo, metallic


def create_poor_bricks2():
	M, size = 1024, [2., 2.]
	N1, N2 = 9, 18
	ratio = N2/N1
	tile_size = size[0]/N1
	border_size = tile_size*0.06

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M
	offset     = int(tile_size/2./dx)

	offset = np.zeros([N1, N2], dtype = np.int32)
	sigma1, sigma2  = 0.04, 0.005
	for m in range(N2):
		row_displacement = np.random.random()*sigma1 - sigma1/2.
		if m%2 == 0:
			row_displacement += tile_size/2.

		for n in range(N1):
			brick_displacement = np.random.random()*sigma2 - sigma2/2.
			displacement = brick_displacement + row_displacement
			offset[n, m]  = int(displacement/dx)

	bumps_sep = tile_size/12.
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j

			n, m = int(x/tile_size), int(y*ratio/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size/ratio

			if eta >= border_size and sita >= border_size:
				z = 0.01
				P = 2*bumps_sep
				heightmap[j, (i + offset[n, m])%M] = (0.5*(np.sin(2*np.pi*eta/P) + 1.))**50*0.002
			else:
				z = -0.01

			image[j, (i + offset[n, m])%M] = z

	image = gaussian_filter(image, 0.005/dx, mode = "wrap")

	noise =  multilayer_noise(size, M, [0.06, 0.02, 0.008, 0.001], [1., 1.5, 1.5, 1.5])
	
	image = image + noise*0.01

	mask_bricks = image >= 0.005

	holes  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.0], cutoff = 5.)
	bumps  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 1.5], cutoff = 5.)
	
	noise1 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	noise2 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	heightmap[ mask_bricks] = (heightmap - holes*0.005 + noise1*0.0005 + bumps*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap + noise2*0.005 + bumps*0.005)[~mask_bricks]

	##Noise Albedo
	mask_noise = (~mask_bricks)|(bumps > 0.)
	metallic[~mask_noise, 3] = 0.3

	albedo[ mask_bricks] = np.array([0.8, 0.45, 0.25, 1.])
	albedo[mask_noise] = np.array([ 1.0,  1.0,  1.0, 1.])

	noise1 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.5], cutoff = 60.)*0.1
	noise2 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.0])*0.5
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		
		albedo[mask_noise, i] = albedo[mask_noise, i]*(1. - noise2)[mask_noise]

	return heightmap, albedo, metallic




def create_brick_tiles():
	M, size = 1024, [2., 2.]
	disalignment, N1, N2 = 10., 10, 20
	tile_size = size[0]/N1
	border_size = tile_size*0.02

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M], dtype = np.float32)
	pattern   = np.zeros([M, M], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	params = np.zeros([N1, N2, 4], dtype = np.float32)
	for i in range(N1):
		for j in range(N2):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size/2.*(j + 0.5)

			darkness = 1. - np.random.random()*0.1

			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = darkness

	dx         = size[0]/M
	offset     = int(tile_size/2./dx)
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(x/tile_size), int(y*2/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size/2.
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				
				z = a*x + b*y + c
				darkness = params[n, m, 3]
				color    = np.array([0.6*darkness, 0.2*darkness, 0.*darkness, 1.])
			else:
				z = -0.05
				color    = np.array([ 0.6,  0.6,  0.6, 1.])

			if m%2 == 0:
				heightmap[j, i] = z
				albedo[j, i]    = color
				pattern[j, i]   = np.sign(z)
			else:
				heightmap[j, (i + offset)%M] = z
				albedo[j, (i + offset)%M]    = color
				pattern[j, (i + offset)%M]   = np.sign(z)


	heightmap = gaussian_filter(heightmap, 0.001/dx, mode = "wrap")

	mask_bricks = pattern != -0.05

	heightmap[~mask_bricks] = heightmap[mask_bricks].mean()
	
	##Smoothness
	metallic[ mask_bricks, 3] = 0.4
	metallic[~mask_bricks, 3] = 0.0
	

	##Noise Bump
	holes =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.], cutoff = 2)
	noise =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1., 1.])
	
	heightmap[ mask_bricks] = (heightmap - holes*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap - noise*0.005)[~mask_bricks]

	##Noise Smoothness
	metallic[ mask_bricks, 3] = (0.1*holes + 0.4*(1. - holes))[mask_bricks]
	
	##Noise Albedo
	noise1 = 0.6*holes + multilayer_noise(size, M, [0.03, 0.01, 0.001], [0.5, 0.7, 2.], cutoff = 30.)*0.3
	noise2 = multilayer_noise(size, M, [0.02, 0.008, 0.001], [1., 1., 1.])*0.3
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		albedo[~mask_bricks, i] = albedo[~mask_bricks, i]*(1. - noise2)[~mask_bricks]

	return heightmap, albedo, metallic


height, albedo, metallic = create_brick_tiles()
tiles = material("brick_tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()



def rough_brick_tiles():
	M, size = 1024, [2., 2.]
	dx         = size[0]/M

	disalignment, N1, N2 = 10., 10, 20
	tile_size = size[0]/N1
	border_size = tile_size*0.040

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M], dtype = np.float32)
	pattern   = np.zeros([M, M], dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	params = np.zeros([N1, N2, 4], dtype = np.float32)
	for i in range(N1):
		for j in range(N2):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size/2.*(j + 0.5)

			deviation = np.random.random()*tile_size*0.1

			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = deviation

	deviations = np.zeros(N2)
	deviations[::2]  = 0
	deviations[1::2] = tile_size/2.
	deviations += np.random.random(N2)*0.1*tile_size
	deviations = np.int32(deviations/dx)
	
	#offset     = int(tile_size/2./dx)
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(np.floor(x/tile_size)), int(np.floor(y*2/tile_size))
			eta, sita = x - n*tile_size, y - m*tile_size/2.

			offset = deviations[m]
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				z = a*x + b*y + c
				
				value    = 1.0

			else:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				z = a*x + b*y + c

				value = -1.0

			heightmap[j, (i + offset)%M] = z
			pattern[j,   (i + offset)%M] = value


	pattern  += multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1., 1.])*0.1
	pattern   = gaussian_filter(pattern, 0.005/dx, mode = "wrap")

	mask_bricks = pattern > 0.01
	heightmap[~mask_bricks] = heightmap[mask_bricks].mean() - 0.05

	heightmap = gaussian_filter(heightmap, 0.001/dx, mode = "wrap")

	albedo[ mask_bricks] = np.array([0.6, 0.2, 0.0, 1.])
	albedo[~mask_bricks] = np.array([0.6, 0.6, 0.6, 1.])
	
	##Smoothness
	metallic[ mask_bricks, 3] = 0.1
	metallic[~mask_bricks, 3] = 0.1
	
	##Noise Bump
	holes =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.], cutoff = 2)
	noise =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1., 1.])
	
	heightmap[ mask_bricks] = (heightmap - holes*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap - noise*0.005)[~mask_bricks]

	##Noise Smoothness
	#metallic[ mask_bricks, 3] = (0.1*holes + 0.1*(1. - holes))[mask_bricks]
	
	##Noise Albedo
	noise1 = 0.6*holes + multilayer_noise(size, M, [0.03, 0.01, 0.001], [0.5, 0.7, 2.], cutoff = 30.)*0.3
	noise2 = multilayer_noise(size, M, [0.02, 0.008, 0.001], [1., 1., 1.])*0.3
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]
		albedo[~mask_bricks, i] = albedo[~mask_bricks, i]*(1. - noise2)[~mask_bricks]

	return heightmap, albedo, metallic


height, albedo, metallic = rough_brick_tiles()
tiles = material("rough_tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()

plt.imshow(height)


def stripes(x, P, stripe_fraction):
	p = stripe_fraction*P/2
	
	h = abs(np.cos(2*np.pi*p/2./P))
	value = abs(np.sin(2*np.pi*x/P))
	if value >= h:
		return 1.0
	else:
		return 0.0

def create_stripped_tiles(disalignment = 15.):
	M, size, N = 1024, [2., 2.], 10
	tile_size = size[0]/N
	border_size = tile_size*0.03

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)
	mask_bricks = np.zeros([M, M], dtype = np.bool)
	pattern   = np.zeros([M, M],    dtype = np.float32)

	dx         = size[0]/M

	bumps_sep = tile_size/8.
	P = 2*bumps_sep

	params = np.zeros([N, N, 4], dtype = np.float32)
	for i in range(N):
		for j in range(N):
			normal = generate_tile_normal(disalignment*np.pi/180.)
			a, b = normal[0]/normal[2], normal[1]/normal[2]
			c = -a*tile_size*(i + 0.5) - b*tile_size*(j + 0.5)
			darkness = 1. - np.random.random()*0.1
			params[i, j, 0] = a
			params[i, j, 1] = b
			params[i, j, 2] = c
			params[i, j, 3] = darkness
	
	for i in range(M):
		x = dx*i
		for j in range(M):
			y = dx*j
			
			n, m = int(x/tile_size), int(y/tile_size)
			eta, sita = x - n*tile_size, y - m*tile_size
			if eta >= border_size and sita >= border_size:
				a, b, c = params[n, m, 0], params[n, m, 1], params[n, m, 2]
				
				z = a*x + b*y + c - stripes(eta, P, 0.2)*0.005

				pattern[i, j] = stripes(eta, P, 0.2)
				darkness = params[n, m, 3]

				mask_bricks[i, j] = True
			else:
				z = -0.01

			image[i, j] = z

	heightmap = gaussian_filter(image, 0.001/dx, mode = "wrap")

	#noise =  multilayer_noise(size, M, [0.06, 0.02, 0.008, 0.001], [1., 1.5, 1.5, 1.5])
	
	#image = image + noise*0.01

	#mask_bricks = image >= 0.005

	holes  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 2.0], cutoff = 5.)
	bumps  =  multilayer_noise(size, M, [0.02, 0.008, 0.001], [1.5, 1.5, 1.5], cutoff = 5.)
	
	noise1 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])
	noise2 =  multilayer_noise(size, M, [0.008, 0.001], [1., 5.])

	##Noise Albedo
	mask_noise = (~mask_bricks)|(bumps > 0.)
	metallic[pattern == 0, 3]   = 0.45
	metallic[~mask_bricks, 3]   = 0.0
	metallic[mask_noise, 3] = 0.0
	#metallic[mask_bricks, 3] = 0.2

	albedo[ mask_bricks] = np.array([0.7, 0.7, 0.65, 1.])
	albedo[~mask_bricks] = np.array([ 0.6,  0.65, 0.5, 1.])

	noise1 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.5], cutoff = 60.)*0.1
	noise2 = multilayer_noise(size, M, [0.06, 0.03, 0.01, 0.001], [1., 1., 1., 1.0])*0.5
	for i in range(3):
		albedo[ mask_bricks, i] = albedo[ mask_bricks, i]*(1. - noise1)[ mask_bricks]*(2. - pattern[mask_bricks])/2.0
		
		albedo[mask_noise, i] = albedo[mask_noise, i]*(1. - noise2)[mask_noise]

	heightmap[ mask_bricks] = (heightmap - holes*0.005 + noise1*0.0005 + bumps*0.005)[ mask_bricks]
	heightmap[~mask_bricks] = (heightmap + noise2*0.005 + bumps*0.005)[~mask_bricks]

	return heightmap, albedo, metallic


height, albedo, metallic = create_stripped_tiles()
tiles = material("stripped_tiles", height, albedo, metallic, [0., 2., 0., 2.], True)
tiles.save()




def create_dirty_glass():
	M, size = 1024, [2., 2.]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M

	x_space = np.linspace(0., size[0], M)
	X, Y = np.meshgrid(x_space, x_space)
	heightmap = np.sin(X*np.pi/size[0])*np.sin(Y*np.pi/size[0])*0.1

	bumps  =  multilayer_noise(size, M, [0.5, 0.1, 0.05, 0.01], [1., 1., 1., 1.], cutoff = .1)
	
	heightmap += bumps*0.001

	##Noise Albedo
	mask_noise = bumps != 0.
	metallic[:, :,  0] = 0.5
	metallic[mask_noise,  3] = 0.0
	metallic[~mask_noise, 3] = 1.0

	albedo[~mask_noise] = np.array([1.0, 1.0, 1.0, 1.])
	albedo[mask_noise]  = np.array([0.6, 0.6, 0.6, 1.])

	return heightmap, albedo, metallic

# from mayavi import mlab


def create_decal_concrete(kind = "concrete"):
	M, size = 256, [0.1, 0.1]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M

	x_space = y_space = np.linspace(-0.05, 0.05, M)
	X, Y = np.meshgrid(x_space, y_space)
	
	dist = ((X/1)**2 + (Y/1)**2)**0.5
	radius = 0.02
	#heightmap = np.sqrt(0.02**2 - dist**2)*0.6
	heightmap = 0.02 - dist
	heightmap[np.isnan(heightmap)] = 0.
	
	n = 10
	noise         =  multilayer_noise(size, M, np.linspace(0.015, 0.002, 5), np.linspace(1., 0.2, 5), cutoff = 0.)*0.01
	heightmap += noise
	heightmap *= -1
	mask = heightmap < -0.01
	heightmap[mask] += 0.01
	heightmap[~mask] = 0.
	heightmap *= 2
	
	plt.imshow(heightmap, cmap = "Greys")
	
	noise         = multilayer_noise(size, M, np.linspace(0.01, 0.0005, n), np.linspace(0.5, 0.2, n), cutoff = 0.)*0.005
	
	heightmap[mask] += noise[mask]
	heightmap[heightmap >= 0.] = 0.

	mask = heightmap < 0.

	heightmap *= 0.5
	# mlab.surf(x_space, y_space, heightmap)
	#plt.imshow(heightmap, cmap = "Greys")
	

	#noise         = multilayer_noise(size, M, np.linspace(0.01, 0.0012, n), np.linspace(0.5, 0.2, n), cutoff = 0.)*0.01
	

	noise         =  multilayer_noise(size, M, np.linspace(0.01, 0.0012, n), np.linspace(0.5, 1.5, n), cutoff = 0.)
	#plt.imshow(noise, cmap = "Greys")
	
	if kind == "concrete":
		color = [0.88, 0.88, 0.88]
		color = [1.0, 1.0, 1.0]
	elif kind == "brick":
		color = [1.0, 1.0, 1.0]
	for i in range(3):
		albedo[:, :, i] = color[i]*(1 - noise*0.5)
	albedo[~mask, 3] = 0.
	albedo[mask, 3]  = 1.0
	return heightmap, albedo, metallic


def create_decal_dirt():
	M, size = 256, [0.1, 0.1]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M

	x_space = y_space = np.linspace(-0.05, 0.05, M)
	X, Y = np.meshgrid(x_space, y_space)
	
	dist = ((X/1)**2 + (Y/1)**2)**0.5
	radius = 0.015
	
	heightmap = 0.020/np.sqrt(1 + ((dist - radius)/0.01)**2)
	#heightmap[np.isnan(heightmap)] = 0.
	
	n = 10
	noise         =  multilayer_noise(size, M, np.linspace(0.015, 0.002, 5), np.linspace(1., 0.2, 5), cutoff = 0.)*0.01
	heightmap += noise
	#heightmap *= -1
	mask = heightmap >= 0.01
	heightmap[mask] += -0.01
	heightmap[~mask] = 0.
	#heightmap *= 2
	
	plt.imshow(heightmap, cmap = "Greys")
	
	noise         = multilayer_noise(size, M, np.linspace(0.01, 0.0005, n), np.linspace(0.5, 0.2, n), cutoff = 0.)*0.005
	
	heightmap += -noise
	heightmap[heightmap <= 0.] = 0.

	mask = heightmap > 0.

	heightmap *= 0.5
	# mlab.surf(x_space, y_space, heightmap)

	noise         =  multilayer_noise(size, M, np.linspace(0.01, 0.0012, n), np.linspace(0.5, 1.5, n), cutoff = 0.)
	#plt.imshow(noise, cmap = "Greys")
	
	color = [1.0, 1.0, 1.0]
	for i in range(3):
		albedo[:, :, i] = color[i]*(1 - noise*1.0)
	albedo[~mask, 3] = 0.
	albedo[mask, 3]  = 1.0
	return heightmap, albedo, metallic

height, albedo, metallic = create_decal_dirt()
tiles = material("decal_dirt", height, albedo, metallic, [0., 0.1, 0., 0.1], True)
tiles.save()


def create_decal_metal():
	M, size = 256, [0.1, 0.1]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M

	x_space = y_space = np.linspace(-0.05, 0.05, M)
	X, Y = np.meshgrid(x_space, y_space)
	
	dist = (X**2 + Y**2)**0.5
	radius = 0.01
	
	heightmap = 0.01/(1 + (dist/radius)**2)
	heightmap[np.isnan(heightmap)] = 0.
	
	heightmap *= -1
	mask = heightmap < -0.001
	heightmap[mask] += 0.001
	heightmap[~mask] = 0.

	
	plt.imshow(heightmap, cmap = "Greys")
	
	# mlab.surf(x_space, y_space, heightmap)
	#plt.imshow(heightmap, cmap = "Greys")
	
	color = [0.6, 0.6, 0.6]
	for i in range(3):
		albedo[:, :, i] = color[i]
	albedo[~mask, 3] = 0.
	albedo[:, :, 3]  = (radius - dist)/radius
	albedo[albedo[:, :, 3] < 0., 3] = 0.

	metallic[:, :, 0] = 1.
	metallic[:, :, 3] = 0.8

	return heightmap, albedo, metallic

def create_decal_plastic():
	M, size = 1024, [0.1, 0.1]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M

	x_space = y_space = np.linspace(-0.05, 0.05, M)
	X, Y = np.meshgrid(x_space, y_space)
	
	dist = (X**2 + Y**2)**0.5
	radius = 0.007
	
	heightmap = 0.02/(1 + (dist/radius)**2)
	
	depth = 0.02/(1 + (0.003/radius)**2)
	hole = heightmap >= depth
	heightmap[hole] = depth
	
	heightmap *= -1
	mask = heightmap < -0.001
	heightmap[mask] += 0.001
	heightmap[~mask] = 0.

	
	plt.imshow(heightmap, cmap = "Greys")
	
	# mlab.surf(x_space, y_space, heightmap)
	#plt.imshow(heightmap, cmap = "Greys")
	
	color = [1.0, 1.0, 1.0]
	for i in range(3):
		albedo[:, :, i] = color[i]
	albedo[hole, 0:3] = 0.
	albedo[~mask, 3] = 0.
	albedo[:, :, 3]  = (3*radius - dist)/radius/2.

	albedo[albedo[:, :, 3] > 1., 3] = 1.
	albedo[albedo[:, :, 3] < 0., 3] = 0.

	metallic[:, :, 0] = 0.
	metallic[:, :, 3] = 0.7
	metallic[hole, 3] = 0.0
	

	return heightmap, albedo, metallic

height, albedo, metallic = create_decal_plastic()
tiles = material("decal_plastic", height, albedo, metallic, [0., 0.1, 0., 0.1], True)
tiles.save()

height, albedo, metallic = create_decal_metal()
tiles = material("decal_metal", height, albedo, metallic, [0., 0.1, 0., 0.1], True)
tiles.save()


for i in range(4):
	height, albedo, metallic = create_decal_concrete("concrete")
	tiles = material("decal_concrete" + str(i + 1), height, albedo, metallic, [0., 0.1, 0., 0.1], True)
	tiles.save()

#for i in range(1):
	#height, albedo, metallic = create_decal_concrete("brick")
	#tiles = material("decal_brick" + str(i + 1), height, albedo, metallic, [0., 0.1, 0., 0.1])
	#tiles.save()


#AO = modulo.estimate_ambient_occlusion(height, [0., 0.1, 0., 0.1])
#plt.imshow(AO)


def create_dirty_wall():
	from scipy.signal import convolve2d
	M, size = 1024, [20., 20.]

	albedo    = np.ones([M, M, 4], dtype = np.float32)
	heightmap = np.zeros([M, M],    dtype = np.float32)
	image     = np.zeros([M, M],    dtype = np.float32)
	metallic  = np.zeros([M, M, 4], dtype = np.float32)

	dx         = size[0]/M


	noise         =  multilayer_noise(size, M, [4., 2., 1., 0.5], [2., 1.5, 1., 0.5], cutoff = 80.)
	grainy_noise1  =  multilayer_noise([2., 2.], M, [.4, 0.2, 0.1, 0.05, 0.02, 0.01], [1., 1., 1., 1., 1., 1.])
	grainy_noise2  =  multilayer_noise([2., 2.], M, [.4, 0.2, 0.1, 0.05, 0.02, 0.01], [1., 1., 1., 1., 1., 1.])
	soft_noise     =  multilayer_noise([2., 2.], M, [.4, 0.2, 0.1, 0.05, 0.02, 0.01], [128., 64., 32., 16., 8., 4.])
	
	
	linesize = 8.
	line = np.zeros([int(linesize/dx), 1], dtype = np.float32)
	line[:, 0] = np.linspace(0., 2., int(linesize/dx))**2
	line[0:int(linesize/2./dx), 0] = 0.
	line *= 1./np.sum(line)

	moisture = convolve2d(noise, line, mode = "same", boundary = "wrap")

	moisture *= 1./moisture.max()

	albedo[:, :, 0] = moisture
	albedo[:, :, 1] = grainy_noise1
	albedo[:, :, 2] = grainy_noise2
	albedo[:, :, 3] = soft_noise
	
	return heightmap, albedo, metallic


create_irregularities()



height, albedo, metallic = create_dirty_wall()
mat = material("dirty_wall", height, albedo, metallic, [0., 2., 0., 2.], True)
mat.save()



#def create_normal_window1(n = 0):
	#M, size, frame_thickness = 128, [1., 1.], 0.05

	#frame_mask = np.zeros([M, M], dtype = np.float32)
	#albedo    = np.ones([M, M, 4], dtype = np.float32)
	#heightmap = np.zeros([M, M],    dtype = np.float32)
	#image     = np.zeros([M, M],    dtype = np.float32)
	#metallic  = np.zeros([M, M, 4], dtype = np.float32)

	#dx         = size[0]/M

	#x_space = np.linspace(0., size[0], M)
	#X, Y = np.meshgrid(x_space, x_space)

	#col_positions = np.linspace(frame_thickness/2., size[0] - frame_thickness/2., n + 2)
	#row_positions = np.linspace(frame_thickness/2., size[0] - frame_thickness/2., n + 2)
	
	##np.sin()
	#for i in range(n + 2):
		#frame_mask[(abs(X - col_positions[i]) <= frame_thickness/2.)] = 1.
		#frame_mask[(abs(Y - row_positions[i]) <= frame_thickness/2.)] = 1.
	
	#heightmap = gaussian_filter(frame_mask*1.0, 0.01/dx)
	#frame_mask = heightmap >= 0.002

	#heightmap[~frame_mask] = 0.
	#heightmap[frame_mask] += -0.002

	#bumps  =  multilayer_noise(size, M, [0.05, 0.01, 0.001], [1., 1., 1.], cutoff = 5)
	
	#heightmap[~frame_mask] += bumps[~frame_mask]*0.002

	###Noise Albedo
	#mask_noise = bumps != 0.
	#metallic[:, :,  0] = 0.5
	#metallic[mask_noise,  3] = 0.0
	#metallic[~mask_noise, 3] = 1.0

	#albedo[~mask_noise] = np.array([1.0, 1.0, 1.0, 1.])
	#albedo[mask_noise]  = np.array([0.6, 0.6, 0.6, 1.])

	#return heightmap, albedo, metallic

height, albedo, metallic = create_dirty_glass()
bricks = material("dirty_glass", height, albedo, metallic, [0., 2., 0., 2.])
bricks.save()


height, albedo, metallic = create_rough_bricks_heighmap()
bricks = material("rough_bricks", height, albedo, metallic, [0., 2., 0., 2.], True)
bricks.save()

#dx = 2./len(height)
#height_smooth = gaussian_filter(height, 0.002/dx, mode = "wrap")
#laplacian     = laplace(height_smooth, mode = "wrap")
#mask_GI = laplacian > 0.
#laplacian[~mask_GI] = 0.

#albedo_GI = np.copy(albedo)
#for i in range(3):
	#albedo_GI[:,:, i] *= 1. - laplacian*100
#plt.imshow(albedo_GI)


height, albedo, metallic = create_poor_bricks2()
bricks = material("poor_bricks2", height, albedo, metallic, [0., 2., 0., 2.], True)
bricks.save()



height, albedo, metallic = create_bricks_heighmap()
bricks = material("bricks", height, albedo, metallic, [0., 2., 0., 2.], True)
bricks.save()


#height, albedo, metallic = create_curtains_material()
#curtains = material("curtains", height, albedo, metallic, [0., 2., 0., 2.])
#curtains.save()


#height, albedo, metallic = create_blind_material()
#blind = material("blind", height, albedo, metallic, [0., 2., 0., 2.])
#blind.save()


#height, albedo, metallic = create_clean_roof()
#normal_roof = material("normal_roof", height, albedo, metallic, [0., 2., 0., 2.])
#normal_roof.save()


height, albedo, metallic = create_rusty_roof()
rusty_roof = material("rusty_roof", height, albedo, metallic, [0., 2., 0., 2.], True)
rusty_roof.save()


height, albedo, metallic = create_plastic_roof()
plastic_roof = material("plastic_roof", height, albedo, metallic, [0., 2., 0., 2.], True)
plastic_roof.save()


#height, albedo, metallic = create_smooth_bricks_heighmap(0.1, 10, 10)
#bricks = material("smooth_bricks", height, albedo, metallic, [0., 2., 0., 2.])
#bricks.save()



#height, albedo, metallic = create_bricks_heighmap(0.1, 10, 10)
#bricks = material("rough_bricks", height, albedo, metallic, [0., 2., 0., 2.])
#bricks.save()
