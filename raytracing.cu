#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include "helper_cuda.h"

#define BLOCK_SIZE 128
#define npartition (1)
#define N_subcells (2)

__device__ static inline float dot_device(float *A, float *B){
	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

__host__ static inline float dot_host(float *A, float *B){
	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

__device__ float norm(float *vector){
	return sqrtf(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
}

/*
__device__ static inline void normalize(float *A, float *B){
	float norm = sqrtf(A[0]*A[0] + A[1]*A[1] + A[2]*A[2]);
	B[0] = A[0]/norm;
	B[1] = A[1]/norm;
	B[2] = A[2]/norm;
}*/


__host__ static inline void normalize_host(float *A, float *B){
	float norm = sqrtf(A[0]*A[0] + A[1]*A[1] + A[2]*A[2]);
	B[0] = A[0]/norm;
	B[1] = A[1]/norm;
	B[2] = A[2]/norm;
}

__host__ static inline void cross_host(float *A, float *B, float *result){
	result[0] = A[1]*B[2] - A[2]*B[1];
	result[1] = A[2]*B[0] - A[0]*B[2];
	result[2] = A[0]*B[1] - A[1]*B[0];
}


/*
__host__ static inline void cross_device(float *A, float *B, float *result){
	result[0] = A[1]*B[2] - A[2]*B[1];
	result[1] = A[2]*B[0] - A[0]*B[2];
	result[2] = A[0]*B[1] - A[1]*B[0];
}*/

__device__ static inline void substract_device(float *A, float *B, float *C){
	C[0] = A[0] - B[0];
	C[1] = A[1] - B[1];
	C[2] = A[2] - B[2];
}

__host__ static inline void substract_host(float *A, float *B, float *C){
	C[0] = A[0] - B[0];
	C[1] = A[1] - B[1];
	C[2] = A[2] - B[2];
}


__device__ static inline void assign_dim_device(float *A, float *B, unsigned int dim){
	for(unsigned int i = 0; i < dim; i++)
		B[i] = A[i];
}

__host__ static inline void assign_dim_host(float *A, float *B, unsigned int dim){
	for(unsigned int i = 0; i < dim; i++)
		B[i] = A[i];
}


__device__ static inline float sign(float *p1, float *p2, float *p3){
	return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1]);
}


__device__ static inline bool PointInTriangle(float *pt, float *v1, float *v2, float *v3){
	float d1, d2, d3;
	bool has_neg, has_pos;
	d1 = sign(pt, v1, v2);
	d2 = sign(pt, v2, v3);
	d3 = sign(pt, v3, v1);

	has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
	has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

	return !(has_neg && has_pos);
}

__device__ static inline int power(int a, int b){
	int value = 1;
	if(b >= 0){
		for(unsigned int i = 0; i < b; i++)
			value *= a;
	}
	else{
		// printf("Problem a = %d, b = %d!!\n", a, b);
		value = 1;
	}
	return value;
}


static inline int power_host(int a, int b){
	int value = 1;
	if(b >= 0){
		for(unsigned int i = 0; i < b; i++)
			value *= a;
	}
	else{
		// printf("Problem a = %d, b = %d!!\n", a, b);
		value = 1;
	}
	return value;
}



__device__ static inline unsigned int check_isin_box(float *vector, float *extent){
	if((vector[0] >= extent[0])&(vector[0] < extent[1])&(vector[1] >= extent[2])&(vector[1] < extent[3])&(vector[2] >= extent[4])&(vector[2] < extent[5]))
		return 1;
	else
		return 0;
}

__host__ static inline unsigned int check_isin_box_host(float *vector, float *extent){
	if((vector[0] >= extent[0])&(vector[0] < extent[1])&(vector[1] >= extent[2])&(vector[1] < extent[3])&(vector[2] >= extent[4])&(vector[2] < extent[5]))
		return 1;
	else
		return 0;
}
__device__ static inline unsigned int check_isin_square(float *vec, unsigned int idx, float *extent){
	if       ((idx == 0)&(vec[1] >= extent[2])&(vec[1] < extent[3])&(vec[2] >= extent[4])&(vec[2] < extent[5]))
		return 1;
	else if  ((idx == 1)&(vec[0] >= extent[0])&(vec[0] < extent[1])&(vec[2] >= extent[4])&(vec[2] < extent[5]))
		return 1;
	else if  ((idx == 2)&(vec[0] >= extent[0])&(vec[0] < extent[1])&(vec[1] >= extent[2])&(vec[1] < extent[3]))
		return 1;
	else
		return 0;
}


__device__ unsigned int intersect_line_cube(float *start, float *end, float *extent, float *t_min_o, float *t_max_o){
	unsigned int intersects, start_in, end_in, isin;
	float vector[3], t_min, t_max, t, touch_point[3], t_intersect;

	substract_device(&end[0], &start[0], &vector[0]);

	intersects = 0;
	t_min = 1.0f;
	t_max = 0.0f;

	start_in = check_isin_box(start, extent);
	end_in   = check_isin_box(end,   extent);

	// printf("Start in = %d, end in = %d, start = (%f, %f, %f), end = (%f, %f, %f), extent = [%f, %f, %f, %f, %f, %f]\n", start_in, end_in, start[0], start[1], start[2], end[0], end[1], end[2], extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);
	if((start_in == 1)&(end_in == 1)){
		intersects = 1;
		t_min  = 0.f;
		t_max  = 1.0f;
	}
	else{
		for(unsigned int idx = 0; idx < 3; idx++){
			for(unsigned int side = 0; side < 2; side++){
				t = (extent[2*idx + side] - start[idx])/vector[idx];


				if((t >= 0.)&(t <= 1.)){
					touch_point[0] = start[0] + t*vector[0];
					touch_point[1] = start[1] + t*vector[1];
					touch_point[2] = start[2] + t*vector[2];

					// printf("t = %f, touch_point = (%f, %f, %f)\n", t, touch_point[0], touch_point[1], touch_point[2]);
					isin = check_isin_square(&touch_point[0], idx, extent);

					// printf("Comparison done, touch_point = (%f, %f, %f), isin = %d\n", touch_point[0], touch_point[1], touch_point[2], isin);

					if(isin)
						intersects = 1;

					if((isin == 1)&(t < t_min))
						t_min = t;
					if((isin == 1)&(t > t_max))
						t_max = t;
				}
			}
		}
		if((start_in == 1)|(end_in == 1)){
			t_intersect = t_min;
			// if(t_intersect != t_max)
				// print("WARNING", start_in, end_in, t_intersect, t_min, t_max)

			if(start_in == 1){
				intersects = 1;
				t_min  = 0.f;
				t_max  = t_intersect;
			}
			else{
				intersects = 1;
				t_min  = t_intersect;
				t_max  = 1.0f;
			}
		}
	}
	*t_min_o = t_min;
	*t_max_o = t_max;
	return intersects;
}

__device__ void min_argsort(float *array, unsigned int N, unsigned int *output){
	unsigned int i, j, idx_minimum, aux_int, j_min = 0;
	float minimum = INFINITY, aux_float;
	// for(i = 0; i < N; i++)
		// output[i] = i;

	for(i = 0; i < N; i++){
		minimum = INFINITY;
		for(j = i; j < N; j++){
			if(array[j] < minimum){
				j_min       = j;
				minimum     = array[j];
				idx_minimum = output[j];
			}
		}
		aux_float = array[i];
		aux_int   = output[i];

		array[i] = minimum;
		output[i] = idx_minimum;

		array[j_min] = aux_float;
		output[j_min] = aux_int;
	}
}


__device__ static inline void trace_line_triangle(float *vertices, float *base, unsigned int *triangles, unsigned int tri_id, float *start_segment, float *end_segment, int *tri_hit, float *u){
	float vert1[3], normal[3], u0, vector[3], base1[3], base2[3], aux[3], vert2_base2[3], vert3_base2[3], start_base2[3], end_base2[3], touch_point[3], zeros[3] = {0.f, 0.f, 0.f};
	unsigned int nattributes = 6;


	assign_dim_device(&base[3*(nattributes*tri_id + 0)],  &base1[0], 3);
	assign_dim_device(&base[3*(nattributes*tri_id + 1)],  &base2[0], 3);
	assign_dim_device(&base[3*(nattributes*tri_id + 2)], &normal[0], 3);
	assign_dim_device(&base[3*(nattributes*tri_id + 3)],  &vert1[0], 3);




	substract_device(&start_segment[0], &vert1[0], &aux[0]);
	start_base2[0] = dot_device(&aux[0], &base1[0]);
	start_base2[1] = dot_device(&aux[0], &base2[0]);
	start_base2[2] = dot_device(&aux[0], &normal[0]);

	substract_device(&end_segment[0], &vert1[0], &aux[0]);
	end_base2[0] = dot_device(&aux[0], &base1[0]);
	end_base2[1] = dot_device(&aux[0], &base2[0]);
	end_base2[2] = dot_device(&aux[0], &normal[0]);

	if(start_base2[2]*end_base2[2] >= 0.){
		*tri_hit = 0;
		*u       = -2.0f;
	}
	else{
		substract_device(&end_segment[0], &start_segment[0], &vector[0]);

		assign_dim_device(&base[3*(nattributes*tri_id + 4)], &vert2_base2[0], 3);
		assign_dim_device(&base[3*(nattributes*tri_id + 5)], &vert3_base2[0], 3);

		u0 = (0.f - start_base2[2])/(end_base2[2] - start_base2[2]);
		touch_point[0] = start_base2[0] + u0*(end_base2[0] - start_base2[0]);
		touch_point[1] = start_base2[1] + u0*(end_base2[1] - start_base2[1]);
		touch_point[2] = 0.f;

		*tri_hit = PointInTriangle(&touch_point[0], &zeros[0], &vert2_base2[0], &vert3_base2[0]);
		*u       = u0*norm(&vector[0]);
	}
}

__managed__ unsigned int *gentree_cells;
__managed__ unsigned int gentree_max_depth, gentree_ntriang, *gentree_cell_count, *gentree_cell_total_count, *gentree_starting_idx, *gentree_depth_offset, *gentree_triangles;
__managed__ float *gentree_extent, *gentree_cell_extent, *gentree_vertices;
__managed__ float *base;

__managed__ double *list_a;

unsigned int geometric_sum_host(unsigned int nsubcells, unsigned int i){
	return (power_host(nsubcells, i + 1) - 1)/(nsubcells - 1);
}

unsigned int get_idx_cell_host(unsigned int depth, unsigned int cell_id){
	return gentree_depth_offset[depth] + cell_id;
}


__device__ unsigned int geometric_sum(unsigned int nsubcells, unsigned int i){
	return (power(nsubcells, i + 1) - 1)/(nsubcells - 1);
}

__device__ unsigned int get_idx_cell(unsigned int depth, unsigned int cell_id){
	return gentree_depth_offset[depth] + cell_id;
}


void locate_vertex(float *vertex, unsigned int depth, int *coords){
	unsigned int isin, j_ant = 0;
	coords[0] = 0;
	for(unsigned int i = 1; i < depth + 1; i++){
		// N = power(2, 3*i);
		for(unsigned int j = N_subcells*j_ant; j < N_subcells*(j_ant + 1); j++){
			isin = check_isin_box_host(vertex, &gentree_cell_extent[6*get_idx_cell_host(i, j) + 0]);

			if(isin == 1){
				coords[i] = j;
				j_ant     = j;
				break;
			}
			else{
				coords[i] = -1;
			}
		}
		if(isin == 0)
			printf("isin = %d, i = %d, j_ant = %d, coords[i] = %d\n", isin, i, j_ant, coords[i]);
	}
}

void subdivide_cell_along_axis(float *extent, float *new_extent1, float *new_extent2){
	float dx, dy, dz, dmax;
	unsigned int i, done = 0;

	dx = extent[1] - extent[0];
	dy = extent[3] - extent[2];
	dz = extent[5] - extent[4];

	dmax = fmaxf(dx, fmaxf(dy, dz));

	for(i = 0; i < 6; i++){
		new_extent1[i] = extent[i];
		new_extent2[i] = extent[i];
	}

	if(dx == dmax){
		new_extent1[1] = extent[0] + (extent[1] - extent[0])*0.5f;
		new_extent2[0] = extent[0] + (extent[1] - extent[0])*0.5f;
		done = 1;
	}
	else if((dy == dmax)&(done == 0)){
		new_extent1[3] = extent[2] + (extent[3] - extent[2])*0.5f;
		new_extent2[2] = extent[2] + (extent[3] - extent[2])*0.5f;
		done = 1;
	}
	else if((dz == dmax)&(done == 0)){
		new_extent1[5] = extent[4] + (extent[5] - extent[4])*0.5f;
		new_extent2[4] = extent[4] + (extent[5] - extent[4])*0.5f;
		done = 1;
	}
}

void subdivide_cell(float *extent, float *new_extent){
	unsigned int nlist, offset;
	float new_extent1[6], new_extent2[6], list_extent1[6*N_subcells], list_extent2[6*N_subcells];

	nlist  = 1;
	assign_dim_host(&extent[0], &list_extent1[0], 6*nlist);
	for(unsigned int i = 0; i < npartition; i++){
		offset = 0;
		for(unsigned int j = 0; j < nlist; j++){

			subdivide_cell_along_axis(&list_extent1[6*j + 0], &new_extent1[0], &new_extent2[0]);

			assign_dim_host(&new_extent1[0], &list_extent2[6*offset + 0], 6);
			offset += 1;

			assign_dim_host(&new_extent2[0], &list_extent2[6*offset + 0], 6);
			offset += 1;
		}
		assign_dim_host(&list_extent2[0], &list_extent1[0], 6*offset);
		nlist = offset;
	}

	assign_dim_host(&list_extent1[0], &new_extent[0], 6*nlist);
}


unsigned int error_counter = 0;

__device__ void check_subcell_intersection_general(float *start, float *end, unsigned int cell_id, unsigned int depth, unsigned int *arg_sorted, unsigned int *nsubcells, float *t_mins, float *t_maxs){
	unsigned int intersects[N_subcells], count = 0, i, idx, offset, subcell_id;
	float u_min[N_subcells], u_max[N_subcells];


	for(i = 0; i < N_subcells; i++){
		idx = i + cell_id*N_subcells;

		subcell_id = get_idx_cell(depth + 1, idx);

		intersects[i] = intersect_line_cube(start, end, &gentree_cell_extent[6*get_idx_cell(depth + 1, idx) + 0], &u_min[i], &u_max[i]);

		count += intersects[i];
		// printf("i = %d, count = %d, intersects = %d, cell_id = %d, depth = %d, umin = %f, umax = %f\n", i, count, intersects[i], cell_id, depth, u_min[0], u_max[0]);
	}

	float u_min_sort[N_subcells], u_max_sort[N_subcells];
	offset = 0;
	for(i = 0; i < N_subcells; i++){
		if(intersects[i] == 1){
			u_min_sort[offset]  = u_min[i];
			u_max_sort[offset]  = u_max[i];
			arg_sorted[offset]  = i;
			offset += 1;
		}
	}


	min_argsort(&u_min_sort[0], count, &arg_sorted[0]);


	for(i = 0; i < count; i++){
		t_mins[i] = u_min_sort[i];    //Already sorted by min_argsort
		t_maxs[i] = u_max_sort[arg_sorted[i]];
	}
	*nsubcells = count;
}



__device__ void raytrace_recursive2(float *vertices, float *base, unsigned int *triangles, unsigned int depth, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length);

__device__ void raytrace_recursive2(float *vertices, float *base, unsigned int *triangles, unsigned int depth, unsigned int cell_id, float *start_segment, float *end_segment, float *extent, int *hit, float *length){
	printf("To check within subcell, current depth = %d\n", depth);
	float u, u_min = INFINITY;    // In physical units

	unsigned int i, tri_id, subcell_id, N_depth, N, id_x0, id_y0, id_z0, nsubcells = power(2, npartition);
	int current_hit = -1, tri_hit;
	*hit = -1;
	*length = -1.0f;

	if(gentree_cell_total_count[get_idx_cell(depth, cell_id)] != 0){
		// Triangles within cell

		printf("Total tri count within cell = %d, at this level = %d\n", gentree_cell_total_count[get_idx_cell(depth, cell_id)], gentree_cell_count[get_idx_cell(depth, cell_id)]);

		for(i = 0; i < gentree_cell_count[get_idx_cell(depth, cell_id)]; i++){
			// printf("In the loop, i = %d\n", i);
			// printf("to get triangle in loop %d, offset = %d, id last = %d\n", i, gentree_cells[gentree_starting_idx[get_idx_cell(depth, cell_id)]], tri_id);
			tri_id = gentree_cells[gentree_starting_idx[get_idx_cell(depth, cell_id)] + i];
			// printf("Got triangle, offset = %d, id = %d\n", gentree_cells[gentree_starting_idx[get_idx_cell(depth, cell_id)]], tri_id);

			// printf("To raytrace against biggest triangles, id = %d\n", tri_id);
			trace_line_triangle(vertices, base, triangles, tri_id, start_segment, end_segment, &tri_hit, &u);
			if((tri_hit == 1)&(u < u_min)){
				u_min       = u;
				current_hit = tri_id;
				*length = u_min;
				*hit    = current_hit;
			}
		}
		if(depth < gentree_max_depth){
			float t_mins[8], t_maxs[8];
			unsigned int count = 0, arg_sorted[8], idx;

			// check_subcell_intersection(&start_segment[0], &end_segment[0], extent, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);

			printf("check subcell intersection general, start = (%f, %f, %f), end = (%f, %f, %f), cell_id = %d, depth = %d\n", start_segment[0], start_segment[1], start_segment[2], end_segment[0], end_segment[1], end_segment[2], cell_id, depth);
			check_subcell_intersection_general(&start_segment[0], &end_segment[0], cell_id, depth, &arg_sorted[0], &count, &t_mins[0], &t_maxs[0]);
			// printf("Done, start = (%f, %f, %f), end = (%f, %f, %f)\n", start_segment[0], start_segment[1], start_segment[2], end_segment[0], end_segment[1], end_segment[2]);

			printf("To go to deeper subcells\n");
			for(unsigned int k = 0; k < count; k++){
				idx = arg_sorted[k];

				subcell_id = cell_id*nsubcells + idx;

				printf("To raytrace deeper: %d, subcell_id = %d, k = %d, count = %d\n", depth + 1, subcell_id, k, count);
				printf("More info, start = (%f, %f, %f), end = (%f, %f, %f), extent = (%f, %f, %f, %f, %f, %f)\n", start_segment[0], start_segment[1], start_segment[2], end_segment[0], end_segment[1], end_segment[2], extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);
				raytrace_recursive2(vertices, base, triangles, depth + 1, subcell_id, &start_segment[0], &end_segment[0], &extent[0], &tri_hit, &u);

				if((tri_hit != -1)&(u < u_min)){
					u_min       = u;
					current_hit = tri_hit;
					*hit    = current_hit;
					*length = u_min;


					break;
				}
			}

		}
	}
}


__global__ void raytrace(float *vertices, float *base, unsigned int *triangles, unsigned int depth, float *extent, int *hits, float *lengths, float *start_segments, float *end_segments, unsigned int nrays){
	unsigned int idx = blockIdx.x*blockDim.x + threadIdx.x, contains;
	float start[3], end[3], t_min, t_max;

	if(idx < nrays){
		assign_dim_device(&start_segments[3*idx + 0], &start[0], 3);
		assign_dim_device(&end_segments[3*idx + 0],     &end[0], 3);

		contains = intersect_line_cube(&start[0], &end[0], &gentree_extent[0], &t_min, &t_max);

		if(contains == 1){
			// Perform raytracing
			raytrace_recursive2(vertices, base, triangles, 0, 0, &start[0], &end[0], &extent[0], &hits[idx], &lengths[idx]);
		}
		else{
			hits[idx]    = -1;
			lengths[idx] = -1.f;
		}
	}
}

extern "C" {

void free_gen_tree(){
	cudaFree(gentree_depth_offset);
	cudaFree(gentree_cells);
	cudaFree(gentree_cell_count);
	cudaFree(gentree_cell_total_count);
	cudaFree(gentree_starting_idx);
	cudaFree(gentree_extent);
	cudaFree(gentree_cell_extent);
}
}

void fill_in_subcell(unsigned int depth, unsigned int subcell_id, unsigned int *offset_out);

void fill_in_subcell(unsigned int depth, unsigned int cell_id, unsigned int *offset_out){
	unsigned int offset = *offset_out, ncurrent_cell, nsubcells = power_host(2, npartition), subcell_id;

	ncurrent_cell = gentree_cell_count[get_idx_cell_host(depth, cell_id)];

	gentree_starting_idx[get_idx_cell_host(depth, cell_id)] = offset;
	offset += ncurrent_cell;

	if(depth < gentree_max_depth){
		for(unsigned int i = 0; i < nsubcells; i++){
			subcell_id = cell_id*nsubcells + i;

			fill_in_subcell(depth + 1, subcell_id, &offset);
		}
	}
	*offset_out = offset;
}
extern "C" {

void generate_general_tree(float *vertices, unsigned int *triangles, unsigned int ntriang, unsigned int depth, float *extent){
	float vert1[3], vert2[3], vert3[3];
	unsigned int i, j, level, offset, cell_count, cell_id, nsubcells = power_host(2, npartition), level_contiguous = 0;
	int idx_locations1[depth + 1], idx_locations2[depth + 1], idx_locations3[depth + 1];

	printf("Initializing tree, depth = %d, ntriang = %d, size = %d\n", depth, ntriang, geometric_sum_host(nsubcells, depth)*sizeof(unsigned int));

	checkCudaErrors(cudaMallocManaged(&list_a,            4*sizeof(double)));

	checkCudaErrors(cudaMallocManaged((&gentree_triangles),            ntriang*3*sizeof(unsigned int)));


	checkCudaErrors(cudaMallocManaged((&gentree_cells),            ntriang*sizeof(unsigned int)));
	checkCudaErrors(cudaMallocManaged((&gentree_cell_count),       geometric_sum_host(nsubcells, depth)*sizeof(unsigned int)));
	checkCudaErrors(cudaMallocManaged((&gentree_cell_total_count), geometric_sum_host(nsubcells, depth)*sizeof(unsigned int)));
	checkCudaErrors(cudaMallocManaged((&gentree_starting_idx),     geometric_sum_host(nsubcells, depth)*sizeof(unsigned int)));
	checkCudaErrors(cudaMallocManaged((&gentree_depth_offset),     (depth + 1)*sizeof(unsigned int)));
	checkCudaErrors(cudaMallocManaged((&gentree_cell_extent),      geometric_sum_host(nsubcells, depth)*6*sizeof(float)));
	checkCudaErrors(cudaMallocManaged((&gentree_extent),           6*sizeof(float)));
	printf("Mallocated\n");

	gentree_ntriang   = ntriang;
	gentree_max_depth = depth;

	unsigned int max_vert_idx = 0, nvert;
	for(i = 0; i < 3*ntriang; i++){
		gentree_triangles[i] = triangles[i];
		if(triangles[i] > max_vert_idx)
			max_vert_idx = triangles[i];
	}
	nvert = max_vert_idx + 1;
	checkCudaErrors(cudaMallocManaged((&gentree_vertices),     3*nvert*sizeof(float)));
	for(i = 0; i < 3*nvert; i++){
		gentree_vertices[i] = vertices[i];
	}




	for(i = 0; i < 6; i++)
		gentree_extent[i] = extent[i];

	for(i = 0; i < depth + 1; i++){
		offset = geometric_sum_host(nsubcells, i - 1);
		gentree_depth_offset[i] = offset;

		for(j = 0; j < power_host(nsubcells, i); j++){
			gentree_cell_count[offset + j]       = 0;
			gentree_cell_total_count[offset + j] = 0;
		}
	}

	assign_dim_host(&extent[0], &gentree_cell_extent[0], 6);

	for(i = 0; i < depth; i++){
		offset = gentree_depth_offset[i];
		for(j = 0; j < power_host(nsubcells, i); j++){

			subdivide_cell(&gentree_cell_extent[6*(offset + j)  + 0], &gentree_cell_extent[6*(offset + power_host(nsubcells, i) + nsubcells*j) + 0]);
		}
	}


	printf("Counting triangles\n");

	for(i = 0; i < ntriang; i++){
		assign_dim_host(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		locate_vertex(&vert1[0], depth, &idx_locations1[0]);
		locate_vertex(&vert2[0], depth, &idx_locations2[0]);
		locate_vertex(&vert3[0], depth, &idx_locations3[0]);

		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j]))
				break;
		}
		level = j - 1;

		// Count triangle at level
		gentree_cell_count[get_idx_cell_host(level, idx_locations1[level])] += 1;

		// Count triangle at all higher levels
		for(j = 0; j < level + 1; j++){
			gentree_cell_total_count[get_idx_cell_host(j, idx_locations1[j])] += 1;
		}


	}


	// gentree_cells = (unsigned int *) malloc(ntriang*sizeof(unsigned int));

	printf("Determining starting index, cumulative sum, tot count = %d\n", gentree_cell_total_count[0]);


	if(level_contiguous == 1){
		// Level-contiguous
		offset = 0;
		for(i = 0; i < depth + 1; i++){
			for(unsigned int idx = 0; idx < power_host(nsubcells, i); idx++){

				cell_count = gentree_cell_count[get_idx_cell_host(i, idx)];

				gentree_starting_idx[get_idx_cell_host(i, idx)] = offset;
				offset += cell_count;
			}
		}
	}
	else{
		// Cell-contiguous
		offset = 0;
		fill_in_subcell(0, 0, &offset);
	}
	printf("offset = %d\n", offset);


	// Reset cell count
	for(i = 0; i < geometric_sum_host(nsubcells, depth); i++)
		gentree_cell_count[i] = 0;

	printf("To set cell listing of triangles\n");
	for(i = 0; i < ntriang; i++){
		assign_dim_host(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);


		locate_vertex(&vert1[0], depth, &idx_locations1[0]);
		locate_vertex(&vert2[0], depth, &idx_locations2[0]);
		locate_vertex(&vert3[0], depth, &idx_locations3[0]);


		for(j = 0; j < depth + 1; j++){
			if((idx_locations1[j] != idx_locations2[j])|(idx_locations1[j] != idx_locations3[j])|(idx_locations2[j] != idx_locations3[j]))
				break;
		}
		level = j - 1;

		cell_id = idx_locations1[level];
		if(cell_id >= power_host(nsubcells, level))
			printf("Error, idx too high, idx = %d, max = %d\n", cell_id, power_host(nsubcells, level));

		offset = gentree_cell_count[get_idx_cell_host(level, cell_id)];

		gentree_cells[gentree_starting_idx[get_idx_cell_host(level, cell_id)] + offset] = i;
		gentree_cell_count[get_idx_cell_host(level, cell_id)] += 1;

	}
}
}

__managed__ int *hits_device;
__managed__ float *lengths_device, *start_device, *end_device;

extern "C" {
void raytracing(float *vertices, unsigned int *triangles, unsigned int ntriang, float *starting_points, float *end_points, unsigned int nrays, int *hits, float *lengths, unsigned int mode){
	float  vert1[3], vert2[3], vert3[3], seg_AB[3], seg_AC[3], normal[3], base1[3], vert2_base2[3], vert3_base2[3], base2[3];
	unsigned int nattributes = 6, i;

	// nrays = 10;
	cudaMallocManaged(&base,           3*nattributes*ntriang * sizeof(float));
	cudaMallocManaged(&hits_device,    nrays * sizeof(int));
	cudaMallocManaged(&lengths_device, nrays * sizeof(float));
	cudaMallocManaged(&start_device,   3*nrays * sizeof(float));
	cudaMallocManaged(&end_device,     3*nrays * sizeof(float));


	printf("Calculating %d triangle normals\n", ntriang);
	for(i = 0; i < ntriang; i++){
		assign_dim_host(&vertices[3*triangles[3*i + 0] + 0], &vert1[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 1] + 0], &vert2[0], 3);
		assign_dim_host(&vertices[3*triangles[3*i + 2] + 0], &vert3[0], 3);

		substract_host(&vert2[0], &vert1[0], &seg_AB[0]);
		substract_host(&vert3[0], &vert1[0], &seg_AC[0]);

		cross_host(&seg_AB[0], &seg_AC[0], &normal[0]);
		normalize_host(&normal[0], &normal[0]);


		normalize_host(&seg_AB[0], &base1[0]);
		cross_host(&normal[0], &base1[0], &base2[0]);


		vert2_base2[0] = dot_host(&seg_AB[0], &base1[0]);
		vert2_base2[1] = dot_host(&seg_AB[0], &base2[0]);
		vert2_base2[2] = dot_host(&seg_AB[0], &normal[0]);

		vert3_base2[0] = dot_host(&seg_AC[0], &base1[0]);
		vert3_base2[1] = dot_host(&seg_AC[0], &base2[0]);
		vert3_base2[2] = dot_host(&seg_AC[0], &normal[0]);


		assign_dim_host(&base1[0],       &base[3*(nattributes*i + 0)], 3);
		assign_dim_host(&base2[0],       &base[3*(nattributes*i + 1)], 3);
		assign_dim_host(&normal[0],      &base[3*(nattributes*i + 2)], 3);
		assign_dim_host(&vert1[0],       &base[3*(nattributes*i + 3)], 3);
		assign_dim_host(&vert2_base2[0], &base[3*(nattributes*i + 4)], 3);
		assign_dim_host(&vert3_base2[0], &base[3*(nattributes*i + 5)], 3);
	}

	printf("Copying data\n");
	for(i = 0; i < 3*nrays; i++){
		start_device[i] = starting_points[i];
		end_device[i]   = end_points[i];
	}


	printf("To actually raytrace %d rays <<<%d, %d>>>\n", nrays, nrays/BLOCK_SIZE + 1, BLOCK_SIZE);

	checkCudaErrors(cudaDeviceSynchronize());
	raytrace<<<nrays/BLOCK_SIZE + 1, BLOCK_SIZE>>>(gentree_vertices, base, gentree_triangles, 0, gentree_extent, hits_device, lengths_device, start_device, end_device, nrays);
	checkCudaErrors(cudaDeviceSynchronize());

	for(i = 0; i < nrays; i++){
		hits[i]    = hits_device[i];
		lengths[i] = lengths_device[i];
	}

	checkCudaErrors(cudaFree(start_device));
	cudaFree(end_device);
	cudaFree(hits_device);
	cudaFree(lengths_device);
	cudaFree(base);
}
}

// nvcc raytracing.cu -rdc=true -lcudadevrt -c -o raytracing.o -shared -Xcompiler -fPIC
// gcc -shared raytracing.o -o raytracing.so -lm -fopenmp
