import tools
import numpy as np
import matplotlib.pyplot as plt
import modulo
import os
from importlib import reload
import lighting_toolkit as light
import time
import geometry_generation as geo_gen


def get_mesh(lod, block_id):
	rest = []
	all_materials, offset = {}, 0

	mesh_lod = light.load_mesh(lod = lod, block_id = block_id)
	for j in range(len(mesh_lod.materials)):
		material = mesh_lod.materials[j]
		if material in all_materials.keys():
			mat_id = all_materials[material]
		else:
			mat_id = offset
			all_materials[material] = offset
			offset += 1

		mesh_lod.submeshes[mesh_lod.materials[j]].additional_attributes = []

		mesh_lod.submeshes[mesh_lod.materials[j]].add_quantity("block_id", block_id, dtype = "int8", dim = 1, per_vert = True)
		mesh_lod.submeshes[mesh_lod.materials[j]].add_quantity("mat_id",     mat_id, dtype = "int8", dim = 1, per_vert = True)
	return mesh_lod, all_materials

block_id = 1
mesh_lod, all_materials = get_mesh(0, block_id)

whole_mesh = tools.combine_submeshes(list(mesh_lod.submeshes.values()), additional_attributes=["uv2", "block_id", "mat_id"], per_vert=True)


vertices, triangles, mat_ids = whole_mesh.vertices, whole_mesh.triangles, whole_mesh.mat_id
surface_count, organized_triangles = modulo.determine_coplanar_surface(vertices, triangles)

print((surface_count > 1).sum())

aux = np.roll(surface_count, 1)
aux[0] = 0
surface_offsets = np.cumsum(aux)

def get_triangle_base(vertices, triangles):
	base = np.zeros([len(triangles), 3, 3], dtype = np.float32)

	vert0 = vertices[triangles[:, 0]]
	seg1  = vertices[triangles[:, 1]] - vert0
	seg2  = vertices[triangles[:, 2]] - vert0

	triang_normals = np.cross(seg1, seg2, axis = 1)
	tri_areas = np.linalg.norm(triang_normals, axis = 1)/2

	for i in range(3): triang_normals[:, i] *= 0.5/tri_areas

	S = triang_normals[:, 0]*vert0[:, 0] + triang_normals[:, 1]*vert0[:, 1] + triang_normals[:, 2]*vert0[:, 2]

	length1 = np.linalg.norm(seg1, axis = 1)
	for i in range(3): base[:, 0, i] = seg1[:, i]/length1

	base[:, 1] = np.cross(triang_normals, base[:, 0])
	base[:, 2] = triang_normals

	return base, S, tri_areas, vert0

base, S, tri_areas, vert0 = get_triangle_base(vertices, triangles)

surface_areas = np.zeros(len(surface_count))

S_mask = np.zeros(len(surface_count), dtype = bool)
for i in range(len(surface_areas)):
	surf_tris  = organized_triangles[surface_offsets[i] : surface_offsets[i] + surface_count[i]]
	surface_areas[i] = np.sum(tri_areas[surf_tris])
	S_mask[i] =        any( S[surf_tris] >= 0 )*any( S[surf_tris] < 0 )



sorted_ids = np.argsort(surface_areas)





def visualize_subset(submesh, tri_ids):
	nvert = submesh.vertices.shape[0]
	mask_triangles = np.zeros(len(submesh.triangles), dtype = bool)

	mask_triangles[tri_ids] = True
	color_highlight = [1., 0., 0., 1.]
	color_grey      = [0.5, 0.5, 0.5, 1.]
	mesh_highlighted = tools.SubMesh(vertices = submesh.vertices, triangles = submesh.triangles[ mask_triangles], colors = [color_highlight]*nvert, uvs = submesh.uvs)
	mesh_rest        = tools.SubMesh(vertices = submesh.vertices, triangles = submesh.triangles[~mask_triangles], colors = [color_grey]*nvert,      uvs = submesh.uvs)

	(mesh_highlighted.get_mesh() + mesh_rest.get_mesh()).plot()


def get_polygon_from_triangles(vertices):
	nvert = len(vertices)
	ntris = int(nvert/3)
	result = []
	for i in range(ntris):
		tri_verts = vertices[3*i:3*(i + 1)]
		result.append(tools.Polygon(tri_verts[0:4, 0:2]))

	return tools.unary_union(result).buffer(1e-6, join_style=2)

def fix_shape(shape, criterium = 1e-6):
	shape_reduct1   = shape.buffer(-criterium, join_style = 2, mitre_limit = 2)
	shape_expanded1 = shape_reduct1.buffer(2*criterium, join_style = 2, mitre_limit = 2)


	shape_expanded2 = shape.buffer(criterium, join_style = 2, mitre_limit = 2)
	shape_reduct2   = shape_expanded2.buffer(-2*criterium, join_style = 2, mitre_limit = 2)

	rings1 = tools.get_rings(shape_expanded1)
	rings2 = tools.get_rings(shape_reduct2)

	N1 = sum([len(ring) for ring in rings1])
	N2 = sum([len(ring) for ring in rings2])
	# print("N1, N2 = ", N1, N2)
	if N1 <= N2:
		# return shape_expanded1.buffer(-criterium, join_style = 2).buffer(criterium, join_style = 2)
		return shape_expanded1
	else:
		# return shape_reduct2.buffer(criterium, join_style = 2).buffer(-criterium, join_style = 2)
		return shape_reduct2

def transform_surface_to_shape(submesh, vert0, block_id, base, S, surface_id, organized_triangles, surface_offsets, surface_count):
	vertices, triangles, mat_ids, normals = submesh.vertices, submesh.triangles, submesh.mat_id, submesh.normals

	surf_tris = organized_triangles[surface_offsets[surface_id]: surface_offsets[surface_id] + surface_count[surface_id]]

	surf_A = surf_tris[S[surf_tris] >= 0.]
	surf_B = surf_tris[S[surf_tris] < 0.]


	if len(surf_A)*len(surf_B) != 0:
		first_tri    = organized_triangles[surface_offsets[surface_id]]
		current_base = base[first_tri]
		origin       = np.dot(vert0[first_tri], base[first_tri, 2])*base[first_tri, 2]

		tris_A = triangles[surf_A]
		tris_B = triangles[surf_B]

		mats_A = mat_ids[tris_A.flatten()]
		mats_B = mat_ids[tris_B.flatten()]

		verts_A = vertices[tris_A.flatten()]*1.
		verts_B = vertices[tris_B.flatten()]*1.

		norms_B = base[surf_B, 2]


		for i in range(3):
			verts_A[:, i] += -origin[i]
			verts_B[:, i] += -origin[i]

		# verts_B[:, 0]*norms_B[:, 0] + verts_B[:, 1]*norms_B[:, 1] + verts_B[:, 2]*norms_B[:, 2]

		verts_A_rot = np.matmul(current_base, verts_A.T).T
		verts_B_rot = np.matmul(current_base, verts_B.T).T

		shape_A = get_polygon_from_triangles(verts_A_rot)
		shape_B = get_polygon_from_triangles(verts_B_rot)

		intersection = shape_A.intersection(shape_B)
		# shape_A = fix_shape(shape_A.difference(intersection))
		# shape_B = fix_shape(shape_B.difference(intersection))


		mesh_mat_original = tools.SubMesh(vertices = vertices, uvs=submesh.uvs, triangles = triangles[surf_tris], colors = [[1.]*4]*len(vertices))

		if intersection.area != 0:
			submeshes = []
			for idx, mats in enumerate([mats_A, mats_B]):
				if idx == 0:
					tris = tris_A
					verts_rot = verts_A_rot*1.
				else:
					tris = tris_B
					verts_rot = verts_B_rot*1.
				for mat_id in np.unique(mats):
					mask_mat = mats == mat_id
					color     = whole_mesh.colors[tris.flatten()[mask_mat][0]]
					# block_id  = whole_mesh.block_id[tris.flatten()[mask_mat][0]]
					verts_mat = verts_rot[mask_mat]

					shape_mat = fix_shape(get_polygon_from_triangles(verts_mat).difference(intersection))
					mesh_mat  = geo_gen.meshify_shape(shape_mat, 0., list(all_materials.keys())[mat_id], color = color, return_submesh = True)


					mesh_mat = tools.combine_submeshes(mesh_mat)

					# mesh_mat.add_quantity("block_id", block_id, dtype = "int8", dim = 1, per_vert = True)
					mesh_mat.add_quantity("mat_id",     mat_id, dtype = "int8", dim = 1, per_vert = True)

					mesh_mat.vertices = np.matmul(current_base.T, mesh_mat.vertices.T).T

					for i in range(3):
						mesh_mat.vertices[:, i] += origin[i]


					mesh_mat.calculate_normals(check = False)

					if mesh_mat.nverts != 0:
						print(len(mesh_mat.vertices), len(mesh_mat.mat_id), mesh_mat.nverts)
						# s = np.dot(mesh_mat.vertices[0], mesh_mat.normals[0])
						s = mesh_mat.vertices[:, 0]*mesh_mat.normals[:, 0] + mesh_mat.vertices[:, 1]*mesh_mat.normals[:, 1] + mesh_mat.vertices[:, 2]*mesh_mat.normals[:, 2]

						if (-1)**idx*s[np.isfinite(s)].mean() <= 0:
							mesh_mat.flip()
							mesh_mat.normals *= -1

						mesh_bug = tools.combine_submeshes([mesh_mat, mesh_mat_original])

						submeshes.append(mesh_mat)
			new_mesh = tools.combine_submeshes(submeshes, additional_attributes=["mat_id"], per_vert=True)
		else:
			new_mesh = tools.SubMesh()
			# new_mesh.add_quantity("block_id", block_id, dtype = "int8", dim = 1, per_vert = True)
			new_mesh.add_quantity("mat_id",     -1,     dtype = "int8", dim = 1, per_vert = True)
			surf_tris = np.zeros(0, dtype=np.int32)
	else:
		new_mesh = tools.SubMesh()
		# new_mesh.add_quantity("block_id", block_id, dtype = "int8", dim = 1, per_vert = True)
		new_mesh.add_quantity("mat_id",     -1,     dtype = "int8", dim = 1, per_vert = True)
		surf_tris = np.zeros(0, dtype=np.int32)

	# print(new_mesh.mat_id)
	return new_mesh, surf_tris


new_meshes, mask_triangles = [], np.ones(whole_mesh.ntris, dtype = bool)
for i in range(100):
	print("i:", i)
	surface_id = sorted_ids[-(i + 1)]
	new_mesh, surf_tris = transform_surface_to_shape(whole_mesh, vert0, block_id, base, S, surface_id, organized_triangles, surface_offsets, surface_count)

	new_meshes.append(new_mesh)
	mask_triangles[surf_tris] = False


whole_mesh.triangles = whole_mesh.triangles[mask_triangles]

new_mesh = tools.combine_submeshes(new_meshes + [whole_mesh], additional_attributes=["mat_id"], per_vert=True)
new_mesh.calculate_normals()
# new_mesh = tools.combine_submeshes(new_meshes, additional_attributes=["mat_id"], per_vert=True)

new_mesh.plot()


# surface_id = sorted_ids[-14]
# visualize_subset(whole_mesh, organized_triangles[surface_offsets[surface_id] : surface_offsets[surface_id] + surface_count[surface_id]])

#
# surf1, surf2 = sorted_ids[-7], sorted_ids[-19]
# S1 = S[organized_triangles[surface_offsets[surf1] : surface_offsets[surf1] + surface_count[surf1]]]
# S2 = S[organized_triangles[surface_offsets[surf2] : surface_offsets[surf2] + surface_count[surf2]]]
