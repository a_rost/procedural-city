import tools
import numpy as np
import matplotlib.pyplot as plt
import modulo
import xatlas
import os
from importlib import reload
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light

plt.ion()

def get_block_ids(vec, extent, depth):
	ids = np.zeros(depth + 1, dtype = np.uint32)
	for i in range(depth + 1):
		dx = (extent[1] - extent[0])*2**(-i)
		dy = (extent[3] - extent[2])*2**(-i)
		dz = (extent[5] - extent[4])*2**(-i)


		id_x = np.int32((vec[0] - extent[0])/dx)
		id_y = np.int32((vec[1] - extent[2])/dy)
		id_z = np.int32((vec[2] - extent[4])/dz)

		ids[i] = np.int32(id_z*(2**(2*i)) + id_y*2**i + id_x)

	return ids, np.array([id_x, id_y, id_z])


def get_block_ids(vec, extent, depth):
	dx = np.zeros(3)
	array = np.zeros(depth + 1, dtype = np.int32)
	ids = np.zeros(3, dtype = np.int32)
	N = 2**depth

	for i in range(depth + 1):
		for j in range(3):
			dx[j] = (extent[2*j + 1] - extent[2*j])/2**i
			ids[j] = np.int32((vec[j] - extent[2*j])/dx[j])

			ids[j] = min(ids[j], N - 1)
			ids[j] = max(ids[j], 0)

		array[i] = ids[2]*2**(2*i) + ids[1]*2**i + ids[0]
	return array


def plot_triangles(triangles, idxs, depth):
	fig, axs = plt.subplots(ncols = 2, nrows = 1)
	used_vertices = np.unique(triangles.flatten())

	axs[0].plot(vertices[used_vertices, 0], vertices[used_vertices, 1], ".")
	axs[0].plot(vertices[triangles[idxs, 0], 0], vertices[triangles[idxs, 0], 1], ".", color = "r")
	axs[0].plot(vertices[triangles[idxs, 1], 0], vertices[triangles[idxs, 1], 1], ".", color = "r")
	axs[0].plot(vertices[triangles[idxs, 2], 0], vertices[triangles[idxs, 2], 1], ".", color = "r")
	axs[0].set_xlim(extent[0], extent[1])
	axs[0].set_ylim(extent[2], extent[3])

	for idx in range(2**(3*depth)):
		corner = get_corner(idx, depth, extent)
		axs[0].plot([corner[0]], [corner[1]], "o", color = "k")

	axs[1].plot(vertices[used_vertices, 0], vertices[used_vertices, 2], ".")
	axs[1].plot(vertices[triangles[idxs, 0], 0], vertices[triangles[idxs, 0], 2], ".", color = "r")
	axs[1].plot(vertices[triangles[idxs, 1], 0], vertices[triangles[idxs, 1], 2], ".", color = "r")
	axs[1].plot(vertices[triangles[idxs, 2], 0], vertices[triangles[idxs, 2], 2], ".", color = "r")
	axs[1].set_xlim(extent[0], extent[1])
	axs[1].set_ylim(extent[4], extent[5])

	for idx in range(2**(3*depth)):
		corner = get_corner(idx, depth, extent)
		axs[1].plot([corner[0]], [corner[2]], "o", color = "k")

def calculate_memory(ntriang, depth):
	mem_bytes = 0
	for i in range(depth):

		estimate = 2*(ntriang/2**(3*i) + 1)
		mem_bytes += estimate*pow(2, 3*i)*4
	print(mem_bytes/10**6, " MB")

def get_corner(idx, depth, extent):
	N = 2**(depth)
	id_z = int(idx/(N**2))
	id_y = int((idx - N**2*id_z)/N)
	id_x = idx - N**2*id_z - N*id_y
	dx, dy, dz = (extent[1] - extent[0])/N, (extent[3] - extent[2])/N, (extent[5] - extent[4])/N
	# print(dx, dy, dz, (dx**2 + dy**2 + dz**2)**0.5)
	return np.array([dx*id_x + extent[0], dy*id_y + extent[2], dz*id_z + extent[4]])



def check_inside(vector, extent):
	if vector[0] >= extent[0] and vector[0] < extent[1] and vector[1] >= extent[2] and vector[1] < extent[3] and vector[2] >= extent[4] and vector[2] < extent[5]:
		return True
	else:
		return False

def is_in_square(vec, idx, extent):
	if   idx == 0 and vec[1] >= extent[2] and vec[1] < extent[3] and vec[2] >= extent[4] and vec[2] < extent[5]:
		return True
	elif idx == 1 and vec[0] >= extent[0] and vec[0] < extent[1] and vec[2] >= extent[4] and vec[2] < extent[5]:
		return True
	elif idx == 2 and vec[1] >= extent[2] and vec[1] < extent[3] and vec[0] >= extent[0] and vec[0] < extent[1]:
		return True
	else:
		return False


def min_argsort(array_orig):
	array = np.copy(array_orig)
	N = len(array)
	output = np.arange(N)

	for i in range(N):
		minimum = np.inf
		for j in range(i, N):
			if array[j] < minimum:
				j_min   = j
				minimum     = array[j]
				idx_minimum = output[j]

		aux_float = array[i]
		aux_int   = output[i]

		array[i] = minimum
		output[i] = idx_minimum

		array[j_min] = aux_float
		output[j_min] = aux_int
	return array, output


def check_closest_intersection(start, end, extent):
	vector = end - start
	outside, t_min, t_max = True, 1., 0.

	start_in = check_inside(start, extent)
	end_in   = check_inside(end, extent)

	if start_in and end_in:
		outside, t_min, t_max = False, 0., 1.

	else:
		for idx in range(3):
			for side in range(2):
				t = (extent[2*idx + side] - start[idx])/vector[idx]
				print(idx, side, t)

				if t >= 0. and t <= 1.:
					touch_point = start + t*vector

					isin = is_in_square(touch_point, idx, extent)
					print(touch_point, isin)
					if isin:
						outside = False

					if isin and t < t_min: t_min = t
					if isin and t > t_max: t_max = t

		if start_in or end_in:
			t_intersect = t_min
			if t_intersect != t_max:
				print("WARNING", start_in, end_in, t_intersect, t_min, t_max)

			if start_in:
				outside, t_min, t_max = False, 0., t_intersect
			else:
				outside, t_min, t_max = False, t_intersect, 1.

	return outside, t_min, t_max

def get_meshes(start_points, end_points, lengths, valid, color):
	directions = end_points - start_points
	dists   = np.linalg.norm(directions, axis  = 1)
	for i in range(3): directions[:, i] *= 1/dists

	hit_points = np.zeros([nvectors, 3])
	for i in range(3): hit_points[:, i] = start_points[:, i] + directions[:, i]*lengths

	ray_triangles = np.reshape(np.arange(valid.sum()*3, dtype = np.int32), [valid.sum(), 3])
	ray_vertices  = np.zeros([3*valid.sum(), 3], dtype = np.float32)

	ray_vertices[0::3, :] = start_points[valid]
	# ray_vertices[0::3, :] = hit_points[valid] + np.random.random(size = [valid.sum(), 3])
	ray_vertices[1::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3
	ray_vertices[2::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3

	colors = np.zeros(shape = [valid.sum()*3, 4])
	colors[:, 0] = color[0]
	colors[:, 1] = color[1]
	colors[:, 2] = color[2]
	colors[:, 3] = 1.0

	meshi = tools.SubMesh(vertices = ray_vertices, triangles = ray_triangles, colors = colors, uvs = np.ones(shape = [valid.sum()*3, 2]))
	meshi.calculate_normals()

	return meshi


if os.path.isdir("map_textures"):
	pass
else:
	os.mkdir("map_textures")

if True:
	mesh_lod0 = tools.Mesh()
	meshes = light.load_meshes(lod = 0)
	for mesh in meshes:
		mesh_lod0 += mesh
else:
	mesh_lod0 = light.load_meshes(lod = 0)[1]

vertices, triangles = mesh_lod0.whole_mesh.vertices, mesh_lod0.whole_mesh.triangles


extent = [vertices[:, 0].min(), vertices[:, 0].max(), vertices[:, 1].min(), vertices[:, 1].max(), vertices[:, 2].min(), vertices[:, 2].max()]
for i in range(6):
	extent[i] += -(-1)**i





nvectors = 500000

start_points = np.zeros([nvectors, 3])
end_points = np.zeros([nvectors, 3])

start_points[:, 0] = (extent[1] + extent[0])*np.random.random()
start_points[:, 1] = (extent[3] + extent[2])*np.random.random()
start_points[:, 2] = extent[5]*np.random.random()

np.random.seed(0)
for i in range(3): start_points[:, i] = extent[2*i] + (extent[2*i + 1] - extent[2*i])*np.random.random(size = nvectors)

for i in range(3): end_points[:, i] = extent[2*i] + (extent[2*i + 1] - extent[2*i])*np.random.random(size = nvectors)






modulo.create_general_tree(vertices, triangles, extent, depth = 8)
# hits2, lengths2 = modulo.raytracing(vertices, triangles, start_points, end_points)
# modulo.free_oct_tree()



modulo.create_oct_tree(vertices, triangles, extent, depth = 8)
hits2, lengths2 = modulo.raytracing(vertices, triangles, start_points, end_points)
modulo.free_oct_tree()



modulo.create_oct_tree(vertices, triangles, extent, depth = 0)
hits1, lengths1 = modulo.raytracing(vertices, triangles, start_points, end_points)
modulo.free_oct_tree()


frac = lengths2/lengths1
valid = (lengths1 != lengths2)
# valid = hits2 != -1

meshi1 = get_meshes(start_points, end_points, lengths1, valid, color = [1., 0., 0.])
meshi2 = get_meshes(start_points, end_points, lengths2, valid, color = [0., 1., 0.])

vista = mesh_lod0 + meshi2.get_mesh()  + meshi1.get_mesh()
# vista.plot()





modulo.create_oct_tree(vertices, triangles, extent, depth = 7)
idx, depth, cell = modulo.triangle_query(idx = 0, depth = 0, ntriang_limit = len(triangles))

modulo.toggle_debug()

new_start = np.zeros([1, 3], dtype = np.float32)
new_end   = np.zeros([1, 3], dtype = np.float32)


id_problem = np.arange(nvectors)[valid][0]

new_start = start_points[id_problem]
new_end   = end_points[id_problem]

tri_id = hits1[id_problem]
print("Depth: ", depth[idx == tri_id], " cell: ", cell[idx == tri_id])
hits3, lengths3 = modulo.raytracing(vertices, triangles, new_start, new_end)




idx2, depth2, cell2 = modulo.triangle_query(idx = 2150, depth = 4, ntriang_limit = len(triangles))

#
#
# #
# mask_hit = hits != 0
#
# hit_points = np.zeros([mask_hit.sum(), 3])
#
#
# plt.plot(vertices[:, 0], vertices[:, 2], ".")
#
# for i in range(len(hit_points)):
# 	plt.plot([start_points[mask_hit][i, 0], hit_points[i, 0]], [start_points[mask_hit][i, 2], hit_points[i, 2]], color = "r")


#
# mask = (vertices[:, 0] >= 47.5)*(vertices[:, 0] <= 50.)*(vertices[:, 1] >= 56.)*(vertices[:, 1] <= 60.)



# ids = get_block_ids(np.array([50., 50., 5.]), extent, depth)
# print(ids)


if False:
	archivo = open("oct_tree_debug.txt", "w")
	for i in range(6):
		archivo.write(str(extent[i]) + " ")
	archivo.write("\n")
	archivo.write(str(len(vertices)) + "\n")
	for i in range(len(vertices)):
		archivo.write(str(vertices[i, 0]) + " " + str(vertices[i, 1]) + " " + str(vertices[i, 2]) + "\n")
	archivo.write(str(len(triangles)) + "\n")
	for i in range(len(triangles)):
		archivo.write(str(triangles[i, 0]) + " " + str(triangles[i, 1]) + " " + str(triangles[i, 2]) + "\n")
	archivo.close()



if False:
	# extent = [4.913214, 123.693092, 3.126290, 136.930679, -0.500000, 68.180000]

	vec = np.array([50., 50., 2.000000])
	# for i in range(3):
	# 	vec[i] = extent[2*i + 0] + (extent[2*i + 1] - extent[2*i + 0])*np.random.random()

	depth = 2



	np.linalg.norm(corner - vec)
