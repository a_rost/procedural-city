import numpy as np
import matplotlib.pyplot as plt
import modulo
import manzana
import tools
import structure_generation as gen
from importlib import reload
import os

gen.DEBUG = True
plt.ion()

seed = np.random.randint(500)
np.random.seed(seed)

layout = [[120.], [140.]]

tools.initialize_maps(layout)
manzana.initialize_maps(layout, distortion = 0.10)

distortion_function = manzana.transform_coords




block_origin = [0., 0., 0.]
xcoord, ycoord, terrains, parcels = manzana.generate_manzana([layout[0][0], layout[1][0]], block_origin)
manzana_shape, sidewalk_shape, street_shape = manzana.get_manzana_shapes(xcoord, ycoord, block_origin)




manzana.plot_block(xcoord, ycoord, terrains, parcels)

parcel = parcels[4]

parcel_position = [parcel.left_corner[0], parcel.left_corner[1]]

props = tools.determine_map_properties([0., layout[0][0], 0., layout[1][0]], parcel_position)
parcel.properties = props


reload(gen)

np.random.seed(seed)
gen.DEBUG = False
recipe   = gen.manage_buildings(parcel)

building = gen.generate_appartment(recipe, parcel, only_meshes = False)
building.mesh.plot()



building.mesh.whole_mesh.calculate_normals()
modulo.optimize_submesh(building.mesh.whole_mesh)
building.mesh.plot()


tris_flat = building.mesh.whole_mesh.triangles.flatten()
max_idx = tris_flat.max()
hist = np.zeros(max_idx + 1)

hist = np.histogram(tris_flat, range = [0., max_idx + 1], bins = max_idx + 1)[0]
# for i in range(max_idx + 1):


# tri1, tri2 = 4170, 4175
#
# terna1 = building.mesh.whole_mesh.triangles[tri1]
# terna2 = building.mesh.whole_mesh.triangles[tri2]
#
# print(building.mesh.whole_mesh.vertices[terna1])
# print(building.mesh.whole_mesh.vertices[terna2])
#
# tools.plot_polygon(recipe.main_shape)
# tools.plot_polygon(recipe.back_shape)



