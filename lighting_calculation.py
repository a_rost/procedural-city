import tools
import numpy as np
import matplotlib.pyplot as plt
import modulo
import os
from importlib import reload
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light

plt.ion()


if os.path.isdir("map_textures"):
	pass
else:
	os.mkdir("map_textures")


def list2str(list_set):
	array = np.array(list_set)

	string = ""
	for i in array:
		string += str(i) + " "
	#string += "\n"
	return string


def write_cubes(mesh, idx, lod):
	if os.path.isdir("data/blocks") == False:
		os.mkdir("data/blocks")
	filename = "data/blocks/block_" + str(idx) + "_lod_" + str(lod) + ".txt"
	archivo = open(filename, "w")

	### Mesh
	archivo.write("Mesh\n")

	if lod == 0:
		n_materials = len(mesh.materials)
		archivo.write("materials: " + str(n_materials) + "\n")

		for material in mesh.materials:
			submesh     = mesh.submeshes[material]
			n_vert      = len(submesh.vertices)
			n_triangles = len(submesh.triangles)

			archivo.write(material + " vertices: " + str(n_vert) + " triangles: " + str(n_triangles) + "\n")
			for i in range(n_vert):
				vertex, uv, color, normal = submesh.vertices[i], submesh.uvs[i], submesh.colors[i], submesh.normals[i]

				string = list2str([vertex[0], vertex[1], vertex[2], normal[0], normal[1], normal[2], uv[0], uv[1], color[0], color[1], color[2], color[3]])
				archivo.write(string + "\n")
			for i in range(n_triangles):
				triangle = submesh.triangles[i]
				string = list2str([triangle[0], triangle[1], triangle[2]])
				archivo.write(string + "\n")

		archivo.close()
		os.system("sed -i 's/\./,/g' " + filename)

	else:
		n_materials = 1
		archivo.write("materials: " + str(n_materials) + "\n")

		submesh     = mesh
		n_vert      = len(submesh.vertices)
		n_triangles = len(submesh.triangles)

		archivo.write("Wall" + " vertices: " + str(n_vert) + " triangles: " + str(n_triangles) + "\n")
		for i in range(n_vert):
			vertex, uv, uv2, color, normal = submesh.vertices[i], submesh.uvs[i], submesh.uv2[i], submesh.colors[i], submesh.normals[i]

			string = list2str([vertex[0], vertex[1], vertex[2], normal[0], normal[1], normal[2], uv[0], uv[1], uv2[0], uv2[1], color[0], color[1], color[2], color[3]])
			archivo.write(string + "\n")
		for i in range(n_triangles):
			triangle = submesh.triangles[i]
			string = list2str([triangle[0], triangle[1], triangle[2]])
			archivo.write(string + "\n")

		archivo.close()
		os.system("sed -i 's/\./,/g' " + filename)


def det_normals(heights, extent):
	N, M = len(heights), len(heights[0])
	dx, dy = (extent[1] - extent[0])/N, (extent[3] - extent[2])/M
	grad = np.gradient(heights)

	vectors = np.zeros([N, M, 3])
	normals = np.zeros([N, M, 3])

	vectors[:,:,0] = -grad[1]/dx
	vectors[:,:,1] = grad[0]/dy
	vectors[:,:,2] = 1.

	norm = np.linalg.norm(vectors, axis = 2)
	for i in range(3): normals[:,:,i] = vectors[:,:,i]/norm

	return normals




# meshes_lod0 = light.load_meshes(lod = 0, usefile =  False, nblocks = 100)

meshes_lod2 = light.load_meshes(lod = 2, usefile = False, nblocks = 100)

if False:
	mesh_lod0 = tools.Mesh()
	for i, mesh in enumerate(meshes_lod0):
		print("Combining mesh lod0: " + str(i))

		mesh_lod0 += mesh

	del meshes_lod0

	mesh_lod0.calculate_whole_mesh()
	submesh_lod0 = mesh_lod0.whole_mesh


count = 0
submesh_lod2_indices = []
submeshes_lod2 = []
for mesh in meshes_lod2:
	submeshes_lod2.append(mesh.whole_mesh)
	submesh_lod2_indices.append(np.ones(len(mesh.whole_mesh.vertices), dtype = np.int32)*count)
	count += 1

# mesh = tools.combine_submeshes(submeshes_lod2)
# submesh_lod0         = tools.combine_submeshes(submeshes_lod0)
submesh_lod2         = tools.combine_submeshes(submeshes_lod2)
submesh_lod2_indices = np.concatenate(submesh_lod2_indices)

# mesh_

# submesh_lod0 = np.load("submesh0.npy", allow_pickle=True).item()

submesh_lod0.normals = modulo.calculate_normals(submesh_lod0.vertices, submesh_lod0.triangles, nthreads = 2)
# submesh_lod0.calculate_normals()

for block_id in range(len(meshes_lod2)*0 + 10, 100):

	mesh = meshes_lod2[block_id].whole_mesh

	tools.generate_uv2(mesh)
	# tools.generate_uv2(mesh_lod2.whole_mesh)

	# os.system("rm AO/*.npy")
	itera =  5

	# cosines = np.cos(np.pi/4) + (1 - np.cos(np.pi/4))*np.random.random(itera)
	# thetas  = np.arccos(cosines)*180/np.pi

	thetas, phis = np.linspace(5, 45, itera), np.linspace(0, 360, itera, endpoint = True)
 #
	# thetas[0:4] = 5
	# thetas[4:8] = 15
	# thetas[8:12] = 45
 #
	# phis[0:4]  = np.array([0., 90., 180., 270.])
	# phis[4:8]  = np.array([0., 90., 180., 270.]) + 45
	# phis[8:12] = np.array([0., 90., 180., 270.])


	# phis   = np.linspace(0, 3*360 + 0, itera, endpoint = False)
	# thetas[0] = 5.
	# thetas[1] = 10.

	cos_weighting = False
	resolution = [2048, 2048]
	averaged_texture, averaged_AO  = np.zeros(resolution + [3], dtype = np.float32), np.zeros(resolution, dtype = np.float32)
	averaged_bump, averaged_normal = np.zeros(resolution, dtype = np.float32), np.zeros(resolution + [3], dtype = np.float32)

	if cos_weighting == False: weights_sum = -np.ones(resolution, dtype = np.float32)
	else:                      weights_sum = np.zeros(resolution, dtype = np.float32)


	# dx = 0.1
	dx = 0.2

	cam_direction = np.array([0., 0., 1.])


	vertices0, normals0, triangles0 = light.rotate_mesh(submesh_lod0, 45., 45.)
	vertices2, normals2, triangles2 = light.rotate_mesh(mesh, 45., 45.)

	if False:
		quantity = np.matmul(normals0, cam_direction)
		quantity[quantity < 0.] = 0.

		extent = modulo.get_extent(vertices2)
		extent = [extent[0] - 50., extent[1] + 50., extent[2] - 50., extent[3] + 50.]
		heightmap_aux, shades, _ = modulo.map_quantity(vertices0, quantity, triangles0, extent, dx = dx/2, optimize = True)


		quantity = mesh.uv2[:, 0]
		heightmap_aux, map_u, _ = modulo.map_quantity(vertices2, quantity, triangles2, extent, dx = dx/2, optimize = True)
		map_u[map_u < quantity.min()] = quantity.min()
		map_u[map_u > quantity.max()] = quantity.max()

		quantity = mesh.uv2[:, 1]
		heightmap_aux, map_v, _ = modulo.map_quantity(vertices2, quantity, triangles2, extent, dx = dx/2, optimize = True)
		map_v[map_v < quantity.min()] = quantity.min()
		map_v[map_v > quantity.max()] = quantity.max()

		shadow = light.wrap_texture(shades, map_u, map_v, res = resolution)

		shadow_texture = np.zeros([resolution[0], resolution[1], 3], dtype = np.float32)
		for i in range(3): shadow_texture[:, :, i] = shadow

		mesh2 = light.wrap_texture2(mesh, shadow_texture)

	for i in range(itera):
		print("Itera: " + str(i))
		theta, phi = thetas[i], phis[i]


		mask_windows = light.correct_windows(mesh_lod0)
		submesh_lod0.colors[mask_windows, 0: 3] = 0.1



		vertices0, normals0, triangles0 = light.rotate_mesh(submesh_lod0, theta, phi)
		vertices1, normals1, triangles1 = light.rotate_mesh(mesh, theta, phi)
		vertices2, normals2, triangles2 = light.rotate_mesh(submesh_lod2, theta, phi)

		for j in range(3): vertices0[mask_windows, j] += cam_direction[j]*0.1
		sky_dir = tools.rotate(tools.rotate(np.array([0., 0., 1.]), tools.vec_up, phi*tools.deg2rad), tools.vec_right, theta*tools.deg2rad)

		# heightmap, extent = modulo.get_heightmap(vertices0, triangles, dx = dx)
		extent       = modulo.get_extent(vertices0)
		block_extent = modulo.get_extent(vertices1)

		extent = [max(block_extent[0] - 30., extent[0]), min(block_extent[1] + 30., extent[1]), max(block_extent[2] - 30., extent[2]), min(block_extent[3] + 30., extent[3])]

		heightmap_aux, map_R, _ = modulo.map_quantity(vertices0, submesh_lod0.colors[:, 0], triangles0, extent, dx = dx/2, optimize = True)
		heightmap_aux, map_G, _ = modulo.map_quantity(vertices0, submesh_lod0.colors[:, 1], triangles0, extent, dx = dx/2, optimize = True)
		heightmap_aux, map_B, _ = modulo.map_quantity(vertices0, submesh_lod0.colors[:, 2], triangles0, extent, dx = dx/2, optimize = True)

		map_color = np.zeros(map_R.shape + (3,), dtype = np.float32)
		map_color[:, :, 0] = map_R
		map_color[:, :, 1] = map_G
		map_color[:, :, 2] = map_B






		heightmap_aux, map_idx, _ = modulo.map_quantity(vertices2, submesh_lod2_indices, triangles2, block_extent, dx = dx/2, optimize = True)

		heightmap_lod0, _, _ = modulo.map_quantity(vertices0, submesh_lod0.colors[:, 2], triangles0, block_extent, dx = dx/2, optimize = True)
		heightmap_lod2, _, _ = modulo.map_quantity(vertices1,         mesh.colors[:, 2], triangles1, block_extent, dx = dx/2, optimize = True)
		bump_map = heightmap_lod0 - heightmap_lod2 + vertices0[:, 2].min() - vertices1[:, 2].min()
		normal_map = det_normals(bump_map, block_extent)

		quantity = mesh.uv2[:, 0]
		heightmap_aux, map_u, _ = modulo.map_quantity(vertices1, quantity, triangles1, block_extent, dx = dx/2, optimize = True)
		map_u[map_u < quantity.min()] = quantity.min()
		map_u[map_u > quantity.max()] = quantity.max()

		quantity = mesh.uv2[:, 1]
		heightmap_aux, map_v, _ = modulo.map_quantity(vertices1, quantity, triangles1, block_extent, dx = dx/2, optimize = True)
		map_v[map_v < quantity.min()] = quantity.min()
		map_v[map_v > quantity.max()] = quantity.max()

		quantity = np.matmul(normals1, cam_direction)
		quantity[quantity < 0.] = 0.
		heightmap_aux, cos, _ = modulo.map_quantity(vertices1, quantity, triangles1, block_extent, dx = dx/2, optimize = True)

		mask_idx = map_idx == block_id
		cos[~mask_idx] = 0.

		map_color_R = light.interpolate(map_color[:, :, 0], extent, block_extent, map_u.shape)
		map_color_G = light.interpolate(map_color[:, :, 1], extent, block_extent, map_u.shape)
		map_color_B = light.interpolate(map_color[:, :, 2], extent, block_extent, map_u.shape)

		# del map_color, map_R, map_G, map_B

		texture_R = light.wrap_texture(map_color_R, map_u, map_v, res = resolution)
		texture_G = light.wrap_texture(map_color_G, map_u, map_v, res = resolution)
		texture_B = light.wrap_texture(map_color_B, map_u, map_v, res = resolution)

		weight = light.wrap_texture(cos, map_u, map_v, res = resolution)

		normal_R = light.wrap_texture(normal_map[:, :, 0], map_u, map_v, res = resolution)
		normal_G = light.wrap_texture(normal_map[:, :, 1], map_u, map_v, res = resolution)
		normal_B = light.wrap_texture(normal_map[:, :, 2], map_u, map_v, res = resolution)
		bump     = light.wrap_texture(bump_map[:, :],      map_u, map_v, res = resolution)


		if cos_weighting:
			averaged_texture[:, :, 0] += texture_R*weight
			averaged_texture[:, :, 1] += texture_G*weight
			averaged_texture[:, :, 2] += texture_B*weight

			averaged_normal[:, :, 0] += normal_R*weight
			averaged_normal[:, :, 1] += normal_G*weight
			averaged_normal[:, :, 2] += normal_B*weight
			averaged_bump            += bump*weight

			weights_sum      += weight
		else:
			mask_higher = weights_sum < weight
			averaged_texture[mask_higher, 0] = texture_R[mask_higher]
			averaged_texture[mask_higher, 1] = texture_G[mask_higher]
			averaged_texture[mask_higher, 2] = texture_B[mask_higher]

			weights_sum[mask_higher] = weight[mask_higher]

			averaged_normal[mask_higher, 0] = normal_R[mask_higher]
			averaged_normal[mask_higher, 1] = normal_G[mask_higher]
			averaged_normal[mask_higher, 2] = normal_B[mask_higher]
			averaged_bump[mask_higher]      = bump[mask_higher]

	if cos_weighting:
		for i in range(3):
			averaged_texture[:, :, i] *= 1/weights_sum
			averaged_normal[:, :, i] *= 1/weights_sum

		averaged_bump *= 1/weights_sum


	averaged_texture[~np.isfinite(averaged_texture)] = 0.
	averaged_texture[averaged_texture > 1.] = 1.



	### FILLING IN HOLES
	fake_vertices = np.zeros(shape = vertices1.shape, dtype = np.float32)
	fake_vertices[:, 0], fake_vertices[:, 1], fake_vertices[:, 2] = mesh.uv2[:, 0], mesh.uv2[:, 1], np.random.random(len(fake_vertices))

	heightmap_aux, uv2_mapping, _ = modulo.map_quantity(fake_vertices, np.ones(len(fake_vertices)), triangles1, [0., 1., 0., 1.], dx = 0.001)

	array = light.interpolate(uv2_mapping, [0., 1., 0., 1.], [0., 1., 0., 1.], averaged_bump.shape)
	mask_map = array != 0.


	for i in range(3):
		averaged_texture[:, :, i] = light.fill_in_holes(averaged_texture[:, :, i], mask_map, 4)
		averaged_normal[:, :, i]  = light.fill_in_holes(averaged_normal[:, :, i], mask_map, 4)
		averaged_bump             = light.fill_in_holes(averaged_bump, mask_map, 4)

	mask_normal = np.linalg.norm(averaged_normal, axis = 2) == 0.
	averaged_normal[mask_normal, 0] = 0.
	averaged_normal[mask_normal, 1] = 0.
	averaged_normal[mask_normal, 2] = 1.

	norma = np.linalg.norm(averaged_normal, axis = 2)
	for i in range(3): averaged_normal[:, :, i] *= 1/norma


	plt.imsave("map_textures/block_" + str(block_id) + "_normal.png", (averaged_normal + 1)/2.)
	plt.imsave("map_textures/block_" + str(block_id) + "_albedo.png", averaged_texture)
	plt.imsave("map_textures/block_" + str(block_id) + "_displacement.png", averaged_bump)


	write_cubes(mesh, block_id, 2)


	# for i in range(3): averaged_texture[:, :, i] = weights_sum
	# mesh2 = light.wrap_texture2(mesh, averaged_texture)










