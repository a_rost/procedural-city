import numpy as np
import tools
from scipy.interpolate import RegularGridInterpolator
from trimesh import Trimesh
from trimesh.visual import texture, TextureVisuals
from PIL import Image

def load_meshes(lod, usefile = True, nblocks = 0):
	meshes = []
	if usefile:
		info = np.genfromtxt("data/blocks/file_list.txt", skip_header=2)
		nblocks = len(info[:, 0])
	for i in range(nblocks):
		if usefile and info[i, lod + 1] == 1.0:
			mesh = np.load("data/blocks/mesh_block_" + str(i) + "_lod" + str(lod) + ".npy", allow_pickle = True).item()
			print([info[i, 4], info[i, 5], 0.])

			meshes.append(mesh)
		else:
			mesh = np.load("data/blocks/mesh_block_" + str(i) + "_lod" + str(lod) + ".npy", allow_pickle = True).item()

			meshes.append(mesh)

	return meshes

def load_mesh(lod, block_id):
	mesh = np.load("data/blocks/mesh_block_" + str(block_id) + "_lod" + str(lod) + ".npy", allow_pickle = True).item()

	return mesh

def load_all(lod, usefile = True, nblocks = 0):
	result = tools.Mesh()
	if usefile:
		info = np.genfromtxt("data/blocks/file_list.txt", skip_header=2)
		nblocks = len(info[:, 0])
	for i in range(nblocks):
		for _lod in range(lod, 3):
			if usefile and info[i, _lod + 1] == 1.0:
				result += load_mesh(_lod, i)
				break
			elif not(usefile):
				result += load_mesh(_lod, i)
				break

	return result


def interpolate(array1, extent1, extent2, res2):
	x_space = np.linspace(extent1[0], extent1[1], len(array1))
	y_space = np.linspace(extent1[2], extent1[3], len(array1[0]))

	x_space2 = np.linspace(extent2[0], extent2[1], res2[0])
	y_space2 = np.linspace(extent2[2], extent2[3], res2[1])

	qwe = RegularGridInterpolator(points = (x_space, y_space), values = array1, bounds_error = False, fill_value = 0.)

	X2, Y2 = np.meshgrid(x_space2, y_space2, indexing = "ij")
	array2 = qwe((X2.flatten(), Y2.flatten()))
	array2 = np.reshape(array2, res2)

	# array2[array2 < array1.min()] = array1.min()
	# array2[array2 > array1.max()] = array1.max()

	return array2



def wrap_texture(quantity, map_u, map_v, res):
	texture_1D = np.zeros(res[0]*res[1], dtype = np.float32)
	quantity_1D = quantity.flatten()
	map_u_1D    = map_u.flatten()
	map_v_1D    = map_v.flatten()

	i, j = np.int32(map_u_1D*res[0]), np.int32(map_v_1D*res[1])
	idx    = i*res[1] + j
	idx[idx >= res[0]*res[1]] += -res[0]*res[1]
	np.add.at(texture_1D, idx, quantity_1D)
	# texture_1D[idx] = quantity_1D

	return np.reshape(texture_1D, res)

def correct_windows(mesh_lod0):
	mat_windows = ["Yard_Door", "Offices2", "Offices_Interior", "Normal_Interior", "Curtains_Interior", "Blind_Interior"]

	mask_windows = np.array([item in mat_windows for item in mesh_lod0.materials])
	idx_array = np.arange(len(mask_windows))[mask_windows]

	mask_windows = np.isin(mesh_lod0.submesh_ids, idx_array)

	return mask_windows


def change_uvs(submesh):
	# uvs = np.copy(submesh.vertices[:, 0:2])
	uvs = submesh.uv2*1.0
	# uvs[:, 0] = submesh.uv2[:, 0]
	uvs[:, 1] = 1 - submesh.uv2[:, 1]
	submesh.uvs = uvs

def get_texture(my_uvs, img):
	material = texture.SimpleMaterial(image=img)
	tex = TextureVisuals(uv=my_uvs, image=img, material=material)
	return tex

def wrap_texture2(submesh, texture):
	M, N = len(texture), len(texture[0])

	change_uvs(submesh)
	# submesh.uvs = 1 - submesh.uv2
	submesh.calculate_normals()

	img = np.ones([N, M, 3], dtype = np.uint8)

	for i in range(3):
		img[:, :, i] = texture[:, :, i].T*255

	img = Image.fromarray(img)
	sqrWidth = np.ceil(np.sqrt(img.size[0]*img.size[1])).astype(int)
	im_resize = img.resize((sqrWidth, sqrWidth))

	texture_visual = get_texture(submesh.uvs, im_resize)
	mesh = Trimesh(vertices=submesh.vertices, faces=submesh.triangles, vertex_normals = submesh.normals, visual = texture_visual)

	mesh.show(background = [0, 0, 0, 255])
	return mesh


def rotate_mesh(mesh, theta, phi):
	vertices, triangles, normals = mesh.vertices,  mesh.triangles, mesh.normals
	vertices = tools.rotate(vertices, tools.vec_up, phi*tools.deg2rad)
	vertices = tools.rotate(vertices, tools.vec_right, theta*tools.deg2rad)

	normals = tools.rotate(normals, tools.vec_up, phi*tools.deg2rad)
	normals = tools.rotate(normals, tools.vec_right, theta*tools.deg2rad)

	return vertices, normals, triangles



def fill_in_holes(broken_array_orig, mask_mapping, nitera):
	# Fills in holes in textures, taking into account what parts of the texture are being used
	reference = np.zeros(shape = broken_array_orig.shape, dtype = np.float32)

	broken_array = broken_array_orig*1.

	for i in range(nitera):
		mask_holes = (broken_array == 0.)*mask_mapping

		reference *= 0.
		reference[~mask_holes] = 1.

		roll1 = np.roll(broken_array, shift = 1, axis = 0)
		ref1  = np.roll(reference,    shift = 1, axis = 0)

		roll2 = np.roll(broken_array, shift = -1, axis = 0)
		ref2  = np.roll(reference,    shift = -1, axis = 0)

		roll3 = np.roll(broken_array, shift = 1, axis = 1)
		ref3  = np.roll(reference,    shift = 1, axis = 1)

		roll4 = np.roll(broken_array, shift = -1, axis = 1)
		ref4  = np.roll(reference,    shift = -1, axis = 1)


		count = (ref1 + ref2 + ref3 + ref4)*mask_holes
		fill  = (roll1 + roll2 + roll3 + roll4)/count
		fill[~mask_holes] = 0.

		fill[~np.isfinite(fill)] = 0.
		broken_array += fill
	return broken_array
