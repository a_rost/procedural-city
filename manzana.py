import numpy as np
import tools
from shapely.geometry import Polygon, LineString
from scipy.interpolate import interp2d

deg2rad = np.pi/180.


maximum_width = 17.
minimum_width = 8.
delta_block   = 1.


def from_world_to_local(left_corner, orientation, positions):
	u = np.array([np.cos(orientation), np.sin(orientation)])
	v = np.array([-np.sin(orientation), np.cos(orientation)])

	offset_positions = np.zeros(positions.shape)
	new_positions    = np.zeros(positions.shape)

	offset_positions[:, 0] = positions[:, 0] -left_corner[0]
	offset_positions[:, 1] = positions[:, 1] -left_corner[1]

	new_positions[:, 0] = u[0]*offset_positions[:, 0] + u[1]*offset_positions[:, 1]
	new_positions[:, 1] = v[0]*offset_positions[:, 0] + v[1]*offset_positions[:, 1]
	return new_positions

def normalize_by_gradient(distribution, extent):
	dx = (extent[1] - extent[0])/len(distribution)
	dy = (extent[3] - extent[2])/len(distribution[0])
	grad = np.gradient(distribution)
	grad[0] *= 1./dx
	grad[1] *= 1./dy

	factor = np.sqrt(grad[0]**2 + grad[1]**2)
	distribution *= 1/factor.max()
	distribution += -distribution.mean()

	return distribution

deviation_x, deviation_y = 0., 0.
def create_distorsion_function(extent, distortion_strength = 0.5):
	global deviation_x, deviation_y

	deviation_x, size_x, size_y = tools.create_distribution(extent, 200.)
	deviation_y, size_x, size_y = tools.create_distribution(extent, 200.)

	deviation_x = normalize_by_gradient(deviation_x, extent)
	deviation_y = normalize_by_gradient(deviation_y, extent)

	distortion_x = interp2d(x = np.linspace(extent[0], extent[1], 100), y = np.linspace(extent[2], extent[3], 100), z = (deviation_x - 0.5)*distortion_strength)
	distortion_y = interp2d(x = np.linspace(extent[0], extent[1], 100), y = np.linspace(extent[2], extent[3], 100), z = (deviation_y - 0.5)*distortion_strength)


	transform_coords = lambda x, y : (x + distortion_x(x, y), y + distortion_y(x, y))

	transform_coords = np.vectorize(transform_coords)

	return transform_coords, size_x, size_y

transform_coords, size_x, size_y = 0., 0., 0.
def initialize_maps(layout, distortion = 0.05):
	global transform_coords, size_x, size_y

	street_width = 10.
	size_x = len(layout[0])*street_width + sum(layout[0])
	size_y = len(layout[1])*street_width + sum(layout[1])
	extent = [0., size_x, 0., size_y]

	transform_coords, size_x, size_y = create_distorsion_function(extent, distortion)
	# transform_coords = lambda x, y: (x*0.8 + 0.2*y, x*0.2 + 0.8*y)
	# transform_coords = np.vectorize(transform_coords)


def get_real_positions(transform_coords, left_corner, width, orientation, positions):
	u = np.array([np.cos(orientation*deg2rad), np.sin(orientation*deg2rad)])
	v = np.array([-np.sin(orientation*deg2rad), np.cos(orientation*deg2rad)])

	world_positions     = np.zeros(positions.shape)
	distorted_positions = np.zeros(positions.shape)
	offset_positions    = np.zeros(positions.shape)
	new_positions       = np.zeros(positions.shape)

	for i in range(2): world_positions[:, i] = left_corner[i] + u[i]*positions[:, 0] + v[i]*positions[:, 1]

	distorted_positions[:, 0], distorted_positions[:, 1] = transform_coords(world_positions[:, 0], world_positions[:, 1])

	front_pos = np.array([[0., 0.], [width, 0.]])
	world_front = np.zeros([2,2])
	distort_front = np.zeros([2, 2])

	for i in range(2): world_front[:, i] = left_corner[i] + u[i]*front_pos[:, 0] + v[i]*front_pos[:, 1]

	distort_front[:, 0], distort_front[:, 1] = transform_coords(world_front[:, 0], world_front[:, 1])

	front_seg = [distort_front[1, 0] - distort_front[0, 0], distort_front[1, 1] - distort_front[0, 1]]

	new_u = np.array(front_seg)/np.linalg.norm(front_seg)
	new_v = np.zeros(2)
	new_v[0] = -new_u[1]
	new_v[1] = new_u[0]

	new_leftcorner  = distort_front[0]

	offset_positions[:, 0] = distorted_positions[:, 0] -new_leftcorner[0]
	offset_positions[:, 1] = distorted_positions[:, 1] -new_leftcorner[1]

	new_positions[:, 0] = new_u[0]*offset_positions[:, 0] + new_u[1]*offset_positions[:, 1]
	new_positions[:, 1] = new_v[0]*offset_positions[:, 0] + new_v[1]*offset_positions[:, 1]

	new_orientation = np.arctan2(new_u[1], new_u[0])*180/np.pi

	return new_positions, new_orientation, new_leftcorner

class Parcel:
	def __init__(self, idx, depth, cells, xcoord, ycoord, direction, block_origin):
		global count
		self.kind = None
		self.idx   = idx     #id of the parcel
		self.name  = "Parcel_" + str(self.idx)
		self.depth = depth   #maximum cell depth
		self.cells = cells   #coordinates of cells

		n_cells = len(cells)
		front, rear = cells[n_cells - 1], cells[0]

		cell_polygons = []
		for cell in cells:
			i, j = cell[1], cell[0]
			coords = [[xcoord[i], ycoord[j]], [xcoord[i + 1], ycoord[j]], [xcoord[i + 1], ycoord[j + 1]], [xcoord[i], ycoord[j + 1]], [xcoord[i], ycoord[j]]]

			cell_polygons.append(tools.Polygon(coords).buffer(10e-8, join_style = 2))


		self.front = front
		self.rear  = rear
		self.length = det_terrain_length(rear[0], rear[1], direction, xcoord, ycoord)
		self.width  = det_terrain_width( rear[0], rear[1], direction, xcoord, ycoord)

		# self.parcel_shape = Polygon([[0., 0.], [self.width, 0.], [self.width, self.length], [0., self.length], [0., 0.]])
	
		self.medianera   = LineString([[self.width, 0.], [self.width, self.length], [0., self.length], [0., 0.]])
		self.form_factor = self.length/self.width
		self.area        = self.width*self.length
		
		i, j = front[0], front[1]

		if direction == 0:
			self.facing   = "down"
			self.rotation = 0.
			self.left_corner   = [xcoord[j], ycoord[0], 0.]
		elif direction == 1:
			self.facing   = "right"
			self.rotation = 90.
			self.left_corner   = [xcoord[-1], ycoord[i], 0.]
		elif direction == 2:
			self.facing   = "up"
			self.rotation = 180.
			self.left_corner   = [xcoord[j + 1], ycoord[-1], 0.]
		else:
			self.facing   = "left"
			self.rotation = 270.
			self.left_corner   = [xcoord[0], ycoord[i + 1], 0.]

		self.left_corner = np.array(self.left_corner)

		N, M = len(ycoord), len(xcoord)
		self.is_corner, self.corner_direction = False, None
		
		if i == 0 and j == 0:
			self.is_corner = True
			if self.facing == "down":
				self.corner_direction = "left"
			elif self.facing == "left":
				self.corner_direction = "right"

		elif i == 0 and j == M - 2:
			self.is_corner = True
			if self.facing == "right":
				self.corner_direction = "left"
			elif self.facing == "down":
				self.corner_direction = "right"

		elif i == N - 2 and j == M - 2:
			self.is_corner = True
			if self.facing == "up":
				self.corner_direction = "left"
			elif self.facing == "right":
				self.corner_direction = "right"

		elif i == N - 2 and j == 0:
			self.is_corner = True
			if self.facing == "left":
				self.corner_direction = "left"
			elif self.facing == "up":
				self.corner_direction = "right"

		if self.is_corner:
			if self.corner_direction == "left":
				self.front       = LineString([[0., self.length], [0., 0.], [self.width, 0.]])
				self.medianera   = LineString([[self.width, 0.], [self.width, self.length], [0., self.length]])
			else:
				self.front       = LineString([[0., 0.], [self.width, 0.], [self.width, self.length]])
				self.medianera   = LineString([[self.width, self.length], [0., self.length], [0., 0.]])
		else:
			self.front       = LineString([[0., 0.], [self.width, 0.]])

		self.old_block_origin = np.array(block_origin)
		#### Distortion
		block_origin   = np.array(block_origin)
		new_blockorigin= np.zeros([2])

		width, rotation = self.width, self.rotation
		parcel_shape2  = np.array(tools.unary_union(cell_polygons).exterior.coords)

		new_medianera = (tools.unary_union(cell_polygons).exterior).difference(self.front.buffer(1e-8, join_style = 2))
		N, M = len(xcoord), len(ycoord)
		manzana_coords2 = np.zeros([2*N + 2*M, 2])

		manzana_coords2[0:N, 0] = xcoord
		manzana_coords2[0:N, 1] = 0.
		manzana_coords2[N:N + M, 0] = xcoord.max()
		manzana_coords2[N:N + M, 1] = ycoord
		manzana_coords2[N + M:2*N + M, 0] = xcoord[::-1]
		manzana_coords2[N + M:2*N + M, 1] = ycoord.max()
		manzana_coords2[2*N + M:2*N + 2*M, 0] = 0.
		manzana_coords2[2*N + M:2*N + 2*M, 1] = ycoord[::-1]

		parcel_coords = from_world_to_local(self.left_corner, self.rotation*np.pi/180, parcel_shape2)
		manzana_coords= from_world_to_local(self.left_corner, self.rotation*np.pi/180, manzana_coords2)

		medianera_coords = np.array(self.medianera.coords)
		# medianera_coords = np.array(new_medianera.coords)
		front_coords     = np.array(self.front.coords)
		new_blockorigin[0], new_blockorigin[1]  = transform_coords([block_origin[0]], [block_origin[1]])


		parcel_pos,    new_orientation, new_leftcorner = get_real_positions(transform_coords, self.left_corner + block_origin, width, rotation, parcel_coords)
		medianera_pos, new_orientation, new_leftcorner = get_real_positions(transform_coords, self.left_corner + block_origin, width, rotation, medianera_coords)
		front_pos,     new_orientation, new_leftcorner = get_real_positions(transform_coords, self.left_corner + block_origin, width, rotation, front_coords)
		manzana_pos,   new_orientation, new_leftcorner = get_real_positions(transform_coords, self.left_corner + block_origin, width, rotation, manzana_coords)



		perimeter = tools.LineString(parcel_pos)
		manzana   = tools.LineString(manzana_pos)

		#tools.plot_polygon(manzana)
		self.parcel_shape = tools.Polygon(parcel_pos)
		self.front        = perimeter.intersection(manzana.buffer(10e-6, join_style = 2))
		self.medianera    = perimeter.difference(manzana.buffer(10e-6, join_style = 2))
		self.area         = self.parcel_shape.area
		self.length       = parcel_pos[:, 1].max()
		self.width        = parcel_pos[:, 0].max() - parcel_pos[:, 0].min()
		self.avg_width    = self.parcel_shape.area/self.length
		self.middle       = parcel_pos[:, 0].min() + self.width/2.
		self.form_factor  = self.length/self.width
		self.rotation     = new_orientation
		length, width, self.orientation, coords, centroid = tools.get_orientation(self.parcel_shape)

		self.left_corner  = np.array([new_leftcorner[0] - new_blockorigin[0], new_leftcorner[1] - new_blockorigin[1], 0.])
		self.block_origin = np.array([new_blockorigin[0], new_blockorigin[1], 0.])

		self.local_i = np.array([ np.cos(self.rotation*deg2rad), np.sin(self.rotation*deg2rad), 0.])
		self.local_j = np.array([-np.sin(self.rotation*deg2rad), np.cos(self.rotation*deg2rad), 0.])
		self.local_k = np.array([0., 0., 1])

def transform_shape(shape, block_origin):
	if shape.type == "Polygon":
		rings = tools.get_rings(shape)
		new_rings = []
		for ring in rings:
			ring = np.array(ring)
			new_ring = np.zeros(ring.shape)

			new_ring[:, 0], new_ring[:, 1] = transform_coords(ring[:, 0] + block_origin[0], ring[:, 1] + block_origin[1])
			new_rings.append(new_ring)

		return tools.Polygon(new_rings[0], holes = new_rings[1:])
	else:
		return tools.Polygon()
debug_shapes = 9
def get_manzana_shapes(xcoord, ycoord, block_origin):
	global debug_shapes
	N, M = len(xcoord), len(ycoord)
	street_width, sidewalk_width = 10., 3.

	manzana_coords2 = np.zeros([2*N + 2*M, 2])

	manzana_coords2[0:N, 0] = xcoord
	manzana_coords2[0:N, 1] = 0.
	manzana_coords2[N:N + M, 0] = xcoord.max()
	manzana_coords2[N:N + M, 1] = ycoord
	manzana_coords2[N + M:2*N + M, 0] = xcoord[::-1]
	manzana_coords2[N + M:2*N + M, 1] = ycoord.max()
	manzana_coords2[2*N + M:2*N + 2*M, 0] = 0.
	manzana_coords2[2*N + M:2*N + 2*M, 1] = ycoord[::-1]


	n, m = 30, 30
	total_coords2 = np.zeros([2*n + 2*m, 2])
	X = np.linspace(-street_width/2., xcoord.max() + street_width/2., n)
	Y = np.linspace(-street_width/2., ycoord.max() + street_width/2., m)
	total_coords2[0:n, 0] = X
	total_coords2[0:n, 1] = Y.min()
	total_coords2[n:n + m, 0] = X.max()
	total_coords2[n:n + m, 1] = Y
	total_coords2[n + m:2*n + m, 0] = X[::-1]
	total_coords2[n + m:2*n + m, 1] = Y.max()
	total_coords2[2*n + m:2*n + 2*m, 0] = X.min()
	total_coords2[2*n + m:2*n + 2*m, 1] = Y[::-1]

	manzana_coords_world = np.zeros(manzana_coords2.shape)

	undistorted_manzana  = tools.Polygon(manzana_coords2)
	undistorted_sidewalk = tools.orient(undistorted_manzana.buffer(sidewalk_width).difference(undistorted_manzana))
	undistorted_street   = tools.Polygon(total_coords2)

	manzana  = transform_shape(undistorted_manzana,  block_origin)
	sidewalk = transform_shape(undistorted_sidewalk, block_origin)
	street   = transform_shape(undistorted_street,   block_origin)

	debug_shapes = manzana, sidewalk, street
	return manzana, sidewalk, street




def generate_xspace(cuadra_size = 110.):
	xcoord = [0., cuadra_size, 15., cuadra_size - 15.]
	
	valid = True
	
	new_xpos1, new_xpos2 = 15., cuadra_size - 15.
	
	while valid:
		new_xpos1 += tools.suggest_value(minimum_width, maximum_width, 8, 2.5, granularity = delta_block)
		if(new_xpos2 - new_xpos1 < minimum_width):
			valid = False
		else:
			xcoord.append(new_xpos1)
		new_xpos2 += -tools.suggest_value(minimum_width, maximum_width, 8, 2.5, granularity = delta_block)
		if(new_xpos2 - new_xpos1 < minimum_width):
			valid = False
		else:
			xcoord.append(new_xpos2)
	xcoord = np.array(xcoord)
	xcoord.sort()
	return xcoord

def det_posibility(I, J, N, M, direction, terrains, level):
	# direction = 0 (down), 1 (right), 2 (up), 3 (left)
	valid = True

	if terrains[I, J] != 0:
		valid = False

	else:
		if direction == 0:
			for i in range(0, I + 1):
				if terrains[i, J] != 0:
					valid = False
					break
		elif direction == 1:
			for j in range(J, M):
				if terrains[I, j] != 0:
					valid = False
					break
		elif direction == 2:
			for i in range(I, N):
				if terrains[i, J] != 0:
					valid = False
					break
		else:
			for j in range(0, J + 1):
				if terrains[I, j] != 0:
					valid = False
					break

		if level == 0:
			if I == 0 and J <= 1:
				if direction == 1 or direction == 2:
					valid = False
			elif I == 0 and J >= M - 2:
				if direction == 2 or direction == 3:
					valid = False
			elif I == 0:
				if direction != 0:
					valid = False

			if J == 0 and I <= 1:
				if direction == 1 or direction == 2:
					valid = False
			elif J == 0 and I >= N - 2:
				if direction == 0 or direction == 1:
					valid = False
			elif J == 0:
				if direction != 3:
					valid = False

			if I == N - 1 and J <= 1:
				if direction == 0 or direction == 1:
					valid = False
			elif I == N - 1 and J >= M - 2:
				if direction == 0 or direction == 3:
					valid = False
			elif I == N - 1:
				if direction != 2:
					valid = False
				
			if J == M - 1 and I <= 1:
				if direction == 2 or direction == 3:
					valid = False
			elif J == M - 1 and I >= N - 2:
				if direction == 0 or direction == 3:
					valid = False
			elif J == M - 1:
				if direction != 1:
					valid = False

	return valid

def det_terrain_length(I, J, direction, xcoord, ycoord):
	if direction == 0:
		length = ycoord[I + 1]
	elif direction == 1:
		length = xcoord[-1] - xcoord[J]
	elif direction == 2:
		length = ycoord[-1] - ycoord[I]
	else:
		length = xcoord[J + 1]
	return length


def det_terrain_width(I, J, direction, xcoord, ycoord):
	if direction == 0:
		width = xcoord[J + 1] - xcoord[J]
	elif direction == 1:
		width = ycoord[I + 1] - ycoord[I]
	elif direction == 2:
		width = xcoord[J + 1] - xcoord[J]
	else:
		width = ycoord[I + 1] - ycoord[I]
	return width



def ocupy_terrain(I, J, N, M, direction_chosen, terrains, idx):
	parcel = []
	if direction_chosen == 0:
		for i in np.arange(I, -1, -1):
			parcel.append((i, J))
	elif direction_chosen == 1:
		for j in range(J, M):
			parcel.append((I, j))
	elif direction_chosen == 2:
		for i in range(I, N):
			parcel.append((i, J))
	else:
		for j in np.arange(J, -1, -1):
			parcel.append((I, j))
	return parcel
 


def find_cell_idx(cells, cell):
	for idx in range(len(cells)):
		if cell == cells[idx]:
			break

	return idx

def check_continuity(cells):
	## Assumming last cell is one of the extremes.
	check = np.zeros(len(cells), dtype = bool)
	check[len(cells) - 1] = True
	for i in range(len(cells)):
		for k in range(len(cells)):
			if check[k]:
				cell  = cells[k]
				up    = (cell[0], cell[1] + 1)
				down  = (cell[0], cell[1] - 1)
				right = (cell[0] + 1, cell[1])
				left  = (cell[0] - 1, cell[1])

				if up    in cells: check[find_cell_idx(cells, up)]    = True
				if down  in cells: check[find_cell_idx(cells, down)]  = True
				if right in cells: check[find_cell_idx(cells, right)] = True
				if left  in cells: check[find_cell_idx(cells, left)]  = True
	# print(sum(check), len(cells))
	if len(cells) == sum(check):
		return True
	else:
		return False



def det_if_neighbors(cells1, cells2, dir1, dir2):
	value = False
	last1, last2 = len(cells1) - 1, len(cells2) - 1
	if dir1 == dir2:
		if dir1 in [0, 2]:
			diff = abs(cells1[last1][1] - cells2[last2][1])
			# print(diff)
			if diff == 1:
				value = True

		else:
			diff = abs(cells1[last1][0] - cells2[last2][0])
			# print(diff)
			if diff == 1:
				value = True

	return value


def exchange_cells(cells1, cells2, depth1, depth2):
	amount_to_take, exchange = 0, False
	ncells2 = len(cells2)
	if ncells2 - 2 > max(depth2 - depth1 + 1, 0):
		amount_to_take = np.random.randint(max(depth2 - depth1 + 1, 0), ncells2 - 2)
		if amount_to_take != 0:
			new_parcel1_cells, new_parcel2_cells = [], []
			for i in range(amount_to_take):
				new_parcel1_cells.append(cells2[i])
			for cell in cells1:
				new_parcel1_cells.append(cell)
			for i in range(amount_to_take, ncells2):
				new_parcel2_cells.append(cells2[i])

			if check_continuity(new_parcel1_cells) and check_continuity(new_parcel2_cells):
				exchange = True

	if exchange:
		return new_parcel1_cells, new_parcel2_cells, min(depth1, depth2), max(depth1, depth2)
	else:
		return cells1, cells2, depth1, depth2


def generate_exchange(cells, directions, depths, nexchanges = 1):
	counter, value = 0, False

	for i in range(3*nexchanges):
		idx1 = np.random.randint(0, len(cells))
		cells1, dir1, depth1 = cells[idx1], directions[idx1], depths[idx1]

		for idx2 in range(len(cells)):
			cells2, dir2, depth2 = cells[idx2], directions[idx2], depths[idx2]

			value = det_if_neighbors(cells1, cells2, dir1, dir2)
			if value:
				break
		if value:
			cells[idx1], cells[idx2], depths[idx1], depths[idx2] = exchange_cells(cells1, cells2, depth1, depth2)

			counter += 1

def distribute_terrains(xcoord, ycoord, block_origin = [0., 0.]):
	M, N = len(xcoord) - 1, len(ycoord) - 1
	
	terrains = np.zeros([N, M], dtype = np.int)
	terrains_depths = np.zeros([N, M], dtype = np.int)
	
	n_xlevels = int(M/2) if M%2 == 0 else int(M/2) + 1
	n_ylevels = int(N/2) if N%2 == 0 else int(N/2) + 1
	
	n_levels = min(n_xlevels, n_ylevels)
	
	coords_per_level = []
	for k in range(n_levels):
		k_set = []
		coords_per_level.append(k_set)
		for i in range(k, N - k):
			for j in range(k, M - k):
				terrains_depths[i, j] = k

	for i in range(N):
		for j in range(M):
			k = terrains_depths[i, j]
			coords_per_level[k].append((i, j))

	
	idx, directions, cells, depths = 1, [], [], []
	for k in np.flip(np.arange(n_levels)):
		rand_parcels = np.random.permutation(np.array(coords_per_level[k]))

		n_current_level = len(rand_parcels)
		
		for p in range(n_current_level):
			I, J = rand_parcels[p][0], rand_parcels[p][1]

			direction_probabilities = np.ones(4)
			for direction in range(4):
				length = det_terrain_length(I, J, direction, xcoord, ycoord)
				direction_probabilities[direction] = 1./length**2
				direction_probabilities[direction] *= det_posibility(I, J, N, M, direction, terrains, k)

			if np.sum(direction_probabilities) != 0:
				direction_chosen = tools.weigh_decision(direction_probabilities)
				parcel_cells = ocupy_terrain(I, J, N, M, direction_chosen, terrains, idx)
				
				for cell in parcel_cells:
					i, j = cell[0], cell[1]
					terrains[i, j] = idx

				depths.append(k)
				cells.append(parcel_cells)
				directions.append(direction_chosen)
				idx += 1

	generate_exchange(cells, directions, depths, nexchanges = 0)
	parcels, idx = [], 1
	for i in range(len(directions)):
		k, direction_chosen, parcel_cells = depths[i], directions[i], cells[i]
		parcel = Parcel(idx, depth = k, cells = parcel_cells, xcoord = xcoord, ycoord = ycoord, direction = direction_chosen, block_origin = block_origin)
		parcels.append(parcel)
		idx += 1

	return terrains, parcels

def generate_manzana(cuadra = [110, 110], block_origin = [0., 0.]):
	xcoord = generate_xspace(cuadra[0])
	ycoord = generate_xspace(cuadra[1])

	terrains,  parcels = distribute_terrains(xcoord, ycoord, block_origin)
	
	#plt.imshow(terrains)
	return xcoord, ycoord, terrains, parcels



def plot_block(xcoord, ycoord, terrains, parcels):
	import matplotlib.pyplot as plt
	plt.ion()
	fig, axs = plt.subplots(ncols = 1, nrows = 1)

	count = 0
	for parcel in parcels:
		cells = parcel.cells
		color = np.random.random(3)*0.5 + np.ones(3)*0.5
		for item in cells:
			x_min = xcoord[item[1]]
			x_max = xcoord[item[1] + 1]
			y_min = ycoord[item[0]]
			y_max = ycoord[item[0] + 1]
			ring  = np.array([[x_min, y_min], [x_max, y_min], [x_max, y_max], [x_min, y_max], [x_min, y_min]])
			axs.fill(ring[:, 0], ring[:, 1], color = color, alpha = 0.9)
		count += 1
