import numpy as np
import matplotlib.pyplot as plt
import triangle as tr
from scipy.interpolate import interp2d
from scipy.spatial import Delaunay
import shapely
from shapely.geometry import box, Polygon, LineString, Point, MultiPolygon, MultiLineString
from shapely.ops import nearest_points, unary_union, linemerge
import modulo
import xatlas

from trimesh import Trimesh
import time
plt.ion()

normal_estimation_totaltime = 0.
triangle_checking_totaltime = 0.

deg2rad, rad2deg = np.pi/180., 180./np.pi
vec_right = np.array([ 1,  0,  0])
vec_left  = np.array([-1,  0,  0])
vec_front = np.array([ 0, -1,  0])
vec_back  = np.array([ 0,  1,  0])
vec_up    = np.array([ 0,  0,  1])
vec_down  = np.array([ 0,  0, -1])


def pick_random(cum_inverse):
	return int(cum_inverse(np.random.random())/delta_block)*delta_block

def suggest_value(minimum, maximum, mean, deviation, granularity = 0.5):
	size = np.random.normal(loc = mean, scale = deviation)
	
	size = max(size, minimum)
	size = min(size, maximum)
	
	return int(size/granularity)*granularity
 
def weigh_decision(prob_original):
	probabilities = np.array(prob_original, dtype = np.float32)
	normalization = np.sum(probabilities)
	probabilities *= 1./normalization
	indexes = np.argsort(probabilities)
	probabilities.sort()
	cumsum = np.cumsum(probabilities)
	value = np.random.random()
	decision = indexes[-1]
	for i in range(len(probabilities)):
		if cumsum[i] >= value:
			decision = indexes[i]
			break
	return decision

def weigh_output(elements, prob_original):
	return elements[weigh_decision(prob_original)]

def generate_random_levels(value, N):
	randoms = np.random.random(N - 1)

	accum = np.array([0] + list(np.cumsum(randoms)))
	accum *= value/accum[-1]

	accum = np.unique(np.int32(accum))

	return accum

def pick_bool(prob):
	return np.random.random() <= prob

def rand_pick(lista):
	return lista[np.random.randint(len(lista))]

def random_pastel_generator():
	## 50% probability to generate white building
	if pick_bool(0.5):
		return np.array([0., 0., 0.])
	else:
		R = np.random.random()
		G = np.random.random()**2
		B = np.random.random()**3
		return np.array([R, G, B])*0.15
	
def invert_coord(x, parcel_width, inverted):
	if inverted:
		return parcel_width - x
	else:
		return x

def invert_building(parcel, kind, invert = False):
	for primitive in kind.cubes:
		if invert:
			primitive.local_center[0] = parcel.width - primitive.local_center[0]
			if primitive.primitive == "window":
				primitive.local_rotation *= -1.

#def det_obstacles(segment, polygon):
	

############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################




def plt_ring(ring, color, linewidth, variable_color = False, axs = None):
	N = len(ring.coords)
	if N > 0:
		ant_point = ring.coords[0]
		for i in range(1, N + 1):
			if i == N:
				pos_point = ring.coords[0]
			else:
				pos_point = ring.coords[i]
				
			if variable_color:
				new_color = color*i/N
				alpha = 0.3
			else:
				new_color = color
				alpha = 1.
			if type(axs) == type(None):
				plt.plot([ant_point[0], pos_point[0]], [ant_point[1], pos_point[1]], color = new_color, linewidth = linewidth, alpha = alpha)
			else:
				axs.plot([ant_point[0], pos_point[0]], [ant_point[1], pos_point[1]], color = new_color, linewidth = linewidth, alpha = alpha)
			ant_point = pos_point

def plt_string(ring, color, linewidth = 1, variable_color = False, axs = None):
	N = len(ring.coords)
	if N > 0:
		ant_point = ring.coords[0]
		for i in range(1, N):
			pos_point = ring.coords[i]

			if variable_color:
				new_color = color*i/N
				alpha = 0.3
			else:
				new_color = color
				alpha = 1.
			if type(axs) == type(None):
				plt.plot([ant_point[0], pos_point[0]], [ant_point[1], pos_point[1]], color = new_color, linewidth = linewidth, alpha = alpha)
			else:
				axs.plot([ant_point[0], pos_point[0]], [ant_point[1], pos_point[1]], color = new_color, linewidth = linewidth, alpha = alpha)
			ant_point = pos_point


def plot_polygon(polygon_object, linewidth = 1, variable_color = False, axs = None):
	if not (polygon_object.__class__ in [shapely.geometry.polygon.Polygon, shapely.geometry.linestring.LineString]):
		for item in polygon_object:
			plot_polygon(item, linewidth, variable_color, axs)
	else:
		if polygon_object.__class__ == shapely.geometry.polygon.Polygon:
			color_inside  = np.random.random(3)
			color_outside = np.random.random(3)

			#outside
			plt_ring(polygon_object.exterior, color_outside, linewidth, variable_color = variable_color, axs = axs)

			plt.axis('equal')

			#inside 
			for ring in polygon_object.interiors:
				plt_ring(ring, color_inside, linewidth, variable_color = variable_color, axs = axs)

			holes = get_holes(polygon_object)
			for center in holes:
				if type(axs) == type(None):
					plt.plot([center[0]], [center[1]], "o")
				else:
					axs.plot([center[0]], [center[1]], "o")


		elif polygon_object.__class__ == shapely.geometry.linestring.LineString:
			plt_string(polygon_object, np.array([0., 0., 0.]), variable_color = variable_color, axs = axs, linewidth = linewidth)
			

def plot_polygon2(axs, polygon, color = "k", alpha = 1.):
	if polygon.__class__ != shapely.geometry.polygon.Polygon:
		for item in polygon:
			plot_polygon2(axs, item)
	else:
		ring = np.array(get_rings(polygon)[0])
		axs.fill(ring[:, 0], ring[:, 1], color = color, alpha = alpha)
			

def plot_facets(building, level = "all"):
	facets = building.facets
	
	if level == "all":
		levels = building.levels
	else:
		levels = [building.levels[level]]
	for facet in facets:
		if facet.kind == "horizontal":
			if facet.level in levels:
				plot_polygon(facet.polygon)

		if facet.kind == "vertical" and facet.facet_adjacency == False:
			segment = facet.edge
			if facet.min_height in levels:
				plt.plot([segment[0][0], segment[1][0]], [segment[0][1], segment[1][1]], label = "id = " + str(facet.idx))
	plt.legend()



def plot_triangulation(polygon):
	triangles = triangulate_within(polygon)
	rings = get_rings(polygon)
	vertices = []
	for ring in rings:
		vertices += [np.array([vert[0], vert[1]]) for vert in ring]

	vertices = np.array(vertices)
	plot_polygon(polygon)
	plt.triplot(vertices[:, 0], vertices[:, 1], triangles)

def plot_rotated_shape(axs, polygon, center, rotation, color = "k", alpha = 1.):
	if polygon.is_empty == False:
		rings = get_rings(polygon)
		new_rings = []
		versor_i = np.array([ np.cos(rotation*deg2rad), np.sin(rotation*deg2rad)])
		versor_j = np.array([-np.sin(rotation*deg2rad), np.cos(rotation*deg2rad)])

		for i in range(len(rings)):
			
			ring = np.array(rings[i])
			coords = np.zeros([len(ring), 2])
			
			coords[:, 0] = ring[:, 0]*versor_i[0] + ring[:, 1]*versor_j[0] + center[0]
			coords[:, 1] = ring[:, 0]*versor_i[1] + ring[:, 1]*versor_j[1] + center[1]

			new_rings.append(coords)
		new_polygon = Polygon(new_rings[0], new_rings[1:])
		
		plot_polygon2(axs, new_polygon, color = color, alpha = alpha)
			
		
def plot_parcels(parcels, extent):
	max_height = 100.
	i = 0
	width  = extent[1] - extent[0]
	height = extent[3] - extent[2]
	aspect_ratio = height/width
	dpi = 1
	fig = plt.figure(figsize=(width, height), dpi = dpi)
	axs = plt.subplot(111)

	axs.set_xlim(extent[0], extent[1])
	axs.set_ylim(extent[2], extent[3])

	axs.axis('off')
	plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
	
	plot_polygon2(axs, box(extent[0], extent[2], extent[1], extent[3]), color = np.zeros(3), alpha = 1.)
	for parcel in parcels:
		
		for k in range(len(parcel.building.built_shapes) - 1):
			shape = parcel.building.built_shapes[k]
			level = parcel.building.levels[k + 1]
			plot_rotated_shape(axs, shape, parcel.left_corner, parcel.rotation, alpha = 1., color = np.ones(3)*level/max_height)

		for facet in parcel.building.facets:
			if facet.kind == "horizontal":
				plot_rotated_shape(axs, facet.polygon, parcel.left_corner, parcel.rotation, alpha = 1., color = np.ones(3)*facet.level/max_height)
		i += 1
	#plt.savefig("height_map.png", bbox_inches = 'tight', pad_inches = -1)
	import io
	io_buf = io.BytesIO()
	fig.savefig(io_buf, format='raw', dpi=dpi)
	io_buf.seek(0)
	img_arr = np.reshape(np.frombuffer(io_buf.getvalue(), dtype=np.uint8),
						newshape=(int(fig.bbox.bounds[3]), int(fig.bbox.bounds[2]), -1))
	io_buf.close()
	return img_arr
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
debug_vertices, debug_triangles = 0., 0.

# def check_triangles(vertices_orig, triangles, criteria = 10e-6):
def check_triangles(vertices_orig, triangles, criteria = 1e-5):
	global triangle_checking_totaltime, debug_vertices, debug_triangles
	timer = time.time()
	
	vertices = np.array(vertices_orig, dtype = np.float32)
	new_triangles, new_vertices, N_triangles, N_vertices = [], [], len(triangles), len(vertices)
	
	if N_triangles != 0:
		debug_vertices, debug_triangles = vertices, triangles
		new_vertices, new_triangles, mask_vertices = modulo.check_triangles(vertices, triangles, criteria)
		mask_usage = mask_vertices != 0
	else:
		new_vertices  = np.zeros([0, 3], dtype = np.float32)
		new_triangles = np.zeros([0, 3], dtype = np.int32)
		mask_usage    = np.zeros(0,  dtype = bool)

	if (len(new_vertices) != N_vertices or len(new_triangles) != N_triangles) and N_triangles*N_vertices != 0:
		deleted_tris, deleted_verts = N_triangles - len(new_triangles), N_vertices - len(new_vertices)
		percent_tris, percent_verts = deleted_tris/N_triangles*100., deleted_verts/N_vertices*100.

	triangle_checking_totaltime += time.time() - timer
	return new_vertices, new_triangles, mask_usage



class SubMesh:
	def calculate_normals(self, optimized = True, check = True):
		if check:
			self.vertices, self.triangles, mask_validity = check_triangles(self.vertices, self.triangles)
			self.uvs       = self.uvs[mask_validity]
			self.colors    = self.colors[mask_validity]
			self.nverts    = self.vertices.shape[0]
			self.ntris     = self.triangles.shape[0]
			for attribute in self.additional_attributes:
				array = getattr(self, attribute)
				array = array[mask_validity]
				setattr(self, attribute, array)

		if optimized:
			self.normals   = modulo.calculate_normals(self.vertices, self.triangles)
		else:
			self.normals   = estimate_normals(self.vertices, self.triangles)

	def add_quantity(self, name, value, dtype, dim, per_vert = True):
		if per_vert:
			if dim == 1:
				array = np.zeros(len(self.vertices), dtype = dtype)
			else:
				array = np.zeros([len(self.vertices), dim], dtype = dtype)
		else:
			if dim == 1:
				array = np.zeros(len(self.triangles), dtype = dtype)
			else:
				array = np.zeros([len(self.triangles), dim], dtype = dtype)

		array[:] = value
		setattr(self, name, array)
		self.additional_attributes.append(name)

	def calculate_area(self, optimized = True):
		if optimized:
			self.area = modulo.calculate_area(self.vertices, self.triangles)
			return self.area

		else:
			self.area = 0.
			largest_segment, tri_id = 0., -1
			for i in range(len(self.triangles)):
				triangle = self.triangles[i]
				vert1, vert2, vert3 = self.vertices[triangle[0]], self.vertices[triangle[1]], self.vertices[triangle[2]]

				side1 = vert2 - vert1
				side2 = vert3 - vert1


				side3 = vert3 - vert2

				if(np.linalg.norm(side1) >= largest_segment or np.linalg.norm(side2) >= largest_segment or np.linalg.norm(side3) >= largest_segment):
					largest_segment = max(np.linalg.norm(side3), max(np.linalg.norm(side1), np.linalg.norm(side2)))
					tri_id = i

				product = np.cross(side1, side2)
				area    = np.linalg.norm(product)/2.
				self.area += area


			return self.area

	def flip(self):
		ntris = len(self.triangles)
		for i in range(ntris):
			triangle = self.triangles[i]
			self.triangles[i] = np.array([triangle[0], triangle[2], triangle[1]])

	def __init__(self, vertices = [], triangles = [], uvs = [], colors = [], material = "default"):
		nvert, ntris = len(vertices), len(triangles)
		self.additional_attributes = []
		self.nverts = nvert
		self.ntris  = ntris

		self.material  = material
		self.normals = np.zeros([nvert, 3], dtype = np.float32)

		if nvert*ntris != 0:
			self.vertices  = np.array(vertices,  dtype = np.float32)
			self.triangles = np.array(triangles, dtype = np.int32)
			self.uvs       = np.array(uvs,       dtype = np.float32)
			self.colors    = np.array(colors,    dtype = np.float32)

		else:
			self.vertices  = np.zeros([0, 3], dtype = np.float32)
			self.triangles = np.zeros([0, 3], dtype = np.int32)
			self.uvs       = np.zeros([0, 2], dtype = np.float32)
			self.colors    = np.ones( [0, 4], dtype = np.float32)
	
	def check_validity(self):
		if len(self.triangles) != 0:
			if self.triangles.max() >= len(self.vertices):
				print("Error with mesh", " idx", self.triangles.max(), " len(vertices): ", len(self.vertices))

	def pivot(self, pivot_point, rotation, translation):
		i, j, k = np.array([1., 0., 0.]), np.array([0., 1., 0.]), np.array([0., 0., 1.])
		u, v, w = np.cos(rotation)*i + np.sin(rotation)*j, -np.sin(rotation)*i + np.cos(rotation)*j, k
		new_vertices = np.copy(self.vertices)

		for i in range(3): self.vertices[:, i] = self.vertices[:, i] - pivot_point[i]
		
		for i in range(3): new_vertices[:, i] = self.vertices[:, 0]*u[i] + self.vertices[:, 1]*v[i] + self.vertices[:, 2]*w[i] + translation[i]

		self.vertices = new_vertices

	def rotate(self, direction, angle):
		new_vertices = np.copy(self.vertices)

		self.vertices = rotate(new_vertices, direction, angle)

	def translate(self, new_position):
		for i in range(3): self.vertices[:, i] += new_position[i]

	def get_mesh(self):
		return Mesh([self.vertices], [self.triangles], [self.uvs], [self.colors], [self.material])

	def plot(self, color = "random"):
		colors = np.ones([len(self.vertices), 4], dtype = np.uint8)
		if color == "random2dsadsa":
			for i in range(3):
				colors[:, i] *= np.random.random(len(self.vertices))
		elif color == "uniformedsadsa":
			for i in range(3):
				colors[:, i] *= 0.5
			pass
		else:
			colors[:, 0] = np.int32(self.colors[:, 0]*255)
			colors[:, 1] = np.int32(self.colors[:, 1]*255)
			colors[:, 2] = np.int32(self.colors[:, 2]*255)

		colors[:, 3] = 255
		print("Mean color: ", np.mean(colors, axis = 0))
		self.trimesh = Trimesh(vertices = self.vertices, faces = self.triangles, vertex_normals = self.normals, vertex_colors = colors, process=False)
		self.trimesh.show(viewer = "gl")

	def save_obj(self, name):
		self.trimesh.export(name + ".obj")
		

class Mesh:
	def calculate_whole_mesh(self, additional_attributes = []):
		self.n_submeshes = len(self.materials)
		submesh_list = []
		for material in self.submeshes.keys():
			submesh = self.submeshes[material]
			submesh_list.append(submesh)
		self.whole_mesh = combine_submeshes(submesh_list, additional_attributes = additional_attributes)
		self.whole_mesh.material = "default"

		self.submesh_ids = np.zeros(len(self.whole_mesh.vertices), dtype = np.int32)

		offset, count = 0, 0
		for material in self.submeshes.keys():
			item = self.submeshes[material]
			# rand_color = np.random.random(4)
			# rand_color[3] = 1.
			# self.whole_mesh.colors[offset: offset + len(item.vertices)] = rand_color
			self.whole_mesh.colors[offset: offset + len(item.vertices)] = item.colors
			self.submesh_ids[offset: offset + len(item.vertices)] = count

			offset += len(item.vertices)
			count  += 1

	def generate_uv2(self):
		for material in self.submeshes.keys():
			item = self.submeshes[material]

			generate_uv2(item)

	def remove_empty(self):
		original_items = list(self.submeshes.keys())
		for item in original_items:
			if len(self.submeshes[item].vertices) == 0 or len(self.submeshes[item].triangles) == 0:
				#print("Removing empty: " + item)
				self.materials.remove(item)
				del self.submeshes[item]

		self.n_submeshes = len(self.materials)

	def __init__(self, vertices_list = [], triangles_list = [], uvs_list = [], colors_list = [], materials_list = []):
		self.n_submeshes = len(materials_list)
		
		self.materials = materials_list
		self.submeshes = dict()

		submesh_list = []
		for i in range(self.n_submeshes):
			material = self.materials[i]
			submesh = SubMesh(vertices_list[i], triangles_list[i], uvs_list[i], colors_list[i], material)
			self.submeshes.update({material: submesh})
			submesh_list.append(submesh)

		self.calculate_whole_mesh()

	def set_submeshes(self, submeshes):
		self.n_submeshes = 0
		self.materials   = []
		for item in submeshes:
			self.materials.append(item.material)
			self.n_submeshes += 1
			self.submeshes[item.material] = item
		self.whole_mesh = combine_submeshes(submeshes)
		self.whole_mesh.material = "default"
		
		offset = 0
		for item in submeshes:
			rand_color = np.random.random(4)
			rand_color[3] = 1.
			# self.whole_mesh.colors[offset: offset + len(item.vertices)] = rand_color
			self.whole_mesh.colors[offset: offset + len(item.vertices)] = item.colors
			offset += len(item.vertices)
		


	def plot(self, mode = "uniform"):
		self.whole_mesh.plot(mode)
		
	def pivot(self, pivot_point, rotation, translation):
		for key, value in self.submeshes.items():
			value.pivot(pivot_point, rotation, translation)
		self.whole_mesh.pivot(pivot_point, rotation, translation)

	def rotate(self, direction, angle):
		for key, value in self.submeshes.items():
			value.rotate(direction, angle)
		self.whole_mesh.rotate(direction, angle)

	def translate(self, vector):
		for key, value in self.submeshes.items():
			value.translate(vector)
		self.whole_mesh.translate(vector)

	def calculate_normals(self, check = False):
		for key, value in self.submeshes.items():
			value.calculate_normals(check = check)

	def flip(self):
		for key, value in self.submeshes.items():
			value.flip()

	def __add__(self, other):
		return combine_meshes(self, other)

def add_submesh(mesh, submesh):
	current_material = submesh.material
	if current_material in mesh.materials:
		submesh1, submesh2 = mesh.submeshes[current_material], submesh
		combination = combine_submeshes([submesh1, submesh2])
	else:
		combination = combine_submeshes([submesh])
		mesh.materials.append(submesh.material)
		mesh.n_submeshes += 1
	mesh.submeshes.update({current_material: combination})
	
def combine_submeshes(mesh_list, get_mat_ids = False, additional_attributes = [], per_vert = False):
	mat_ids, mat_idx = [], 0
	if len(mesh_list) != 0:
		vertex_list, triangle_list, uv_list, color_list, additional_lists = [], [], [], [], []

		attributes_types = []
		for i in range(len(additional_attributes)):
			additional_lists.append([])
			att = getattr(mesh_list[0], additional_attributes[i])
			dtype = att.dtype
			if len(att.shape) == 1:
				dim = 1
			else:
				dim = att.shape[1]

			attributes_types.append((dtype, dim))

		vertex_offset = 0

		for mesh in mesh_list:
			nvert = len(mesh.vertices)

			vertex_list.append(mesh.vertices)
			triangle_list.append(mesh.triangles + vertex_offset)
			uv_list.append(mesh.uvs)
			color_list.append(mesh.colors)
			for i in range(len(additional_attributes)):
				values = getattr(mesh, additional_attributes[i])
				additional_lists[i].append(values)

			vertex_offset += nvert
			mat_ids.append(np.ones(nvert, dtype = np.int32)*mat_idx)
			mat_idx += 1

		material      = mesh_list[0].material
		vertices      = np.concatenate(vertex_list)
		triangles     = np.concatenate(triangle_list)
		uvs           = np.concatenate(uv_list)
		colors        = np.concatenate(color_list)

		other_attributes = []
		for i in range(len(additional_attributes)):
			other_attributes.append(np.concatenate(additional_lists[i]))

		submesh = SubMesh(vertices, triangles, uvs, colors, material)
		for i in range(len(additional_attributes)):
			dtype, dim =  attributes_types[i][0],  attributes_types[i][1]
			submesh.add_quantity(additional_attributes[i], other_attributes[i], dtype = dtype, dim = dim, per_vert = per_vert)
		return submesh


	else:
		vertices  = np.zeros([0, 3], dtype = np.float32)
		uvs       = np.zeros([0, 2], dtype = np.float32)
		triangles = np.zeros([0, 3], dtype = np.int32)
		colors= np.ones([0, 4],  dtype = np.float32)
		material  = "default"

		return SubMesh(vertices, triangles, uvs, colors, material)

def combine_meshes(mesh1, mesh2):
	dict_submeshes, list_materials = {}, []
	
	for key, value in mesh1.submeshes.items():
		list_materials.append(key)
		dict_submeshes[key] = [value]

	for key, value in mesh2.submeshes.items():
		if key in list_materials:
			dict_submeshes[key].append(value)
		else:
			dict_submeshes[key] = [value]
			list_materials.append(key)
	result = Mesh()
	submeshes = []
	for key in list_materials:
		submeshes.append(combine_submeshes(dict_submeshes[key]))
	result.set_submeshes(submeshes)
	return result

def nearest(point, geom):
	rings = []
	if geom.__class__ == shapely.geometry.polygon.Polygon:
		rings = get_rings(geom)
	else:
		for item in geom:
			rings += get_rings(item)

	new_geometry = MultiLineString(rings)
	
	return nearest_points(point, new_geometry)
			

def snap(geom1, geom2, tolerance):
	if geom1.__class__ == shapely.geometry.polygon.Polygon:
		rings = get_rings(geom1)
		
		new_rings = []
		for ring in rings:
			new_ring = []
			
			for vertex in ring:
				point = Point(vertex)
				
				point1, point2 = nearest(point, geom2)

				if point1.distance(point2) <= tolerance:
					point1 = point2
				
				new_ring.append(point1.coords[0])
			new_rings.append(new_ring)
		return Polygon(new_rings[0], new_rings[1:]).buffer(0)

	else:
		objects = []
		for item in geom1:
			objects.append(snap(item, geom2, tolerance))
		return objects

def get_holes(polygon):
	##### Shitty script
	rings = get_rings(polygon)
	holes = []
	if len(rings) >= 2:
		for i in range(1, len(rings)):
			ring = Polygon(np.array(rings[i]))
			center = ring.centroid
			if center.within(ring) == False:
				print("Buggy, failed to find inner point")
				nitera = 100

				bounds = ring.envelope
				envelope_coord_array = np.array(get_rings(bounds)[0])
				extent = [envelope_coord_array[:,0].min(), envelope_coord_array[:,0].max(), envelope_coord_array[:,1].min(), envelope_coord_array[:,1].max()]
				
				for i in range(nitera):
					x = np.random.random()*(extent[1] - extent[0]) + extent[0]
					y = np.random.random()*(extent[3] - extent[2]) + extent[2]
					center = Point([x, y])
					if center.within(ring):
						break
			holes.append(np.array(center.coords[0]))
				
					
	return np.array(holes)

def determine_extent(shape):
	if shape.type in ["Polygon", "MultiPolygon"]:
		bounds = shape.envelope
		envelope_coord_array = np.array(get_rings(bounds)[0])
		extent = [envelope_coord_array[:,0].min(), envelope_coord_array[:,0].max(), envelope_coord_array[:,1].min(), envelope_coord_array[:,1].max()]

		length  = extent[3] - extent[2]
		width   = extent[1] - extent[0]
		center = [(extent[0] + extent[1])/2., (extent[2] + extent[3])/2.]

		return extent, center, width, length
	elif shape.type == "Point":
		coords = shape.coords[0]
		return [coords[0], coords[0], coords[1], coords[1]], np.array(coords), 0., 0.
	else:
		return [0., 0., 0., 0.], np.zeros(2), 0., 0.

def det_angles(ring):
	segment1 = np.array([ring[-2][0] - ring[-1][0], ring[-2][1] - ring[-1][1], 0.])
	nseg = len(ring)
	
	angles = np.zeros(nseg)
	signs  = np.zeros(nseg)
	concavity = np.zeros(nseg - 1)
	for i in range(nseg - 1):
		segment2 = np.array([ring[i + 1][0] - ring[i][0], ring[i + 1][1] - ring[i][1], 0.])
		cosine = np.dot(segment1, segment2)/np.linalg.norm(segment1)/np.linalg.norm(segment2)
		
		sign = np.sign(np.cross(segment2, segment1)[2]/np.linalg.norm(segment1)/np.linalg.norm(segment2))
		signs[i] = sign

		if cosine > 1.: cosine = 1.
		elif cosine < -1.: cosine = -1.
		if sign == 1:
			angles[i] = np.mod(np.arccos(cosine), 2*np.pi)
		else:
			angles[i] = 2*np.pi - np.mod(np.arccos(cosine), 2*np.pi)
		segment1 = -segment2
	angles[-1] = angles[0]
	signs[-1]  = signs[0]

	for i in range(nseg - 1):
		concavity[i] = signs[i] + signs[i + 1]
	
	return angles, concavity

def fix_triangulation(tr_input, tr_output):
	vertices1, vertices2 = tr_input["vertices"], tr_output["vertices"]
	repeated = []

	correct_idx = np.zeros(len(vertices2), dtype = np.int32)
	for j in range(len(vertices2)):
		vert2 = vertices2[j]
		for i in range(len(vertices1)):
			vert1 = vertices1[i]
			
			diff = vert1 - vert2
			distance = np.linalg.norm(diff)
			if distance <= 0.00001:
				correct_idx[j] = i

	triangles = tr_output["triangles"]
	new_triangles = []
	for triangle in triangles:
		i, j, k = triangle[0], triangle[1], triangle[2]
		
		i, j, k = correct_idx[i], correct_idx[j], correct_idx[k]
		
		if i != j and i != k and j != k:
			new_triangles.append([i, j, k])
	return np.array(new_triangles)


def triangulate_within(polygon, return_vertex = False):
	holes = get_holes(polygon)

	rings = get_rings(polygon)
	
	vertices_tot = []
	segments_tot = []
	vertex_markers = []
	segment_markers= []
	
	nrings = len(rings)
	offset = 0
	for i in range(nrings):
		ring = rings[i]
		nseg = len(ring)
		vertices_tot += list(ring)[0:nseg - 1]
		vertex_markers += list(np.ones(nseg - 1, dtype = np.int32)*i)
		
		segments = [[offset + i, offset + i + 1] for i in range(nseg - 1)]
		segments[nseg - 2][1] = offset
		
		segments_tot += segments
		segment_markers += list(np.ones(nseg - 1, dtype = np.int32)*i)


		offset = len(vertices_tot)
	tr_input = {"vertices": np.array(vertices_tot) + np.random.random([len(vertices_tot), 2])*0.0000001, "segments": np.array(segments_tot), "vertex_markers": np.array(vertex_markers), "segment_markers": np.array(segment_markers)}

	if len(holes) > 0:
		tr_input["holes"] = np.array(holes)
	tr_output = tr.triangulate(tr_input, "pS0")
	#tr_output = tr.triangulate(tr_input, 'rpa1.2')

	if return_vertex:
		return fix_triangulation(tr_input, tr_output), np.array(tr_output["vertices"])
	else:
		return fix_triangulation(tr_input, tr_output)
		#return np.array(tr_output["triangles"])



def create_box(center, scales, rotation = None):
	if scales[0]*scales[1] >= 0.01**2:
		extent = [center[0] - scales[0]/2., center[0] + scales[0]/2., center[1] - scales[1]/2., center[1] + scales[1]/2.]
		objeto = box(extent[0], extent[2], extent[1], extent[3])

		if rotation == None:
			orientation = 0.
		else:
			orientation = rotation
		return shapely.affinity.rotate(objeto, orientation*180./np.pi)
	else:
		return Polygon()

def create_box_from_extent(extent):
	if extent[1] - extent[0] > 0. and extent[3] - extent[2] > 0.:
		return create_box([(extent[0] + extent[1])/2., (extent[2] + extent[3])/2.], [extent[1] - extent[0], extent[3] - extent[2]], 0.)
	else:
		return Polygon()


sarasa = 0
def orient(polygon_object):
	global sarasa
	if polygon_object.type == "Polygon":
		if polygon_object.area != 0.:
			return shapely.geometry.polygon.orient(polygon_object)
		else:
			return Polygon()
	elif polygon_object.type == "MultiPolygon":
		result = []
		for polygon in polygon_object:
			if polygon.is_empty == False and polygon.type == "Polygon":
				result.append(shapely.geometry.polygon.orient(polygon))
			elif polygon.type == "Polygon":
				result.append(polygon)
		sarasa = result
		return MultiPolygon(result)
	elif polygon_object.type == "GeometryCollection":
		result = []
		for polygon in polygon_object:
			oriented = orient(polygon)
			if oriented.area != 0.:
				result.append(oriented)
		if len(result) == 1:
			return result[0]
		else:
			return MultiPolygon(result)
	else:
		return Polygon()

Normal_Debug = 0.0
def estimate_normals(vertices, triangles):
	global Normal_Debug
	global normal_estimation_totaltime
	time0 = time.time()

	N_vert = len(vertices)
	normals = np.zeros([N_vert, 3])
	Areas   = np.zeros(N_vert)
	
	Normal_Debug = vertices, triangles
	for triangle in triangles:
		vert1, vert2, vert3 = vertices[triangle[0]], vertices[triangle[1]], vertices[triangle[2]]
		
		side1 = vert2 - vert1
		side2 = vert3 - vert1
		
		product = np.cross(side1, side2)
		area    = np.linalg.norm(product)/2.
		normal  = product/(2*area)
		
		for i in range(3):
			Areas[triangle[i]]   += area
			normals[triangle[i]] += area*normal

		if (np.linalg.norm(normals[triangle[i]]) == 0.):
			print(area)
			print(normal)
			print(vertices)
			print(triangles)
			#raise ValueError('Bad normal')
			print("Bad normal")
		
	for i in range(N_vert):
		normals[i] *= 1./np.linalg.norm(normals[i])
			
	normal_estimation_totaltime += time.time() - time0
	return normals


def det_inner_distance(polygon):
	if polygon.area != 0.:
		ring_orig = get_rings(polygon.simplify(0.001))[0]
		ring = ring_orig[0: len(ring_orig) - 1]
		min_dist = 10000000
		nsegs = len(ring)
		if nsegs > 3:
			for i in range(nsegs):
				rolled_coords = np.roll(ring, -i, axis = 0)
				line1 = LineString(rolled_coords[0:2])
				line2 = LineString(rolled_coords[2:nsegs])
				
				dist = line1.distance(line2)

				if dist < min_dist:
					min_dist = dist
		else:
			min_dist = 0.0
	else:
		min_dist = 0.0
	return min_dist

def get_position_direction(shape, t):
	success, position, direction = False, np.zeros(2), np.zeros(2)
	shape = orient(shape)

	exterior = shape.exterior
	total_length = exterior.length

	rings = get_rings(shape)
	if total_length != 0.:
		length_count = 0.
		for ring in rings:
			line = LineString(ring)
			if length_count + line.length >= t*total_length:
				position, direction = position_along_polygon(ring, t*total_length)
				success = True
				break
			length_count += ring.length

	return success, position, direction


def concatenate_triangle_mesh(submesh):
	tris, verts, colors, uvs = submesh.triangles, submesh.vertices, submesh.colors, submesh.uvs

	ntris, new_verts, new_uvs, new_colors, new_tris = len(tris), [], [], [], []

	for j in range(ntris):
		triangle = tris[j]
			
		new_verts  = new_verts + [verts[triangle[0]], verts[triangle[1]], verts[triangle[2]]]
		new_uvs    = new_uvs + [uvs[triangle[0]], uvs[triangle[1]], uvs[triangle[2]]]
		new_colors = new_colors + [colors[triangle[0]], colors[triangle[1]], colors[triangle[2]]]
		new_tris   = new_tris  + [[3*j + 0, 3*j + 1, 3*j + 2]]
	
	return SubMesh(vertices = new_verts, triangles = new_tris, uvs = new_uvs, colors = new_colors, material = submesh.material)

def wrap_uvs_method1(submesh):
	submesh.calculate_normals()

	verts   = submesh.vertices
	normals = submesh.normals
	uvs     = submesh.uvs
	for j in range(len(normals)):
		dir3, dir2 = np.array([0., 0., 1.]), normals[j]
		if np.dot(dir2, dir3) >= 0.9999:
			dir3, dir2 = np.array([1., 0., 0.]), normals[j]

		dir3 = dir3 - dir2*np.dot(dir2, dir3)
		dir3 *= 1.0/np.linalg.norm(dir3)
		dir1 = np.cross(dir3, dir2)

		vert = verts[j]
		uvs[j, 0] = np.dot(vert, dir1)
		uvs[j, 1] = np.dot(vert, dir3)
	return uvs


# def generate_border():




def choose_biggest(shapes):
	if shapes.type == "Polygon":
		return shapes
	elif shapes.type == "MultiPolygon":
		areas = np.array([shape.area for shape in shapes])
		i = np.where(areas == areas.max())[0][0]

		return shapes[i]
	else:
		return Polygon()

def choose_longest(strings):
	if strings.type == "LineString":
		return strings
	elif strings.type == "MultiLineString":
		lengths = np.array(thing.length for thing in strings)
		print("largos, ", lengths)
		i = np.where(lengths == lengths.max())[0][0]
		return strings[i]
	else:
		return LineString()



def remove_junk_segments(geometry, criterium = 10e-5):
	if geometry.type == "Polygon":
		if geometry.area != 0.:
			rings = get_rings(geometry)

			holes = []
			for i in range(len(rings)):
				ring = rings[i]
				r1 = np.array(ring)
				r2 = np.roll(ring, 1, axis = 0)
				segs = np.linalg.norm(r2 - r1, axis = 1)

				if i == 0:
					new_ring = r1[segs >= criterium]
				else:
					holes.append(r1[segs >= criterium])

			return Polygon(shell = new_ring, holes = holes)
		else:
			return Polygon()

	elif geometry.type in ["MultiPolygon", "GeometryCollection"]:
		shapes = []
		if geometry.is_empty == False:
			for item in geometry:
				corrected = remove_junk_segments(item, criterium)
				shapes.append(corrected)
			return MultiPolygon(shapes)
		else:
			return Polygon()

	else:
		return Polygon()

def remove_junk(shape):
	if shape.type in ["Polygon", "MultiPolygon"]:
		return shape
	else:
		return Polygon()

debug_shape = 0

def adjust_shape(shape, distance):
	global debug_shape
	shape_eroded = remove_junk(shape.buffer(-distance, join_style = 2))
	if shape_eroded.area != 0.:
		debug_shape = shape_eroded, shape
		shape_dilated = (shape_eroded.buffer(distance*1.01, join_style = 2, mitre_limit = 2)).intersection(shape)

		return shape_dilated
	else:
		return Polygon()

def get_rings(polygon):
	if polygon.__class__ == shapely.geometry.polygon.Polygon:
		rings = [polygon.exterior.coords] + [ interior_ring.coords for interior_ring in polygon.interiors]
		return rings
	elif polygon.__class__ == shapely.geometry.linestring.LineString:
		return [polygon.coords]
	else:
		rings = []
		for item in polygon:
			rings += get_rings(item)
		return rings

def segment_in_polygon(segment, polygon, seg_level, polygon_level):
	criteria, segment = 0.001, np.array(segment)
	rings = get_rings(polygon)
	ids = []
	presence = np.zeros(len(rings), dtype = np.bool)
	
	nrings = len(rings)
	for j in range(nrings):
		ring = rings[j]
		nseg = max(len(ring) - 1, 0)
		indexes = np.zeros(nseg, dtype = np.bool)
		for i in range(nseg):
			edge = ring[i: i + 2]
			edge = np.array([edge[1], edge[0]])
			
			diff = edge - segment
			width = np.linalg.norm(segment[1] - segment[0])
			vec1  = (segment[1] - segment[0])/width
			
			point1, point2 = edge[0] - segment[0], edge[1] - segment[0]
			
			u1 = np.dot(point1, vec1)
			v1 = np.linalg.norm(point1 - u1*vec1)

			u2 = np.dot(point2, vec1)
			v2 = np.linalg.norm(point2 - u2*vec1)

			extreme1_inside = (u1 > -criteria)&(u1 < width + criteria)&(v1 < criteria)
			extreme2_inside = (u2 > -criteria)&(u2 < width + criteria)&(v2 < criteria)
			
			if extreme1_inside&extreme2_inside&(abs(polygon_level - seg_level) < criteria):
				indexes[i] = True

		ids.append(indexes)
		presence[j] = np.sum(indexes, dtype = np.bool)
	return ids

def point_in_ring(coords, path, criteria = 0.0000001):
	point = Point(coords)
	# path = LineString(ring)

	distance = point.distance(path)
	
	if distance <= criteria:
		return True
	else:
		return False


def list_polygons(polygon):
	if polygon.type == "MultiPolygon":
		polygons = []
		for item in polygon:
			polygons.append(item)
		return polygons
	elif polygon.type == "Polygon":
		return [polygon]
	else:
		return [Polygon()]

def position_along_polygon(linestring, position):
	npoints = len(linestring)
	
	seg_sum = 0.
	for i in range(npoints - 1):
		start   = np.array(linestring[i])
		end     = np.array(linestring[i + 1])
		segment = end - start
		seg_length = np.linalg.norm(segment)
		if seg_sum + seg_length >= position:
			break
		seg_sum += seg_length
	direction = segment/seg_length

	direction = direction if np.isfinite(direction).all() else np.array([0, 1])
	point = start + direction*(position - seg_sum)
	return point, direction


def create_distribution(extent, scale):
	size_x, size_y = extent[1] - extent[0], extent[3] - extent[2]
	dist_map = modulo.perlin2D(size = [size_x, size_y], scale = scale, M = 100)
	
	dist_map = (dist_map - dist_map.min())/(dist_map.max() - dist_map.min())
	return dist_map, size_x, size_y

def create_hendidura(extent, res):
	x = np.linspace(extent[0], extent[1], res[0])
	y = np.linspace(extent[2], extent[3], res[1])
	X, Y = np.meshgrid(x, y, indexing = "ij")

	Lx = extent[1] - extent[0]
	Ly = extent[3] - extent[2]

	h = 0.5 + (1 - np.sin((X - extent[0])*np.pi/Lx)*np.sin((Y - extent[2])*np.pi/Ly))/2.
	return h



def generate_properties_map(layout):
	street_width = 10.
	size_x = len(layout[0])*street_width + sum(layout[0])
	size_y = len(layout[1])*street_width + sum(layout[1])
	extent = [0., size_x, 0., size_y]

	wealth_poverty         = create_distribution(extent, 200.)
	commercial_residencial = create_distribution(extent, 200.)
	floor_number           = create_distribution(extent, 200.)*15
	floor_sigma            = (create_distribution(extent, 200.) + 0.5)/1.5*floor_number/2.
	
	fig, axs = plt.subplots(ncols = 4, nrows = 1)
	axs[0].imshow(wealth_poverty,         origin = "lower")
	axs[1].imshow(commercial_residencial, origin = "lower")
	axs[2].imshow(floor_number,           origin = "lower")
	axs[3].imshow(floor_sigma,            origin = "lower")
	return {"wealth": wealth_poverty, "residencial": commercial_residencial, "height": floor_number, "height_sigma": floor_sigma}

noise_height, noise_sigma = 0, 0

def initialize_maps(layout):
	global noise_height, noise_sigma
	from scipy.interpolate import interp2d

	street_width = 10.
	size_x = len(layout[0])*street_width + sum(layout[0])
	size_y = len(layout[1])*street_width + sum(layout[1])
	extent = [0., size_x, 0., size_y]

	map_noise_height, size_x, size_y = create_distribution(extent, 200.)
	map_noise_sigma,  size_x, size_y = create_distribution(extent, 200.)
	
	map_noise_height *= create_hendidura(extent, map_noise_height.shape)

	noise_height = interp2d(x = np.linspace(0., size_x, 100), y = np.linspace(0., size_y, 100), z = map_noise_height)
	noise_sigma  = interp2d(x = np.linspace(0., size_x, 100), y = np.linspace(0., size_y, 100), z = map_noise_sigma)

def determine_map_properties(extent, coords):

	u, v = (coords[0] - extent[0])/(extent[1] - extent[0]), (coords[1] - extent[2])/(extent[3] - extent[2])

	wealth = 1. - u
	residencial    = 1. - v
	
	#max_height, min_height = 25., 5.
	#max_height, min_height = 20., 8.
	max_height, min_height = 18., 5.

	dev_h, dev_s = noise_height(coords[0], coords[1]), noise_sigma(coords[0], coords[1])

	#average_height = (1. - u + dev_h)*(max_height - min_height) + min_height
	#sigma_height   = ((u - 1.0 + dev_s)*(-2**-0.5) + -(v - 1.)*2**-0.5)*(0.1 - 0.4)/2**0.5 + 0.4
	average_height = dev_h*(max_height - min_height) + min_height
	sigma_height   = dev_s*0.8
	
	return wealth, residencial, average_height, sigma_height

def better_linemerge(lines):
	if lines.type == "LineString":
		if len(lines.coords) <= 2:
			return lines
		else:
			return linemerge(lines)
	else:
		return linemerge(lines)
	
def railing_along_line(line, bar_size, bar_separation):
	points = list(line.coords)
	nsegs  = len(points) - 1

	minishapes = []

	for k in range(nsegs):
		seg    = np.array([[points[k][0], points[k][1]], [points[k + 1][0], points[k + 1][1]]])
		vector = seg[1] - seg[0]
		angle  = np.arctan2(vector[1], vector[0])

		seg_length = np.linalg.norm(vector)

		n = int(seg_length/bar_separation)

		bar_separation = (seg_length - bar_size)/n

		for i in range(n):
			point = seg[0] + vector*i/n

			if i != 0:
				minishapes.append(create_box(point, [bar_size*0.5, bar_size*0.5], angle))
			else:
				minishapes.append(create_box(point, [bar_size, bar_size], angle))

		if k == nsegs - 1:
			minishapes.append(create_box(seg[1], [bar_size, bar_size], angle))

	minishapes = remove_junk(unary_union(minishapes))
	return minishapes

def get_exterior(shape):
	if shape.type == "Polygon":
		return shape.exterior
	elif shape.type == "MultiPolygon":
		rings = []
		for item in shape:
			if item.type == "Polygon":
				rings.append(item.exterior)
		return unary_union(rings)


def get_intersections(ring, u, v, s_min, ds):
	npoints = len(ring)
	new_points = []
	for i in range(npoints):
		A = np.array(ring[i])
		if i != npoints - 1:
			B = np.array(ring[i + 1])
		else:
			B = np.array(ring[0])

		s_a = A[0]*u[0] + A[1]*u[1]
		s_b = B[0]*u[0] + B[1]*u[1]
		j_a = int((s_a - s_min)/ds)
		j_b = int((s_b - s_min)/ds)

		seg_dir = (B - A)/np.linalg.norm(B - A)
		new_points.append(A)
		if j_a != j_b:
			if j_a < j_b:
				for j in range(j_a + 1, j_b + 1):
					dx = (s_min + j*ds - A[0]*u[0] - A[1]*u[1])/(seg_dir[0]*u[0] + seg_dir[1]*u[1])
					new_points.append(A + dx*seg_dir)
			else:
				for j in range(j_a, j_b, -1):
					dx = (s_min + j*ds - A[0]*u[0] - A[1]*u[1])/(seg_dir[0]*u[0] + seg_dir[1]*u[1])
					new_points.append(A + dx*seg_dir)

	if npoints > 0:
		new_points.append(new_points[0])
	return new_points

def get_orientation(facet_shape):
	rectangle = facet_shape.minimum_rotated_rectangle
	coords = np.array(rectangle.exterior.coords)
	centroid = np.array(rectangle.centroid)
	if True:
		A, B = coords[1] - coords[0],  coords[2] - coords[1]

		if np.linalg.norm(A) >= np.linalg.norm(B):
			orientation = np.arctan2(A[1], A[0]) - np.pi/2
			length, width  = np.linalg.norm(A),  np.linalg.norm(B)
		else:
			orientation = np.arctan2(B[1], B[0]) - np.pi/2
			length, width  = np.linalg.norm(B),  np.linalg.norm(A)

	return length, width, orientation, coords, centroid




def rotate(vertices, vector, angle):
	w = vector/np.linalg.norm(vector)
	uw = vec_right
	if abs(np.dot(uw, w)) >= 0.99:
		uw = vec_up

	u = (uw - np.dot(uw, w)*w)/np.linalg.norm(uw - np.dot(uw, w)*w)
	v = np.cross(w, u)

	rot_matrix1 = np.zeros([3, 3], dtype = np.float32)

	rot_matrix1[:, 0] = u
	rot_matrix1[:, 1] = v
	rot_matrix1[:, 2] = w

	rot_matrix2 = rot_matrix1.T

	vertices2 = np.matmul(rot_matrix2, vertices.T).T

	u2 = np.cos(angle)*u + np.sin(angle)*v
	v2 =-np.sin(angle)*u + np.cos(angle)*v

	rot_matrix3 = np.zeros([3, 3], dtype = np.float32)
	rot_matrix3[:, 0] = u2
	rot_matrix3[:, 1] = v2
	rot_matrix3[:, 2] = w

	vertices3 = np.matmul(rot_matrix3, vertices2.T).T
	return vertices3


def generate_uv2(mesh):
	mesh.calculate_normals()

	vmapping, indices, uvs = xatlas.parametrize(mesh.vertices, mesh.triangles)

	mesh.vertices = mesh.vertices[vmapping]
	mesh.triangles = indices
	mesh.normals   = mesh.normals[vmapping]
	mesh.uvs       = mesh.uvs[vmapping]
	mesh.colors    = mesh.colors[vmapping]
	mesh.uv2       = uvs


