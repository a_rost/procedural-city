import numpy as np
import manzana
import tools
from importlib import reload
layout = [[90.], [110.]]



reload(manzana)
# for parcel in parcels:
	# print(check_continuity(parcel.cells))






tools.initialize_maps(layout)
manzana.initialize_maps(layout, distortion = 0.22)

distortion_function = manzana.transform_coords
block_origin = [15, 20, 0.]
xcoord, ycoord, terrains, parcels = manzana.generate_manzana([150, 120.], block_origin)


# generate_exchange(parcels, nexchanges = 50)
manzana.plot_block(xcoord, ycoord, terrains, parcels)
