from importlib import reload
import numpy as np
import matplotlib.pyplot as plt
import lighting_toolkit as light
import modulo
# import modulo_cuda
import tools
import time

plt.ion()

clamp = lambda x, a, b: a if x < a else (b if x > b else x)

benchmark = True
def get_height(heightmap, extent, point2D):
	N, M = heightmap.shape
	dx, dy = (extent[1] - extent[0])/N, (extent[3] - extent[2])/M

	I = int((point2D[0] - extent[0])/dx)
	J = int((point2D[1] - extent[2])/dy)

	I = max(min(I, N - 1), 0)
	J = max(min(J, M - 1), 0)

	return heightmap[I, J]

def correct_position(heightmap, extent, point3D):
	h = get_height(heightmap, extent, point3D)

	return np.array([point3D[0], point3D[1], max(point3D[2], h + 0.1)])

def generate_point(heightmap, extent):
	h_min, h_max = heightmap.min(), heightmap.max()

	x, y, z = np.random.random()*(extent[1] - extent[0]) + extent[0], np.random.random()*(extent[3] - extent[2]) + extent[2], np.random.random()*(h_max - h_min) + h_min
	result = correct_position(heightmap, extent, [x, y, z])
	return result





if False:
	mesh = light.load_all(0, usefile = False, nblocks = 3)
	# mesh = light.load_mesh(lod = 0, block_id = 1)
	# # mesh = light.load_all(0, usefile = True)

	ray_distance = 100.

	M, eta = 1000, 100
	np.random.seed(0)
	starts, ends, count = np.zeros([M*eta, 3], dtype = np.float32), np.zeros([M*eta, 3], dtype = np.float32), 0
	heightmap, extent = modulo.get_heightmap(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, dx = 0.5)

	for i in range(M):
		point1 = generate_point(heightmap, extent)

		for j in range(eta):
			point2 = generate_point(heightmap, extent)
   #
			vector = point2 - point1

			distance = clamp(np.linalg.norm(vector), 0., ray_distance)
			new_point2 = correct_position(heightmap, extent, point1 + vector/(np.linalg.norm(vector))*distance)

			starts[count] = point1
			ends[count]   = new_point2
			count += 1
	np.save("benchmark_data2", {"starts": starts, "ends": ends})
	np.save("mesh2", mesh)

elif True:
	data = np.load("benchmark_data2.npy", allow_pickle=True).item()
	starts, ends = data["starts"], data["ends"]
	mesh = np.load("mesh2.npy", allow_pickle=True).item()

	dx = 0.5
	heightmap, extent = modulo.get_heightmap(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, dx = dx)
else:
	mesh = light.load_all(0, usefile = False, nblocks = 1)
	dx = 0.5
	heightmap, extent = modulo.get_heightmap(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, dx = dx)


h_min, h_max = mesh.whole_mesh.vertices[:, 2].min(), mesh.whole_mesh.vertices[:, 2].max()
extent2 = [extent[0] - 1, extent[1] + 1, extent[2] - 1., extent[3] + 1., h_min - 1., h_max + 1]




modulo.generate_BVH(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = 15)
hits, lengths_bvh = modulo.raytracing(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, starts, ends, mode = 1)
modulo.free_bvh()


modulo.create_gen_tree(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = 15, optimized = True)
hits, lengths1 = modulo.raytracing(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, starts, ends, mode = 0)
modulo.free_gen_tree()


fig, axs = plt.subplots(ncols = 2, nrows = 1)
axs[0].plot(lengths1, lengths_bvh, ".")


def get_meshes(start_points, end_points, lengths, valid, color):
	nvectors = len(start_points)
	directions = end_points - start_points
	dists   = np.linalg.norm(directions, axis  = 1)
	for i in range(3): directions[:, i] *= 1/dists

	hit_points = np.zeros([nvectors, 3])
	for i in range(3): hit_points[:, i] = start_points[:, i] + directions[:, i]*lengths

	valid *= np.isfinite(np.linalg.norm(start_points - hit_points, axis = 1))
	ray_triangles = np.reshape(np.arange(valid.sum()*3, dtype = np.int32), [valid.sum(), 3])
	ray_vertices  = np.zeros([3*valid.sum(), 3], dtype = np.float32)

	ray_vertices[0::3, :] = start_points[valid]
	# ray_vertices[0::3, :] = hit_points[valid] + np.random.random(size = [valid.sum(), 3])
	ray_vertices[1::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3
	ray_vertices[2::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3

	colors = np.zeros(shape = [valid.sum()*3, 4])
	colors[:, 0] = np.repeat(color[valid, 0], 3)
	colors[:, 1] = np.repeat(color[valid, 1], 3)
	colors[:, 2] = np.repeat(color[valid, 2], 3)
	colors[:, 3] = 1.0

	meshi = tools.SubMesh(vertices = ray_vertices, triangles = ray_triangles, colors = colors, uvs = np.ones(shape = [valid.sum()*3, 2]))
	meshi.calculate_normals()

	return meshi

def visualize(starts, ends, hits, lenghts):
	hit_points = starts*1.
	for i in range(3): hit_points[:, i] += (ends - starts)[:, i]*lenghts/np.linalg.norm(ends - starts, axis = 1)

	# meshi = get_meshes(hit_points, starts, ends[:,0], valid = hits != -1, color = np.array([[0.5, 0.5, 0.5, 1.]]*len(starts)))

	meshi = get_meshes(starts, ends, lenghts, valid = hits, color =  np.array([[0.5, 0.5, 0.5, 1.]]*len(starts)))
	vista = mesh + meshi.get_mesh()
	# vista = meshi.get_mesh()

	vista.plot()



def create_cube_from_extent(extent, color):
	vert1 = np.array([extent[0], extent[2], extent[4]])
	vert2 = np.array([extent[0], extent[3], extent[4]])
	vert3 = np.array([extent[0], extent[2], extent[5]])
	vert4 = np.array([extent[0], extent[3], extent[5]])
	vert5 = np.array([extent[1], extent[2], extent[4]])
	vert6 = np.array([extent[1], extent[3], extent[4]])
	vert7 = np.array([extent[1], extent[2], extent[5]])
	vert8 = np.array([extent[1], extent[3], extent[5]])

	tri1  = [0, 3, 1]
	tri2  = [0, 2, 3]

	tri3  = [4, 5, 7]
	tri4  = [4, 7, 6]

	tri5  = [4, 6, 2]
	tri6  = [4, 2, 0]

	tri7  = [3, 7, 5]
	tri8  = [3, 5, 1]

	tri9  = [6, 7, 2]
	tri10 = [2, 7, 3]

	tri11 = [0, 1, 5]
	tri12 = [0, 5, 4]

	mesh = tools.SubMesh(vertices = np.array([vert1, vert2, vert3, vert4, vert5, vert6, vert7, vert8]), triangles = [tri1, tri2, tri3, tri4, tri5, tri6, tri7, tri8, tri9, tri10, tri11, tri12], colors = np.array([color]*8), uvs = np.array([[0., 0.]]*8))

	return mesh


def get_children_extent_mesh(cell_id, include_parent = True):
	result, extent = modulo.check_bvh(cell_id)
	print("Cell extent", extent)
	mesh = tools.Mesh()
	if include_parent:
		color = list(np.random.random(3)) + [1.]
		mesh += create_cube_from_extent(extent, color).get_mesh()

	nchildren, children_offset, children_ids = result[2], result[3], []

	for i in range(nchildren):
		new_result, _ = modulo.check_bvh(children_offset + i)
		children_ids.append(new_result[4])

	for i in range(nchildren):
		new_result, extent = modulo.check_bvh(children_ids[i])
		print("sub Cell extent", extent)

		color = list(np.random.random(3)) + [1.]
		mesh += create_cube_from_extent(extent, color).get_mesh()

	return mesh





def visualize_extents(depth, color = [1.]*4):
	nsubcells = 2**depth
	result = tools.Mesh()
	for i in range(nsubcells):
		total_count, count = modulo.get_triangle_count(depth, i)
		if total_count != 0:
			extent = modulo.get_extent_gentree(depth, i)
			result += create_cube_from_extent(extent, color).get_mesh()
	return result

def visualize_subset(mesh, tri_ids):
	meshi = tools.SubMesh(vertices = mesh.whole_mesh.vertices, triangles = mesh.whole_mesh.triangles[tri_ids], colors = mesh.whole_mesh.colors, uvs = mesh.whole_mesh.uvs)
	meshi.plot()

if benchmark:
	depths, times_opt, times_non = [], [], []
	for depth in range(20, 10, -1):
		print("Depth: ", depth)
		modulo.generate_BVH(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = depth)
		# modulo.create_gen_tree(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = depth, optimized = True)
		# modulo_cuda.create_gen_tree(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = depth)
		start = time.time()
		hits, lengths1 = modulo.raytracing(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, starts, ends, mode = 1)
		# hits, lengths = modulo_cuda.raytracing(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, starts, ends, mode = 2)
		end   = time.time()
		delta1 = end - start
		modulo.free_bvh()


		modulo.create_gen_tree(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = depth, optimized = False)
		start = time.time()
		hits, lengths2 = modulo.raytracing(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, starts, ends, mode = 0)
		end   = time.time()
		delta2 = end - start

		print("optimized vs non optimized", delta1, delta2)
		depths.append(depth)
		times_opt.append(delta1)
		times_non.append(delta2)

		modulo.free_gen_tree()

times_opt = np.array(times_opt)
times_non = np.array(times_non)

print("optimized", times_opt)
print("non opt  ", times_non)
print("Speed up", times_non/times_opt)

axs[1].plot(depths, times_opt)
axs[1].plot(depths, times_non)
