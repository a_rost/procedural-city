import tools
import numpy as np
#from shapely.geometry import MultiPoint
from importlib import reload
from shapely.ops import unary_union

#N = 30
#point_coords = np.random.random([N, 2])*10.

#points = MultiPoint(point_coords).buffer(1.)
#holes  = MultiPoint(np.random.random([N, 2])*10.).buffer(1.)

#points = points.difference(holes)
#tools.plot_polygon(points)


#shape = tools.create_box([3., 4.], [1., 2.])


#walls = tools.extrude_rings(shape, 0., 4., "metal")
#lid   = tools.meshify_shape(shape, 4., "metal")
#mesh = tools.extrude_shape(points, 0., 4., "metal")
#mesh.submeshes["metal"].triangles
##mesh.plot()


def combine_rectangles(total_width, total_length, n):
	size = [total_width, tools.suggest_value(8, total_length, total_length/3., 4, 0.5)]
	
	shape1 = tools.create_box([size[0]/2., size[1]/2.], size, 0.)

	parcel_shape = tools.create_box([total_width/2., total_length/2.], [total_width, total_length], 0.)
	total_shape = shape1
	
	for i in range(n):
		for j in range(20):
			width  = tools.suggest_value(4, total_width/2.,  4, 4, 0.4)
			length = tools.suggest_value(4, total_length/2., 4, 4, 0.4)
			
			center_x = tools.suggest_value(width/2., total_width - width/2., total_width/2., 3, 0.2)
			center_y = tools.suggest_value(length/2., total_length - length/2., size[1], 8, 0.2)
			shape2 = tools.create_box([center_x, center_y], [width, length], 0.)
		
			intersect = shape2.intersection(total_shape)
			
			print(intersect.area, shape2.area)
			if intersect.area != 0. and intersect.area/(shape2.area) <= 0.8:
				total_shape = unary_union([total_shape, shape2])
				tools.plot_polygon(shape2, 0.3)
				break
			#total_shape = unary_union([total_shape, shape2])

		#print(width, length, center_x, center_y)
		
	
	complement = parcel_shape.difference(total_shape)
	complement = tools.adjust_shape(complement, 1)
	total_shape = parcel_shape.difference(complement)
	return tools.adjust_shape(total_shape, 1), parcel_shape

#def find_minimum_distance(shape):
	#rings = tools.get_rings(shape)
	#minimum_distance = 100.
	#for ring in rings:
		#nseg = len(ring)
		#ring = ring[0:nseg - 1]
		#nseg += -1
		#print("ring: ", ring)
		#print("nseg: ", nseg)
		#if nseg > 4:
			#for i in range(nseg):
				#rolled_coords = np.roll(ring, 1 - i, axis = 0)
				
				#segment = rolled_coords[1:3]
				#line = tools.LineString(list(rolled_coords[3:]) + [rolled_coords[0]])
				#dist = tools.LineString(segment).distance(line)
				
				#print("rolled", rolled_coords)
				#print("seg: ", segment)
				#print("dist: ", dist)
				#print("line: ", line)
				#if dist < minimum_distance: minimum_distance = dist
	#return minimum_distance

	
def build_roof(start_shape, dx = 0.3):
	shape = start_shape
	for i in range(5):
		tools.plot_polygon(shape)
		inner_shape = shape.buffer(-dx, join_style = 2)
		shape = inner_shape

	
shape, parcel = combine_rectangles(10., 40., 2)
tools.plot_polygon([shape, parcel])



#shape1 = tools.Polygon([[0., 0.], [1., 0.], [1., 1.], [0., 1.], [0., 0.]])
#shape2 = tools.Polygon([[0.5, 1.], [1., 1.], [1., 2.], [0.5, 2.], [0.5, 1.]])
#shape3 = gen.unary_union([shape1, shape2])
#shape4 = shape3.difference(shape2)

#shape5 = shape4.buffer(1.4, join_style=2).buffer(-1.4, join_style=2)

#shape6 = shape4.intersection(shape5)
#shape7 = shape4.union(shape6)

#import numpy as np
#import structure_generation as gen
#import manzana
#import tools
#from importlib import reload

#xcoord, ycoord, terrains, parcels = manzana.generate_manzana([130, 130])
#parcel = parcels[22]

#recipe = gen.generate_appartment2(parcel)

#building, shape_info = gen.generate_appartment(recipe, parcel)
#gen.check_facades(building.facades)

#building.mesh.plot()
##tools.plot_polygon(built_shapes)
##tools.plot_polygon(shapes_to_substract)

import numpy as np
import modulo
import matplotlib.pyplot as plt
from mayavi import mlab
plt.ion()

heightmap = (1. - plt.imread("Model4.png")[:, :, 0]).T*140.
extent = [0., 1705., 0., 959.]
#extent = [0., 959., 0., 1705.]

plt.imshow(heightmap.T, origin = "lower", extent = extent)

position = [916, 610., 55.]

N = 100
rays = np.zeros([N, 3], dtype = np.float32)

theta = (np.random.random(N) - 0.5)*np.pi/2. + np.pi/2.
phi   = np.random.random(N)*2*np.pi
rays[:, 0] = np.cos(phi)*np.sin(theta)
rays[:, 1] = np.sin(phi)*np.sin(theta)
rays[:, 2] = np.cos(theta)

lengths = np.ones(N)*200.

modulo.load_map(heightmap, extent)
intersections = modulo.raycast(position, rays, lengths)

#plt.imshow(heightmap.T, origin = "lower")
#plt.plot([position[0]], [position[1]], "o")
mlab.surf(heightmap, extent = extent + [0., 140.])

for i in range(N):
	if intersections[i] > 0.:
		point1 = np.array(position)
		point2 = point1 + rays[i]*intersections[i]
		#plt.plot([point1[0], point2[0]], [point1[1], point2[1]], color = "k")
		mlab.plot3d([point1[0], point2[0]], [point1[1], point2[1]], [point1[2], point2[2]], tube_radius=.3)

asd = np.zeros([1, 2])
asd[0, 0] = 995
asd[0, 1] = 93

modulo.evaluate_heights(asd)


mask = intersections >= 0.
color = np.ones([mask.sum(), 3])
for i in range(3): color[:, i] = intersections[mask]/200.
plt.scatter(phi[mask], np.pi/2. - theta[mask], c = color)




import numpy as np
import modulo
import matplotlib.pyplot as plt
from mayavi import mlab
plt.ion()

heightmap = (1. - plt.imread("Model4.png")[:, :, 0]).T*140.
extent = [0., 1705., 0., 959.]
dx = (extent[1] - extent[0])/len(heightmap)
surface = modulo.determine_surface(heightmap, dx, 3.)


position = np.array([0., 5., 100.])
velocity = np.array([1., 1., 1.])
fixed_p  = np.array([30., 0., 100.])

modulo.load_map(heightmap, extent)
modulo.load_map(surface,   extent)

trajectories = []
for i in range(1):
	E = 9.81*position[2] + 0.5*np.linalg.norm(velocity)**2
	print("Energy: " + str(E))
	mlab.points3d([position[0]], [position[1]], [position[2]])
	trajectory, final_vel, final_pos =  modulo.solve_pendulum(position, velocity, fixed_p)

	
	position, velocity = final_pos, final_vel
	fixed_p += np.array([10., 0., 0.])
	trajectories.append(trajectory)
	print(velocity, position)

trajectory = np.concatenate(trajectories)
plt.plot(trajectory[:, 0], trajectory[:, 1])

R = np.linalg.norm(np.array(position) - np.array(fixed_p))*np.ones(len(trajectory))

points = np.zeros([len(trajectory), 3])
points[:, 0] = R*np.cos(trajectory[:, 1])*np.sin(trajectory[:, 0])
points[:, 1] = R*np.sin(trajectory[:, 1])*np.sin(trajectory[:, 0])
points[:, 2] = R*np.cos(trajectory[:, 0])

mlab.plot3d(points[:, 0], points[:, 1], points[:, 2], tube_radius=.3)



def generate_gaussian_random(radius):
	U1, U2 = np.random.random(), np.random.random()

	X = np.sqrt(-2*np.log(U1))*np.cos(2*np.pi*U2)
	Y = np.sqrt(-2*np.log(U1))*np.sin(2*np.pi*U2)

	return X


asd1 = tools.create_box([1., 1.], [2., 1.], 0.)
asd2 = tools.create_box([2., 0.], [1., 3.], 0.)

asd = gen.unary_union([asd1, asd2])
tools.plot_polygon(asd)
qwe = tools.extrude_rings(asd, 0., 10., "std", np.ones(4))

qwe.submeshes["std"].vertices


def polygon_inner_dist(polygon):
	rings = tools.get_rings(polygon)
	
	if




import tools
import structure_generation as gen
import numpy as np

asd1 = tools.create_box([1., 4.5], [2., 5.], 0.)
asd2 = tools.create_box([4., 2.5], [4., 5], 0.)
#asd3 = tools.create_box([3., 7], [5., 8.], 0.)
#asd4 = tools.create_box([8, 7], [3., 5.], 0.)



#shapes = []
#for i in range(10):
	#shapes.append(tools.create_box([np.random.random()*5, np.random.random()*5], [2., 2.]))

#asd = gen.unary_union(shapes)
asd = gen.unary_union([asd1, asd2])

qwe = tools.extrude_shape(asd, 0.,1 , "default", color1 = [1.]*4, color2 = [1.]*4)
qwe.plot()




tools.plot_polygon(asd)

shape = asd
whole_shape = asd
#tools.plot_polygon(asd3)

qwe, success = tools.generate_complex_roof(shape, whole_shape, 0., 1., "default1", "default2", dx = 0.1, kind = "quadratic", nlevels = 10)
#qwe.submeshes["default1"].calculate_normals()
if success:
	qwe.plot()



surf.plot()



shape = asd
shape_built = asd4

object_length, object_width, minimal_length = 2., 1., 3.

def spawn_along_perimeter(shape, shape_built, spawnable_area, object_length, object_width, minimal_length = 3.0, dist2facade = 1.5):

	spawnable_area = spawnable_area.intersection(shape.difference(shape_built.buffer(dist2facade, join_style = 2)))

	perimeter = np.array(tools.get_rings(tools.orient(shape))[0])

	position, success = np.zeros(2), False

	nsides = len(perimeter) - 1
	for i in range(nsides):
		side_length = np.linalg.norm(perimeter[i + 1] - perimeter[i])
		if side_length >= minimal_length:
			position_along_side = tools.suggest_value(0.3, 0.7, 0.5, 0.2, 0.01)*side_length
			direction1 = (perimeter[i + 1] - perimeter[i])/side_length
			direction2 = np.array([-direction1[1], direction1[0]])
			position   = perimeter[i] + direction1*position_along_side + direction2*(object_width/2. + 0.15)
			orientation = np.arctan2(direction1[1], direction1[0])
			occupied_space = tools.create_box(position, [object_length, object_width], orientation)
			
			if occupied_space.intersection(spawnable_area).area/(object_length*object_width) >= 0.999:
				success = True
				break

	spawnable_area = spawnable_area.difference(occupied_space)
	return success, position, spawnable_area


tools.plot_polygon(shape)
tools.plot_polygon(shape_built)
tools.plot_polygon(spawnable_area)
tools.plot_polygon(occupied_space)


surf = tools.generate_v_roof(shape, whole_shape, 0., 1.0, "default1", "default2", kind = "side")


roof = tools.generate_complex_roof(shape, whole_shape, 0., .5, "default1", "default2", dx = 0.05, kind = "quadratic")
roof.plot()








def populate_squares(shape, size = 5., n = 3):
	shapes = []

	extent, center, width, length = determine_extent(shape)
	for i in range(n):

		offset_x = tools.suggest_value(extent[0] + size/2., extent[1] - size/2., extent[0] + width/2., width/2., 1)
		offset_y = tools.suggest_value(extent[2] + size/2., extent[3] - size/2., extent[2] + width/2., length/2., 1)
		size_x   = size
		size_y   = size

		new_shape = tools.create_box([offset_x, offset_y], [size_x, size_y], 0.).intersection(shape)
		
		shapes.append(new_shape)
	return unary_union(shapes)


def generate_random_shapes(shape, nlevels = 5):
	whole_shapes = []
	extent, center, width, length = determine_extent(shape)
	current_shape = tools.create_box(center, [width, length], 0.).intersection(shape)
	
	for i in range(nlevels):
		if current_shape.area != 0.:
			whole_shapes.append(current_shape)

			m = int(shape.area/25.*1)
			print("MM:", m)
			new_shape = populate_squares(shape, size = 5., n = m)
			extent, center, width, length = determine_extent(current_shape)

			offset_x = tools.suggest_value(extent[0], extent[1], extent[0] + width/2., 4., 1)
			offset_y = tools.suggest_value(extent[2], extent[3], extent[2] + length/2., 4., 1)
			size_x   = tools.suggest_value(2., 4.,  2., 1., 1)
			size_y   = tools.suggest_value(2., 4.,  2., 1., 1)

			yard = tools.create_box([offset_x, offset_y], [size_x, size_y], 0.)
			
			#current_shape = new_shape.difference(yard)
			current_shape = new_shape.intersection(current_shape)
	return whole_shapes

shapes = generate_random_shapes(tools.create_box([5., 25.], [10., 50], 0.))

mesh = tools.Mesh()
for i in range(len(shapes)):
	shape = shapes[i]

	mesh += tools.extrude_shape(shape, i*5, (i + 1)*5., "wall", [1., 1., 1. ,1.])
mesh.plot()




shape1 = tools.create_box([0.5, 2.5], [1, 5], 0.)
tools.plot_polygon(shape1)

shape2 = unary_union([tools.create_box([0.5, 1.5], [1, 3], 0.), tools.create_box([0.5, 4.5], [1, 1], 0.)])

shape3 = unary_union([tools.create_box([0.5, 0.5], [1, 1], 0.), tools.create_box([0.5, 2.5], [1, 1], 0.)])

shapes = [shape1, shape2, shape3]


roofs, levels = determine_roofs(shapes)












#####
import tools
import numpy as np
#from shapely.geometry import MultiPoint
from importlib import reload
from shapely.ops import unary_union
import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()

whole_shape = tools.create_box([0.5, 2.5], [1, 5], 0.)

built_shape = tools.unary_union([tools.create_box([0.5, 3.0], [1.4, 1.4], 0.), tools.create_box([0., 0.], [0.5, 1.0], 0.)])

non_built = whole_shape.difference(built_shape)
shape = non_built[0]

mesh = tools.generate_railing(whole_shape, built_shape, shape, shape, 0., "material1", "material2")

#mesh = tools.extrude_shape(shape, 0., 1.5, "material1", color1 = [1.]*4, color2 = [1.]*4)

tris, verts = mesh.whole_mesh.triangles, mesh.whole_mesh.vertices


reload(gen)

fachada = gen.generate_sample_facade()


qwe = gen.facade_model()
qwe.mesh = caja
asd = qwe.get_facade(nfloors = 10, ncols = 6, total_width = 20., floor_height = 3., position = np.zeros(3), orientation = 3*np.pi/180)
#wins, doors = gen.check_quads(verts, tris)

#plt.plot(verts[:, 0], verts[:, 2], ".")
verts = asd.submeshes["material1"].vertices
plt.plot(verts[:, 0], verts[:, 2], ".")

#mesh.plot()

#tools.plot_polygon(whole_shape)
#tools.plot_polygon(built_shape)
#tools.plot_polygon(wall_line)
#tools.plot_polygon(shapes)






#####
import tools
import numpy as np

from importlib import reload

import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()


#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon

caja1 = tools.create_box([5, 10], [10, 20], 0.)
caja2 = tools.create_box([7.5, 12.5], [6, 5], 0.)
caja3 = tools.create_box([15, 10], [10, 20], 0.)

shape = caja1.difference(caja2)
built_shape = unary_union([caja2, caja3])
whole_shape = unary_union([built_shape, shape])

tools.plot_polygon(shape)
tools.plot_polygon(built_shape)

occupied_space = Point([8, 18]).buffer(1.5)
tools.plot_polygon(occupied_space)

free_space = (shape.difference(occupied_space)).difference(built_shape.buffer(1,  join_style = 2))
tools.plot_polygon(free_space)





#output_positions, orientation, occupied_space = gen.spread_within_area(1./16, "uniform", free_space, 0., 0.5, 1.0, number_limit = 5, limit_mode = "distance")

if False:
	output_positions, orientation, occupied_space = gen.spread_along_perimeter(shape, free_space, 0., 4, 2., 0.5)

	free_space = (shape.difference(occupied_space)).difference(built_shape.buffer(1,  join_style = 2))
	tools.plot_polygon(free_space)




kind, N = "Asadores", 2

asset_id = gen.model_assets.types[kind][np.random.randint(len(gen.model_assets.types[kind]))]

radius, depth, width = gen.model_assets.radii[asset_id], gen.model_assets.depths[asset_id], gen.model_assets.widths[asset_id]

positions = []

positions, orientations, occupied_space = gen.spread_along_perimeter(shape, free_space, 0., N, width, depth)


N = len(positions)
free_space = free_space.difference(occupied_space)
tools.plot_polygon(free_space)


things              = gen.Objects()
things.tags         = [kind]*N

things.asset_ids    = [np.random.randint(len(self.types[kind]))]*N

things.positions    = positions
things.sizes        = [1.0]*N
things.radii        = [1.0]*N
things.orientations = orientations
objects.merge(things)




meshes = generate_objects_mesh(objects_list)






##### Experiment
#####
import tools
import numpy as np
import shape_generation as shape_gen
from importlib import reload

import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()


#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon


terrain = Polygon([[0., 0.], [10., 0.], [15., 25.], [15., 30.], [0., 30], [0., 0.]]).difference(tools.create_box([2., 15.], [4., 15.], 0.))


tools.plot_polygon(terrain, linewidth = 2.)

shape = shape_gen.generate_shape(terrain)

tools.plot_polygon(shape, linewidth = 2.)



###############
#####
import tools
import numpy as np

from importlib import reload

import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()


#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon

caja1 = tools.create_box([1., 5.], [2, 10.], 0.)
caja2 = tools.create_box([4., 9.], [8, 2.], 0.)
shape = unary_union([caja1, caja2])

tools.plot_polygon(shape)

def create_bush(area, density = 5., height = 1.0):
	total_shape = area.convex_hull
	N = total_shape.area*density







###########
###########
import tools
import roofs
import numpy as np

from importlib import reload

import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()

def create_mesh(building):
	facets = building.facets

	mesh = tools.Mesh()

	for i in range(building.n_levels - 1):
		shape = building.built_shapes[i]

		mesh += tools.extrude_rings(shape, building.levels[i], building.levels[i + 1], "standard", [1]*4)

	for facet in facets:
		if facet.kind == "horizontal":
			if facet.polygon.is_empty == False:
				level = facet.level if facet.level != 0. else 0.15
				if facet.tag in ["terrace", "small_yard"]:
					mesh += tools.meshify_shape(facet.polygon, level, "standard", [0.5]*4)
				else:
					mesh += tools.meshify_shape(facet.polygon, level, "standard", [0.5]*4)
	return mesh


#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon




level0 = tools.create_box([5., 20.], [10, 40.], 0.)
level1 = tools.create_box([5., 15.], [10, 30.], 0.)

level2_a = tools.create_box([5., 5.], [10, 10.], 0.)
level2_b = tools.create_box([5., 25.], [10, 10.], 0.)

level2 = unary_union([level2_a, level2_b])

level3_a = tools.create_box([1.25, 5.], [2.5, 10.], 0.)
level3_b = tools.create_box([10 - 1.25, 5.], [2.5, 10.], 0.)

level3 = unary_union([level3_a, level3_b])

whole_shapes = [level0, level1, level2, level3]
built_shapes = [level1, level2, level3, tools.Polygon()]
levels       = [0, 4.2, 8.4, 12.5]

#tools.plot_polygon(level0)
#tools.plot_polygon(level1)
#tools.plot_polygon(level2)
#tools.plot_polygon(level3)

rooves, levels_idx = roofs.determine_roofs(whole_shapes)

for i in range(1000):
	new_whole, new_built, new_levels = roofs.generate_roofs(whole_shapes, built_shapes, levels)


#for shape in new_whole:
	#tools.plot_polygon(shape)


medianera = LineString([[10., 0.], [10., 40.], [0., 40.], [0., 0.]])
shape_info = gen.building_shape(new_built, new_whole, new_levels, False, np.array([1., 0.]), medianera)
shape_info.determine_facets()
gen.process_facets(shape_info)


building_mesh  = create_mesh(shape_info)
building_mesh.plot()


############################
############################
############################
############################


import tools
import roofs
import numpy as np

from importlib import reload

import structure_generation as gen
import matplotlib.pyplot as plt
plt.ion()



#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon




level0 = tools.create_box([5., 20.], [10, 40.], 0.)
level1 = tools.create_box([5., 15.], [10, 30.], 0.)

level2_a = tools.create_box([5., 5.], [10, 10.], 0.)
level2_b = tools.create_box([5., 25.], [10, 10.], 0.)

level2 = unary_union([level2_a, level2_b])

level3_a = tools.create_box([1.25, 5.], [2.5, 10.], 0.)
level3_b = tools.create_box([10 - 1.25, 5.], [2.5, 10.], 0.)




level2_b = unary_union([level2_b, tools.create_box([5., 20.], [3., 4.], 0.)])
shapes = level1.difference(level2_b)

tools.plot_polygon(level2_b)
tools.plot_polygon(shapes)

mesh, val = tools.generate_techito(shapes, level2_b, 3.0, "standard", width = 1.5, slope = 0.3)



############################
############################
############################
############################


import tools
import roofs
import numpy as np

from importlib import reload

import structure_generation as gen
import back_structures as back
import shape_generation as shape_gen
import matplotlib.pyplot as plt
plt.ion()



#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon




level0 = tools.create_box([5., 40.], [10, 20.], 0.)



terrain = tools.create_box([5., 20.], [10, 40.], 0.)

shape_collection, levels = back.generate_cosa(terrain, 3)




tools.plot_polygon(terrain)

for shape in shape_collection:
	tools.plot_polygon(shape)






shape = tools.create_box([5., 20.], [10, 40.], 0.)
front = tools.create_box([5, 5], [10, 10], 0.)

ring = tools.get_rings(shape.difference(front))[0]
perimeter = tools.LineString(ring).difference(front.buffer(10e-6))

tools.plot_polygon(shape)
tools.plot_polygon(tools.LineString(ring))
tools.plot_polygon(perimeter, linewidth = 3)

yard_size = tools.suggest_value(2, 4, 3., 1., 0.1)
yard_sep  = 7 + yard_size
nyards = max(int(perimeter.length/yard_sep) - 1, 1)

positions = np.linspace(yard_size/2., perimeter.length - yard_size, nyards)

patios = []
for i in range(nyards):
	position, direction = tools.position_along_polygon(perimeter.coords, positions[i])

	patio = tools.create_box(position, [yard_size, 5.], rotation = np.arctan2(direction[1], direction[0]))
	patios.append(patio)

patios = unary_union(patios)


shape_collection, levels = back.generate_cosa(level0)

tools.plot_polygon(level0)
for shape in shape_collection:
	tools.plot_polygon(shape)









############################
############################
############################
############################


import tools
import roofs
import numpy as np

from importlib import reload

import structure_generation as gen
import back_structures as back
import shape_generation as shape_gen
import matplotlib.pyplot as plt
plt.ion()



#models = gen.models_catalogue()
from shapely.ops import unary_union, Point, LineString, Polygon



levels_unite, shapes_unite, shapes_substract, levels_substract = [], [], [], []

level = tools.create_box([5., 20.], [10, 40.], 0.)
shapes_unite.append(level)
levels_unite.append(0.)
tools.plot_polygon(level)

level = tools.create_box([5., 5.], [10, 10.], 0.)
shapes_unite.append(level)
levels_unite.append(3.)
tools.plot_polygon(level)

level = tools.create_box([5., 30.], [10, 20.], 0.)
shapes_unite.append(level)
levels_unite.append(6.)
tools.plot_polygon(level)

level = tools.create_box([5., 30.], [10, 10.], 0.)
shapes_unite.append(level)
levels_unite.append(9.)
tools.plot_polygon(level)

level = tools.create_box([5., 5.], [10, 5.], 0.)
shapes_unite.append(level)
levels_unite.append(12.)
tools.plot_polygon(level)



level = tools.create_box([5., 15.], [5, 5.], 0.)
shapes_substract.append(level)
levels_substract.append(0.)
tools.plot_polygon(level)

level = tools.create_box([5., 19.], [5, 5.], 0.)
shapes_substract.append(level)
levels_substract.append(3.)
tools.plot_polygon(level)

level = tools.create_box([5., 2.5], [5, 5.], 0.)
shapes_substract.append(level)
levels_substract.append(3.)
tools.plot_polygon(level)

level = tools.create_box([5., 35], [5, 5.], 0.)
shapes_substract.append(level)
levels_substract.append(6.)
tools.plot_polygon(level)




# whole_shapes, unite_shapes, yards_shapes = shape_gen.better_combine(shapes_unite, levels_unite, shapes_substract, levels_substract)



level0 = tools.create_box([5., 20.], [10, 40.], 0.)
level1 = tools.create_box([5., 15.], [10, 30.], 0.)

level2_a = tools.create_box([5., 5.], [10, 10.], 0.)
level2_b = tools.create_box([5., 25.], [10, 10.], 0.)

level2 = unary_union([level2_a, level2_b])

level3_a = tools.create_box([1.25, 5.], [2.5, 10.], 0.)
level3_b = tools.create_box([10 - 1.25, 5.], [2.5, 10.], 0.)




level2_b = unary_union([level2_b, tools.create_box([5., 20.], [3., 4.], 0.)])
shapes = level1.difference(level2_b)

tools.plot_polygon(level2_a)
tools.plot_polygon(level0)
# tools.plot_polygon(shapes)






mesh = tools.generate_general_roof(level2_a, level0, 3.0, 3., "material1", "material2", kind = "curved", orientation = -1, extension = 0.5)[0]
mesh.plot()


############################
############################
############################
############################


import tools
import roofs
import numpy as np

from importlib import reload

#import structure_generation as gen
import back_structures as back
import shape_generation as shape_gen
import matplotlib.pyplot as plt
plt.ion()
from shapely.ops import unary_union, Point, LineString, Polygon


def generate_general_roof(facet_shape_orig, available, level, height, material1, material2, kind = "curved", orientation = -1, extension = 0.5):
	facet_shape = facet_shape_orig.buffer(extension).intersection(available)
	underlid_shape = facet_shape.difference(facet_shape_orig)
	underlid    = tools.meshify_shape(underlid_shape, level, material1, [1]*4, flip = True)
	if facet_shape.area != 0.0:
		length, width, orientation, coords, centroid = tools.get_orientation(facet_shape)
		u = np.array([ np.cos(orientation), np.sin(orientation)])
		v = np.array([-np.sin(orientation), np.cos(orientation)])
		s_min, s_max = (coords[:, 0]*u[0] + coords[:, 1]*u[1]).min(), (coords[:, 0]*u[0] + coords[:, 1]*u[1]).max()

		roof = tools.Mesh()
		res = 0.2

		nrep = max(int(width/res), 1)
		dx = width/nrep

		shapes = []
		perimeter = []
		for ring in tools.get_rings(tools.orient(facet_shape)):
			perimeter.append(tools.get_intersections(ring, u, v, s_min, dx))

		for i in range(nrep):
			center = [centroid[0]  + (i*dx - width/2 + dx/2.)*u[0], centroid[1]  + (i*dx - width/2 + dx/2.)*u[1]]
			minishape = tools.create_box(center, [dx, length], orientation).intersection(facet_shape)
			surf = tools.meshify_shape(minishape, level, material1, np.ones(4))
			roof += surf

		nverts = len(roof.submeshes[material1].vertices)
		new_vertices = np.zeros([nverts, 3])
		for i in range(nverts):
			vert = roof.submeshes[material1].vertices[i]
			s = vert[0]*u[0] + vert[1]*u[1]

			if kind == "curved":
				vert[2] = tools.curve(level, s_min, s_max, s)
			elif kind == "wave":
				vert[2] = tools.sine(level, s_min, s_max, s)
			elif kind == "twosided":
				vert[2] = tools.twosided(level, s_min, s_max, s)

			new_vertices[i] = vert
		roof.submeshes[material1].vertices = new_vertices

		walls = tools.extrude_rings_list(perimeter, level, level + 1.0, material2, color = np.ones(4))


		nverts = len(walls.submeshes[material2].vertices)
		new_vertices = np.zeros([nverts, 3])
		for i in range(nverts):
			vert = walls.submeshes[material2].vertices[i]
			s = vert[0]*u[0] + vert[1]*u[1]

			if vert[2] >= level + 0.5:
				if kind == "curved":
					vert[2] = tools.curve(level, s_min, s_max, s)
				elif kind == "wave":
					vert[2] = tools.sine(level, s_min, s_max, s)
				elif kind == "twosided":
					vert[2] = tools.twosided(level, s_min, s_max, s)

			new_vertices[i] = vert
		walls.submeshes[material2].vertices = new_vertices
		walls.submeshes[material2].uvs = tools.wrap_uvs_method1(walls.submeshes[material2])

		return roof + walls + underlid, True
	else:
		return tools.Mesh(), False




level0 = tools.create_box([5., 40.], [10, 20.], 0.)



terrain = tools.create_box([5., 20.], [10, 40.], 0.)
built = Polygon([[0., 0.], [10., 0.], [15., 25.], [15., 30.], [0., 30], [0., 0.]]).difference(tools.create_box([2., 15.], [4., 15.], 0.))
# whole = Polygon([[0., 0.], [10., 0.], [15., 25.], [15., 30.], [0., 30], [0., 0.]])
extent, center, width, length = tools.determine_extent(built)
whole = tools.create_box_from_extent(extent)


tools.plot_polygon(built)
tools.plot_polygon(whole)

walls = tools.extrude_rings(built, 0., 2., "default", [1]*4)
mesh = generate_general_roof(built, whole, 2., 1., "material1", "material2", kind = "twosided", orientation = -1)[0] + walls

mesh.plot()




built = built.buffer(10e-5)
qwe = built.buffer(-0.15, join_style = 2)
wall_shape = built.difference(qwe)

tools.plot_polygon(wall_shape)






shape = tools.Polygon([[0., 0.], [1., 0.], [0., 10.], [0., 0.]])




### bushes

terrain = tools.create_box([5., 10.], [10, 20.], 0.)

shape = terrain.difference(terrain.buffer(-1))
imperfections = tools.extrude_shape(shape, 0., 1.0, "Wood", [1]*4)


bushes = tools.generate_bushes(shape, 0., density = 50., size = 0.2)







import numpy as np
import matplotlib.pyplot as plt
import modulo
import manzana
import tools
import time
import structure_generation as gen
from importlib import reload


facade_mesh = gen.facade_assets["front"][8].mesh

verts = facade_mesh.whole_mesh.vertices

plt.plot(verts[:, 1], verts[:, 2], ".")


shapes = from_facade_shapes(facade_mesh)
mesh = get_mesh(shapes, [5., 15.]*5, [0, 0, 3, 3, 6, 6, 9, 9, 12, 12], 1.0, 1.0, 20., 15.)



def generate_facade_mesh(facade_list):
	facade_mesh = tools.Mesh()

	for i in range(len(facade_list.tags)):
		# print(facade_list.x_scales[i])
		height      = facade_list.z_scales[i]
		avail_width = facade_list.widths[i]
		position    = facade_list.positions[i]
		orientation = facade_list.orientations[i]*np.pi/180. + np.pi/2.
		asset_name  = facade_list.tags[i]
		asset       = facade_assets[asset_name][np.mod(facade_list.asset_ids[i], len(facade_assets[asset_name]))]
		asset_height= asset.height
		asset_width = asset.width

		scale_factor_x = min(facade_list.x_scales[i]/10., avail_width/asset.max_width)

		individual_facade_mesh = tools.Mesh()

		color1, color2, color3 = facade_list.colors1[i], facade_list.colors2[i], facade_list.colors3[i]

		parte1 = asset.mesh.submeshes["color1"]
		parte2 = asset.mesh.submeshes["color2"]
		parte3 = asset.mesh.submeshes["color3"]
		parte1.material = "Smooth_Wall"
		parte2.material = "Smooth_Wall"
		parte3.material = "Smooth_Wall"

		for j in range(4):
			parte1.colors[:, j] = color1[j]
			parte2.colors[:, j] = color2[j]
			parte3.colors[:, j] = color3[j]

		individual_facade_mesh += parte1.get_mesh() + parte2.get_mesh() + parte3.get_mesh()

		parte_win = asset.mesh.submeshes["window_reference"]
		mat_dict  = {"Yard_Door": [], "Offices2": [], "Offices_Interior": [], "Normal_Interior": [], "Curtains_Interior": [], "Blind_Interior": []}
		for k in range(len(asset.windows)):
			window  = asset.windows[k]
			x_scale = asset.win_scales[k][0]
			z_scale = asset.win_scales[k][1]

			#verices = parte_win.mesh
			if   np.mod(k, 3) == 0: win_mat = facade_list.windows1[i]
			elif np.mod(k, 3) == 1: win_mat = facade_list.windows2[i]
			elif np.mod(k, 3) == 2: win_mat = facade_list.windows3[i]

			if win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
				mat_dict["Blind_Interior"] += window

			elif win_mat == "curtains":
				mat_dict["Curtains_Interior"] += window

			elif win_mat == "glass":
				mat_dict["Normal_Interior"] += window

			elif win_mat == "normal":
				mat_dict["Normal_Interior"] += window

			aperture = 0. if np.random.random() <= 0.4 else np.random.random()
			R, G, B  = np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5, np.random.random()*0.5 + 0.5

			# actual_size_x = width/asset_width*x_scale
			actual_size_x = scale_factor_x*x_scale
			actual_size_z = height/asset_height*z_scale

			parte_win.colors[window, 2] = actual_size_x/5.0
			parte_win.colors[window, 3] = actual_size_z/5.0

			if win_mat in ["glass", "curtains"]:
				parte_win.colors[window, 0] = R
				parte_win.colors[window, 1] = aperture*0.5

			elif win_mat == "normal":
				parte_win.colors[window, 0] = np.random.random()
				parte_win.colors[window, 1] = np.random.random()

			elif win_mat in ["blind_metal", "blind_plastic", "blind_wood"]:
				if   win_mat == "blind_wood":    parameter = 0.0
				elif win_mat == "blind_metal":   parameter = 0.5
				else:                            parameter = 1.0

				parte_win.colors[window, 0] = parameter
				parte_win.colors[window, 1] = aperture

		parte_win = mesh_decompose(parte_win, mat_dict)

		individual_facade_mesh += parte_win

		for other in asset.mesh.materials:
			if not(other in ["color1", "color2", "color3", "window_reference"]):
				individual_facade_mesh += asset.mesh.submeshes[other].get_mesh()

		vec_i = np.array([ np.cos(orientation), np.sin(orientation), 0.])
		vec_j = np.array([-np.sin(orientation), np.cos(orientation), 0.])

		for material in individual_facade_mesh.materials:
			verts  = np.copy(individual_facade_mesh.submeshes[material].vertices)
			nverts = len(verts)

			rotated_verts = np.zeros(verts.shape)

			rotated_verts[:, 0] = position[0] + verts[:, 0]*vec_i[0] + verts[:, 1]*vec_j[0]*scale_factor_x + verts[:, 2]*vec_up[0]*height/asset_height
			rotated_verts[:, 1] = position[1] + verts[:, 0]*vec_i[1] + verts[:, 1]*vec_j[1]*scale_factor_x + verts[:, 2]*vec_up[1]*height/asset_height
			rotated_verts[:, 2] = position[2] + verts[:, 0]*vec_i[2] + verts[:, 1]*vec_j[2]*scale_factor_x + verts[:, 2]*vec_up[2]*height/asset_height

			individual_facade_mesh.submeshes[material].vertices = rotated_verts
		facade_mesh += individual_facade_mesh

	facade_mesh.submeshes["Smooth_Wall"].uvs = tools.wrap_uvs_method1(facade_mesh.submeshes["Smooth_Wall"])

	return facade_mesh











reload(classes)
# nbuildings = len(buildings)
c_facade = buildings[np.random.randint(nbuildings)].complex_facades[2]
asd = classes.get_mesh_from_shapes(c_facade.presets, c_facade.facet, gen.facade_assets, c_facade.xpos, c_facade.zpos, c_facade.app_width, c_facade.floor_height)
asd.whole_mesh.plot("sasd")

verts = asd.whole_mesh.vertices

# verts = tools.rotate(verts, tools.vec_right, 90*tools.deg2rad)
plt.plot(verts[:, 0], verts[:, 1], ".")
#



asd.rotate(tools.vec_right, 90*tools.deg2rad)
