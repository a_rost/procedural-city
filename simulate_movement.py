from importlib import reload
import numpy as np
import matplotlib.pyplot as plt
import lighting_toolkit as light
import modulo
import tools
import time

plt.ion()

clamp = lambda x, a, b: a if x < a else (b if x > b else x)

benchmark = False
def get_height(heightmap, extent, point2D):
	N, M = heightmap.shape
	dx, dy = (extent[1] - extent[0])/N, (extent[3] - extent[2])/M

	I = int((point2D[0] - extent[0])/dx)
	J = int((point2D[1] - extent[2])/dy)

	I = max(min(I, N - 1), 0)
	J = max(min(J, M - 1), 0)

	return heightmap[I, J]

def correct_position(heightmap, extent, point3D):
	h = get_height(heightmap, extent, point3D)

	return np.array([point3D[0], point3D[1], max(point3D[2], h + 0.1)])

def generate_point(heightmap, extent):
	h_min, h_max = heightmap.min(), heightmap.max()

	x, y, z = np.random.random()*(extent[1] - extent[0]) + extent[0], np.random.random()*(extent[1] - extent[0]) + extent[0], np.random.random()*(h_max - h_min) + h_min
	result = correct_position(heightmap, extent, [x, y, 0.])
	return result


def dist_point_to_edge(point, edge1, edge2):
	length    = np.linalg.norm(edge2 - edge1)
	direction = (edge2 - edge1)/length

	mu = np.dot(point - edge1, direction)

	if mu < 0.:
		return np.linalg.norm(point - edge1)
	elif mu > length:
		return np.linalg.norm(point - edge2)
	else:
		return np.linalg.norm(edge1 + direction*mu - point)





def sign(p1, p2, p3):
	return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1])


def PointInTriangle(pt, v1, v2, v3):
	d1 = sign(pt, v1, v2)
	d2 = sign(pt, v2, v3)
	d3 = sign(pt, v3, v1)

	has_neg = (d1 < 0) or (d2 < 0) or (d3 < 0)
	has_pos = (d1 > 0) or (d2 > 0) or (d3 > 0)

	return not(has_neg and has_pos)



def compute_distance_point_to_triangle(point, vertices, triangle):
	closest_point = -1
	vert1 = vertices[triangle[0]]
	vert2 = vertices[triangle[1]]
	vert3 = vertices[triangle[2]]

	dist_edge1 = dist_point_to_edge(point, vert1, vert2)
	dist_edge2 = dist_point_to_edge(point, vert2, vert3)
	dist_edge3 = dist_point_to_edge(point, vert3, vert1)

	dist_min = min(min(dist_edge1, dist_edge2), dist_edge3)

	if dist_min == dist_edge1:
		closest_point = 0
	elif dist_min == dist_edge2:
		closest_point = 1
	else:
		closest_point = 2

	seg1 = vert2 - vert1
	seg2 = vert3 - vert1

	normal = np.cross(seg1, seg2)/np.linalg.norm(np.cross(seg1, seg2))
	vec1   = seg1/np.linalg.norm(seg1)
	vec2   = np.cross(normal, seg1)

	v0 = [0., 0.]
	v1 = [np.linalg.norm(seg1), 0.]

	aux = point - vert1
	pt =  [np.dot(aux, vec1), np.dot(aux, vec2)]
	v2 = [np.dot(seg2, vec1), np.dot(seg2, vec2)]


	distance_to_plane = np.linalg.norm(pt[0]*vec1 + pt[1]*vec2 + vert1 - point)
	if PointInTriangle(pt, v0, v1, v2):
		closest_point = 3
		print("Point in triangle")
		dist_min = min(dist_min, distance_to_plane)

	return dist_min, closest_point



def visualize_subset(mesh, tri_ids):
	meshi = tools.SubMesh(vertices = mesh.whole_mesh.vertices, triangles = mesh.whole_mesh.triangles[tri_ids], colors = mesh.whole_mesh.colors, uvs = mesh.whole_mesh.uvs)
	meshi.plot()



mesh = light.load_all(1, usefile = False, nblocks = 1)
dx = 0.5
heightmap, extent = modulo.get_heightmap(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, dx = dx)


h_min, h_max = heightmap.min(), heightmap.max()
extent2 = [extent[0] - 1, extent[1] + 1, extent[2] - 1., extent[3] + 1., h_min - 1., h_max + 1]

ray_distance = 30.

modulo.create_gen_tree(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, extent2, depth = 20)

# tris = modulo.retrieve_tree_triangles(2, 0)

modulo.get_triangle_count(0, 0)

neighbors, count = modulo.link_triangles(mesh.whole_mesh.vertices, mesh.whole_mesh.triangles, link_dist = 1., multiplier = 1600)
offsets = np.cumsum(count) - count


def get_ref_system_triangle(vertices, triangle):
	vert1, vert2, vert3 = vertices[triangle[0]], vertices[triangle[1]], vertices[triangle[2]]

	seg1, seg2 = vert2 - vert1, vert3 - vert1
	normal = np.cross(seg1, seg2)/np.linalg.norm(np.cross(seg1, seg2))

	vec1 = seg1/np.linalg.norm(seg1)

	vec2 = np.cross(normal, vec1)

	return vert1, seg1, seg2, vec1, vec2, normal

def get_normal(vertices, triangle):
	vert1, vert2, vert3 = vertices[triangle[0]], vertices[triangle[1]], vertices[triangle[2]]

	seg1, seg2 = vert2 - vert1, vert3 - vert1
	normal = np.cross(seg1, seg2)/np.linalg.norm(np.cross(seg1, seg2))
	return normal

def formula_plane(vertices, triangle, point):
	origin = vertices[triangle[0]]
	normal = get_normal(vertices, triangle)
	return np.dot(normal, point - origin)

def get_hinge_vector(vertices, triangle1, triangle2):
	### points that belong to both planes defined by triangle1 and triangle2 can be written like: p0 + mu*q, for any real mu
	origin1 = vertices[triangle1[0]]
	origin2 = vertices[triangle2[0]]

	normal1 = get_normal(vertices, triangle1)
	normal2 = get_normal(vertices, triangle2)

	q = np.cross(normal1, normal2)/np.linalg.norm(np.cross(normal1, normal2))

	A1, A2 = np.dot(normal1, origin1), np.dot(normal2, origin2)
	B = np.dot(normal1, normal2)

	eta = (A1 - A2*B)/(1 - B**2)
	sita = A2 - eta*B
	p0 = eta*normal1 + sita*normal2
	return p0, q

vertices, triangles = mesh.whole_mesh.vertices, mesh.whole_mesh.triangles
triangle1, triangle2 = triangles[2], triangles[800]

p0, q = get_hinge_vector(vertices, triangle1, triangle2)

print(formula_plane(vertices, triangles[2], p0))
print(formula_plane(vertices, triangles[800], p0))


def det_min_triangle_distance(vertices, current_tri_id, point):
	dist_min, id_min, min_on_area = np.inf, -1, False

	for i in range(count[current_tri_id]):
		tri = neighbors[offsets[current_tri_id] + i]
		distance, on_area = compute_distance_point_to_triangle(point, vertices, triangles[tri])
		if distance < dist_min:
			dist_min, id_min, min_on_area = distance, tri, on_area

	return dist_min, id_min, min_on_area


def get_spawn_triangle(vertices, triangles, ntrials = 100):
	for j in range(ntrials):
		tri_id = np.random.randint(0, len(triangles))

		triangle = triangles[tri_id]

		vert1, seg1, seg2, vec1, vec2, normal = get_ref_system_triangle(vertices, triangle)

		start_point = vert1 + seg1*0.5 + seg2*0.5 + 0.5*normal

		point = start_point*1.
		dist_min, id_min, min_on_area = np.inf, -1, False

		dist_min, id_min, min_on_area = det_min_triangle_distance(vertices, tri_id, point)

		if abs(dist_min - 0.5) <= 0.0001:
			print("Total trials:", j + 1)
			break

	start_direction = np.cos(0.5)*vec1 + np.sin(0.5)*vec2
	return tri_id, start_point, start_direction

tri_id, start_point, start_direction = get_spawn_triangle(vertices, triangles)




