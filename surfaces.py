import numpy as np
import matplotlib.pyplot as plt
import modulo
#from mayavi import mlab

plt.ion()

extent = [0., 100., 0., 100]

		
def function(height_map, dx, dy, x_min, y_min, x, y):
	N, M = len(height_map), len(height_map[0])
	
	i, j = int((x - x_min)/dx), int((y - y_min)/dy)

	i, j = max(i, 0), max(j, 0)
	i, j = min(i, N - 2), min(j, M - 2)

	f1 = height_map[i + 0, j + 0]
	f2 = height_map[i + 0, j + 1]
	f3 = height_map[i + 1, j + 0]
	f4 = height_map[i + 1, j + 1]
	
	g1 = f1 + (f2 - f1)*(y - (y_min + j*dy))/dy
	g2 = f3 + (f4 - f3)*(y - (y_min + j*dy))/dy

	return g1 + (g2 - g1)*(x - (x_min + i*dx))/dx


class HeightMap:
	def __init__(self, heightmap, extent):
		self.heightmap = heightmap
		self.extent    = extent
		self.N, self.M = heightmap.shape
		self.dx, self.dy = (extent[1] - extent[0])/self.N, (extent[3] - extent[2])/self.M
	
	def get_height(self, point):
		return function(self.heightmap, self.dx, self.dy, self.extent[0], self.extent[2], point[0], point[1])

	def get_distance(self, point1, point2):
		h1 = self.get_height(point1)
		h2 = self.get_height(point2)
		
		A = np.array([point1[0], point1[1], h1])
		B = np.array([point2[0], point2[1], h2])
		return np.linalg.norm(A - B)

	def gradient(self, point, dh = 0.1):
		grad = np.zeros(2)

		point1, point2 = [point[0] + dh, point[1]], [point[0] - dh, point[1]]
		h1 = self.get_height(point1)
		h2 = self.get_height(point2)

		grad[0] = (h1 - h2)*0.5/dh

		point1, point2 = [point[0], point[1] + dh], [point[0], point[1] - dh]

		h1 = self.get_height(point1)
		h2 = self.get_height(point2)

		grad[1] = (h1 - h2)*0.5/dh
		return grad
		
	def transform(self, point2D):
		return np.array([point2D[0], point2D[1], self.get_height(point2D)])
#def gradient(height_map, point1, point2, point):
	#dx = 0.1

	#Dx1 = distance(point1, [point[0] - dx, point[1]], height_map) + distance(point2, [point[0] - dx, point[1]], height_map)
	#Dx2 = distance(point1, [point[0] + dx, point[1]], height_map) + distance(point2, [point[0] + dx, point[1]], height_map)
	
	#Dy1 = distance(point1, [point[0], point[1] - dx], height_map) + distance(point2, [point[0], point[1] - dx], height_map)
	#Dy2 = distance(point1, [point[0], point[1] + dx], height_map) + distance(point2, [point[0], point[1] + dx], height_map)

	#grad = [(Dx2 - Dx1)*0.5/dx, (Dy2 - Dy1)*0.5/dx]
	#return grad

#def gradient(height_map, point):

#def transform(point, height_map):
	#return np.array([point[0], point[1], function(height_map, 1, 1, extent[0],  extent[2], point[0], point[1])])

#def geodesic(point1, point2, height_map, precision = 1.):
	#nitera, beta = 1000, 0.5
	#distance = np.linalg.norm(point2 - point1)
	#initial_path = np.linspace(point1, point2, max(int(distance/precision), 1))
	#orig_path = np.copy(initial_path)
	##initial_path[1:len(initial_path) - 1,0] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	##initial_path[1:len(initial_path) - 1,1] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	#path  = np.copy(initial_path)
	#Nseg = len(initial_path)
	#Mpoints = np.zeros([Nseg, 2])

	#if Nseg >= 3:
		#for i in range(nitera):
			#length = 0.
			#for n in range(1, Nseg - 1):
				#p1, p2 = height_map.transform(path[n - 1]), height_map.transform(path[n + 1])
				#p = height_map.transform(path[n])
				#length += np.linalg.norm(p - p1)
				
				#grad = height_map.gradient(p)

				#u = np.array([1, 0, grad[0]])
				#v = np.array([0, 1, grad[1]])

				##print(grad)
				##print(u)
				##print(v)
				#normal = np.cross(u, v)/np.linalg.norm(np.cross(u, v))

				#vec1 = p1 - p
				#vec2 = p2 - p
				
				#vec = vec1 + vec2
				#vec *= np.random.normal(size = 3, loc = 1., scale = 0.05)

				#vec = vec - normal*np.dot(normal, vec)
				
				#norma = np.linalg.norm(vec)
				#if norma >= 1.:
					#vec *= 1./norma
				
				#Mpoints[n] = vec[0:2]
			##print(Mpoints)
				#path[n] += Mpoints[n]*beta
			#length += np.linalg.norm(p2 - p)
			#if i == 0:
				#first = length
			#print("lenght " + str(length/first))
			#plt.plot(path[:, 0], path[:, 1])
	#plt.plot(path[:, 0], path[:, 1], linewidth = 3., color = "k")
	#plt.plot(orig_path[:, 0], orig_path[:, 1], linewidth = 3., color = "r", linestyle = "dashed")
	
	#return path

def generate_point(point1, point2, height_map, on_surface = False):
	straight_distance = np.linalg.norm(point2 - point1)
	direction = (point2 - point1)/straight_distance
	
	perpendicular = np.cross(direction, np.array([0., 0., 1.]))
	perpendicular *= 1./np.linalg.norm(perpendicular)

	min_dist, point = 100., (point1 + point2)*0.5
	for i in range(20):
		if i == 0:
			new_point = (point1 + point2)/2.
		else:
			new_point = (point2 + point1)/2. + perpendicular*straight_distance*((i-10)/10.)
			
		p_surface = height_map.transform(new_point)
		
		if new_point[2] < p_surface[2] or on_surface == True:
			new_point[2] = p_surface[2]

		distance = np.linalg.norm(new_point - point1) + np.linalg.norm(point2 - new_point)
		if distance < min_dist:
			min_dist = distance
			point    = new_point
	return point, min_dist


def enderezar(height_map, path, nitera = 1, on_surface = False):
	#npoints = int((len(path) + 1)/2 - 2)
	npoints = len(path)
	n_pares   = int((npoints - 1)/2) - 1
	n_impares = int((npoints - 1)/2)

	for i in range(nitera):
		for j in range(n_pares):
			point1, point2 = path[2*j + 1], path[2*j + 3]
			point, distance = generate_point(point1, point2, height_map, on_surface)
			path[2*j + 2] = point
		for j in range(n_impares):
			point1, point2 = path[2*j + 0], path[2*j + 2]
			point, distance = generate_point(point1, point2, height_map, on_surface)
			path[2*j + 1] = point
	return np.array(path)

def generate_path(point1, point2, height_map, nsubdivision = 5, on_surface = False):
	#point1, point2 = transform(point1_2D, height_map), transform(point2_2D, height_map)
	path = [point1, point2]
	for i in range(nsubdivision):
		npoints = len(path)
		new_path = []
		print("n orig: ", npoints)

		for j in range(npoints - 1):
			point1, point2 = path[j], path[j + 1]
			new_path.append(point1)
			point = (point1 + point2)*0.5
			new_path.append(point)

		new_path.append(path[npoints - 1])

		path = enderezar(height_map, new_path, 3, on_surface)
		plt.plot(path[:, 0], path[:, 1], linewidth = 1., color = np.array([i/nsubdivision, 0.3, 1. - i/nsubdivision]))
	#path = enderezar(height_map, path, 5)
	return np.array(path)

def geodesic(point1_2D, point2_2D, height_map, precision = 1.):
	nitera, beta = 50, 0.5

	point1, point2 = height_map.transform(point1_2D), height_map.transform(point2_2D)

	distance = np.linalg.norm(point2 - point1)
	#initial_path = np.linspace(point1, point2, max(int(distance/precision), precision))
	initial_path = generate_path(point1, point2, height_map)
	orig_path = np.copy(initial_path)
	#initial_path[1:len(initial_path) - 1,0] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	#initial_path[1:len(initial_path) - 1,1] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	path  = np.copy(initial_path)
	Nseg = len(initial_path)
	Mpoints = np.zeros([Nseg, 3])

	if Nseg <= 3:
		for i in range(nitera):
			length = 0.
			for n in range(1, Nseg - 1):
				p1, p2, p = path[n - 1], path[n + 1], path[n]

				p_surface = height_map.transform(p)
				if p_surface[2] > p[2]:
					on_surface = True
					p[2] = p_surface[2]
				else:
					on_surface = False
				
				length += np.linalg.norm(p - p1)
				
				grad = gradient(height_map, p)

				u = np.array([1, 0, grad[0]])
				v = np.array([0, 1, grad[1]])

				normal = np.cross(u, v)/np.linalg.norm(np.cross(u, v))

				vec1 = p1 - p
				vec2 = p2 - p
				
				vec = vec1 + vec2
				#vec *= np.random.normal(size = 3, loc = 1., scale = 0.05)

				if on_surface:
					vec = vec - normal*np.dot(normal, vec)
				
				norma = np.linalg.norm(vec)
				if norma >= 1.:
					vec *= 1./norma
				
				Mpoints[n] = vec

				path[n] += Mpoints[n]*beta
				
			length += np.linalg.norm(p2 - p)
			if i == 0:
				first = length
			print("lenght " + str(length/first))
			plt.plot(path[:, 0], path[:, 1])
	
	return path





def geodesic_on_surface(point1_2D, point2_2D, height_map):
	nitera, beta = 50, 0.5

	point1, point2 = height_map.transform(point1_2D), height_map.transform(point2_2D)

	distance = np.linalg.norm(point2 - point1)
	#initial_path = np.linspace(point1, point2, max(int(distance/precision), precision))
	initial_path = generate_path(point1, point2, height_map, on_surface = True, nsubdivision = 10)
	orig_path = np.copy(initial_path)
	#initial_path[1:len(initial_path) - 1,0] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	#initial_path[1:len(initial_path) - 1,1] +=  np.random.random(len(initial_path) - 2)*distance/10. - distance/20.
	path  = np.copy(initial_path)
	Nseg = len(initial_path)
	Mpoints = np.zeros([Nseg, 3])

	if Nseg <= 3:
		for i in range(nitera):
			length = 0.
			for n in range(1, Nseg - 1):
				p1, p2, p = path[n - 1], path[n + 1], path[n]

				p = height_map.transform(p)

				#p[2] = p_surface[2]

				
				length += np.linalg.norm(p - p1)
				
				grad = gradient(height_map, p)

				u = np.array([1, 0, grad[0]])
				v = np.array([0, 1, grad[1]])

				normal = np.cross(u, v)/np.linalg.norm(np.cross(u, v))

				vec1 = p1 - p
				vec2 = p2 - p
				
				vec = vec1 + vec2
				#vec *= np.random.normal(size = 3, loc = 1., scale = 0.05)

				vec = vec - normal*np.dot(normal, vec)
				
				norma = np.linalg.norm(vec)
				if norma >= 1.:
					vec *= 1./norma
				
				Mpoints[n] = vec

				path[n] += Mpoints[n]*beta
				path[n] = height_map.transform(path[n])
				
			length += np.linalg.norm(p2 - p)
			if i == 0:
				first = length
			print("lenght " + str(length/first))
			plt.plot(path[:, 0], path[:, 1])
	
	return path

def load_heightmap(filename):
	output = np.loadtxt(filename, max_rows = 1, dtype = np.float32)
	N, M = np.int32(output[0]), np.int32(output[1])
	extent, interesting_extent = output[2:6], output[6: 10]
	
	print(extent)
	surface = np.zeros([N, M], dtype = np.float32)
	
	output = np.loadtxt(filename, skiprows = 1, dtype = np.float32)
	for i in range(N):
		for j in range(M):
			surface[i, j] = output[i*M + j]

	heightmap = HeightMap(surface, extent)
	heightmap.interesting_extent = interesting_extent
	return heightmap


#heightmap = load_heightmap("/home/agustin/unity-game/Assets/Resources/map_2.txt")


#point1 = np.random.random()*(heightmap.interesting_extent[1] - heightmap.interesting_extent[0]) + heightmap.interesting_extent[0], np.random.random()*(heightmap.interesting_extent[3] - heightmap.interesting_extent[2]) + heightmap.interesting_extent[2]
#point2 = np.random.random()*(heightmap.interesting_extent[1] - heightmap.interesting_extent[0]) + heightmap.interesting_extent[0], np.random.random()*(heightmap.interesting_extent[3] - heightmap.interesting_extent[2]) + heightmap.interesting_extent[2]


#path = geodesic_on_surface(point1, point2, heightmap)
#path = geodesic(point1, point2, heightmap)
#modulo.load_map(surface, extent)
#path = modulo.geodesic(point1, point2, surface, nitera = 5)
#path[:, 2] = modulo.evaluate_heights(path[:, 0:2])

#mlab.surf(heightmap.heightmap, extent = list(heightmap.extent) + [0., heightmap.heightmap.max()])
#mlab.plot3d(path[:, 0], path[:, 1], path[:, 2], tube_radius = 1.)

#plt.imshow(heightmap.heightmap.T, origin = "lower", extent = heightmap.extent)
#plt.plot(path[:, 0], path[:, 1], color = "k")
#plt.plot([point1[0], point2[0]], [point1[1], point2[1]], "o")

#import matplotlib.pyplot as plt
#plt.ion()
#import modulo
#plt.imshow(modulo.perlin2D([1., 1.], 0.001, 1000))

#noise = modulo.perlin2D([1., 1.], 2**0, 2048)
#for i in range(10):
	#noise += modulo.perlin2D([1., 1.], 2**(-i), 2048)*i
#plt.imshow(noise)
