
import tools
import numpy as np
import time
import matplotlib.pyplot as plt
import modulo
import xatlas
import os
# from PIL import Image
# from trimesh.visual import texture, TextureVisuals
from importlib import reload
# from trimesh import Trimesh
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light

plt.ion()

meshes_lod0 = light.load_meshes(lod = 0, usefile = False, nblocks = 1)
meshes_lod2 = light.load_meshes(lod = 2, usefile = False, nblocks = 1)

mesh_lod2, mesh_lod0 = tools.Mesh(), tools.Mesh()
for mesh in meshes_lod2:
	mesh_lod2 += mesh
mesh_lod2 = mesh_lod2.whole_mesh
for mesh in meshes_lod0:
	mesh_lod0 += mesh

######################
####3 remove overlapping areas
######################
"""
mesh = meshes_lod0[0]
mesh.calculate_normals()
mesh.calculate_whole_mesh()

vertices, triangles  = mesh.whole_mesh.vertices, mesh.whole_mesh.triangles

vert0 = vertices[triangles[:, 0]]
seg1  = vertices[triangles[:, 1]] - vert0
seg2  = vertices[triangles[:, 2]] - vert0

triang_normals = np.cross(seg1, seg2, axis = 1)
norma = np.linalg.norm(triang_normals, axis = 1)
for i in range(3): triang_normals[:, i] *= 1/norma

triang_normals_op = -triang_normals

# vert0 = np.repeat(vert0, 2, axis = 0)
# triang_normals = np.repeat(triang_normals, 2, axis = 0)

# triang_normals[1::2] *= -1

S = vert0[:, 0]*triang_normals[:, 0] + vert0[:, 1]*triang_normals[:, 1] + vert0[:, 2]*triang_normals[:, 2]
theta = np.arccos(triang_normals[:, 2])
phi   = np.mod(np.arctan2(triang_normals[:, 1], triang_normals[:, 0]) + 2*np.pi, 2*np.pi)

terna1 = np.zeros([len(triangles), 3], dtype = np.float32)
terna1[:, 0] = S
terna1[:, 1] = theta
terna1[:, 2] = phi


theta = np.arccos(triang_normals_op[:, 2])
phi   = np.mod(np.arctan2(triang_normals_op[:, 1], triang_normals_op[:, 0]) + 2*np.pi, 2*np.pi)

terna2 = np.zeros([len(triangles), 3], dtype = np.float32)
terna2[:, 0] = -S
terna2[:, 1] = theta
terna2[:, 2] = phi




unique_terna1, inverse, indices, counts = np.unique(terna1, axis = 0, return_index = True, return_inverse=True, return_counts = True)
unique_terna2, inverse, indices, counts = np.unique(terna2, axis = 0, return_index = True, return_inverse=True, return_counts = True)

list_idxs = []
for i in range(len(unique_terna1)):
	list_idxs.append([])


for i in range(len(triangles)):
	list_idxs[indices[i]].append(i)


coincidences = []
for i in range(len(unique_terna1)):
	if i%10 == 0: print(i)
	params1 = unique_terna1[i]
	mask = (params1 == unique_terna2[i + 1: len(unique_terna2)]).any(axis = 1)

	for j in range(mask.sum()):
		coincidences.append([i, np.arange(i + 1, len(unique_terna2))[mask][j]])

	for j in range(i + 1, len(unique_terna2)):
		params2 = unique_terna2[j]

		if (params1 == params2).all():
			coincidences.append([i, j])
"""

######################
######################
######################
# s





ambient_occlusion = True




# city_mesh.whole_mesh.calculate_normals()
mesh = mesh_lod2.whole_mesh
# mesh = meshes_lod0[0].whole_mesh
# mesh = city_mesh.whole_mesh
# mesh = city_mesh.submeshes["Wall"]
mesh.calculate_normals()

vmapping, indices, uvs = xatlas.parametrize(mesh.vertices, mesh.triangles)

mesh.vertices = mesh.vertices[vmapping]
mesh.triangles = indices
mesh.normals   = mesh.normals[vmapping]
mesh.uvs       = mesh.uvs[vmapping]
mesh.colors    = mesh.colors[vmapping]
mesh.uv2       = uvs


# os.system("rm AO/*.npy")
itera, optimize = 50, True
nthreads = 18

if ambient_occlusion:
	np.random.seed(1)
	cosines = np.random.random(itera)
	thetas = np.arccos(cosines)*180/np.pi
	# phis   = np.random.random(itera)*360
	# thetas = np.ones(itera)*60
	phis   = np.linspace(0., 360., itera, endpoint = False)

else:
	latitude, rotation = 33*np.pi/180., 15.*np.pi/180
	thetas = np.zeros(itera)
	phis   = np.zeros(itera)
	rot_matrix1 = np.array([[np.cos(rotation), np.sin(rotation), 0.], [-np.sin(rotation), np.cos(rotation), 0.], [0., 0., 1]]).T
	rot_matrix2 = np.array([[1., 0., 0.], [0., np.cos(latitude), -np.sin(latitude)], [0., np.sin(latitude), np.cos(latitude)]]).T

	rot_matrix = np.matmul(rot_matrix1, rot_matrix2)
	for i in range(itera):
		angle = np.pi*i/(itera - 1)
		vec_primed = np.array([np.cos(angle), 0., np.sin(angle)])
		vec = np.matmul(rot_matrix, vec_primed)
		thetas[i] = np.arccos(vec[2])*180/np.pi
		phis[i]   = np.arctan2(vec[1], vec[0])*180/np.pi


resolution = [2048, 2048]
averaged_texture  = np.zeros(resolution + [3], dtype = np.float32)
weights_sum = np.zeros(resolution, dtype = np.float32)

# dx = 0.1
dx = 0.1
timer = 0.
for i in range(itera):
	print("Itera: " + str(i))
	# theta = i*45/itera
	theta, phi = thetas[i], phis[i]

	vertices, normals, triangles = light.rotate_mesh(mesh, theta, phi)
	cam_direction = np.array([0., 0., 1.])

	sky_dir = tools.rotate(np.array([0., 0., 1.]), tools.vec_up, phi*tools.deg2rad)
	sky_dir = tools.rotate(sky_dir, tools.vec_right, theta*tools.deg2rad)

	block_extent = modulo.get_extent(vertices)

	time_start = time.time()
	quantity = mesh.uv2[:, 0]
	heightmap_aux, map_u, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)

	quantity = mesh.uv2[:, 1]
	heightmap_aux, map_v, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)

	quantity = np.matmul(normals, np.array([0., 0., 1]))
	quantity[quantity < 0.] = 0.
	heightmap_aux, cos, _ = modulo.map_quantity(vertices, quantity, triangles, block_extent, dx = dx, nthreads = nthreads, optimize = optimize)

	time_end = time.time()
	timer += time_end - time_start

	weight = light.wrap_texture(cos, map_u, map_v, res = resolution)*2

	weights_sum      += weight/itera

print("Time per iteration: ", timer/itera, " [s]")


# weights_sum[~np.isfinite(weights_sum)] = 0.
# weights_sum[~np.isfinite(weights_sum)] = 0.

# averaged_texture = np.zeros(resolution + [3], dtype = np.float32)
# for i in range(3): averaged_texture[:, :, i] = weights_sum

fake_vertices = np.zeros(shape = vertices.shape, dtype = np.float32)
fake_vertices[:, 0], fake_vertices[:, 1], fake_vertices[:, 2] = mesh.uv2[:, 0], mesh.uv2[:, 1], np.random.random(len(fake_vertices))
mesh_lod0.calculate_whole_mesh()
mask_windows = light.correct_windows(mesh_lod0)[vmapping]
fake_vertices[mask_windows, 2] = 1.1

mesh.colors[mask_windows, 0:3] = 0.
heightmap_aux, uv2_mapping, _ = modulo.map_quantity(fake_vertices, np.ones(len(fake_vertices)), triangles, [0., 1., 0., 1.], dx = 0.0005)
heightmap_aux, texture_R, _ = modulo.map_quantity(fake_vertices, mesh.colors[:, 0], triangles, [0., 1., 0., 1.], dx = 0.0005)
heightmap_aux, texture_G, _ = modulo.map_quantity(fake_vertices, mesh.colors[:, 1], triangles, [0., 1., 0., 1.], dx = 0.0005)
heightmap_aux, texture_B, _ = modulo.map_quantity(fake_vertices, mesh.colors[:, 2], triangles, [0., 1., 0., 1.], dx = 0.0005)


averaged_texture = np.zeros(weights_sum.shape + (3, ), dtype = np.float32)
averaged_texture[:, :, 0] = light.interpolate(texture_R, [0., 1., 0., 1.], [0., 1., 0., 1.], weights_sum.shape)
averaged_texture[:, :, 1] = light.interpolate(texture_G, [0., 1., 0., 1.], [0., 1., 0., 1.], weights_sum.shape)
averaged_texture[:, :, 2] = light.interpolate(texture_B, [0., 1., 0., 1.], [0., 1., 0., 1.], weights_sum.shape)

array = light.interpolate(uv2_mapping, [0., 1., 0., 1.], [0., 1., 0., 1.], averaged_texture[:, :, 0].shape)
mask_map = array != 0.

for i in range(3): averaged_texture[:, :, i] *= weights_sum
# for i in range(3):
	# averaged_texture[:, :, i] = light.fill_in_holes(averaged_texture[:, :, i], mask_map, 4)


mesh2 = light.wrap_texture2(mesh, averaged_texture)




