import tools
import numpy as np
import matplotlib.pyplot as plt
import modulo_cuda
import modulo
import os
from importlib import reload
from scipy.ndimage import gaussian_filter
import lighting_toolkit as light
import time

plt.ion()


if os.path.isdir("map_textures"):
	pass
else:
	os.mkdir("map_textures")


def list2str(list_set):
	array = np.array(list_set)

	string = ""
	for i in array:
		string += str(i) + " "
	#string += "\n"
	return string



def get_meshes(start_points, end_points, lengths, valid, color):
	nvectors = len(start_points)
	directions = end_points - start_points
	dists   = np.linalg.norm(directions, axis  = 1)
	for i in range(3): directions[:, i] *= 1/dists

	hit_points = np.zeros([nvectors, 3])
	for i in range(3): hit_points[:, i] = start_points[:, i] + directions[:, i]*lengths

	valid *= np.isfinite(np.linalg.norm(start_points - hit_points, axis = 1))
	ray_triangles = np.reshape(np.arange(valid.sum()*3, dtype = np.int32), [valid.sum(), 3])
	ray_vertices  = np.zeros([3*valid.sum(), 3], dtype = np.float32)

	ray_vertices[0::3, :] = start_points[valid]
	# ray_vertices[0::3, :] = hit_points[valid] + np.random.random(size = [valid.sum(), 3])
	ray_vertices[1::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3
	ray_vertices[2::3, :] = hit_points[valid] + (np.random.random(size = [valid.sum(), 3]) - 0.5)*0.3

	colors = np.zeros(shape = [valid.sum()*3, 4])
	colors[:, 0] = np.repeat(color[valid, 0], 3)
	colors[:, 1] = np.repeat(color[valid, 1], 3)
	colors[:, 2] = np.repeat(color[valid, 2], 3)
	colors[:, 3] = 1.0

	meshi = tools.SubMesh(vertices = ray_vertices, triangles = ray_triangles, colors = colors, uvs = np.ones(shape = [valid.sum()*3, 2]))
	meshi.calculate_normals()

	return meshi

def det_normals(heights, extent):
	N, M = len(heights), len(heights[0])
	dx, dy = (extent[1] - extent[0])/N, (extent[3] - extent[2])/M
	grad = np.gradient(heights)

	vectors = np.zeros([N, M, 3])
	normals = np.zeros([N, M, 3])

	vectors[:,:,0] = -grad[1]/dx
	vectors[:,:,1] = grad[0]/dy
	vectors[:,:,2] = 1.

	norm = np.linalg.norm(vectors, axis = 2)
	for i in range(3): normals[:,:,i] = vectors[:,:,i]/norm

	return normals


def get_positions_from_heightmap(heightmap, extent):
	x_space, y_space = np.linspace(extent[0], extent[1], len(heightmap)), np.linspace(extent[2], extent[3], len(heightmap[0]))

	shape = heightmap.shape
	X, Y = np.meshgrid(x_space, y_space, indexing = "ij")

	I, J = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing = "ij")

	vertices = np.zeros([shape[0]*shape[1], 3], dtype = np.float32)

	vertices[:, 0] = X.flatten()
	vertices[:, 1] = Y.flatten()
	vertices[:, 2] = heightmap.flatten()

	return vertices

def get_hits_uv_coords(hits, vert0, seg1, seg2, triang_normals, start_points, end_points, lengths):
	directions = end_points - start_points
	hit_points = start_points*0.
	for i in range(3): directions[:, i] *= 1/np.linalg.norm(end_points - start_points, axis = 1)

	for i in range(3): hit_points[:, i] = start_points[:, i] + directions[:, i]*lengths

	valid = hits != -1
	tri_ids = hits[valid]
	hit2 = hit_points[valid] - vert0[tri_ids]

	vector   = np.zeros([len(seg1), 3], dtype = np.float32)
	matrices = np.zeros([len(seg1), 3, 3], dtype = np.float32)

	matrices[:, :, 0] = seg1
	matrices[:, :, 1] = seg2
	matrices[:, :, 2] = triang_normals

	inv_matrices = np.linalg.inv(matrices)

	new_vec = np.zeros([len(tri_ids), 3], dtype = np.float32)

	hit3 = (seg1[tri_ids] + seg2[tri_ids])*0.5
	for i in range(len(tri_ids)):
		tri_id = tri_ids[i]
		new_vec[i] = np.matmul(inv_matrices[tri_id, :, :], hit2[i, :])

	return new_vec, tri_ids




def undo_rotations(vector, theta_light, phi_light):
	aux = tools.rotate(vector, tools.vec_right, -theta_light*tools.deg2rad)
	return tools.rotate(aux, tools.vec_up, -phi_light*tools.deg2rad)



meshes_lod0 = light.load_meshes(lod = 0, usefile = False, nblocks = 10)
# meshes_lod1 = light.load_meshes(lod = 1)
meshes_lod2 = light.load_meshes(lod = 2, usefile = False, nblocks = 10)

submeshes_lod0, submeshes_lod2 = [], []
mesh_lod0 = tools.Mesh()
for mesh in meshes_lod0:
	submeshes_lod0.append(mesh.whole_mesh)
	mesh_lod0 += mesh

count = 0
submesh_lod2_indices = []
for mesh in meshes_lod2[0:5]:
	submeshes_lod2.append(mesh.whole_mesh)
	submesh_lod2_indices.append(np.ones(len(mesh.whole_mesh.vertices), dtype = np.int32)*count)
	count += 1

# mesh = tools.combine_submeshes(submeshes_lod2)
submesh_lod0         = tools.combine_submeshes(submeshes_lod0)
submesh_lod2         = tools.combine_submeshes(submeshes_lod2)
submesh_lod2_indices = np.concatenate(submesh_lod2_indices)

# mesh_

mesh_lod0.calculate_whole_mesh()
submesh_lod0 = mesh_lod0.whole_mesh



block_id = 0

def obtain_optimized_mesh(block_id):
	meshes_to_combine = []
	uvs2    = []


	mesh_block = submeshes_lod0[block_id]
	mesh_block.calculate_normals()
	tools.generate_uv2(mesh_block)

	for i in range(len(submeshes_lod2)):
		if i != block_id:
			mesh_to_add = submeshes_lod0[i]
			mesh_to_add.calculate_normals()
			uvs2.append(mesh_to_add.uvs*0.)
			meshes_to_combine.append(mesh_to_add)
		else:
			uvs2.append(mesh_block.uv2)
			meshes_to_combine.append(mesh_block)
	submesh  = tools.combine_submeshes(meshes_to_combine)
	submesh.uv2 = np.concatenate(uvs2)

	return submesh, mesh_block


# mesh = meshes_lod2[0].whole_mesh
mesh, mesh_block = obtain_optimized_mesh(block_id)
mesh.calculate_normals()
# tools.generate_uv2(mesh)

fake_vertices = mesh_block.vertices*0.
fake_vertices[:, 0] = mesh_block.uv2[:, 0]
fake_vertices[:, 1] = mesh_block.uv2[:, 1]
uv_area = modulo.calculate_area(fake_vertices, mesh_block.triangles)
real_area = mesh_block.calculate_area()


	# tools.generate_uv2(mesh_lod2.whole_mesh)

	# os.system("rm AO/*.npy")
# itera =  12

	# cosines = np.cos(np.pi/4) + (1 - np.cos(np.pi/4))*np.random.random(itera)
	# thetas  = np.arccos(cosines)*180/np.pi


theta_light, phi_light = 45., 45.


resolution = [2048, 2048]

darea = real_area/((resolution[0]*resolution[1])*uv_area)

dx = 0.1


vertices1_rot, normals1_rot, triangles1 = light.rotate_mesh(mesh, theta_light, phi_light)
vertices2_rot, normals2_rot, triangles2 = light.rotate_mesh(submeshes_lod0[block_id], theta_light, phi_light)


heightmap, block_extent = modulo.get_heightmap(vertices2_rot, triangles2, dx = dx)

mask = heightmap != 0.




cam_direction = np.array([0., 0., 1.])

quantity = np.matmul(normals1_rot, cam_direction)
quantity[quantity < 0.] = 0.
heightmap, cos, _ = modulo.map_quantity(vertices1_rot, quantity, triangles1, block_extent, dx = dx, optimize = True)

heightmap += vertices1_rot[:, 2].min()


vert0_rot = vertices1_rot[triangles1[:, 0]]
seg1_rot = vertices1_rot[triangles1[:, 1]] - vert0_rot
seg2_rot = vertices1_rot[triangles1[:, 2]] - vert0_rot

triang_normals_rot = np.cross(seg1_rot, seg2_rot, axis = 1)
norma = np.linalg.norm(triang_normals_rot, axis = 1)
for i in range(3): triang_normals_rot[:, i] *= 1/norma

map_normals = np.zeros(cos.shape + (3,) )
for i in range(3):
	heightmap_aux, map_normals[:, :, i], _ = modulo.map_quantity(vertices1_rot, np.repeat(triang_normals_rot[:, i], 3), triangles1, block_extent, dx = dx, optimize = True, vertex_wise = False)


tang_map1 = map_normals*0.
tang_map1[:, :, 0] = 1.

dot = tang_map1[:, :, 0]*map_normals[:, :, 0] + tang_map1[:, :, 1]*map_normals[:, :, 1] + tang_map1[:, :, 2]*map_normals[:, :, 2]

for i in range(3): tang_map1[:, :, i] += -dot*map_normals[:, :, i]

norm = np.linalg.norm(tang_map1, axis=2)
for i in range(3): tang_map1[:, :, i] *= 1/norm

tang_map2 = np.cross(map_normals, tang_map1, axis = 2)


heightmap_aux, texture_R, _ = modulo.map_quantity(vertices1_rot, mesh.colors[:, 0], triangles1, block_extent, dx = dx)
heightmap_aux, texture_G, _ = modulo.map_quantity(vertices1_rot, mesh.colors[:, 1], triangles1, block_extent, dx = dx)
heightmap_aux, texture_B, _ = modulo.map_quantity(vertices1_rot, mesh.colors[:, 2], triangles1, block_extent, dx = dx)


ray_colors = np.zeros([len(heightmap)*len(heightmap[0]), 3], dtype = np.float32)
ray_colors[:, 0] = texture_R.flatten()
ray_colors[:, 1] = texture_G.flatten()
ray_colors[:, 2] = texture_B.flatten()

ray_colors = ray_colors[mask.flatten()]


nitera = 50



vertices1      = undo_rotations(vertices1_rot,    theta_light, phi_light)
vert0          = undo_rotations(vert0_rot, theta_light, phi_light)
seg1           = undo_rotations(seg1_rot, theta_light, phi_light)
seg2           = undo_rotations(seg2_rot, theta_light, phi_light)
triang_normals = undo_rotations(triang_normals_rot, theta_light, phi_light)


extent = [vertices1[:, 0].min(), vertices1[:, 0].max(), vertices1[:, 1].min(), vertices1[:, 1].max(), vertices1[:, 2].min(), vertices1[:, 2].max()]
for i in range(6):
		extent[i] += -(-1)**i

depth = 19
# modulo_cuda.create_gen_tree(vertices1, triangles1, extent, depth = depth)
# modulo.create_gen_tree(vertices1, triangles1, extent, depth = depth, optimized = False)
modulo.generate_BVH(vertices1, triangles1, extent, depth = 20)

hist_color = np.zeros([resolution[0], resolution[1], 3], dtype = np.float32)
hist_mono  = np.zeros([resolution[0], resolution[1]], dtype = np.float32)

time_accum = 0.
for i in range(nitera):
	start_points = get_positions_from_heightmap(heightmap + 0.1, block_extent)

	u        = np.random.random(size = map_normals.shape[0:2])
	theta    = np.arcsin(u**0.5)
	# theta    = np.arccos(costheta)
	phi      = np.random.random(size = map_normals.shape[0:2])*2*np.pi

	directions = map_normals*0
	for i in range(3):
		directions[:, :, i] = np.sin(theta)*np.cos(phi)*tang_map1[:, :, i] + np.sin(theta)*np.sin(phi)*tang_map2[:, :, i] + np.cos(theta)*map_normals[:, :, i]

	directions = np.reshape(directions, [directions.shape[0]*directions.shape[1], 3])

	end_points = start_points + directions*100


	start_points = start_points[mask.flatten()]
	end_points   = end_points[mask.flatten()]


	start_points = undo_rotations(start_points, theta_light, phi_light)
	end_points   = undo_rotations(end_points,   theta_light, phi_light)



	start = time.time()
	# hits, lengths = modulo_cuda.raytracing(vertices1, triangles1, start_points, end_points, mode = 2)
	hits, lengths = modulo.raytracing(vertices1, triangles1, start_points, end_points, mode = 1)
	end   = time.time()

	time_accum += end - start


	if False:
		hit_points = start_points*1.
		for i in range(3): hit_points[:, i] += (end_points - start_points)[:, i]*lengths/np.linalg.norm(end_points - start_points, axis = 1)

		meshi = get_meshes(hit_points, start_points, end_points[:,0]*0.0 + 1, valid = hits != -1, color = ray_colors)

		# meshi = get_meshes(start_points, end_points, lengths, valid = np.ones(end_points.shape[0], dtype = bool), color = ray_colors)
		vista = mesh.get_mesh() + meshi.get_mesh()
		vista.plot()


	new_vec, tri_ids = get_hits_uv_coords(hits, vert0, seg1, seg2, triang_normals, start_points, end_points, lengths)

	uv1 = mesh.uv2[triangles1[:, 0]]
	uv2 = mesh.uv2[triangles1[:, 1]]
	uv3 = mesh.uv2[triangles1[:, 2]]


	seguv1, seguv2 = uv2 - uv1, uv3 - uv1

	interpolated_uvs = np.zeros([len(tri_ids), 2], dtype = np.float32)

	for i in range(2): interpolated_uvs[:, i] = uv1[tri_ids, i] + seguv1[tri_ids, i]*new_vec[:, 0] + seguv2[tri_ids, i]*new_vec[:, 1]


	directions = end_points - start_points
	for i in range(3): directions[:, i] *= 1/np.linalg.norm(end_points - start_points, axis = 1)
	ray_normals = np.zeros(len(tri_ids), dtype = np.float32)
	for i in range(3): ray_normals += -directions[hits != -1, i]*triang_normals[tri_ids, i]

	ray_normals[ray_normals <= 0.] = 0.

	valid = hits != -1
	for i in range(3):
		# hist_color[:, :, i] = np.histogram2d(interpolated_uvs[:, 0], interpolated_uvs[:, 1], weights = ray_normals*ray_colors[tri_ids, i], range = [[0., 1.], [0., 1.]], bins = resolution)[0]
		hist_color[:, :, i] += np.histogram2d(interpolated_uvs[:, 0], interpolated_uvs[:, 1], weights = ray_colors[valid, i], range = [[0., 1.], [0., 1.]], bins = resolution)[0]

	hist_mono += np.histogram2d(interpolated_uvs[:, 0], interpolated_uvs[:, 1], range = [[0., 1.], [0., 1.]], bins = resolution)[0]

	# hist_red = np.histogram2d(interpolated_uvs[:, 0], interpolated_uvs[:, 1], weights = ray_colors[tri_ids, 0], range = [[0., 1.], [0., 1.]], bins = resolution)[0]

		# hist_color[:, :, i] += np.histogram2d(interpolated_uvs[:, 0], interpolated_uvs[:, 1], range = [[0., 1.], [0., 1.]], bins = resolution)[0]

print("Time average raytracing:", time_accum/nitera)
modulo.free_gen_tree()





def denoising(array_orig, mask_mapping, p = 0.5):
	reference = mask_mapping*1.
	array     = array_orig*1.

	p0 = p
	p1 = (1 - p)/4.

	roll1 = np.roll(array,     shift = 1, axis = 0)
	ref1  = np.roll(reference, shift = 1, axis = 0)

	roll2 = np.roll(array,     shift = -1, axis = 0)
	ref2  = np.roll(reference, shift = -1, axis = 0)

	roll3 = np.roll(array,     shift = 1, axis = 1)
	ref3  = np.roll(reference, shift = 1, axis = 1)

	roll4 = np.roll(array,     shift = -1, axis = 1)
	ref4  = np.roll(reference, shift = -1, axis = 1)

	quantity_sum = ((roll1 + roll2 + roll3 + roll4)*p1 + array*p0)*mask_mapping
	weights_sum  = ((ref1  +  ref2 +  ref3 + ref4)*p1  + reference*p0)*mask_mapping

	result = quantity_sum/weights_sum
	result[~np.isfinite(result)] = 0.
	return result


### FILLING IN HOLES
fake_vertices = np.zeros(shape = vertices1.shape, dtype = np.float32)
fake_vertices[:, 0], fake_vertices[:, 1], fake_vertices[:, 2] = mesh.uv2[:, 0], mesh.uv2[:, 1], np.random.random(len(fake_vertices))

heightmap_aux, uv2_mapping, _ = modulo.map_quantity(fake_vertices, np.ones(len(fake_vertices)), triangles1, [0., 1., 0., 1.], dx = 0.001)
array = light.interpolate(uv2_mapping, [0., 1., 0., 1.], [0., 1., 0., 1.], hist_color[:,:, 0].shape)
mask_map = array != 0.

weights = gaussian_filter(mask_map*1., 1)






factor = dx**2/nitera/darea

baked_lighting = np.zeros(shape = hist_color.shape, dtype = np.float32)
# for i in range(3): baked_lighting[:, :, i] =  gaussian_filter(hist_color[:, :, i]*factor, 1)/weights
for i in range(3): baked_lighting[:, :, i] =  denoising(hist_color[:, :, i]*factor, mask_map, p = 0.5)


# for i in range(3): baked_lighting[:, :, i] =  hist_mono[:, :]*factor

baked_lighting[baked_lighting > 1.] = 1.
# plt.imsave("map_textures/block_" + str(0) + "_indirect.png", baked_lighting)

mesh2 = light.wrap_texture2(mesh_block, baked_lighting)

