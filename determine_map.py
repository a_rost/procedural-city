import modulo
import numpy as np
import matplotlib.pyplot as plt
import tools
#import surfaces

#from mayavi import mlab
import os

plt.ion()


def determine_interesting_space(heightmap, extent, margin = 10.):
	skyline1 = np.max(heightmap, axis = 1)
	skyline2 = np.max(heightmap, axis = 0)
	
	x_space = np.linspace(extent[0], extent[1], len(heightmap),    endpoint = False)
	y_space = np.linspace(extent[2], extent[3], len(heightmap[0]), endpoint = False)
	
	built1 = x_space[skyline1 >= 1.]
	built2 = y_space[skyline2 >= 1.]

	interesting_extent = [built1.min() - margin, built1.max() + margin, built2.min() - margin, built2.max() + margin]
	return interesting_extent

def save_heightmap(filename, surface, extent, interesting_extent):
	N, M = len(surface), len(surface[0])
	archivo = open(filename, "w")
	archivo.write(str(N) + " ")
	archivo.write(str(M) + " ")
	archivo.write(str(extent[0]) + " ")
	archivo.write(str(extent[1]) + " ")
	archivo.write(str(extent[2]) + " ")
	archivo.write(str(extent[3]) + " ")

	archivo.write(str(interesting_extent[0]) + " ")
	archivo.write(str(interesting_extent[1]) + " ")
	archivo.write(str(interesting_extent[2]) + " ")
	archivo.write(str(interesting_extent[3]) + "\n")

	for i in range(N):
		for j in range(M):
			archivo.write(str(surface[i, j]) + "\n")
	archivo.close()



database = np.genfromtxt("data/blocks/file_list.txt", skip_header=2)
nblocks = len(database)
city_mesh = tools.Mesh()

mask_lod0 = database[:, 1] == 1
mask_lod1 = database[:, 2] == 1
mask_lod2 = database[:, 3] == 1

for i in range(nblocks):
	if mask_lod0[i]:
		mesh = np.load("data/blocks/mesh_block_" + str(i) + "_lod0.npy", allow_pickle = True).item()
	elif mask_lod1[i]:
		mesh = np.load("data/blocks/mesh_block_" + str(i) + "_lod1.npy", allow_pickle = True).item()
	elif mask_lod2[i]:
		mesh = np.load("data/blocks/mesh_block_" + str(i) + "_lod2.npy", allow_pickle = True).item()
	else:
		pass
	city_mesh += mesh



heightmap, extent = modulo.get_heightmap(city_mesh.whole_mesh.vertices, city_mesh.whole_mesh.triangles, dx = 0.25)

#camera_center = [600., 300.]
#size = 400.
#import matplotlib.pyplot as plt
#plt.ion()

#image = plt.imread(path + "Model.png")[:, :, 0]
#heightmap = (1. - image).T*140.
#heightmap = np.flip(heightmap, axis = 1)

#dx = 2*size/len(heightmap)
#aspect_ratio = len(heightmap[0])/len(heightmap)

#extent = [camera_center[0] - aspect_ratio*size, camera_center[0] + aspect_ratio*size, camera_center[1] - size, camera_center[1] + size]

#
# data = np.load("data/heightmap.npy", allow_pickle = True).item()
# heightmap, extent = data["heightmap"], data["extent"]

# plt.imshow(heightmap.T, origin = "lower", extent = extent)


#surfaces.extent = extent

interesting_extent = determine_interesting_space(heightmap, extent)
#x_space, y_space = np.linspace(extent[0], extent[1], 90), np.linspace(extent[2], extent[3], 110)


#height_map = (tools.plot_parcels(parcels, extent = extent)[:, :, 0]/256.*100.).T
#

dx = min((extent[1] - extent[0])/len(heightmap[0]), (extent[3] - extent[2])/len(heightmap[1]))
surface_5 = modulo.determine_surface(heightmap, dx, 5.)
surface_2 = modulo.determine_surface(heightmap, dx, 2.)

save_heightmap("data/map_5.txt", surface_5, extent, interesting_extent)
save_heightmap("data/map_2.txt", surface_2, extent, interesting_extent)
save_heightmap("data/map_0.txt", heightmap, extent, interesting_extent)


# plt.imshow(surface_5.T, origin = "lower", extent = extent)
